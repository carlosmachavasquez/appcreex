<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('membresia')->insert([
//            'name' => 'Aprendiz',
//            'precio_mes' => 50.00,
//            'precio_anual' => 397.00,
//            'estado' => 1,
//            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
//            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
//        ]);
//        DB::table('membresia')->insert([
//            'name' => 'Emprendedor',
//            'precio_mes' => 50.00,
//            'precio_anual' => 697.00,
//            'estado' => 1,
//            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
//            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
//        ]);
//        DB::table('membresia')->insert([
//            'name' => 'Inversionista',
//            'precio_mes' => 50.00,
//            'precio_anual' => 1197.00,
//            'estado' => 1,
//            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
//            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
//        ]);
        DB::table('membresia')->insert([
            'name' => 'Inversionista VIP',
            'precio_mes' => 50.00,
            'precio_anual' => 1997.00,
            'estado' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
