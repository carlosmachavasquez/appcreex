<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('membresia_id');
            $table->string('nickname')->unique();
            $table->string('name');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('cellphone');
            $table->string('password')->nullable();
            $table->string('wallet')->default('');
            $table->integer('id_referidor')->nullable();
            $table->integer('estado')->default(0);
            $table->integer('nivel')->default(1);
            $table->integer('cnivel1')->default(0);
            $table->integer('cnivel2')->default(0);
            $table->integer('cnivel3')->default(0);
            $table->string('role')->default('invitado');
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('membresia_id')->references('id')->on('membresia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
