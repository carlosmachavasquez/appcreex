<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLeccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leccion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->text('descripcion')->nullable();
            $table->string('video');
            $table->string('duracion')->nullable();
            $table->boolean('visto')->default(0);//para ver si se vio el video o no
            $table->boolean('estado')->default(1);
            $table->integer('usuario_creacion');
            $table->integer('usuario_modificacion');
            $table->timestamps();
            $table->integer('curso_id')->unsigned();
            $table->foreign('curso_id')->references('id')->on('curso');
        });

        DB::table('leccion')->insert(array(
            'id' => '1',
            'titulo' => 'introduccion a la programacion',
            'descripcion' => 'aprenderas mucho' ,
            'video' => 'sdasasdasdasd',
            'duracion' => '2h 40min',
            'visto' => 0,
            'estado' => 1,
            'usuario_creacion' => 1,
            'usuario_modificacion' => 1,
            'curso_id' => 1)); 
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
