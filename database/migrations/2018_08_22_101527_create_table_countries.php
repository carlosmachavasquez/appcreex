<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCountries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alpha2', 20)->nullable();
            $table->string('alpha3', 20)->nullable();
            $table->string('langCS', 50)->nullable();
            $table->string('langDE', 50)->nullable();
            $table->string('langEN', 50)->nullable();
            $table->string('langES', 50)->nullable();
            $table->string('langFR', 50)->nullable();
            $table->string('langIT', 50)->nullable();
            $table->string('langNL', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
