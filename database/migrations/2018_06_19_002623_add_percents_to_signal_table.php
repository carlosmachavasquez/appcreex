<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPercentsToSignalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('signals', function (Blueprint $table) {
            $table->string('pricet30mpercent')->nullable();
            $table->string('pricet1hpercent')->nullable();
            $table->string('pricet3hpercent')->nullable();
            $table->string('pricet6hpercent')->nullable();
            $table->string('pricet12hpercent')->nullable();
            $table->string('pricet24hpercent')->nullable();
            $table->string('pricet48hpercent')->nullable();
            $table->string('priceAverage')->nullable();
            $table->string('priceAveragePercent')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('signals', function (Blueprint $table) {
            $table->dropColumn('pricet30mpercent');
            $table->dropColumn('pricet1hpercent');
            $table->dropColumn('pricet3hpercent');
            $table->dropColumn('pricet6hpercent');
            $table->dropColumn('pricet12hpercent');
            $table->dropColumn('pricet24hpercent');
            $table->dropColumn('pricet48hpercent');
            $table->dropColumn('priceAverage');
            $table->dropColumn('priceAveragePercent');
        });
    }
}
