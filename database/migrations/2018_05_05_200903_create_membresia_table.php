<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembresiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membresia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('Aprendiz');//Emprendedor, inversionista
            $table->decimal('precio_mes',10,2)->default(0);//Emprendedor, inversionista
            $table->decimal('precio_anual',10,2)->default(0);//Emprendedor, inversionista
            $table->integer('estado')->default(0);
            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membresia');
    }
}
