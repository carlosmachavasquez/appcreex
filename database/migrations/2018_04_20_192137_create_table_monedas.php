<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMonedas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monedas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('moneda');
            $table->boolean('estado')->default(1);
            $table->integer('usuario_creacion');
            $table->integer('usuario_modificacion');
            $table->timestamps();
            $table->integer('portafolio_id')->unsigned();
            $table->foreign('portafolio_id')->references('id')->on('portafolio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
