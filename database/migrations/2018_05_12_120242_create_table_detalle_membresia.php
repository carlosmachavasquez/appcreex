<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetalleMembresia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_membresia', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('membresia_id');
            $table->string('item');
            $table->timestamps();
            $table->foreign('membresia_id')->references('id')->on('membresia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
