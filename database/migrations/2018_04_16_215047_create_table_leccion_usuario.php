<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLeccionUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leccion_usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('visto')->default(0);
            $table->integer('usuario');
            $table->boolean('estado')->default(1);
            $table->integer('usuario_creacion');
            $table->integer('usuario_modificacion');
            $table->timestamps();
            $table->integer('leccion_id')->unsigned();
            $table->foreign('leccion_id')->references('id')->on('leccion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
