<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPricesToSignalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('signals', function (Blueprint $table) {
            $table->string('pricet30m')->nullable();
            $table->string('pricet1h')->nullable();
            $table->string('pricet3h')->nullable();
            $table->string('pricet6h')->nullable();
            $table->string('pricet12h')->nullable();
            $table->string('pricet24h')->nullable();
            $table->string('pricet48h')->nullable();
            $table->string('flag')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('signals', function (Blueprint $table) {
            $table->dropColumn('pricet30m');
            $table->dropColumn('pricet1h');
            $table->dropColumn('pricet3h');
            $table->dropColumn('pricet6h');
            $table->dropColumn('pricet12h');
            $table->dropColumn('pricet24h');
            $table->dropColumn('pricet48h');
            $table->dropColumn('flag');
        });
    }
}
