<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableForex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forex', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pair', 255);
            $table->string('order_type')->default('sell');
            $table->string('market_execution', 50)->default(0);
            $table->string('sell_limit', 50)->default(0);
            $table->string('stop_loss', 50)->default(0);
            $table->string('take_prof1', 50)->default(0);
            $table->string('take_prof2', 50)->default(0);
            $table->string('take_prof3', 50)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
