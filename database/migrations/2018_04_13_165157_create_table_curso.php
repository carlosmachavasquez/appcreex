<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCurso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curso', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->integer('membresia')->default(0);
            $table->string('nivel');
            $table->text('descripcion')->nullable();
            $table->string('video')->nullable();
            $table->string('imagen')->nullable();
            $table->boolean('estado')->default(1);
            $table->integer('usuario_creacion');
            $table->integer('usuario_modificacion');
            $table->timestamps();
            $table->integer('educategoria_id')->unsigned();
            $table->foreign('educategoria_id')->references('id')->on('educategoria');
        });

        DB::table('curso')->insert(array(
        'id' => '1',
        'titulo' => 'Programacion',
        'nivel' => 'Intermedio',
        'descripcion' => '' ,
        'video' => 'sdasasdasdasd',
        'imagen' => 'default.png',
        'estado' => 1,
        'usuario_creacion' => 1,
        'usuario_modificacion' => 1,
        'educategoria_id' => 1));
    }

    

    public function down()
    {
        //
    }
}
