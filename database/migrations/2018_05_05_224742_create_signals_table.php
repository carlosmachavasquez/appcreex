<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('membresia_id');
            $table->string('coinname');
            $table->string('coinsymbol');
            $table->string('coinimgurl');
            $table->string('exchange');
            $table->string('cointradeurl');
            $table->string('coinprice');
            $table->string('target1');
            $table->string('target2');
            $table->string('target3');
            $table->string('stoploss');
            $table->timestamps();
            $table->foreign('membresia_id')->references('id')->on('membresia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signals');
    }
}
