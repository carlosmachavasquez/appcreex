<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEduCategoria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educategoria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('descripcion')->nullable();
            $table->boolean('estado')->defaul(1);
            $table->integer('usuario_creacion');
            $table->integer('usuario_modificacion');
            $table->timestamps();
        });

        DB::table('educategoria')->insert(array(
            'id' => '1',
            'nombre' => 'Computacion',
            'descripcion' => '' ,
            'estado' => 1,
            'usuario_creacion' => 1,
            'usuario_modificacion' => 1));
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
