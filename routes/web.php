<?php

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
| Define the routes for your Frontend pages here
|
*/

Route::get('/',function(){ return view('auth/login'); });

// Route::get('/', [
//     'as' => 'home', 'uses' => 'FrontendController@home'
// ]);

Auth::routes();
/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
| Route group for Backend prefixed with "admin".
| To Enable Authentication just remove the comment from Admin Middleware
|
*/

Route::group([
    'prefix' => 'dashboard/admin',
    'middleware' => 'admin'
], function () {

    // Dashboard
    //----------------------------------

    Route::get('/', [
        'as' => 'admin.dashboard', 'uses' => 'dashboard\user\DashboardController@index'
    ]);

    //Upload images
    //---------------------------------- 
    // Route::get('/fileupload', [
    //     'as' => 'admin.dashboard.fileupload', 'uses' => 'dashboard\UploadController@index'
    // ]);

    Route::post('/fileUpload', [
        'as'=>'fileUpload','uses'=>'dashboard\UploadController@fileUpload'
    ]);

    // Señales
    //----------------------------------    
    Route::group(['prefix' => 'signals'], function () {

        Route::get('/', [
            'as' => 'admin.signals.index', 'uses' => 'dashboard\SignalController@index'
        ]);

       Route::get('/create', [
            'as' => 'admin.signals.create', 'uses' => 'dashboard\SignalController@create'
        ]);

       Route::post('/store', [
            'as' => 'admin.signals.store', 'uses' => 'dashboard\SignalController@store'
        ]);

       Route::get('/show/{id}', [
            'as' => 'admin.signals.show', 'uses' => 'dashboard\SignalController@show'
        ]);

       Route::get('/edit/{id}', [
            'as' => 'admin.signals.edit', 'uses' => 'dashboard\SignalController@edit'
        ]);

       Route::put('/update/{id}', [
            'as' => 'admin.signals.update', 'uses' => 'dashboard\SignalController@update'
        ]);

       Route::delete('/destroy/{id}', [
            'as' => 'admin.signals.destroy', 'uses' => 'dashboard\SignalController@destroy'
        ]);
    });

    // Educacion

    Route::group(['prefix' => 'education'], function () {

        Route::group(['prefix' => 'course'], function () {

            Route::get('/', [
                'as' => 'admin.education.course.index', 'uses' => 'dashboard\CursoController@index_admin'
            ]);

            Route::get('/listcourse', [
                'as' => 'admin.education.courselistcourse', 'uses' => 'dashboard\CursoController@listar_curso'
            ]);

            Route::post('/register', [
                'as' => 'admin.education.course.register', 'uses' => 'dashboard\CursoController@registrar_curso'
            ]);

            Route::get('/show/{id}', [
                'as' => 'admin.education.course.show', 'uses' => 'dashboard\CursoController@mostrar_curso'
            ]);

            Route::post('/update', [
                'as' => 'admin.education.course.update', 'uses' => 'dashboard\CursoController@actualizar_curso'
            ]);

            Route::post('/remove', [
                'as' => 'admin.education.course.remove', 'uses' => 'dashboard\CursoController@eliminar_curso'
            ]);

        });  

        Route::group(['prefix' => 'category'], function () {

            Route::get('/', [
                'as' => 'admin.education.category.index', 'uses' => 'dashboard\EduCategoriaController@index'
            ]);

            Route::get('/listar_categoria_edu', [
                'as' => 'admin.education.category.index', 'uses' => 'dashboard\EduCategoriaController@listar_categoria_edu'
            ]);

            Route::post('/register', [
                'as' => 'admin.education.category.register', 'uses' => 'dashboard\EduCategoriaController@registrar_categoria'
            ]);

            Route::get('/show/{id}', [
                'as' => 'admin.education.category.show', 'uses' => 'dashboard\EduCategoriaController@mostrar_categoria'
            ]);

            Route::post('/update', [
                'as' => 'admin.education.category.update', 'uses' => 'dashboard\EduCategoriaController@actualizar_categoria'
            ]);

            Route::post('/remove', [
                'as' => 'admin.education.category.remove', 'uses' => 'dashboard\EduCategoriaController@eliminar_categoria'
            ]);
        }); 

        Route::group(['prefix' => 'leccion'], function () {

            Route::get('/', [
                'as' => 'admin.education.leccion.index', 'uses' => 'dashboard\LeccionController@index'
            ]);

            Route::get('/listar_leccion', [
                'as' => 'admin.education.leccion.listar', 'uses' => 'dashboard\LeccionController@listar_leccion'
            ]);

            Route::post('/registrar', [
                'as' => 'admin.education.leccion.registrar', 'uses' => 'dashboard\LeccionController@registrar_leccion'
            ]);

            Route::get('/mostrar/{id}', [
                'as' => 'admin.education.leccion.show', 'uses' => 'dashboard\LeccionController@mostrar_leccion'
            ]);

            Route::post('/actualizar', [
                'as' => 'admin.education.leccion.registrar', 'uses' => 'dashboard\LeccionController@actualizar_leccion'
            ]);

            Route::post('/eliminar', [
                'as' => 'admin.education.leccion.registrar', 'uses' => 'dashboard\LeccionController@eliminar_leccion'
            ]);

            //archivos ligados a leccion
            Route::get('/mostrar_leccion_archivo/{id}', [
                'as' => 'admin.education.leccion.show.leccion_archivo', 'uses' => 'dashboard\ArchivoController@mostrar_leccion_archivo'
            ]);

            Route::get('/mostrar_archivo/{id}', [
                'as' => 'admin.education.leccion.show.archivo', 'uses' => 'dashboard\ArchivoController@mostrar_archivo'
            ]);

            Route::post('/subir_archivo', [
                'as' => 'admin.education.leccion.subir_archivo', 'uses' => 'dashboard\ArchivoController@subir_archivo'
            ]);

            Route::post('/eliminar_archivo', [
                'as' => 'admin.education.leccion.eliminar_archivo', 'uses' => 'dashboard\ArchivoController@eliminar_archivo'
            ]);
        });
        
    });  

    //membresia
    Route::group(['prefix' => 'membresia'], function () {

        Route::get('/', [
                'as' => 'admin.membresia.index', 'uses' => 'dashboard\MembresiaController@index'
            ]);

            Route::get('/listar_membresia', [
                'as' => 'admin.membresia.listar', 'uses' => 'dashboard\MembresiaController@listar_membresia'
            ]);

            Route::post('/registrar', [
                'as' => 'admin.membresia.registrar', 'uses' => 'dashboard\MembresiaController@registrar_membresia'
            ]);

            Route::get('/mostrar/{id}', [
                'as' => 'admin.membresia.show', 'uses' => 'dashboard\MembresiaController@mostrar_membresia'
            ]);

            Route::post('/actualizar', [
                'as' => 'admin.membresia.registrar', 'uses' => 'dashboard\MembresiaController@actualizar_membresia'
            ]);

            Route::post('/eliminar', [
                'as' => 'admin.membresia.eliminar', 'uses' => 'dashboard\MembresiaController@eliminar_membresia'
            ]);

            //archivos ligados a leccion
            Route::get('/mostrar_items/{id}', [
                'as' => 'admin.membresia.items', 'uses' => 'dashboard\MembresiaController@listar_items'
            ]);

            Route::post('/add_item', [
                'as' => 'admin.membresia.add_item', 'uses' => 'dashboard\MembresiaController@registrar_item'
            ]);

            Route::post('/eliminar_item', [
                'as' => 'admin.membresia.eliminar_item', 'uses' => 'dashboard\MembresiaController@eliminar_item'
            ]);
        
    });  

    // Activacion manual
    //----------------------------------    
    Route::group(['prefix' => 'activation'], function () {

        Route::get('/', [
            'as' => 'admin.activation.index', 'uses' => 'dashboard\ActivationController@index'
        ]);

       
        Route::post('/activar', [
            'as' => 'admin.activation.activar', 'uses' => 'dashboard\ActivationController@activar'
        ]);

        Route::post('/desactivar', [
            'as' => 'admin.activation.desactivar', 'uses' => 'dashboard\ActivationController@desactivar'
        ]);

       
    });

    // usuarios
    //----------------------------------    
    Route::group(['prefix' => 'usuarios'], function () {
        Route::get('/', [
            'as' => 'admin.usuarios.index', 'uses' => 'dashboard\UsuarioController@index'
        ]);

        Route::get('/export', [
            'as' => 'admin.usuarios.export', 'uses' => 'dashboard\UsuarioController@export'
        ]);
    });


    // Productos
    //----------------------------------    
    Route::group(['prefix' => 'producto'], function () {
        Route::get('/', [
            'as' => 'admin.productos.index', 'uses' => 'dashboard\ProductosController@index'
        ]);

        Route::post('/registrar', [
            'as' => 'admin.productos.registrar', 'uses' => 'dashboard\ProductosController@store'
        ]);

         Route::get('/editar', [
            'as' => 'admin.productos.edit', 'uses' => 'dashboard\ProductosController@edit'
        ]);

        Route::post('/desactivar', [
            'as' => 'admin.productos.desactivar', 'uses' => 'dashboard\ProductosController@desactivar'
        ]);    
    });


});


Route::group([
    'prefix' => 'dashboard/user',
    'middleware' => 'user'
], function () {

    // Dashboard
    //----------------------------------

    Route::get('/', [
        'as' => 'user.dashboard', 'uses' => 'dashboard\user\DashboardController@index'
    ]);

    //Portafolio
    //------------------------------
    Route::group(['prefix' => 'portafolio'], function () {

        Route::get('/', [
            'as' => 'user.portafolio.index', 'uses' => 'dashboard\PortafolioController@index'
        ]);
        Route::get('/getMonedas', [
            'as' => 'user.portafolio.getMonedas', 'uses' => 'dashboard\PortafolioController@getMonedas'
        ]);
        Route::post('/registrar', [
            'as' => 'user.portafolio.registrar', 'uses' => 'dashboard\PortafolioController@registrar'
        ]);
        Route::get('/mostrar/{id}', [
            'as' => 'user.portafolio.mostrar', 'uses' => 'dashboard\PortafolioController@mostrar_portafolio'
        ]);

        Route::get('/listar_moneda/{portafolio_id}', [
            'as' => 'user.portafolio.listar', 'uses' => 'dashboard\OperacionesController@listar_moneda'
        ]);

        Route::get('/getMonedaUni/{moneda}', [
            'as' => 'user.portafolio.precio', 'uses' => 'dashboard\OperacionesController@getMonedaUni'
        ]);

        Route::post('/add_moneda', [
            'as' => 'user.portafolio.agregar.moneda', 'uses' => 'dashboard\OperacionesController@add_moneda'
        ]);

        Route::post('/add_operacion', [
            'as' => 'user.portafolio.agregar.operacion', 'uses' => 'dashboard\OperacionesController@add_operacion'
        ]);

        Route::get('/show_operacion/{id_moneda}', [
            'as' => 'user.portafolio.agregar', 'uses' => 'dashboard\OperacionesController@mostrar_operaciones'
        ]);

        Route::post('/eliminar', [
            'as' => 'user.portafolio.eliminar', 'uses' => 'dashboard\PortafolioController@eliminar_portafolio'
        ]);

        Route::post('/eliminar_moneda', [
            'as' => 'user.moneda.eliminar', 'uses' => 'dashboard\OperacionesController@eliminar_moneda'
        ]);

       
    });

    //Icos
    //------------------------------
    Route::group(['prefix' => 'icos'], function () {
        Route::get('/', [
            'as' => 'user.icos.index', 'uses' => 'dashboard\IcosController@index'
        ]);
        Route::get('/detalle/{id}', [
            'as' => 'user.icos.detalle', 'uses' => 'dashboard\IcosController@detalleico'
        ]);
    });

    //Noticias
    //------------------------------
    Route::group(['prefix' => 'noticias'], function () {
        Route::get('/', [
            'as' => 'user.noticias.index', 'uses' => 'dashboard\NoticiasController@index'
        ]);
        Route::get('/mostrar/{id}', [
            'as' => 'user.noticias.detalle', 'uses' => 'dashboard\NoticiasController@mostrar_noticia'
        ]);
        Route::get('/tipo/{tipo}', [
            'as' => 'user.noticias.tipo', 'uses' => 'dashboard\NoticiasController@select_tipo'
        ]);
        Route::post('/enviar_correo', [
            'as' => 'user.noticias.enviar_correo', 'uses' => 'dashboard\NoticiasController@enviar_correo'
        ]);
    });

    //Crusos
    //------------------------------
    Route::group(['prefix' => 'cursos'], function () {
        Route::get('/', [
            'as' => 'user.cursos.index', 'uses' => 'dashboard\CursoController@index_user'
        ]);
        Route::get('/{id}', [
            'as' => 'user.cursos.mostrar', 'uses' => 'dashboard\CursoController@interna_curso'
        ]);
        Route::get('/video/{id}', [
            'as' => 'user.cursos.video', 'uses' => 'dashboard\CursoController@ver_video'
        ]);


    });

    //forex
    //------------------------------
    Route::group(['prefix' => 'forex'], function () {
        Route::get('/', [
            'as' => 'user.forex.index', 'uses' => 'dashboard\ForexController@index'
        ]);
        Route::post('/enviar_correo', [
            'as' => 'user.forex.enviar_correo', 'uses' => 'dashboard\ForexController@enviar_correo'
        ]);
    });

    // Settings
    //----------------------------------

    Route::group(['prefix' => 'settings'], function () {

        Route::get('/', [
            'as' => 'user.settingsuser.index', 'uses' => 'dashboard\user\SettingsUserController@index'
        ]);

       Route::put('/updateprofile', [
            'as' => 'user.settingsuser.updateprofile', 'uses' => 'dashboard\user\SettingsUserController@updateprofile'
        ]);

       Route::post('/updateprofilepassword', [
            'as' => 'user.settingsuser.updateprofilepassword', 'uses' => 'dashboard\user\SettingsUserController@updateprofilepassword'
        ]);

       Route::put('/updateprofilewallet', [
            'as' => 'user.settingsuser.updateprofilewallet', 'uses' => 'dashboard\user\SettingsUserController@updateprofilewallet'
        ]);
    });

    // Señales
    //----------------------------------    
    Route::group(['prefix' => 'signals'], function () {

        Route::get('/', [
            'as' => 'user.signals.index', 'uses' => 'dashboard\user\SignalsController@index'
        ]);
    });

    // CoinMarket
    //----------------------------------    
    Route::group(['prefix' => 'coinmarket'], function () {

        Route::get('/', [
            'as' => 'user.coinmarket.index', 'uses' => 'dashboard\user\CoinMarketController@index'
        ]);

        Route::get('/{coinsymbol}/{id}', [
            'as' => 'user.coinmarket.single', 'uses' => 'dashboard\user\CoinMarketController@single'
        ]);
    });

    // Educacion
    Route::group(['prefix' => 'education'], function () {

        Route::group(['prefix' => 'course'], function () {

            Route::get('/', [
                'as' => 'user.education.course.index', 'uses' => 'dashboard\CursoController@index_user'
            ]);

        }); 

        Route::get('/leccion/mostrar_archivos/{id}', [
            'as' => 'user.education.leccion.mostrar_archivos', 'uses' => 'dashboard\CursoController@mostrar_archivos'
        ]);



    });

    //Pagos nuevos
    //------------------------------
    Route::group(['prefix' => 'payments'], function () {
        Route::get('/', [
            'as' => 'user.payments.index', 'uses' => 'dashboard\user\PaymentsController@index'
        ]);
        Route::get('/buy', [
            'as' => 'user.payments.buy', 'uses' => 'dashboard\user\PaymentsController@buy'
        ]);
        Route::get('/buy/{membresia}', [
            'as' => 'user.payments.buy.pay', 'uses' => 'dashboard\user\PaymentsController@pay'
        ]);

        Route::get('/membresia/show/{id}', [
            'as' => 'user.membresia.show', 'uses' => 'dashboard\user\PaymentsController@mostrar_membresia'
        ]);

        //logueo api
        Route::post('/inicio_session', [
            'as' => 'user.payments.session', 'uses' => 'dashboard\user\PaymentsController@inicio_session'
        ]);
        Route::get('/inicio_session', [
            'as' => 'user.payments.session.index', 'uses' => 'dashboard\user\PaymentsController@index'
        ]);
    });

    //CLASES EN VIVO
    //------------------------------
    Route::group(['prefix' => 'envivo'], function () {
        Route::get('/', [
            'as' => 'user.envivo.index', 'uses' => 'dashboard\user\EnvivoController@index'
        ]);

    });

    //REFERIDOS
    //------------------------------
    Route::group(['prefix' => 'referrals'], function () {
        Route::get('/', [
            'as' => 'user.referrals.index', 'uses' => 'dashboard\user\ReferralsController@index'
        ]);


    });


    // Pagos Creex
    //----------------------------------
    Route::group(['prefix' => 'pagos'], function () {

        Route::get('/', [
            'as' => 'user.pagos.index', 'uses' => 'dashboard\PagoController@index'
        ]);
    });

    // Productos
    //----------------------------------    
    Route::group(['prefix' => 'producto'], function () {
        Route::get('/', [
            'as' => 'user.productos.index', 'uses' => 'dashboard\user\ProductosController@index'
        ]);

        Route::post('/pagar', [
            'as' => 'user.productos.pagar', 'uses' => 'dashboard\user\ProductosController@pagar'
        ]);

           
    });


});

Route::group([
    'prefix' => 'dashboard/trader',
    'middleware' => 'trader'
], function () {

    Route::get('/', [
        'as' => 'trader.dashboard', 'uses' => 'dashboard\TraderController@index'
    ]);

    //Creación de señales
    Route::group(['prefix' => 'signals'], function () {

        Route::get('/', [
            'as' => 'trader.signals.index', 'uses' => 'dashboard\TraderController@index'
        ]);

       Route::get('/create', [
            'as' => 'trader.signals.create', 'uses' => 'dashboard\TraderController@create'
        ]);

       Route::post('/store', [
            'as' => 'trader.signals.store', 'uses' => 'dashboard\TraderController@store'
        ]);

       Route::get('/show/{id}', [
            'as' => 'trader.signals.show', 'uses' => 'dashboard\TraderController@show'
        ]);

       Route::get('/edit/{id}', [
            'as' => 'trader.signals.edit', 'uses' => 'dashboard\TraderController@edit'
        ]);

       Route::put('/update/{id}', [
            'as' => 'trader.signals.update', 'uses' => 'dashboard\TraderController@update'
        ]);

       Route::delete('/destroy/{id}', [
            'as' => 'trader.signals.destroy', 'uses' => 'dashboard\TraderController@destroy'
        ]);
    });

    // forex
    //----------------------------------    
    Route::group(['prefix' => 'forex'], function () {

        Route::get('/', [
            'as' => 'trader.forex.index', 'uses' => 'dashboard\ForexController@index'
        ]);

       Route::get('/create', [
            'as' => 'trader.forex.create', 'uses' => 'dashboard\ForexController@create'
        ]);

       Route::post('/store', [
            'as' => 'trader.forex.store', 'uses' => 'dashboard\ForexController@store'
        ]);

       Route::get('/show/{id}', [
            'as' => 'trader.forex.show', 'uses' => 'dashboard\ForexController@show'
        ]);

       Route::get('/edit/{id}', [
            'as' => 'trader.forex.edit', 'uses' => 'dashboard\ForexController@edit'
        ]);

       Route::post('/update/{id}', [
            'as' => 'trader.forex.update', 'uses' => 'dashboard\ForexController@update'
        ]);

       Route::delete('/destroy/{id}', [
            'as' => 'trader.forex.destroy', 'uses' => 'dashboard\ForexController@destroy'
        ]);
    });

    // Live Señales
    //----------------------------------
    Route::group(['prefix' => 'livesignals'], function () {

        Route::get('/', [
            'as' => 'trader.livesignals.index', 'uses' => 'dashboard\user\SignalsController@index'
        ]);
    });

    // CoinMarket
    //----------------------------------
    Route::group(['prefix' => 'coinmarket'], function () {

        Route::get('/{coinsymbol}/{id}', [
            'as' => 'trader.coinmarket.single', 'uses' => 'dashboard\user\CoinMarketController@single'
        ]);
    });

});


Route::group([
    'prefix' => 'dashboard/invitado',
    'middleware' => 'invitado'
], function () {

    Route::get('/', [
        'as' => 'invitado.dashboard', 'uses' => 'dashboard\user\PaymentsController@buy'
    ]);
      //Pagos nuevos
    //------------------------------
    Route::group(['prefix' => 'payments'], function () {

        Route::get('/', [
            'as' => 'invitado.payments.index', 'uses' => 'dashboard\user\PaymentsController@index'
        ]);

        Route::get('/buy', [
            'as' => 'invitado.payments.buy', 'uses' => 'dashboard\user\PaymentsController@buy'
        ]);

        Route::get('/buy/{membresia}', [
            'as' => 'invitado.payments.buy.membresia', 'uses' => 'dashboard\user\PaymentsController@pago_membresia'
        ]);

        Route::get('/charges/{membresia}', [
            'as' => 'invitado.payments.charges.membresia', 'uses' => 'dashboard\user\PaymentsController@gen_charges'
        ]);

        Route::get('/membresia/show/{id}', [
            'as' => 'invitado.membresia.show', 'uses' => 'dashboard\user\PaymentsController@mostrar_membresia'
        ]);

        Route::post('/membresia/pago', [
            'as' => 'invitado.membresia.pago', 'uses' => 'dashboard\PagoController@pagar_membresia'
        ]);

        //ejemplos api coinbase
        Route::get('/coinbase', [
            'as' => 'user.payments.coinbase', 'uses' => 'dashboard\user\PaymentsController@coinbase'
        ]);

        Route::post('/membresia/coinbase', [
            'as' => 'invitado.membresia.coinbase', 'uses' => 'dashboard\PagoController@paycoinbase'
        ]);

    });


});