-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: creexapp2
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `archivo`
--

DROP TABLE IF EXISTS `archivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archivo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `archivo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `leccion_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `archivo_leccion_id_foreign` (`leccion_id`),
  CONSTRAINT `archivo_leccion_id_foreign` FOREIGN KEY (`leccion_id`) REFERENCES `leccion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archivo`
--

LOCK TABLES `archivo` WRITE;
/*!40000 ALTER TABLE `archivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `archivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargos`
--

DROP TABLE IF EXISTS `cargos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `membresia_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `code_charge` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargos`
--

LOCK TABLES `cargos` WRITE;
/*!40000 ALTER TABLE `cargos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cargos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coinbase_webhook_calls`
--

DROP TABLE IF EXISTS `coinbase_webhook_calls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coinbase_webhook_calls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci,
  `exception` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coinbase_webhook_calls`
--

LOCK TABLES `coinbase_webhook_calls` WRITE;
/*!40000 ALTER TABLE `coinbase_webhook_calls` DISABLE KEYS */;
/*!40000 ALTER TABLE `coinbase_webhook_calls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `code` int(3) NOT NULL,
  `alpha2` varchar(2) NOT NULL,
  `alpha3` varchar(3) NOT NULL,
  `langCS` varchar(45) NOT NULL,
  `langDE` varchar(45) NOT NULL,
  `langEN` varchar(45) NOT NULL,
  `langES` varchar(45) NOT NULL,
  `langFR` varchar(45) NOT NULL,
  `langIT` varchar(45) NOT NULL,
  `langNL` varchar(45) NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `alpha2` (`alpha2`),
  UNIQUE KEY `alpha3` (`alpha3`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (4,'AF','AFG','AfghanistÃ¡n','Afghanistan','Afghanistan','AfganistÃ¡n','Afghanistan','Afghanistan','Afghanistan'),(8,'AL','ALB','AlbÃ¡nie','Albanien','Albania','Albania','Albanie','Albania','AlbaniÃ«'),(10,'AQ','ATA','Antarctica','Antarktis','Antarctica','Antartida','Antarctique','Antartide','Antarctica'),(12,'DZ','DZA','AlÅ¾Ã­rsko','Algerien','Algeria','Argelia','AlgÃ©rie','Algeria','Algerije'),(16,'AS','ASM','AmerickÃ¡ Samoa','Amerikanisch-Samoa','American Samoa','Samoa americana','Samoa AmÃ©ricaines','Samoa Americane','Amerikaans Samoa'),(20,'AD','AND','Andorra','Andorra','Andorra','Andorra','Andorre','Andorra','Andorra'),(24,'AO','AGO','Angola','Angola','Angola','Angola','Angola','Angola','Angola'),(28,'AG','ATG','Antigua a Barbuda','Antigua und Barbuda','Antigua and Barbuda','Antigua y Barbuda','Antigua-et-Barbuda','Antigua e Barbuda','Antigua en Barbuda'),(31,'AZ','AZE','AzerbajdÅ¾Ã¡n','Aserbaidschan','Azerbaijan','AzerbaiyÃ¡n','AzerbaÃ¯djan','Azerbaijan','Azerbeidzjan'),(32,'AR','ARG','Argentina','Argentinien','Argentina','Argentina','Argentine','Argentina','ArgentiniÃ«'),(36,'AU','AUS','AustrÃ¡lie','Australien','Australia','Australia','Australie','Australia','AustraliÃ«'),(40,'AT','AUT','Rakousko','Ã–sterreich','Austria','Austria','Autriche','Austria','Oostenrijk'),(44,'BS','BHS','Bahamy','Bahamas','Bahamas','Bahamas','Bahamas','Bahamas','Bahama\'s'),(48,'BH','BHR','Bahrajn','Bahrain','Bahrain','Bahrain','BahreÃ¯n','Bahrain','Bahrein'),(50,'BD','BGD','BangladÃ©Å¡','Bangladesch','Bangladesh','Bangladesh','Bangladesh','Bangladesh','Bangladesh'),(51,'AM','ARM','ArmÃ©nie','Armenien','Armenia','Armenia','ArmÃ©nie','Armenia','ArmeniÃ«'),(52,'BB','BRB','Barbados','Barbados','Barbados','Barbados','Barbade','Barbados','Barbados'),(56,'BE','BEL','Belgie','Belgien','Belgium','BÃ©lgica','Belgique','Belgio','BelgiÃ«'),(60,'BM','BMU','Bermuda','Bermuda','Bermuda','Bermuda','Bermudes','Bermuda','Bermuda'),(64,'BT','BTN','BhutÃ¡n','Bhutan','Bhutan','Bhutan','Bhoutan','Bhutan','Bhutan'),(68,'BO','BOL','BolÃ­vie','Bolivien','Bolivia','Bolivia','Bolivie','Bolivia','Bolivia'),(70,'BA','BIH','Bosna a Hercegovina','Bosnien und Herzegowina','Bosnia and Herzegovina','Bosnia y Herzegovina','Bosnie-HerzÃ©govine','Bosnia Erzegovina','BosniÃ«-Herzegovina'),(72,'BW','BWA','Botswana','Botswana','Botswana','Botswana','Botswana','Botswana','Botswana'),(74,'BV','BVT','Bouvet Island','Bouvetinsel','Bouvet Island','Isla Bouvet','ÃŽle Bouvet','Isola di Bouvet','Bouvet'),(76,'BR','BRA','BrazÃ­lie','Brasilien','Brazil','Brasil','BrÃ©sil','Brasile','BraziliÃ«'),(84,'BZ','BLZ','Belize','Belize','Belize','Belize','Belize','Belize','Belize'),(86,'IO','IOT','BritskÃ© IndickooceÃ¡nskÃ© teritorium','Britisches Territorium im Indischen Ozean','British Indian Ocean Territory','Territorio OceÃ¡nico de la India BritÃ¡nica','Territoire Britannique de l\'OcÃ©an Indien','Territori Britannici dell\'Oceano Indiano','British Indian Ocean Territory'),(90,'SB','SLB','Å alamounovy ostrovy','Salomonen','Solomon Islands','Islas SalomÃ³n','ÃŽles Salomon','Isole Solomon','Salomonseilanden'),(92,'VG','VGB','BritskÃ© PanenskÃ© ostrovy','Britische Jungferninseln','British Virgin Islands','Islas VÃ­rgenes BritÃ¡nicas','ÃŽles Vierges Britanniques','Isole Vergini Britanniche','Britse Maagdeneilanden'),(96,'BN','BRN','Brunej','Brunei Darussalam','Brunei Darussalam','Brunei Darussalam','BrunÃ©i Darussalam','Brunei Darussalam','Brunei'),(100,'BG','BGR','Bulharsko','Bulgarien','Bulgaria','Bulgaria','Bulgarie','Bulgaria','Bulgarije'),(104,'MM','MMR','Myanmar','Myanmar','Myanmar','Mianmar','Myanmar','Myanmar','Myanmar'),(108,'BI','BDI','Burundi','Burundi','Burundi','Burundi','Burundi','Burundi','Burundi'),(112,'BY','BLR','BÄ›lorusko','Belarus','Belarus','Belarus','BÃ©larus','Bielorussia','Wit-Rusland'),(116,'KH','KHM','KambodÅ¾a','Kambodscha','Cambodia','Camboya','Cambodge','Cambogia','Cambodja'),(120,'CM','CMR','Kamerun','Kamerun','Cameroon','CamerÃºn','Cameroun','Camerun','Kameroen'),(124,'CA','CAN','Kanada','Kanada','Canada','CanadÃ¡','Canada','Canada','Canada'),(132,'CV','CPV','Ostrovy ZelenÃ©ho mysu','Kap Verde','Cape Verde','Cabo Verde','Cap-vert','Capo Verde','KaapverdiÃ«'),(136,'KY','CYM','KajmanskÃ© ostrovy','Kaimaninseln','Cayman Islands','Islas CaimÃ¡n','ÃŽles CaÃ¯manes','Isole Cayman','Caymaneilanden'),(140,'CF','CAF','StÅ™edoafrickÃ¡ republika','Zentralafrikanische Republik','Central African','RepÃºblica Centroafricana','RÃ©publique Centrafricaine','Repubblica Centroafricana','Centraal-Afrikaanse Republiek'),(144,'LK','LKA','SrÃ­ Lanka','Sri Lanka','Sri Lanka','Sri Lanka','Sri Lanka','Sri Lanka','Sri Lanka'),(148,'TD','TCD','ÄŒad','Tschad','Chad','Chad','Tchad','Ciad','Tsjaad'),(152,'CL','CHL','Chile','Chile','Chile','Chile','Chili','Cile','Chili'),(156,'CN','CHN','ÄŒÃ­na','China','China','China','Chine','Cina','China'),(158,'TW','TWN','Tchajwan','Taiwan','Taiwan','TaiwÃ¡n','TaÃ¯wan','Taiwan','Taiwan'),(162,'CX','CXR','Christmas Island','Weihnachtsinsel','Christmas Island','Isla Navidad','ÃŽle Christmas','Isola di Natale','Christmaseiland'),(166,'CC','CCK','KokosovÃ© ostrovy','Kokosinseln','Cocos (Keeling) Islands','Islas Cocos (Keeling)','ÃŽles Cocos (Keeling)','Isole Cocos','Cocoseilanden'),(170,'CO','COL','Kolumbie','Kolumbien','Colombia','Colombia','Colombie','Colombia','Colombia'),(174,'KM','COM','Komory','Komoren','Comoros','Comoros','Comores','Comore','Comoren'),(175,'YT','MYT','Mayotte','Mayotte','Mayotte','Mayote','Mayotte','Mayotte','Mayotte'),(178,'CG','COG','KonÅ¾skÃ¡ republika Kongo','Republik Kongo','Republic of the Congo','Congo','RÃ©publique du Congo','Repubblica del Congo','Republiek Congo'),(180,'CD','COD','DemokratickÃ¡ republika Kongo Kongo','Demokratische Republik Kongo','The Democratic Republic Of The Congo','RepÃºblica DemocrÃ¡tica del Congo','RÃ©publique DÃ©mocratique du Congo','Repubblica Democratica del Congo','Democratische Republiek Congo'),(184,'CK','COK','Cookovy ostrovy','Cookinseln','Cook Islands','Islas Cook','ÃŽles Cook','Isole Cook','Cookeilanden'),(188,'CR','CRI','Kostarika','Costa Rica','Costa Rica','Costa Rica','Costa Rica','Costa Rica','Costa Rica'),(191,'HR','HRV','Chorvatsko','Kroatien','Croatia','Croacia','Croatie','Croazia','KroatiÃ«'),(192,'CU','CUB','Kuba','Kuba','Cuba','Cuba','Cuba','Cuba','Cuba'),(196,'CY','CYP','Kypr','Zypern','Cyprus','Chipre','Chypre','Cipro','Cyprus'),(203,'CZ','CZE','ÄŒesko','Tschechische Republik','Czech Republic','Chequia','RÃ©publique TchÃ¨que','Repubblica Ceca','TsjechiÃ«'),(204,'BJ','BEN','Benin','Benin','Benin','Benin','BÃ©nin','Benin','Benin'),(208,'DK','DNK','DÃ¡nsko','DÃ¤nemark','Denmark','Dinamarca','Danemark','Danimarca','Denemarken'),(212,'DM','DMA','Dominika','Dominica','Dominica','Dominica','Dominique','Dominica','Dominica'),(214,'DO','DOM','DominikÃ¡nskÃ¡ republika','Dominikanische Republik','Dominican Republic','RepÃºblica Dominicana','RÃ©publique Dominicaine','Repubblica Dominicana','Dominicaanse Republiek'),(218,'EC','ECU','EkvÃ¡dor','Ecuador','Ecuador','Ecuador','Ã‰quateur','Ecuador','Ecuador'),(222,'SV','SLV','Salvador','El Salvador','El Salvador','El Salvador','El Salvador','El Salvador','El Salvador'),(226,'GQ','GNQ','RovnÃ­kovÃ¡ Guinea','Ã„quatorialguinea','Equatorial Guinea','Guinea Ecuatorial','GuinÃ©e Ã‰quatoriale','Guinea Equatoriale','Equatoriaal Guinea'),(231,'ET','ETH','Etiopie','Ã„thiopien','Ethiopia','EtiopÃ­a','Ã‰thiopie','Etiopia','EthiopiÃ«'),(232,'ER','ERI','Eritrea','Eritrea','Eritrea','Eritrea','Ã‰rythrÃ©e','Eritrea','Eritrea'),(233,'EE','EST','Estonsko','Estland','Estonia','Estonia','Estonie','Estonia','Estland'),(234,'FO','FRO','FaerskÃ© ostrovy','FÃ¤rÃ¶er','Faroe Islands','Islas Faroe','ÃŽles FÃ©roÃ©','Isole Faroe','FaerÃ¶er'),(238,'FK','FLK','FalklandskÃ© ostrovy','Falklandinseln','Falkland Islands','Islas Malvinas','ÃŽles (malvinas) Falkland','Isole Falkland','Falklandeilanden'),(239,'GS','SGS','JiÅ¾nÃ­ Georgie a JiÅ¾nÃ­ Sandwichovy ostrovy','SÃ¼dgeorgien und die SÃ¼dlichen Sandwichinsel','South Georgia and the South Sandwich Islands','Georgia del Sur e Islas Sandwich del Sur','GÃ©orgie du Sud et les ÃŽles Sandwich du Sud','Sud Georgia e Isole Sandwich','Zuid-GeorgiÃ« en de Zuidelijke Sandwicheiland'),(242,'FJ','FJI','FidÅ¾i','Fidschi','Fiji','Fiji','Fidji','Fiji','Fiji'),(246,'FI','FIN','Finsko','Finnland','Finland','Finlandia','Finlande','Finlandia','Finland'),(248,'AX','ALA','Ã…land Islands','Ã…land-Inseln','Ã…land Islands','IslasÃ…land','ÃŽles Ã…land','Ã…land Islands','Ã…land Islands'),(250,'FR','FRA','Francie','Frankreich','France','Francia','France','Francia','Frankrijk'),(254,'GF','GUF','FrancouzskÃ¡ Guayana','FranzÃ¶sisch-Guayana','French Guiana','Guinea Francesa','Guyane FranÃ§aise','Guyana Francese','Frans-Guyana'),(258,'PF','PYF','FrancouzskÃ¡ PolynÃ©sie','FranzÃ¶sisch-Polynesien','French Polynesia','Polinesia Francesa','PolynÃ©sie FranÃ§aise','Polinesia Francese','Frans-PolynesiÃ«'),(260,'TF','ATF','FrancouzskÃ¡ jiÅ¾nÃ­ teritoria','FranzÃ¶sische SÃ¼d- und Antarktisgebiete','French Southern Territories','Territorios SureÃ±os de Francia','Terres Australes FranÃ§aises','Territori Francesi del Sud','Franse Zuidelijke en Antarctische gebieden'),(262,'DJ','DJI','DÅ¾ibutsko','Dschibuti','Djibouti','Djibouti','Djibouti','Gibuti','Djibouti'),(266,'GA','GAB','Gabon','Gabun','Gabon','GabÃ³n','Gabon','Gabon','Gabon'),(268,'GE','GEO','GruzÃ­nsko','Georgien','Georgia','Georgia','GÃ©orgie','Georgia','GeorgiÃ«'),(270,'GM','GMB','Gambie','Gambia','Gambia','Gambia','Gambie','Gambia','Gambia'),(275,'PS','PSE','PalestinskÃ¡ ÃºzemÃ­','PalÃ¤stinensische Autonomiegebiete','Occupied Palestinian Territory','Palestina','Territoire Palestinien OccupÃ©','Territori Palestinesi Occupati','Palestina'),(276,'DE','DEU','NÄ›mecko','Deutschland','Germany','Alemania','Allemagne','Germania','Duitsland'),(288,'GH','GHA','Ghana','Ghana','Ghana','Ghana','Ghana','Ghana','Ghana'),(292,'GI','GIB','Gibraltar','Gibraltar','Gibraltar','Gibraltar','Gibraltar','Gibilterra','Gibraltar'),(296,'KI','KIR','Kiribati','Kiribati','Kiribati','Kiribati','Kiribati','Kiribati','Kiribati'),(300,'GR','GRC','Å˜ecko','Griechenland','Greece','Grecia','GrÃ¨ce','Grecia','Griekenland'),(304,'GL','GRL','GrÃ³nsko','GrÃ¶nland','Greenland','Groenlandia','Groenland','Groenlandia','Groenland'),(308,'GD','GRD','Grenada','Grenada','Grenada','Granada','Grenade','Grenada','Grenada'),(312,'GP','GLP','Guadeloupe','Guadeloupe','Guadeloupe','Guadalupe','Guadeloupe','Guadalupa','Guadeloupe'),(316,'GU','GUM','Guam','Guam','Guam','Guam','Guam','Guam','Guam'),(320,'GT','GTM','Guatemala','Guatemala','Guatemala','Guatemala','Guatemala','Guatemala','Guatemala'),(324,'GN','GIN','Guinea','Guinea','Guinea','Guinea','GuinÃ©e','Guinea','Guinee'),(328,'GY','GUY','Guyana','Guyana','Guyana','Guayana','Guyana','Guyana','Guyana'),(332,'HT','HTI','Haiti','Haiti','Haiti','HaitÃ­','HaÃ¯ti','Haiti','Haiti'),(334,'HM','HMD','HeardÅ¯v ostrov a McDonaldovy ostrovy','Heard und McDonaldinseln','Heard Island and McDonald Islands','Islas Heard e Islas McDonald','ÃŽles Heard et Mcdonald','Isola Heard e Isole McDonald','Heard- en McDonaldeilanden'),(336,'VA','VAT','VatikÃ¡n','Vatikanstadt','Vatican City State','Estado Vaticano','Saint-SiÃ¨ge (Ã©tat de la CitÃ© du Vatican)','CittÃ  del Vaticano','Vaticaanstad'),(340,'HN','HND','Honduras','Honduras','Honduras','Honduras','Honduras','Honduras','Honduras'),(344,'HK','HKG','Hong Kong','Hongkong','Hong Kong','Hong Kong','Hong-Kong','Hong Kong','Hongkong'),(348,'HU','HUN','MaÄarsko','Ungarn','Hungary','HungrÃ­a','Hongrie','Ungheria','Hongarije'),(352,'IS','ISL','Island','Island','Iceland','Islandia','Islande','Islanda','IJsland'),(356,'IN','IND','Indie','Indien','India','India','Inde','India','India'),(360,'ID','IDN','IndonÃ©sie','Indonesien','Indonesia','Indonesia','IndonÃ©sie','Indonesia','IndonesiÃ«'),(364,'IR','IRN','ÃrÃ¡n','Islamische Republik Iran','Islamic Republic of Iran','IrÃ¡n','RÃ©publique Islamique d\'Iran','Iran','Iran'),(368,'IQ','IRQ','IrÃ¡k','Irak','Iraq','Irak','Iraq','Iraq','Irak'),(372,'IE','IRL','Irsko','Irland','Ireland','Irlanda','Irlande','Eire','Ierland'),(376,'IL','ISR','Izrael','Israel','Israel','Israel','IsraÃ«l','Israele','IsraÃ«l'),(380,'IT','ITA','ItÃ¡lie','Italien','Italy','Italia','Italie','Italia','ItaliÃ«'),(384,'CI','CIV','PobÅ™eÅ¾Ã­ slonoviny','CÃ´te d\'Ivoire','CÃ´te d\'Ivoire','Costa de Marfil','CÃ´te d\'Ivoire','Costa d\'Avorio','Ivoorkust'),(388,'JM','JAM','Jamajka','Jamaika','Jamaica','Jamaica','JamaÃ¯que','Giamaica','Jamaica'),(392,'JP','JPN','Japonsko','Japan','Japan','JapÃ³n','Japon','Giappone','Japan'),(398,'KZ','KAZ','KazachstÃ¡n','Kasachstan','Kazakhstan','KazajstÃ¡n','Kazakhstan','Kazakhistan','Kazachstan'),(400,'JO','JOR','JordÃ¡nsko','Jordanien','Jordan','Jordania','Jordanie','Giordania','JordaniÃ«'),(404,'KE','KEN','KeÅˆa','Kenia','Kenya','Kenia','Kenya','Kenya','Kenia'),(408,'KP','PRK','SevernÃ­ Korea','Demokratische Volksrepublik Korea','Democratic People\'s Republic of Korea','Corea','RÃ©publique Populaire DÃ©mocratique de CorÃ©e','Corea del Nord','Noord-Korea'),(410,'KR','KOR','JiÅ¾nÃ­ Korea','Republik Korea','Republic of Korea','Corea','RÃ©publique de CorÃ©e','Corea del Sud','Zuid-Korea'),(414,'KW','KWT','Kuvajt','Kuwait','Kuwait','Kuwait','KoweÃ¯t','Kuwait','Koeweit'),(417,'KG','KGZ','KyrgyzstÃ¡n','Kirgisistan','Kyrgyzstan','KirgistÃ¡n','Kirghizistan','Kirghizistan','KirgiziÃ«'),(418,'LA','LAO','Laos','Demokratische Volksrepublik Laos','Lao People\'s Democratic Republic','Laos','RÃ©publique DÃ©mocratique Populaire Lao','Laos','Laos'),(422,'LB','LBN','Libanon','Libanon','Lebanon','LÃ­bano','Liban','Libano','Libanon'),(426,'LS','LSO','Lesotho','Lesotho','Lesotho','Lesoto','Lesotho','Lesotho','Lesotho'),(428,'LV','LVA','LotyÅ¡sko','Lettland','Latvia','Letonia','Lettonie','Lettonia','Letland'),(430,'LR','LBR','LibÃ©rie','Liberia','Liberia','Liberia','LibÃ©ria','Liberia','Liberia'),(434,'LY','LBY','Libye','Libysch-Arabische Dschamahirija','Libyan Arab Jamahiriya','Libia','Jamahiriya Arabe Libyenne','Libia','LibiÃ«'),(438,'LI','LIE','LichtenÅ¡tejnsko','Liechtenstein','Liechtenstein','Liechtenstein','Liechtenstein','Liechtenstein','Liechtenstein'),(440,'LT','LTU','Litva','Litauen','Lithuania','Lituania','Lituanie','Lituania','Litouwen'),(442,'LU','LUX','Lucembursko','Luxemburg','Luxembourg','Luxemburgo','Luxembourg','Lussemburgo','Groothertogdom Luxemburg'),(446,'MO','MAC','Macao','Macao','Macao','Macao','Macao','Macao','Macao'),(450,'MG','MDG','Madagaskar','Madagaskar','Madagascar','Madagascar','Madagascar','Madagascar','Madagaskar'),(454,'MW','MWI','Malawi','Malawi','Malawi','Malawi','Malawi','Malawi','Malawi'),(458,'MY','MYS','Malajsie','Malaysia','Malaysia','Malasia','Malaisie','Malesia','MaleisiÃ«'),(462,'MV','MDV','Maledivy','Malediven','Maldives','Maldivas','Maldives','Maldive','Maldiven'),(466,'ML','MLI','Mali','Mali','Mali','Mali','Mali','Mali','Mali'),(470,'MT','MLT','Malta','Malta','Malta','Malta','Malte','Malta','Malta'),(474,'MQ','MTQ','Martinik','Martinique','Martinique','Martinica','Martinique','Martinica','Martinique'),(478,'MR','MRT','MauretÃ¡nie','Mauretanien','Mauritania','Mauritania','Mauritanie','Mauritania','MauritaniÃ«'),(480,'MU','MUS','Mauricius','Mauritius','Mauritius','Mauricio','Maurice','Maurizius','Mauritius'),(484,'MX','MEX','Mexiko','Mexiko','Mexico','MÃ©xico','Mexique','Messico','Mexico'),(492,'MC','MCO','Monako','Monaco','Monaco','MÃ³naco','Monaco','Monaco','Monaco'),(496,'MN','MNG','Mongolsko','Mongolei','Mongolia','Mongolia','Mongolie','Mongolia','MongoliÃ«'),(498,'MD','MDA','Moldavsko','Moldawien','Republic of Moldova','Moldavia','RÃ©publique de Moldova','Moldavia','Republiek MoldaviÃ«'),(500,'MS','MSR','Montserrat','Montserrat','Montserrat','Montserrat','Montserrat','Montserrat','Montserrat'),(504,'MA','MAR','Maroko','Marokko','Morocco','Marruecos','Maroc','Marocco','Marokko'),(508,'MZ','MOZ','Mosambik','Mosambik','Mozambique','Mozambique','Mozambique','Mozambico','Mozambique'),(512,'OM','OMN','OmÃ¡n','Oman','Oman','OmÃ¡n','Oman','Oman','Oman'),(516,'NA','NAM','NamÃ­bie','Namibia','Namibia','Namibia','Namibie','Namibia','NamibiÃ«'),(520,'NR','NRU','Nauru','Nauru','Nauru','Nauru','Nauru','Nauru','Nauru'),(524,'NP','NPL','NepÃ¡l','Nepal','Nepal','Nepal','NÃ©pal','Nepal','Nepal'),(528,'NL','NLD','Nizozemsko','Niederlande','Netherlands','Holanda','Pays-Bas','Paesi Bassi','Nederland'),(530,'AN','ANT','NizozemskÃ© Antily','NiederlÃ¤ndische Antillen','Netherlands Antilles','Antillas Holandesas','Antilles NÃ©erlandaises','Antille Olandesi','Nederlandse Antillen'),(533,'AW','ABW','Aruba','Aruba','Aruba','Aruba','Aruba','Aruba','Aruba'),(540,'NC','NCL','NovÃ¡ Kaledonie','Neukaledonien','New Caledonia','Nueva Caledonia','Nouvelle-CalÃ©donie','Nuova Caledonia','Nieuw-CaledoniÃ«'),(548,'VU','VUT','Vanuatu','Vanuatu','Vanuatu','Vanuatu','Vanuatu','Vanuatu','Vanuatu'),(554,'NZ','NZL','NovÃ½ ZÃ©land','Neuseeland','New Zealand','Nueva Zelanda','Nouvelle-ZÃ©lande','Nuova Zelanda','Nieuw-Zeeland'),(558,'NI','NIC','Nikaragua','Nicaragua','Nicaragua','Nicaragua','Nicaragua','Nicaragua','Nicaragua'),(562,'NE','NER','Niger','Niger','Niger','NÃ­ger','Niger','Niger','Niger'),(566,'NG','NGA','NigÃ©rie','Nigeria','Nigeria','Nigeria','NigÃ©ria','Nigeria','Nigeria'),(570,'NU','NIU','Niue','Niue','Niue','Niue','NiuÃ©','Niue','Niue'),(574,'NF','NFK','Norfolk Island','Norfolkinsel','Norfolk Island','Islas Norfolk','ÃŽle Norfolk','Isola Norfolk','Norfolkeiland'),(578,'NO','NOR','Norsko','Norwegen','Norway','Noruega','NorvÃ¨ge','Norvegia','Noorwegen'),(580,'MP','MNP','SeveromariÃ¡nskÃ© ostrovy','NÃ¶rdliche Marianen','Northern Mariana Islands','Islas de Norte-Mariana','ÃŽles Mariannes du Nord','Isole Marianne Settentrionali','Noordelijke Marianen'),(581,'UM','UMI','United States Minor Outlying Islands','Amerikanisch-Ozeanien','United States Minor Outlying Islands','Islas Ultramarinas de Estados Unidos','ÃŽles Mineures Ã‰loignÃ©es des Ã‰tats-Unis','Isole Minori degli Stati Uniti d\'America','United States Minor Outlying Eilanden'),(583,'FM','FSM','MikronÃ©sie','Mikronesien','Federated States of Micronesia','Micronesia','Ã‰tats FÃ©dÃ©rÃ©s de MicronÃ©sie','Stati Federati della Micronesia','MicronesiÃ«'),(584,'MH','MHL','Marshallovy ostrovy','Marshallinseln','Marshall Islands','Islas Marshall','ÃŽles Marshall','Isole Marshall','Marshalleilanden'),(585,'PW','PLW','Palau','Palau','Palau','Palau','Palaos','Palau','Palau'),(586,'PK','PAK','PakistÃ¡n','Pakistan','Pakistan','PakistÃ¡n','Pakistan','Pakistan','Pakistan'),(591,'PA','PAN','Panama','Panama','Panama','PanamÃ¡','Panama','PanamÃ¡','Panama'),(598,'PG','PNG','Papua NovÃ¡ Guinea','Papua-Neuguinea','Papua New Guinea','PapÃºa Nueva Guinea','Papouasie-Nouvelle-GuinÃ©e','Papua Nuova Guinea','Papoea-Nieuw-Guinea'),(600,'PY','PRY','Paraguay','Paraguay','Paraguay','Paraguay','Paraguay','Paraguay','Paraguay'),(604,'PE','PER','Peru','Peru','Peru','PerÃº','PÃ©rou','PerÃ¹','Peru'),(608,'PH','PHL','FilipÃ­ny','Philippinen','Philippines','Filipinas','Philippines','Filippine','Filippijnen'),(612,'PN','PCN','Pitcairn','Pitcairninseln','Pitcairn','Pitcairn','Pitcairn','Pitcairn','Pitcairneilanden'),(616,'PL','POL','Polsko','Polen','Poland','Polonia','Pologne','Polonia','Polen'),(620,'PT','PRT','Portugalsko','Portugal','Portugal','Portugal','Portugal','Portogallo','Portugal'),(624,'GW','GNB','Guinea-Bissau','Guinea-Bissau','Guinea-Bissau','Guinea-Bissau','GuinÃ©e-Bissau','Guinea-Bissau','Guinee-Bissau'),(626,'TL','TLS','VÃ½chodnÃ­ Timor','Timor-Leste','Timor-Leste','Timor Leste','Timor-Leste','Timor Est','Oost-Timor'),(630,'PR','PRI','Portoriko','Puerto Rico','Puerto Rico','Puerto Rico','Porto Rico','Porto Rico','Puerto Rico'),(634,'QA','QAT','Katar','Katar','Qatar','Qatar','Qatar','Qatar','Qatar'),(638,'RE','REU','Reunion','RÃ©union','RÃ©union','ReuniÃ³n','RÃ©union','Reunion','RÃ©union'),(642,'RO','ROU','Rumunsko','RumÃ¤nien','Romania','RumanÃ­a','Roumanie','Romania','RoemeniÃ«'),(643,'RU','RUS','Rusko','Russische FÃ¶deration','Russian Federation','Rusia','FÃ©dÃ©ration de Russie','Federazione Russa','Rusland'),(646,'RW','RWA','Rwanda','Ruanda','Rwanda','Ruanda','Rwanda','Ruanda','Rwanda'),(654,'SH','SHN','SvatÃ¡ Helena','St. Helena','Saint Helena','Santa Helena','Sainte-HÃ©lÃ¨ne','Sant\'Elena','Sint-Helena'),(659,'KN','KNA','SvatÃ½ Kitts a Nevis','St. Kitts und Nevis','Saint Kitts and Nevis','Santa Kitts y Nevis','Saint-Kitts-et-Nevis','Saint Kitts e Nevis','Saint Kitts en Nevis'),(660,'AI','AIA','Anguilla','Anguilla','Anguilla','Anguilla','Anguilla','Anguilla','Anguilla'),(662,'LC','LCA','SvatÃ¡ Lucie','St. Lucia','Saint Lucia','Santa LucÃ­a','Sainte-Lucie','Santa Lucia','Saint Lucia'),(666,'PM','SPM','SvatÃ½ Pierre a Miquelon','St. Pierre und Miquelon','Saint-Pierre and Miquelon','San Pedro y Miquelon','Saint-Pierre-et-Miquelon','Saint Pierre e Miquelon','Saint-Pierre en Miquelon'),(670,'VC','VCT','SvatÃ½ Vincenc a Grenadiny','St. Vincent und die Grenadinen','Saint Vincent and the Grenadines','San Vincente y Las Granadinas','Saint-Vincent-et-les Grenadines','Saint Vincent e Grenadine','Saint Vincent en de Grenadines'),(674,'SM','SMR','San Marino','San Marino','San Marino','San Marino','Saint-Marin','San Marino','San Marino'),(678,'ST','STP','SvatÃ½ TomÃ¡Å¡ a PrincÅ¯v ostrov','SÃ£o TomÃ© und PrÃ­ncipe','Sao Tome and Principe','Santo TomÃ© y PrÃ­ncipe','Sao TomÃ©-et-Principe','Sao Tome e Principe','Sao TomÃ© en Principe'),(682,'SA','SAU','SaudskÃ¡ ArÃ¡bie','Saudi-Arabien','Saudi Arabia','Arabia SaudÃ­','Arabie Saoudite','Arabia Saudita','Saoedi-ArabiÃ«'),(686,'SN','SEN','Senegal','Senegal','Senegal','Senegal','SÃ©nÃ©gal','Senegal','Senegal'),(690,'SC','SYC','Seychely','Seychellen','Seychelles','Seychelles','Seychelles','Seychelles','Seychellen'),(694,'SL','SLE','Sierra Leone','Sierra Leone','Sierra Leone','Sierra Leona','Sierra Leone','Sierra Leone','Sierra Leone'),(702,'SG','SGP','Singapur','Singapur','Singapore','Singapur','Singapour','Singapore','Singapore'),(703,'SK','SVK','Slovensko','Slowakei','Slovakia','Eslovaquia','Slovaquie','Slovacchia','Slowakije'),(704,'VN','VNM','Vietnam','Vietnam','Vietnam','Vietnam','Viet Nam','Vietnam','Vietnam'),(705,'SI','SVN','Slovinsko','Slowenien','Slovenia','Eslovenia','SlovÃ©nie','Slovenia','SloveniÃ«'),(706,'SO','SOM','SomÃ¡lsko','Somalia','Somalia','Somalia','Somalie','Somalia','SomaliÃ«'),(710,'ZA','ZAF','JiÅ¾nÃ­ Afrika','SÃ¼dafrika','South Africa','SudÃ¡frica','Afrique du Sud','Sud Africa','Zuid-Afrika'),(716,'ZW','ZWE','Zimbabwe','Simbabwe','Zimbabwe','Zimbabue','Zimbabwe','Zimbabwe','Zimbabwe'),(724,'ES','ESP','Å panÄ›lsko','Spanien','Spain','EspaÃ±a','Espagne','Spagna','Spanje'),(732,'EH','ESH','ZÃ¡padnÃ­ Sahara','Westsahara','Western Sahara','SÃ¡hara Occidental','Sahara Occidental','Sahara Occidentale','Westelijke Sahara'),(736,'SD','SDN','SÃºdÃ¡n','Sudan','Sudan','SudÃ¡n','Soudan','Sudan','Sudan'),(738,'SS','SSD','JiÅ¾nÃ­ SÃºdÃ¡n','SÃ¼dsudan','South Sudan','SudÃ¡n del Sur','Soudan du Sud','Sudan del Sud','Zuid-Soedan'),(740,'SR','SUR','Surinam','Suriname','Suriname','SurinÃ¡m','Suriname','Suriname','Suriname'),(744,'SJ','SJM','Å picberky a Jan Mayen','Svalbard and Jan Mayen','Svalbard and Jan Mayen','Esvalbard y Jan Mayen','Svalbard etÃŽle Jan Mayen','Svalbard e Jan Mayen','Svalbard'),(748,'SZ','SWZ','Svazijsko','Swasiland','Swaziland','Suazilandia','Swaziland','Swaziland','Swaziland'),(752,'SE','SWE','Å vÃ©dsko','Schweden','Sweden','Suecia','SuÃ¨de','Svezia','Zweden'),(756,'CH','CHE','Å vÃ½carsko','Schweiz','Switzerland','Suiza','Suisse','Svizzera','Zwitserland'),(760,'SY','SYR','SÃ½rie','Arabische Republik Syrien','Syrian Arab Republic','Siria','RÃ©publique Arabe Syrienne','Siria','SyriÃ«'),(762,'TJ','TJK','TadÅ¾ikistÃ¡n','Tadschikistan','Tajikistan','TajikistÃ¡n','Tadjikistan','Tagikistan','Tadzjikistan'),(764,'TH','THA','Thajsko','Thailand','Thailand','Tailandia','ThaÃ¯lande','Tailandia','Thailand'),(768,'TG','TGO','Togo','Togo','Togo','Togo','Togo','Togo','Togo'),(772,'TK','TKL','Tokelau','Tokelau','Tokelau','Tokelau','Tokelau','Tokelau','Tokelau -eilanden'),(776,'TO','TON','Tonga','Tonga','Tonga','Tongo','Tonga','Tonga','Tonga'),(780,'TT','TTO','Trinidad a Tobago','Trinidad und Tobago','Trinidad and Tobago','Trinidad y Tobago','TrinitÃ©-et-Tobago','Trinidad e Tobago','Trinidad en Tobago'),(784,'AE','ARE','SpojenÃ© ArabskÃ© EmirÃ¡ty','Vereinigte Arabische Emirate','United Arab Emirates','EmiratosÃrabes Unidos','Ã‰mirats Arabes Unis','Emirati Arabi Uniti','Verenigde Arabische Emiraten'),(788,'TN','TUN','Tunisko','Tunesien','Tunisia','TÃºnez','Tunisie','Tunisia','TunesiÃ«'),(792,'TR','TUR','Turecko','TÃ¼rkei','Turkey','TurquÃ­a','Turquie','Turchia','Turkije'),(795,'TM','TKM','TurkmenistÃ¡n','Turkmenistan','Turkmenistan','TurmenistÃ¡n','TurkmÃ©nistan','Turkmenistan','Turkmenistan'),(796,'TC','TCA','Turks a ostrovy Caicos','Turks- und Caicosinseln','Turks and Caicos Islands','Islas Turks y Caicos','ÃŽles Turks et CaÃ¯ques','Isole Turks e Caicos','Turks- en Caicoseilanden'),(798,'TV','TUV','Tuvalu','Tuvalu','Tuvalu','Tuvalu','Tuvalu','Tuvalu','Tuvalu'),(800,'UG','UGA','Uganda','Uganda','Uganda','Uganda','Ouganda','Uganda','Oeganda'),(804,'UA','UKR','Ukrajina','Ukraine','Ukraine','Ucrania','Ukraine','Ucraina','OekraÃ¯ne'),(807,'MK','MKD','Makedonie','Ehem. jugoslawische Republik Mazedonien','The Former Yugoslav Republic of Macedonia','Macedonia','L\'ex-RÃ©publique Yougoslave de MacÃ©doine','Macedonia','MacedoniÃ«'),(818,'EG','EGY','Egypt','Ã„gypten','Egypt','Egipto','Ã‰gypte','Egitto','Egypte'),(826,'GB','GBR','VelkÃ¡ BritÃ¡nie','Vereinigtes KÃ¶nigreich von GroÃŸbritannien u','United Kingdom','Reino Unido','Royaume-Uni','Regno Unito','Verenigd Koninkrijk'),(833,'IM','IMN','Ostrov Man','Insel Man','Isle of Man','Isla de Man','ÃŽle de Man','Isola di Man','Eiland Man'),(834,'TZ','TZA','TanzÃ¡nie','Vereinigte Republik Tansania','United Republic Of Tanzania','Tanzania','RÃ©publique-Unie de Tanzanie','Tanzania','Tanzania'),(840,'US','USA','USA','Vereinigte Staaten von Amerika','United States','Estados Unidos','Ã‰tats-Unis','Stati Uniti d\'America','Verenigde Staten'),(850,'VI','VIR','AmerickÃ© PanenskÃ© ostrovy','Amerikanische Jungferninseln','U.S. Virgin Islands','Islas VÃ­rgenes Estadounidenses','ÃŽles Vierges des Ã‰tats-Unis','Isole Vergini Americane','Amerikaanse Maagdeneilanden'),(854,'BF','BFA','Burkina Faso','Burkina Faso','Burkina Faso','Burkina Faso','Burkina Faso','Burkina Faso','Burkina Faso'),(858,'UY','URY','Uruguay','Uruguay','Uruguay','Uruguay','Uruguay','Uruguay','Uruguay'),(860,'UZ','UZB','UzbekistÃ¡n','Usbekistan','Uzbekistan','UzbekistÃ¡n','OuzbÃ©kistan','Uzbekistan','Oezbekistan'),(862,'VE','VEN','Venezuela','Venezuela','Venezuela','Venezuela','Venezuela','Venezuela','Venezuela'),(876,'WF','WLF','Wallis a Futuna','Wallis und Futuna','Wallis and Futuna','Wallis y Futuna','Wallis et Futuna','Wallis e Futuna','Wallis en Futuna'),(882,'WS','WSM','Samoa','Samoa','Samoa','Samoa','Samoa','Samoa','Samoa'),(887,'YE','YEM','Jemen','Jemen','Yemen','Yemen','YÃ©men','Yemen','Jemen'),(891,'CS','SCG','Serbia and Montenegro','Serbien und Montenegro','Serbia and Montenegro','Serbia y Montenegro','Serbie-et-MontÃ©nÃ©gro','Serbia e Montenegro','ServiÃ« en Montenegro'),(894,'ZM','ZMB','Zambie','Sambia','Zambia','Zambia','Zambie','Zambia','Zambia');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curso` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nivel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `educategoria_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `curso_educategoria_id_foreign` (`educategoria_id`),
  CONSTRAINT `curso_educategoria_id_foreign` FOREIGN KEY (`educategoria_id`) REFERENCES `educategoria` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
INSERT INTO `curso` VALUES (1,'Introducción al Blockchain','basico',NULL,'280792040','conf534301.jpg',1,1,1,NULL,'2018-07-19 13:26:29',1),(2,'Introducción al CriptoTrading','Intermedio',NULL,'280814220','conf514801.jpg',1,1,1,'2018-06-12 10:32:20','2018-07-19 14:56:23',2),(3,'Introducción a las ICOs','basico',NULL,'280813777','conf232681.jpg',1,1,1,'2018-06-16 23:02:56','2018-07-19 14:53:19',4),(4,'Introducción a la Evolución Personal','basico',NULL,'280813496','conf5546111.jpg',1,1,1,'2018-06-16 23:03:19','2018-07-19 14:50:28',7),(5,'Introducción a la Minería POW','Intermedio',NULL,'280813320','conf442781.jpg',1,1,1,'2018-06-16 23:03:34','2018-07-19 14:49:53',3),(6,'Introducción a la Minería POS y Masternodes','avanzado',NULL,'280805095','conf62881.jpg',1,1,1,'2018-06-16 23:03:48','2018-07-19 14:37:59',5),(7,'Introducción a Forex','Intermedio',NULL,'280791075','conf462881.jpg',1,1,1,'2018-06-16 23:04:07','2018-07-19 13:25:23',6);
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_membresia`
--

DROP TABLE IF EXISTS `detalle_membresia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_membresia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `membresia_id` int(10) unsigned NOT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detalle_membresia_membresia_id_foreign` (`membresia_id`),
  CONSTRAINT `detalle_membresia_membresia_id_foreign` FOREIGN KEY (`membresia_id`) REFERENCES `membresia` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_membresia`
--

LOCK TABLES `detalle_membresia` WRITE;
/*!40000 ALTER TABLE `detalle_membresia` DISABLE KEYS */;
INSERT INTO `detalle_membresia` VALUES (1,1,'6 Beneficios','2018-05-13 03:32:54','2018-05-13 03:32:54'),(2,1,'Certificación en Forex en VIVO','2018-05-13 03:32:57','2018-05-13 03:32:57'),(3,1,'Certificación en Blockchain en VIVO','2018-05-13 03:33:09','2018-05-13 03:33:09'),(4,1,'Análisis fundamental del Mercado Criptográfico Semanal en VIVO','2018-05-13 03:33:18','2018-05-13 03:33:18'),(5,1,'Análisis fundamental del Mercado Forex Semanal en VIVO','2018-05-13 03:33:25','2018-05-13 03:33:25'),(6,1,'Certificación en Evolución Personal en VIVO.','2018-05-13 03:34:53','2018-05-13 03:34:53'),(7,2,'9 Beneficios','2018-05-13 03:34:59','2018-05-13 03:34:59'),(8,2,'Certificación en Forex en VIVO','2018-05-13 03:35:03','2018-05-13 03:35:03'),(9,2,'Certificación en Blockchain en VIVO','2018-05-13 03:35:08','2018-05-13 03:35:08'),(10,2,'Certificación en CriptoTrading en VIVO','2018-05-13 03:35:12','2018-05-13 03:35:12'),(11,2,'Certificación en ICOs en VIVO','2018-05-13 03:35:16','2018-05-13 03:35:16'),(12,2,'Análisis fundamental del Mercado Criptográfico Semanal en VIVO','2018-05-13 03:35:20','2018-05-13 03:35:20'),(13,2,'1 Solicitud de Revisión de tu Portafolio Personal en VIVO al mes.','2018-05-13 03:35:25','2018-05-13 03:35:25'),(14,2,'Análisis fundamental del Mercado Forex Semanal en VIVO.','2018-05-13 03:35:30','2018-05-13 03:35:30'),(15,2,'Certificación en Evolución Personal en VIVO','2018-05-13 03:35:34','2018-05-13 03:35:34'),(16,3,'12 Beneficios','2018-05-13 03:36:44','2018-05-13 03:36:44'),(17,3,'Certificación en Forex en VIVO','2018-05-13 03:36:50','2018-05-13 03:36:50'),(18,3,'Certificación en Blockchain en VIVO','2018-05-13 03:36:54','2018-05-13 03:36:54'),(19,3,'Certificación en CriptoTrading en VIVO','2018-05-13 03:36:58','2018-05-13 03:36:58'),(20,3,'Certificación en ICOs en VIVO','2018-05-13 03:37:02','2018-05-13 03:37:02'),(21,3,'Certificación en Minería POS en VIVO','2018-05-13 03:37:07','2018-05-13 03:37:07'),(22,3,'Certificación en Masternodes en VIVO','2018-05-13 03:37:11','2018-05-13 03:37:11'),(23,3,'Certificación en Minería POW en VIVO','2018-05-13 03:37:16','2018-05-13 03:37:16'),(24,3,'\nAnálisis fundamental del Mercado Criptográfico Semanal en VIVO','2018-05-13 03:37:20','2018-05-13 03:37:20'),(25,3,'2 Solicitudes de Revisión de tu Portafolio Personal en VIVO al mes','2018-05-13 03:37:27','2018-05-13 03:37:27'),(26,5,'Compartir las Actualizaciones de un Portafolio Elite','2018-05-13 03:37:32','2018-05-13 03:37:32'),(27,5,'Aplicar a un Crowfunding para tu Start-up','2018-05-13 03:37:36','2018-05-13 03:37:36'),(28,5,'Cripto-Tarjeta','2018-05-13 03:37:40','2018-05-13 03:37:40'),(29,5,'10% de ComisiÃ³n de tu 1era LÃ­nea de Referidos','2018-05-13 03:37:45','2018-05-13 03:37:45'),(30,5,'10% de ComisiÃ³n de tu 2da LÃ­nea de Referidos','2018-05-13 03:37:49','2018-05-13 03:37:49'),(31,5,'5% de ComisiÃ³n de tu 3era LÃ­nea de Referidos','2018-05-13 03:37:53','2018-05-13 03:37:53'),(32,3,'Análisis fundamental del Mercado Forex Semanal en VIVO',NULL,NULL),(33,3,'Certificación en Evolución Personal en VIVO',NULL,NULL);
/*!40000 ALTER TABLE `detalle_membresia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `educategoria`
--

DROP TABLE IF EXISTS `educategoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `educategoria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL,
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `educategoria`
--

LOCK TABLES `educategoria` WRITE;
/*!40000 ALTER TABLE `educategoria` DISABLE KEYS */;
INSERT INTO `educategoria` VALUES (1,'Blockchain','Aquí estarán los videos de introducción de las clases en vivo de Blockchain',1,1,1,NULL,'2018-06-05 17:50:57'),(2,'CriptoTrading',NULL,1,1,1,'2018-06-12 10:31:27','2018-06-16 22:56:04'),(3,'Minería POW',NULL,1,1,1,'2018-06-12 10:31:39','2018-07-19 09:41:50'),(4,'ICOs',NULL,1,1,1,'2018-06-16 22:53:20','2018-06-16 22:53:20'),(5,'Minería POS y Masternodes',NULL,1,1,1,'2018-06-16 22:53:40','2018-07-19 09:42:02'),(6,'Forex',NULL,1,1,1,'2018-06-16 22:54:03','2018-06-16 22:54:03'),(7,'Evolución Personal',NULL,1,1,1,'2018-07-19 09:42:31','2018-07-19 09:42:31');
/*!40000 ALTER TABLE `educategoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forex`
--

DROP TABLE IF EXISTS `forex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forex` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pair` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'sell',
  `market_execution` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `sell_limit` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `stop_loss` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `take_prof1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `take_prof2` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `take_prof3` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forex`
--

LOCK TABLES `forex` WRITE;
/*!40000 ALTER TABLE `forex` DISABLE KEYS */;
/*!40000 ALTER TABLE `forex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo`
--

DROP TABLE IF EXISTS `grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `ids_menu` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo`
--

LOCK TABLES `grupo` WRITE;
/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
INSERT INTO `grupo` VALUES (1,'Administrador','1,44,41,45,46,16,18,19,20,48,49,50,51,61,53,54,55,42,43,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,13,2,3,5,6,7,12,65'),(2,'Alumno','1,42,43,26,27,29,30,31,32,33,34,35,36,38,39'),(3,'Influencer','1,42,43,26,27,29,30,31,32,33,34,35,36,38,39,65'),(4,'Inactivo','1,44,42,43,38,39');
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leccion`
--

DROP TABLE IF EXISTS `leccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leccion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duracion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visto` tinyint(1) NOT NULL DEFAULT '0',
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `curso_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `leccion_curso_id_foreign` (`curso_id`),
  CONSTRAINT `leccion_curso_id_foreign` FOREIGN KEY (`curso_id`) REFERENCES `curso` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leccion`
--

LOCK TABLES `leccion` WRITE;
/*!40000 ALTER TABLE `leccion` DISABLE KEYS */;
INSERT INTO `leccion` VALUES (14,'1. Presentación',NULL,'277878213','10:37 minutos',0,1,1,1,'2018-06-30 18:27:47','2018-07-19 16:38:34',1),(15,'2. Las criptomonedas',NULL,'277880659','7:37 minutos',0,1,1,1,'2018-06-30 18:28:11','2018-07-19 16:39:10',1),(16,'3. Ethereum',NULL,'277872198','5:17 minutos',0,1,1,1,'2018-06-30 18:28:33','2018-07-19 16:39:44',1),(17,'4. ICOs y DAICOs',NULL,'277872200','2:18 minutos',0,1,1,1,'2018-06-30 18:28:53','2018-07-19 16:40:18',1),(18,'5. Hyperledger',NULL,'277872203','2:30 minutos',0,1,1,1,'2018-06-30 18:28:53','2018-07-19 16:42:51',1),(20,'1. Presentación',NULL,'269389341','2:02 minutos',0,1,1,1,'2018-07-01 08:51:46','2018-07-19 16:32:39',2),(22,'2. El trading',NULL,'269389887','8:18 minutos',0,1,1,1,'2018-07-01 08:52:49','2018-07-19 16:33:39',2),(24,'3. Mercados bursátiles y financieros',NULL,'269391928','6:54 minutos',0,1,1,1,'2018-07-01 08:55:30','2018-07-19 16:35:13',2),(26,'4. Exchanges y billeteras','Descripción de la lección','269392919','7:14 minutos',0,1,1,1,'2018-07-01 08:56:44','2018-07-19 16:36:06',2),(27,'5. Plataformas y órdenes','Descripción de la lección','269395106','2:25 minutos',0,1,1,1,'2018-07-01 08:58:41','2018-07-19 16:36:52',2),(28,'1. Presentación','Descripción de la lección','268335324','7:48 minutos',0,1,1,1,'2018-07-01 09:56:37','2018-07-19 15:30:53',5),(29,'2. Herramientas de minado','Descripción de la lección','268333819','4:48 minutos',0,1,1,1,'2018-07-01 09:58:09','2018-07-19 15:38:03',5),(30,'3. Hardware y armado de rig','Descripción de la lección','268333497','3:08 minutos',0,1,1,1,'2018-07-01 09:59:21','2018-07-19 15:38:52',5),(31,'4. Instalación de windows, simplemining y over clock',NULL,'268334666','3:30 minutos',0,1,1,1,'2018-07-01 10:00:58','2018-07-19 15:40:24',5),(32,'5. Pooles de minería',NULL,'268337467','1:13 minutos',0,1,1,1,'2018-07-01 10:02:01','2018-07-19 15:41:34',5),(33,'6. Configuración Asic',NULL,'268333312','1:41 minutos',0,1,1,1,'2018-07-01 10:02:59','2018-07-19 15:43:49',5),(34,'7. Minería de 5 criptomonedas','Descripción de la lección','268336829','1:41 minutos',0,1,1,1,'2018-07-01 10:03:56','2018-07-19 15:46:57',5),(35,'6. Multi-Change',NULL,'277872204','1:34 minutos',0,1,1,1,'2018-07-04 05:16:36','2018-07-19 16:44:01',1),(36,'7. Red Lyra',NULL,'277872205','2:30 minutos',0,1,1,1,'2018-07-04 05:17:20','2018-07-19 16:45:06',1),(37,'8. El futuro','Descripción de la lección','277872206','3:21 minutos',0,1,1,1,'2018-07-04 05:18:01','2018-07-19 16:45:39',1),(38,'1. ¿Qué es el mercado de Forex?',NULL,'278045854','32:39 minutos',0,1,1,1,'2018-07-04 05:19:17','2018-07-19 15:28:28',7),(39,'1. Presentación',NULL,'268330899','3:05 minutos',0,1,1,1,'2018-07-19 15:12:02','2018-07-19 15:15:05',6),(40,'2. ROI y Mastenodes',NULL,'268332583','1:07 minutos',0,1,1,1,'2018-07-19 15:15:57','2018-07-19 15:15:57',6),(41,'3. VPS y sus configuraciones',NULL,'268332935','2:43 minutos',0,1,1,1,'2018-07-19 15:17:35','2018-07-19 15:17:35',6),(42,'4. Recuperación de billeteras',NULL,'268332472','1:10 minutos',0,1,1,1,'2018-07-19 15:18:31','2018-07-19 15:18:31',6),(43,'5. Configuraciones Cold Wallet',NULL,'268329628','1:18 minutos',0,1,1,1,'2018-07-19 15:24:56','2018-07-19 15:24:56',6),(44,'6. Linux y billetera Hot',NULL,'268330399','1:14 minutos',0,1,1,1,'2018-07-19 15:25:41','2018-07-19 15:25:41',6),(45,'7. Recomendaciones finales','<p><br></p>','268331528','1:05 minutos',0,1,1,1,'2018-07-19 15:26:37','2018-07-19 15:27:41',6);
/*!40000 ALTER TABLE `leccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leccion_usuario`
--

DROP TABLE IF EXISTS `leccion_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leccion_usuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visto` tinyint(1) NOT NULL DEFAULT '0',
  `usuario` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `leccion_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `leccion_usuario_leccion_id_foreign` (`leccion_id`),
  CONSTRAINT `leccion_usuario_leccion_id_foreign` FOREIGN KEY (`leccion_id`) REFERENCES `leccion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leccion_usuario`
--

LOCK TABLES `leccion_usuario` WRITE;
/*!40000 ALTER TABLE `leccion_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `leccion_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `descripcion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pago culqi',
  `membresia` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ruta` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` VALUES (1,'JoseMBit','2018-07-21 16:02:57','No se ha podido registrar el pago en el api','8','url:dashboard/admin/activation','2018-07-21 14:02:57','2018-07-21 14:02:57');
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membresia`
--

DROP TABLE IF EXISTS `membresia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membresia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Aprendiz',
  `precio_mes` decimal(10,2) NOT NULL DEFAULT '0.00',
  `precio_anual` decimal(10,2) NOT NULL DEFAULT '0.00',
  `estado` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membresia`
--

LOCK TABLES `membresia` WRITE;
/*!40000 ALTER TABLE `membresia` DISABLE KEYS */;
INSERT INTO `membresia` VALUES (1,'Aprendiz',50.00,397.00,1,'2018-07-19 22:00:00','2018-05-13 02:35:56'),(2,'Emprendedor',50.00,697.00,1,'2018-07-19 22:00:00',NULL),(3,'Inversionista',50.00,1197.00,1,'2018-07-19 22:00:00',NULL),(4,'Inversionista VIP',50.00,1997.00,1,'2018-07-13 12:22:49','2018-07-13 12:22:49'),(5,'InversionistA',10.00,10.00,1,'2018-07-17 09:08:40','2018-07-17 09:08:40'),(6,'Aprendiz',0.00,0.00,1,'2018-07-19 22:00:00',NULL),(7,'EmprendedoR',0.00,0.00,1,'2018-07-19 22:00:00',NULL),(8,'Inversionista viP',0.00,0.00,1,'2018-07-19 22:00:00',NULL);
/*!40000 ALTER TABLE `membresia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_100000_create_password_resets_table',1),(2,'2018_04_13_051412_create_table_edu_categoria',1),(3,'2018_04_13_165157_create_table_curso',1),(4,'2018_04_15_161708_create_table_leccion',1),(5,'2018_04_15_184126_create_table_archivo',1),(6,'2018_04_16_215047_create_table_leccion_usuario',1),(7,'2018_04_17_230802_create_settings_table',1),(8,'2018_04_20_191547_create_table_portafolio',1),(9,'2018_04_20_192137_create_table_monedas',1),(10,'2018_04_21_094217_create_table_operaciones',1),(11,'2018_05_02_214255_create_table_noticias',1),(12,'2018_05_05_200903_create_membresia_table',1),(13,'2018_05_05_223228_create_users_table',1),(14,'2018_05_05_223802_add_providers_to_users_table',1),(15,'2018_05_05_224742_create_signals_table',1),(16,'2018_05_12_120242_create_table_detalle_membresia',1),(17,'2018_05_19_000604_add_prices_to_signals_table',1),(18,'2018_05_22_171803_create_table_pagos',2),(19,'2018_06_19_002623_add_percents_to_signal_table',3),(20,'2018_06_23_225139_add_rangeprice_to_signal_table',3),(21,'2018_07_02_171442_create_coinbase_webhook_calls_table',4),(22,'2018_07_13_110624_add_description_to_pagos_table',4),(23,'2018_07_13_112932_add_datepay_activohasta_to_pagos_table',4),(24,'2018_07_13_163702_add_charge_id_to_pagos_table',5),(25,'2018_07_13_212059_create_cargos_table',6),(26,'2018_07_16_175717_create_table_forex',7),(27,'2018_07_20_184506_create_table_log',8);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mlm_chistorial`
--

DROP TABLE IF EXISTS `mlm_chistorial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mlm_chistorial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `us_ref` varchar(255) NOT NULL,
  `monto` float(8,2) NOT NULL,
  `fecha` int(11) NOT NULL,
  `dia` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `semana` int(11) NOT NULL,
  `plan` int(11) NOT NULL,
  `bono` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mlm_chistorial`
--

LOCK TABLES `mlm_chistorial` WRITE;
/*!40000 ALTER TABLE `mlm_chistorial` DISABLE KEYS */;
/*!40000 ALTER TABLE `mlm_chistorial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mlm_comisiones`
--

DROP TABLE IF EXISTS `mlm_comisiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mlm_comisiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `detalles` varchar(255) NOT NULL,
  `fecha` int(11) NOT NULL,
  `monto` int(11) NOT NULL,
  `fecha_pago` int(11) NOT NULL,
  `dia` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `semana` int(11) NOT NULL,
  `procesado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mlm_comisiones`
--

LOCK TABLES `mlm_comisiones` WRITE;
/*!40000 ALTER TABLE `mlm_comisiones` DISABLE KEYS */;
/*!40000 ALTER TABLE `mlm_comisiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mlm_mchistorial`
--

DROP TABLE IF EXISTS `mlm_mchistorial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mlm_mchistorial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `monto` float(8,2) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `plan` int(11) NOT NULL,
  `fecha` int(11) NOT NULL,
  `dia` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `semana` int(11) NOT NULL,
  `fechaexp` int(11) NOT NULL,
  `fecha_pago` int(11) NOT NULL,
  `id_factura` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mlm_mchistorial`
--

LOCK TABLES `mlm_mchistorial` WRITE;
/*!40000 ALTER TABLE `mlm_mchistorial` DISABLE KEYS */;
/*!40000 ALTER TABLE `mlm_mchistorial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mlm_paquetes`
--

DROP TABLE IF EXISTS `mlm_paquetes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mlm_paquetes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `precio` float(8,2) NOT NULL,
  `impuesto` float(8,2) NOT NULL,
  `pmin` float(8,2) NOT NULL,
  `moneda` int(11) NOT NULL,
  `duracion` int(11) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `niveles` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mlm_paquetes`
--

LOCK TABLES `mlm_paquetes` WRITE;
/*!40000 ALTER TABLE `mlm_paquetes` DISABLE KEYS */;
INSERT INTO `mlm_paquetes` VALUES (1,'Aprendiz','Aprendiz',597.00,0.00,0.00,0,365,'bir','10;5;5'),(2,'Emprendedor','Emprendedor',1097.00,0.00,0.00,0,365,'bir','10;5;5'),(3,'Inversionista','Inversionista',1997.00,0.00,0.00,0,365,'bir','10;10;10');
/*!40000 ALTER TABLE `mlm_paquetes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monedas`
--

DROP TABLE IF EXISTS `monedas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monedas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moneda` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `portafolio_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `monedas_portafolio_id_foreign` (`portafolio_id`),
  CONSTRAINT `monedas_portafolio_id_foreign` FOREIGN KEY (`portafolio_id`) REFERENCES `portafolio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monedas`
--

LOCK TABLES `monedas` WRITE;
/*!40000 ALTER TABLE `monedas` DISABLE KEYS */;
INSERT INTO `monedas` VALUES (1,'ETH',1,1,1,'2018-06-03 21:34:34','2018-06-03 21:34:34',1),(2,'DASH',0,1,1,'2018-06-05 05:04:16','2018-06-30 18:01:59',1),(3,'MAN',1,1,1,'2018-06-05 20:26:38','2018-06-05 20:26:38',1),(4,'ETH',0,1,1,'2018-06-12 20:29:49','2018-06-12 20:29:49',2),(5,'EOS',1,1,1,'2018-06-14 11:59:18','2018-06-14 11:59:18',1),(6,'LTC',1,1,1,'2018-06-14 13:38:28','2018-06-14 13:38:28',1),(7,'ADA',1,1,1,'2018-06-14 21:27:50','2018-06-14 21:27:50',1),(8,'STRAT',1,1,1,'2018-06-15 08:59:14','2018-06-15 08:59:14',1),(9,'NEO',1,1,1,'2018-06-15 14:51:26','2018-06-15 14:51:26',1),(10,'LTC',1,1,1,'2018-06-18 19:57:18','2018-06-18 19:57:18',1),(11,'EOSDAC',1,1,1,'2018-06-18 21:51:20','2018-06-18 21:51:20',1),(12,'EOS',1,1,1,'2018-06-18 21:54:00','2018-06-18 21:54:00',1),(13,'ADA',1,1,1,'2018-06-19 13:23:35','2018-06-19 13:23:35',1),(14,'VEN',1,1,1,'2018-06-19 17:44:57','2018-06-19 17:44:57',1),(15,'TRX',1,1,1,'2018-06-20 13:09:28','2018-06-20 13:09:28',1),(16,'XMR',1,1,1,'2018-06-20 17:48:24','2018-06-20 17:48:24',1),(17,'BNB',1,1,1,'2018-06-20 19:47:51','2018-06-20 19:47:51',1),(18,'LOOM',1,1,1,'2018-06-20 21:18:17','2018-06-20 21:18:17',1),(19,'MAN',1,1,1,'2018-06-25 18:59:24','2018-06-25 18:59:24',1),(20,'VEN',1,1,1,'2018-06-26 17:39:36','2018-06-26 17:39:36',1),(21,'WOLF',1,1,1,'2018-06-29 07:19:14','2018-06-29 07:19:14',1),(22,'PCN',1,1,1,'2018-06-29 09:53:59','2018-06-29 09:53:59',1),(23,'BTS',1,1,1,'2018-06-29 14:45:08','2018-06-29 14:45:08',1),(24,'ARG',1,1,1,'2018-07-02 12:04:26','2018-07-02 12:04:26',1),(25,'EBST',1,1,1,'2018-07-02 13:05:50','2018-07-02 13:05:50',1),(26,'NPXS',1,1,1,'2018-07-02 19:06:42','2018-07-02 19:06:42',1),(27,'XRP',1,1,1,'2018-07-03 11:31:38','2018-07-03 11:31:38',1),(28,'LTC',1,1,1,'2018-07-03 21:29:12','2018-07-03 21:29:12',1),(29,'BCH',1,1,1,'2018-07-04 17:46:21','2018-07-04 17:46:21',1),(30,'XRP',1,1,1,'2018-07-05 10:11:18','2018-07-05 10:11:18',1),(31,'XRP',1,1,1,'2018-07-07 17:47:06','2018-07-07 17:47:06',1),(32,'xrp',1,13,13,'2018-07-09 20:26:06','2018-07-09 20:26:06',3),(33,'ETH',1,13,13,'2018-07-11 20:13:13','2018-07-11 20:13:13',3),(34,'etc',0,13,13,'2018-07-11 22:23:24','2018-07-12 10:24:24',3),(35,'XRP',1,13,13,'2018-07-12 10:24:53','2018-07-12 10:24:53',3),(36,'LTC',1,13,13,'2018-07-12 15:39:25','2018-07-12 15:39:25',3),(37,'XLM',1,13,13,'2018-07-12 18:59:38','2018-07-12 18:59:38',3),(38,'NEO',1,13,13,'2018-07-13 15:50:40','2018-07-13 15:50:40',3),(39,'BTC',1,12,12,'2018-07-18 09:22:53','2018-07-18 09:22:53',5),(40,'DSH',0,12,12,'2018-07-18 09:25:12','2018-07-18 09:26:59',5),(41,'ETH',1,1,1,'2018-07-21 09:04:39','2018-07-21 09:04:39',1);
/*!40000 ALTER TABLE `monedas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticia`
--

DROP TABLE IF EXISTS `noticia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_es` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticia`
--

LOCK TABLES `noticia` WRITE;
/*!40000 ALTER TABLE `noticia` DISABLE KEYS */;
INSERT INTO `noticia` VALUES (1,'Cointelegraph','https://es.cointelegraph.com/rss','https://cointelegraph.com/rss',1,1,1,NULL,NULL),(2,'Bitcoin','https://noticias.bitcoin.com/feed/','https://news.bitcoin.com/feed/',1,1,1,NULL,NULL),(3,'CoinStaker','https://www.coinstaker.com/es/feed/','https://www.coinstaker.com/feed/',1,1,1,NULL,NULL),(4,'Bitcoinmagazine',NULL,'https://bitcoinmagazine.com/feed',1,1,1,NULL,NULL),(5,'Newsbtc.com',NULL,'https://www.newsbtc.com/feed/',1,1,1,NULL,NULL),(6,'Cryptocurrencynews.com',NULL,'https://cryptocurrencynews.com/feed/',1,1,1,NULL,NULL),(7,'CCN.com',NULL,'https://www.ccn.com/feed/',1,1,1,NULL,NULL),(8,'CryptoNoticias.com','https://www.criptonoticias.com/feed/','https://cointelegraph.com/rss',1,1,1,NULL,NULL),(9,'ElBitcoin.org','https://elbitcoin.org/feed/',NULL,1,1,1,NULL,NULL),(10,'SobreBitCoin','http://sobrebitcoin.com/feed/',NULL,1,1,1,NULL,NULL);
/*!40000 ALTER TABLE `noticia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operaciones`
--

DROP TABLE IF EXISTS `operaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operaciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `operacion` tinyint(1) NOT NULL DEFAULT '0',
  `moneda` tinyint(1) NOT NULL DEFAULT '0',
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `moneda_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operaciones_moneda_id_foreign` (`moneda_id`),
  CONSTRAINT `operaciones_moneda_id_foreign` FOREIGN KEY (`moneda_id`) REFERENCES `monedas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operaciones`
--

LOCK TABLES `operaciones` WRITE;
/*!40000 ALTER TABLE `operaciones` DISABLE KEYS */;
INSERT INTO `operaciones` VALUES (1,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-03 21:34:34','2018-06-03 21:34:34',1),(2,10,'2018-06-05',310.00,0,1,1,1,1,'2018-06-05 05:03:42','2018-06-05 05:03:42',1),(4,10,'2018-06-05',315.00,0,1,1,1,1,'2018-06-05 05:04:48','2018-06-05 05:04:48',1),(6,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-05 20:26:38','2018-06-05 20:26:38',3),(7,10000,'2018-06-05',0.76,0,0,1,1,1,'2018-06-05 20:30:46','2018-06-05 20:30:46',3),(8,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-12 20:29:49','2018-06-12 20:29:49',4),(9,10,'2018-06-12',501.29,0,0,1,1,1,'2018-06-12 20:30:57','2018-06-12 20:30:57',4),(10,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-14 11:59:18','2018-06-14 11:59:18',5),(11,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-14 13:38:28','2018-06-14 13:38:28',6),(12,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-14 21:27:50','2018-06-14 21:27:50',7),(13,1000,'2018-06-15',0.17,1,0,1,1,1,'2018-06-14 21:29:03','2018-06-14 21:29:03',7),(14,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-15 08:59:14','2018-06-15 08:59:14',8),(15,1000,'2018-06-15',3.12,0,0,1,1,1,'2018-06-15 09:00:06','2018-06-15 09:00:06',8),(16,300,'2018-06-15',3.25,1,0,1,1,1,'2018-06-15 09:01:09','2018-06-15 09:01:09',8),(17,500,'2018-06-15',3.06,0,0,1,1,1,'2018-06-15 09:02:00','2018-06-15 09:02:00',8),(18,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-15 14:51:26','2018-06-15 14:51:26',9),(19,100,'2018-06-15',39.05,0,0,1,1,1,'2018-06-15 14:52:27','2018-06-15 14:52:27',9),(20,30,'2018-06-15',40.36,1,0,1,1,1,'2018-06-15 14:53:26','2018-06-15 14:53:26',9),(21,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-18 19:57:18','2018-06-18 19:57:18',10),(22,50,'2018-06-18',98.52,0,0,1,1,1,'2018-06-18 19:58:19','2018-06-18 19:58:19',10),(23,20,'2018-06-18',105.25,1,0,1,1,1,'2018-06-18 19:59:11','2018-06-18 19:59:11',10),(24,20,'2018-06-18',105.25,1,0,1,1,1,'2018-06-18 19:59:18','2018-06-18 19:59:18',10),(25,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-18 21:51:20','2018-06-18 21:51:20',11),(26,100,'2018-06-19',10.62,0,0,1,1,1,'2018-06-18 21:52:35','2018-06-18 21:52:35',11),(27,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-18 21:54:00','2018-06-18 21:54:00',12),(28,100,'2018-06-19',10.62,0,0,1,1,1,'2018-06-18 21:54:34','2018-06-18 21:54:34',12),(29,40,'2018-06-19',11.29,1,0,1,1,1,'2018-06-18 21:55:42','2018-06-18 21:55:42',12),(30,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-19 13:23:35','2018-06-19 13:23:35',13),(31,10000,'2018-06-19',0.17,0,0,1,1,1,'2018-06-19 13:24:51','2018-06-19 13:24:51',13),(32,4000,'2018-06-19',0.18,1,0,1,1,1,'2018-06-19 13:25:47','2018-06-19 13:25:47',13),(33,2000,'2018-06-19',0.17,0,0,1,1,1,'2018-06-19 13:26:47','2018-06-19 13:26:47',13),(34,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-19 17:44:57','2018-06-19 17:44:57',14),(35,1000,'2018-06-19',3.22,0,0,1,1,1,'2018-06-19 17:45:59','2018-06-19 17:45:59',14),(36,300,'2018-06-19',3.24,1,0,1,1,1,'2018-06-19 18:00:37','2018-06-19 18:00:37',14),(37,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-20 13:09:28','2018-06-20 13:09:28',15),(38,100000,'2018-06-20',0.05,0,0,1,1,1,'2018-06-20 13:10:35','2018-06-20 13:10:35',15),(39,50000,'2018-06-20',0.05,1,0,1,1,1,'2018-06-20 13:11:48','2018-06-20 13:11:48',15),(40,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-20 17:48:24','2018-06-20 17:48:24',16),(41,10000,'2018-06-20',122.99,0,0,1,1,1,'2018-06-20 17:49:31','2018-06-20 17:49:31',16),(42,9500,'2018-06-20',123.13,1,0,1,1,1,'2018-06-20 17:50:27','2018-06-20 17:50:27',16),(43,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-20 19:47:51','2018-06-20 19:47:51',17),(44,100,'2018-06-20',16.30,0,0,1,1,1,'2018-06-20 19:48:33','2018-06-20 19:48:33',17),(45,50,'2018-06-20',16.98,1,0,1,1,1,'2018-06-20 19:49:44','2018-06-20 19:49:44',17),(46,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-20 21:18:17','2018-06-20 21:18:17',18),(47,1000,'2018-06-21',0.19,0,0,1,1,1,'2018-06-20 21:19:16','2018-06-20 21:19:16',18),(48,500,'2018-06-21',0.20,1,0,1,1,1,'2018-06-20 21:20:33','2018-06-20 21:20:33',18),(49,300,'2018-06-21',0.18,0,0,1,1,1,'2018-06-20 21:21:19','2018-06-20 21:21:19',18),(50,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-25 18:59:24','2018-06-25 18:59:24',19),(51,5000,'2018-06-25',0.44,0,0,1,1,1,'2018-06-25 19:00:30','2018-06-25 19:00:30',19),(52,3000,'2018-06-25',0.45,1,0,1,1,1,'2018-06-25 19:01:27','2018-06-25 19:01:27',19),(53,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-26 17:39:36','2018-06-26 17:39:36',20),(54,3000,'2018-06-26',2.50,0,0,1,1,1,'2018-06-26 17:40:40','2018-06-26 17:40:40',20),(55,1400,'2018-06-26',2.51,1,0,1,1,1,'2018-06-26 17:41:27','2018-06-26 17:41:27',20),(56,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-29 07:19:14','2018-06-29 07:19:14',21),(57,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-29 09:53:59','2018-06-29 09:53:59',22),(58,0,'2018-04-21',0.00,0,0,1,1,1,'2018-06-29 14:45:08','2018-06-29 14:45:08',23),(59,2000,'2018-06-29',0.00,0,0,1,1,1,'2018-06-29 14:45:51','2018-06-29 14:45:51',23),(60,0,'2018-04-21',0.00,0,0,1,1,1,'2018-07-02 12:04:26','2018-07-02 12:04:26',24),(61,0,'2018-04-21',0.00,0,0,1,1,1,'2018-07-02 13:05:50','2018-07-02 13:05:50',25),(62,10000,'2018-07-02',0.12,0,0,1,1,1,'2018-07-02 13:06:48','2018-07-02 13:06:48',25),(63,8000,'2018-07-02',0.12,1,0,1,1,1,'2018-07-02 13:07:46','2018-07-02 13:07:46',25),(64,0,'2018-04-21',0.00,0,0,1,1,1,'2018-07-02 19:06:42','2018-07-02 19:06:42',26),(65,280000,'2018-07-02',0.00,0,0,1,1,1,'2018-07-02 19:08:11','2018-07-02 19:08:11',26),(66,280000,'2018-07-02',0.00,0,0,1,1,1,'2018-07-02 19:08:52','2018-07-02 19:08:52',26),(67,0,'2018-04-21',0.00,0,0,1,1,1,'2018-07-03 11:31:38','2018-07-03 11:31:38',27),(68,1000,'2018-07-04',0.49,0,0,1,1,1,'2018-07-03 11:32:30','2018-07-03 11:32:30',27),(69,500,'2018-07-04',0.50,1,0,1,1,1,'2018-07-03 11:33:47','2018-07-03 11:33:47',27),(70,0,'2018-04-21',0.00,0,0,1,1,1,'2018-07-03 21:29:12','2018-07-03 21:29:12',28),(71,10,'2018-07-04',84.47,0,0,1,1,1,'2018-07-03 21:30:05','2018-07-03 21:30:05',28),(72,5,'2018-07-05',84.46,1,0,1,1,1,'2018-07-03 21:30:59','2018-07-03 21:30:59',28),(73,0,'2018-04-21',0.00,0,0,1,1,1,'2018-07-04 17:46:21','2018-07-04 17:46:21',29),(74,20,'2018-07-04',766.09,0,0,1,1,1,'2018-07-04 17:47:20','2018-07-04 17:47:20',29),(75,10,'2018-07-04',779.63,1,0,1,1,1,'2018-07-04 17:48:03','2018-07-04 17:48:03',29),(76,0,'2018-04-21',0.00,0,0,1,1,1,'2018-07-05 10:11:18','2018-07-05 10:11:18',30),(77,0,'2018-04-21',0.00,0,0,1,1,1,'2018-07-07 17:47:06','2018-07-07 17:47:06',31),(78,1000,'2018-07-07',0.49,0,0,1,1,1,'2018-07-07 17:47:35','2018-07-07 17:47:35',31),(79,500,'2018-07-07',0.54,1,0,1,1,1,'2018-07-07 17:48:18','2018-07-07 17:48:18',31),(80,0,'2018-04-21',0.00,0,0,1,13,13,'2018-07-09 20:26:06','2018-07-09 20:26:06',32),(81,0,'2018-04-21',0.00,0,0,1,13,13,'2018-07-11 20:13:13','2018-07-11 20:13:13',33),(82,10,'2018-07-11',441.28,0,0,1,13,13,'2018-07-11 20:14:58','2018-07-11 20:14:58',33),(83,4,'2018-07-11',443.84,1,0,1,13,13,'2018-07-11 20:16:13','2018-07-11 20:16:13',33),(86,0,'2018-04-21',0.00,0,0,1,13,13,'2018-07-12 10:24:53','2018-07-12 10:24:53',35),(87,0,'2018-04-21',0.00,0,0,1,13,13,'2018-07-12 15:39:25','2018-07-12 15:39:25',36),(88,100,'2018-07-12',76.48,0,0,1,13,13,'2018-07-12 15:40:28','2018-07-12 15:40:28',36),(89,50,'2018-07-12',77.10,1,0,1,13,13,'2018-07-12 15:41:23','2018-07-12 15:41:23',36),(90,0,'2018-04-21',0.00,0,0,1,13,13,'2018-07-12 18:59:38','2018-07-12 18:59:38',37),(91,10000,'2018-07-12',0.19,0,0,1,13,13,'2018-07-12 19:00:11','2018-07-12 19:00:11',37),(92,5000,'2018-07-12',0.20,1,0,1,13,13,'2018-07-12 19:01:30','2018-07-12 19:01:30',37),(93,0,'2018-04-21',0.00,0,0,1,13,13,'2018-07-13 15:50:40','2018-07-13 15:50:40',38),(94,1000,'2018-07-13',33.17,0,0,1,13,13,'2018-07-13 15:51:40','2018-07-13 15:51:40',38),(95,800,'2018-07-13',33.24,1,0,1,13,13,'2018-07-13 15:52:37','2018-07-13 15:52:37',38),(96,0,'2018-04-21',0.00,0,0,1,12,12,'2018-07-18 09:22:54','2018-07-18 09:22:54',39),(98,0,'2018-04-21',0.00,0,0,1,1,1,'2018-07-21 09:04:39','2018-07-21 09:04:39',41),(99,10,'2018-07-21',467.82,0,0,1,1,1,'2018-07-21 09:06:00','2018-07-21 09:06:00',41),(100,6,'2018-07-21',475.16,1,0,1,1,1,'2018-07-21 09:07:33','2018-07-21 09:07:33',41);
/*!40000 ALTER TABLE `operaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagos`
--

DROP TABLE IF EXISTS `pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `membresia_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  `estado` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_membresia` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_pago` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_activo_hasta` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chargeId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pagos_membresia_id_foreign` (`membresia_id`),
  KEY `pagos_user_id_foreign` (`user_id`),
  CONSTRAINT `pagos_membresia_id_foreign` FOREIGN KEY (`membresia_id`) REFERENCES `membresia` (`id`),
  CONSTRAINT `pagos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagos`
--

LOCK TABLES `pagos` WRITE;
/*!40000 ALTER TABLE `pagos` DISABLE KEYS */;
INSERT INTO `pagos` VALUES (1,8,18,0.00,1,'2018-07-21 14:41:24','2018-07-21 14:41:24','Aprobado por el admin - 50% Financiado',NULL,NULL,NULL,NULL),(2,7,33,0.00,1,'2018-07-21 14:47:13','2018-07-21 14:47:13','Asesor',NULL,NULL,NULL,NULL),(3,5,36,10.00,1,'2018-07-21 14:54:26','2018-07-21 14:54:26','Aprobado por Admin',NULL,NULL,NULL,NULL),(4,8,12,0.00,1,'2018-07-21 15:04:16','2018-07-21 15:04:16','Aprobado por ADMIN',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('sanchez.ssg67@gmail.com','$2y$10$Dz298eWkcUgqMJ0FYwa8LOGlregCcf7QwQawILRlCOUPY6Rx8ZwGK','2018-07-17 10:36:54'),('fa.mejia@hotmail.com','$2y$10$9lty9j2ioeKN7jaCAUB05e7oAUWapdrCH25q4xzPMD/YmvFp7gzwy','2018-07-21 09:03:59');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portafolio`
--

DROP TABLE IF EXISTS `portafolio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portafolio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portafolio`
--

LOCK TABLES `portafolio` WRITE;
/*!40000 ALTER TABLE `portafolio` DISABLE KEYS */;
INSERT INTO `portafolio` VALUES (1,'JUAN RAMON',1,1,1,'2018-06-01 18:55:04','2018-06-01 18:55:04'),(2,'Elias Bernier',0,1,1,'2018-06-12 20:29:36','2018-06-28 11:11:26'),(3,'Esteban',1,1,1,'2018-07-09 20:25:46','2018-07-09 20:25:46'),(4,'JUAN RAMON',1,1,1,'2018-07-15 12:47:32','2018-07-15 12:47:32'),(5,'alicia',1,12,12,'2018-07-18 09:22:30','2018-07-18 09:22:30'),(6,'FS',1,15,15,'2018-07-18 11:32:03','2018-07-18 11:32:03');
/*!40000 ALTER TABLE `portafolio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `signals`
--

DROP TABLE IF EXISTS `signals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `membresia_id` int(10) unsigned NOT NULL,
  `coinname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coinsymbol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coinimgurl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exchange` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cointradeurl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coinprice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stoploss` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pricet30m` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet1h` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet3h` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet6h` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet12h` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet24h` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet48h` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet30mpercent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet1hpercent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet3hpercent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet6hpercent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet12hpercent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet24hpercent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet48hpercent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priceAverage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priceAveragePercent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rangeprice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `signals_membresia_id_foreign` (`membresia_id`),
  CONSTRAINT `signals_membresia_id_foreign` FOREIGN KEY (`membresia_id`) REFERENCES `membresia` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `signals`
--

LOCK TABLES `signals` WRITE;
/*!40000 ALTER TABLE `signals` DISABLE KEYS */;
INSERT INTO `signals` VALUES (24,1,'VeriCoin','VRC','https://www.cryptocompare.com/media/20068/vrc.png','Bittrex','https://bittrex.com/','0.000042','0.000050','0.000059','0.000069','0.000039','2018-06-30 10:25:55','2018-07-02 11:04:32','0.00004678','0.00004678','0.00004594','0.00004562','0.00004601','0.00004644','0.00004495','B','11.38','11.38','9.38','8.62','9.55','10.57','7.02','0.00004678','11.38',NULL),(25,1,'AdToken','ADT','https://www.cryptocompare.com/media/1383829/adt.png','Bittrex','https://bittrex.com/','0.00000200','0.00000400','0.00000600','0.00000800','0.00000185','2018-06-30 10:29:25','2018-07-02 11:04:31','0.00000204','0.00000204','0.00000202','0.00000211','0.00000198','0.00000204','0.00000210','B','2.00','2.00','1.00','5.50','-1.00','2.00','5.00','0.00000211','5.50',NULL),(26,1,'Neblio','NEBL','https://www.cryptocompare.com/media/1384016/nebl.png','Binance','https://binance.com/','0.0005800','0.0006370','0.0006650','0.0006800','0.0005200','2018-06-30 10:36:45','2018-07-02 11:04:31','0.00058110','0.00057570','0.00058900','0.00059970','0.00058930','0.00059620','0.00064950','B','0.19','-0.74','1.55','3.40','1.60','2.79','11.98','0.00064950','11.98',NULL),(27,1,'Binance Coin','BNB','https://www.cryptocompare.com/media/1383947/bnb.png','Binance','https://binance.com/','0.002140','0.002354','0.002461','0.002568','0.001930','2018-07-03 06:18:18','2018-07-05 09:11:21','0.00217100','0.00217000','0.00215800','0.00214700','0.00214600','0.00209800','0.00207800','B','1.45','1.40','0.84','0.33','0.28','-1.96','-2.90','0.00217100','1.45',NULL),(28,1,'Salt Lending','SALT','https://www.cryptocompare.com/media/9350744/salt.jpg','Bittrex','https://bittrex.com/','0.0001900','0.00022','0.00031','0.00047','0.00014','2018-07-03 06:23:49','2018-07-05 09:11:21','0.00019680','0.00019730','0.00019710','0.00019530','0.00018650','0.00019200','0.00019260','B','3.58','3.84','3.74','2.79','-1.84','1.05','1.37','0.00019730','3.84',NULL),(29,1,'Kore','KORE','https://www.cryptocompare.com/media/14543972/kore.png','Bittrex','https://bittrex.com/','0.00025','0.00030','0.00040','0.00050','0.00022','2018-07-03 06:29:27','2018-07-05 09:11:20','0.00025000','0.00025370','0.00025020','0.00025560','0.00024510','0.00024000','0.00025520','B','0.00','1.48','0.08','2.24','-1.96','-4.00','2.08','0.00025560','2.24',NULL),(30,1,'Pundi X','NPXS','https://www.cryptocompare.com/media/27010505/pxs.png','Binance','https://binance.com/','0.00000063','0.00000072','0.0000080','0.00000100','0.00000054','2018-07-04 07:51:14','2018-07-07 13:28:17','0.00000065','0.00000065','0.00000063','0.00000064','0.00000064','0.00000060','0.00000062','B','3.17','3.17','0.00','1.59','1.59','-4.76','-1.59','0.00000065','3.17',NULL),(31,1,'Verge','XVG','https://www.cryptocompare.com/media/12318032/xvg.png','Binance','www.binance.com','0.00000358','0.00000374','0.00000386','0.00000402','0.00000340','2018-07-05 12:15:36','2018-07-07 13:28:17','0.00000373','0.00000374','0.00000371','0.00000376','--','0.00000374','0.00000379','B','4.19','4.47','3.63','5.03','--','4.47','5.87','0.00000379','5.87',NULL),(32,1,'Tronix','TRX','https://www.cryptocompare.com/media/12318089/trx.png','binance','www.binance.com','0.00000511','0.00000550','0.00000600','0.00000650','0.00000471','2018-07-05 17:14:06','2018-07-07 17:41:21','0.00000572','0.00000569','0.00000568','0.00000564','0.00000554','0.00000553','0.00000556','B','11.94','11.35','11.15','10.37','8.41','8.22','8.81','0.00000572','11.94',NULL),(33,1,'Ethereum Classic','ETC','https://www.cryptocompare.com/media/33752295/etc_new.png','Binance','www.binance.com','0.002660','0.002718','0.002750','0.002800','0.002500','2018-07-05 17:25:26','2018-07-07 17:41:20','0.00272100','0.00270300','0.00269900','0.00264200','0.00272300','0.00276900','0.00277000','B','2.29','1.62','1.47','-0.68','2.37','4.10','4.14','0.00277000','4.14',NULL),(34,1,'QuarkChain','QKC','https://www.cryptocompare.com/media/33434307/qkc.jpg','Binance','www.binance.com','0.00001150','0.00001200','0.00001280','0.00001380','0.00001085','2018-07-05 17:55:12','2018-07-08 19:44:26','0.00001155','0.00001179','0.00001227','--','0.00001248','0.00001156','0.00001184','B','0.43','2.52','6.70','--','8.52','0.52','2.96','0.00001248','8.52',NULL),(35,1,'Vechain','VEN','https://www.cryptocompare.com/media/12318129/ven.png','Binance','https://binance.com/','0.0003750','0.0004125','0.0004400','0.0004900','0.0003375','2018-07-06 07:50:12','2018-07-08 19:44:25','0.00037350','0.00037260','0.00038200','0.00038080','0.00037020','0.00037470','0.00038300','B','-0.40','-0.64','1.87','1.55','-1.28','-0.08','2.13','0.00038300','2.13',NULL),(36,1,'SelfKey','KEY','https://www.cryptocompare.com/media/14761912/key.png','Binance','https://binance.com/','0.00000287','0.00000315','0.00000360','0.00000420','0.00000255','2018-07-06 07:52:30','2018-07-08 19:44:25','0.00000286','0.00000290','0.00000297','0.00000316','0.00000307','0.00000322','0.00000307','B','-0.35','1.05','3.48','10.10','6.97','12.20','6.97','0.00000322','12.20',NULL),(37,1,'Stratis','STRAT','https://www.cryptocompare.com/media/351303/stratis-logo.png','Binance','https://binance.com/','0.0003850','0.0004235','0.0004620','0.0004980','0.0003300','2018-07-06 08:29:21','2018-07-08 19:44:25','0.00040430','0.00039920','0.00040160','0.00039810','0.00039500','0.00038920','0.00039120','B','5.01','3.69','4.31','3.40','2.60','1.09','1.61','0.00040430','5.01',NULL),(38,1,'Stellar','XLM','https://www.cryptocompare.com/media/20696/str.png','Binance','www.binance.com','0.00003044','0.00003160','0.00003200','0.00003300','0.00002905','2018-07-06 09:46:55','2018-07-08 19:44:25','0.00003148','0.00003168','0.00003126','0.00003129','0.00003155','0.00003097','0.00003107','B','3.42','4.07','2.69','2.79','3.65','1.74','2.07','0.00003168','4.07',NULL),(39,1,'Decentraland','MANA','https://www.cryptocompare.com/media/1383903/mana.png','Binance','www.binance.com','0.00001500','0.00001620','0.00001700','0.00001900','0.00001405','2018-07-06 09:59:15','2018-07-08 19:44:02','0.00001543','0.00001523','0.00001526','0.00001526','0.00001486','0.00001498','0.00001522','B','2.87','1.53','1.73','1.73','-0.93','-0.13','1.47','0.00001543','2.87',NULL),(40,1,'ZClassic','ZCL','https://www.cryptocompare.com/media/351926/zcl.png','Bittrex','www.bittrex.com','0.00121605','0.00129500','0.00136605','0.00161605','0.00113669','2018-07-08 09:14:26','2018-07-10 19:33:55','0.00126500','0.00126900','0.00127500','0.00126700','0.00126200','0.00132900','0.00124800','B','4.03','4.35','4.85','4.19','3.78','9.29','2.63','0.00132900','9.29',NULL),(41,1,'AppCoins','APPC','https://www.cryptocompare.com/media/12318370/app.png','Binance','www.binance.com','0.00003109','0.00003300','0.00003450','0.00004000','0.002850','2018-07-08 09:18:04','2018-07-10 19:33:54','0.00003280','0.00003314','0.00003289','0.00003210','0.00003244','0.00003709','0.00003203','B','5.50','6.59','5.79','3.25','4.34','19.30','3.02','0.00003709','19.30',NULL),(42,1,'0x','ZRX','https://www.cryptocompare.com/media/1383799/zrx.png','Binance','www.binance.com','0.00014620','0.00015310','0.00016000','0.00018500','0.00014263','2018-07-08 09:20:04','2018-07-10 19:07:00','0.00014720','0.00014660','0.00014690','0.00014430','0.00014430','0.00014290','0.00013210','B','0.68','0.27','0.48','-1.30','-1.30','-2.26','-9.64','0.00014720','0.68',NULL),(44,1,'UpToken','UP','https://www.cryptocompare.com/media/12318374/up.png','Bittrex','www.bittrex.com','0.00000730','0.00000780','0.00000830','0.00000950','0.00000670','2018-07-08 20:39:32','2018-07-11 16:51:58','0.00000723','0.00000723','0.00000735','0.00000724','0.00000733','0.00000715','0.00000687','B','-0.96','-0.96','0.68','-0.82','0.41','-2.05','-5.89','0.00000735','0.68',NULL),(45,1,'EOS','EOS','https://www.cryptocompare.com/media/1383652/eos_1.png','Binance','www.binance.com','0.0012221','0.0013221','0.0014321','0.00125221','0.0011475','2018-07-09 14:14:16','2018-07-11 16:51:58','0.00122100','0.00122100','0.00115600','0.00114800','0.00115300','0.00115900','--','B','-0.09','-0.09','-5.41','-6.06','-5.65','-5.16','--','0.00122100','-0.09',NULL),(46,1,'Siacoin','SC','https://www.cryptocompare.com/media/20726/siacon-logo.png','Binance','https://binance.com/','0.00000165','0.00000182','0.00000198','0.00000237','0.00000147','2018-07-10 07:00:48','2018-07-12 08:41:19','0.00000164','0.00000165','0.00000169','0.00000171','0.00000168','0.00000169','0.00000163','B','-0.61','0.00','2.42','3.64','1.82','2.42','-1.21','0.00000171','3.64',NULL),(47,1,'Verge','XVG','https://www.cryptocompare.com/media/12318032/xvg.png','Binance','www.binance.com','0.00000345','0.00000380','0.00000420','0.00000500','0.00000328','2018-07-10 10:52:46','2018-07-12 15:34:29','0.00000351','0.00000350','0.00000351','0.00000343','0.00000339','0.00000348','0.00000344','B','1.74','1.45','1.74','-0.58','-1.74','0.87','-0.29','0.00000351','1.74',NULL),(48,1,'QTUM','QTUM','https://www.cryptocompare.com/media/1383382/qtum.png','Bittrex','www.bittrex.com','0.00121047','0.00131000','0.00135047','0.00139471','0.00118057','2018-07-11 17:34:21','2018-07-14 08:53:31','0.00122900','0.00123100','0.00121300','0.00121200','0.00120400','0.00121800','0.00123000','B','1.53','1.70','0.21','0.13','-0.53','0.62','1.61','0.00123100','1.70',NULL),(49,1,'Tezos','XTZ','https://www.cryptocompare.com/media/1383651/xbt.png','gate.io','gate.io','0.00034541','0.00034741','0.00034941','0.00035541','0.00033541','2018-07-16 11:33:28','2018-07-18 12:54:16','0.00036490','0.00034700','0.00035650','0.00035270','0.00035140','0.00032680','0.00032110','B','5.64','0.46','3.21','2.11','1.73','-5.39','-7.04','0.00036490','5.64',NULL),(50,1,'NEM','XEM','https://www.cryptocompare.com/media/20490/xem.png','Binance','www.binance.com','0.00002538','0.00002588','0.00002648','0.00002798','0.00002436','2018-07-16 22:49:24','2018-07-19 12:59:40','0.00002610','0.00002606','0.00002607','0.00002591','0.00002610','0.00002580','0.00002553','B','2.84','2.68','2.72','2.09','2.84','1.65','0.59','0.00002610','2.84',NULL),(51,1,'Tronix','TRX','https://www.cryptocompare.com/media/12318089/trx.png','Binance','www.binance.com','0.00000509','0.00000529','0.00000565','0.00000655','0.00000485','2018-07-16 22:50:56','2018-07-18 23:35:51','0.00000542','0.00000545','0.00000547','0.00000548','0.00000543','0.00000543','0.00000523','B','6.48','7.07','7.47','7.66','6.68','6.68','2.75','0.00000548','7.66',NULL),(52,1,'Aeron','ARN','https://www.cryptocompare.com/media/12318261/arn.png','Binance','www.binance.com','0.00009430','0.00009490','0.00009550','0.00009670','0.00009320','2018-07-17 08:33:20','2018-07-19 09:47:02','0.00009441','0.00009450','0.00009321','0.00009499','0.00009762','0.00010160','0.00009316','B','0.12','0.21','-1.16','0.73','3.52','7.74','-1.21','0.00010160','7.74',NULL),(53,1,'Verge','XVG','https://www.cryptocompare.com/media/12318032/xvg.png','Bittrex','www.bittrex.com','0.00000355','0.00000365','0.00000375','0.00000425','0.00000310','2018-07-17 14:08:50','2018-07-19 14:31:58','0.00000360','0.00000358','0.00000358','0.00000364','0.00000359','0.00000356','0.00000342','B','1.41','0.85','0.85','2.54','1.13','0.28','-3.66','0.00000364','2.54',NULL),(54,1,'Po.et','POE','https://www.cryptocompare.com/media/1383828/poe.png','Binance','www.binance.com','0.00000263','0.00000283','0.00000313','0.00000369','0.00000223','2018-07-17 14:16:22','2018-07-19 14:31:58','0.00000266','0.00000270','0.00000266','0.00000267','0.00000267','0.00000269','0.00000253','B','1.14','2.66','1.14','1.52','1.52','2.28','-3.80','0.00000270','2.66',NULL),(55,1,'KeyCoin','KEY*','https://www.cryptocompare.com/media/20331/key.png','Binance','www.binance.com','0.00000185','0.00000190','0.00000198','0.00000210','0.00000178','2018-07-17 23:01:58','2018-07-19 23:38:34','0.00000210','0.00000210','0.00000210','0.00000209','0.00000206','0.00000202','0.00000193','B','13.51','13.51','13.51','12.97','11.35','9.19','4.32','0.00000210','13.51',NULL),(56,1,'Verge','XVG','https://www.cryptocompare.com/media/12318032/xvg.png','Binance','www.binance.com','0.00000320','0.00000330','0.00000350','0.00000370','0.00000310','2018-07-20 04:27:34','2018-07-22 09:49:21','0.00000318','0.00000318','0.00000314','0.00000306','0.00000311','0.00000316','0.00000313','B','-0.62','-0.62','-1.88','-4.38','-2.81','-1.25','-2.19','0.00000318','-0.62',NULL),(57,1,'Ardor','ARDR','https://www.cryptocompare.com/media/351736/ardr.png','Binance','www.binance.com','0.00002708','0.00002758','0.00002800','0.00002968','0.00002638','2018-07-20 04:33:02','2018-07-22 09:49:20','0.00002714','0.00002708','0.00002624','0.00002605','0.00002552','0.00002499','0.00002701','B','0.22','0.00','-3.10','-3.80','-5.76','-7.72','-0.26','0.00002714','0.22',NULL),(58,1,'SingularityNET','AGI','https://www.cryptocompare.com/media/25792653/agi.png','Binance','www.binance.com','0.00001300','0.00001370','0.00001400','0.00001520','0.00001220','2018-07-20 04:36:33','2018-07-22 09:49:19','0.00001322','0.00001320','0.00001305','0.00001270','0.00001211','0.00001206','0.00001183','B','1.69','1.54','0.38','-2.31','-6.85','-7.23','-9.00','0.00001322','1.69',NULL),(59,1,'Monetha','MTH','https://www.cryptocompare.com/media/1383976/mth.png','Binance','www.binance.com','0.00000500','0.00000530','0.00000570','0.00000620','0.00000485','2018-07-20 04:45:52','2018-07-22 09:49:18','0.00000525','0.00000537','0.00000521','0.00000525','0.00000518','0.00000521','0.00000523','B','5.00','7.40','4.20','5.00','3.60','4.20','4.60','0.00000537','7.40',NULL);
/*!40000 ALTER TABLE `signals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `membresia_id` int(10) unsigned NOT NULL,
  `nickname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cellphone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wallet` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `id_referidor` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  `nivel` int(11) NOT NULL DEFAULT '1',
  `cnivel1` int(11) NOT NULL DEFAULT '0',
  `cnivel2` int(11) NOT NULL DEFAULT '0',
  `cnivel3` int(11) NOT NULL DEFAULT '0',
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `facebook_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `github_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_nickname_unique` (`nickname`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_membresia_id_foreign` (`membresia_id`),
  CONSTRAINT `users_membresia_id_foreign` FOREIGN KEY (`membresia_id`) REFERENCES `membresia` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,4,'creex','CEO','MASTER','ceo@grupocreex.com','996005788','$2y$10$8lXQp3eE3fosp/y5kesWduJE2lF84PEcw8B4OMCXEz0KyqYtllMNS','',NULL,1,1,0,0,0,'user',NULL,NULL,NULL,NULL,NULL,NULL),(2,4,'ADMINISTRADOR','ADMIN','Grupo Creex','administracion@grupocreex.com','996005788','$2y$10$JZ5Z/37lPBhgtUbCPQAqc.71HmykVJWINh4nRUilT9yt9cmfMoD8i','',1,1,1,0,0,0,'invitado','8qSbHGSpXiLFiTSh2nKSsL6bX6AyBSfE0ZHokpw7J9misRZ7evedV9SOdPQ7','2018-07-21 07:18:52','2018-07-21 07:18:52',NULL,NULL,NULL),(3,4,'soporte','Soporte','Grupo Creex','soporte@grupocreex.com','996005788','$2y$10$NtZ5zAnlbp.9.eD/EYgtseRl.HCR1xMkkyxcbrJtVGV9Krmi7fY.6','',1,1,1,0,0,0,'admin','r7BQF9uZfG1JvYYRuuqdQvrAA23j5aXCakx5vODVNYKRzgFImVFJ02SFYpq6','2018-07-21 07:20:35','2018-07-21 07:20:35',NULL,NULL,NULL),(4,4,'developer','Developer','Grupo Creex','programacion@grupocreex.com','996005788','$2y$10$.mTlFt4XBeT6GsxfXt39oudt7tgQilrZwvoC.n2a2nV0tIX9ruITq','',1,1,1,0,0,0,'invitado','MrBL9iN4dDWQGZwsvnuE8twwHaYhZg4Z7JON82qVMW0KzrjEL8IZDI2lM818','2018-07-21 07:22:23','2018-07-21 07:22:23',NULL,NULL,NULL),(5,4,'gestion','Gestion','Grupo Creex','gestion@grupocreex.com','996005788','$2y$10$072/iZ93Z7i3QtEKjG.sgeoFYOYt78tApHQH07QdXlhRupTO.Ygxm','',1,1,1,0,0,0,'invitado','tH13c0I4JOUi0iZbai4clRgPamt82TgqR7ekFHNmGw4YYJimHcQ5dtJqYCf9','2018-07-21 07:23:51','2018-07-21 07:23:51',NULL,NULL,NULL),(6,3,'denis12018','Denis','Gutierrez','denisesteban12@gmail.com','0050432091884','$2y$10$U7yv5RfICLyduZFowE2FUOkJMtW5V.pyItQX1CjrMAsw.1F2bP4KG','',1,1,1,0,0,0,'user','Nk2fSHl8SJibmk7d9SJLyOjbWKnvWYCRYqAdYbk3A1a7cU4TrT8nERA2SIOQ','2018-07-21 07:30:45','2018-07-21 07:30:45',NULL,NULL,NULL),(7,1,'Hdhd','Steven','Sánchez González','sanchez.ssg67@gmail.com','60463141','$2y$10$4mLnNFWNhh5u7bhg2y90h.kG51bu5LuRvcUm2vh/FERcGRfUxOJM.','',6,1,1,0,0,0,'user','UWQtyvy92tlct7fsyxLoVPj1gvCSX2dJc67dj8fsejlwXttdcVYbCe8XUwih','2018-07-21 07:32:06','2018-07-21 07:32:06',NULL,NULL,NULL),(8,1,'edgardof18','Edgardo Florentino','Barahona Mejia','edgarbarahona_28@hotmail.com','50494779703','$2y$10$U9oum2JNqPhxyafO6KhZSO62cmMVPm8ps0fus9KlOTsMr2TEQ8JVm','',6,1,1,0,0,0,'user','YeTklZ8XhbubyoacDoLspDehAwiOY1Gxn6vQpWOWm2uVP8U1m1m9ArPvJTqJ','2018-07-21 07:33:10','2018-07-21 07:33:10',NULL,NULL,NULL),(9,1,'Ivan','Ivan Antonio','Gutierrez castellon','ivangutierrezcas1@gmail.com','0050499790547','$2y$10$UU2V5JP4gUEtrApFWTcURei5MY3Es5B6I8SqUzd3b6k9g1YAsinAe','',6,1,1,0,0,0,'user','BX9pBLQNBwhRsaPcWnmkxuyWVtfhwmZVuHXwkl7QZUUhhTDvFAZstXzQnxpu','2018-07-21 07:34:45','2018-07-21 07:34:45',NULL,NULL,NULL),(10,4,'mgrafael','Rafael','Roy','Mografael@gmail.com','573183402158','$2y$10$1g0kBYoSBy4nqJNjEze8Rekoj5xgL1S7dhUvT8WfpemHZdag4.E32','',1,1,1,0,0,0,'user','mqCHa3jogFpF2AONaVteVqufpbXf8VDfNT1ENg6qKKY27tgjPTT1SfSHoQI9','2018-07-21 07:36:52','2018-07-21 07:36:52',NULL,NULL,NULL),(11,4,'CJARA31194','Carkis','Muñoz','cmunozjara82@gmail.com','+573014239728','$2y$10$QWGzpcjoEf3ii9P/RZUTxOVtgpMC4xXFg/XNXTs7HD0hVfT9qzMWq','',1,1,1,0,0,0,'user','TX9iUeozhqCKi6K1fqlbFe3ugFnUV4Rn5tOWUDpM5lWYA2YJi98XKahsd8Q7','2018-07-21 07:38:11','2018-07-21 07:38:11',NULL,NULL,NULL),(12,1,'legendary','legendary','Legndaryevol','Juverronny1998@gmail.com','993490009','$2y$10$yY2HUM.gj4PfHHy/QoZa..BLsrZT4QQGxA1r05Ao7/hmEU1Q0nA92','',1,1,1,0,0,0,'user','OZ2C1jAo3prK2W6ITuDF5KGp2VTKGuuex94Hu41vEXjtHBj7mJvhMTKJqWgF','2018-07-21 07:39:31','2018-07-21 15:04:16',NULL,NULL,NULL),(13,2,'jaimevillavalle','JAIME','VILLANUEVA','jaimealbertovillavalle@gmail.com','50683936449','$2y$10$0Ah1rH4kLwVFJ2St4EzmxuqfBNRxTneAZWBcn3Ehzf2XF.ge7mEHC','',6,1,1,0,0,0,'user','dh1bN38OPPlXZcBUlwvrOqA8M2ZUzdXzr2Dv4TO82PEUAEi6fdI1GsydxaVY','2018-07-21 07:41:20','2018-07-21 07:41:20',NULL,NULL,NULL),(14,1,'building4success2','Elideth','Abreu','drelideabreu@gmail.com','8013811928','$2y$10$EXehIzoGeQRS6VG0t2f3qemQNumki5QeHlBIJogTALZNCx1xE9c9W','',1,1,1,0,0,0,'invitado','m9viR5RdjsnUraKpUrAjHrRqv9P9KFRvaevnvFRPtXX6gHx4NXvvBR1WjfnL','2018-07-21 07:42:46','2018-07-21 07:42:46',NULL,NULL,NULL),(15,1,'isaac','Francisco','Suero','Francisco@fsc.com.do','8096399999','$2y$10$OcRMzJvel0TXLC19tVZxYOYU/ffRgnbVVCQBH8e9aNGQOByJ/GwHO','',1,1,1,0,0,0,'user','sY3ZVXCsNfsMR7WxgNJbcl44COBBCZ6SIZ0g6XtDL753xEBIyLG2vtMzuNlG','2018-07-21 07:43:56','2018-07-21 07:43:56',NULL,NULL,NULL),(16,1,'taquesbrasil','Maikon','Taques','taquesbrasil@gmail.com','551998166-4002','$2y$10$8KVxlzU67ET06hLK7Y1.2O5JL1Hp8eldkvI5G1pPS9L4fNjvZ81Bi','',1,1,1,0,0,0,'user','lWYX80SwFX9wssGadJUso0qAUZD8uyN452DfLGGRJVKWNEV69oAvNGPGBDTr','2018-07-21 07:44:57','2018-07-21 07:44:57',NULL,NULL,NULL),(17,1,'Mesparza','Marco','Esparza','marco.esparza.montejo@gmail.com','956381996','$2y$10$6CeG8ootcSlPv3nwWaMe0OrmxGb49flsoBJCiy7e58XD.EvJu2C8O','',1,1,1,0,0,0,'user','FOuZKnfwYpaS1dOZDqdDPOgyEmM0fVwF2ZSkD5Zl94f9JzZmrKhU5WlAUDg3','2018-07-21 07:50:27','2018-07-21 07:50:27',NULL,NULL,NULL),(18,1,'Diana','Liz','Cely','dicelybooking@gmail.com','+573219120018','$2y$10$6CeG8ootcSlPv3nwWaMe0OrmxGb49flsoBJCiy7e58XD.EvJu2C8O','',1,1,1,0,0,0,'user','r0BtC5fzXQddDaKwsKFB6W06RFVqyW8NIUEOF262Gj5ReefXDujsG2wOwcNL','2018-07-21 07:53:51','2018-07-21 14:41:24',NULL,NULL,NULL),(19,1,'edgardocreex12018','Edgardo','Barahona','barahonaedgar0@gmail.com','50494779703','$2y$10$Nzb9fCi4kaHzuLOz7tZIuuqfryhEJcfUbtQVNnMLl3xRyAIUd0aQm','',6,1,1,0,0,0,'user','I03T8b2TDxk5kY7KPK6tBbzySG8JNBdRMsrFQoAZBfrGtJ74xQ05O4QBIy18','2018-07-21 07:55:57','2018-07-21 07:55:57',NULL,NULL,NULL),(20,1,'eochoab','Eduardo','Ochoa','eduardo.ochoa07@gmail.com','+5939961315666','$2y$10$kOn2vFXVydJCxBJwL1mRMOSg1hqQ42Ve2AP5sX4uR7P0syHX6PCO2','',1,1,1,0,0,0,'user','xrPdfrYbMRA52dMsPf02I0o4SJwJYbQwtDBeNVTK1R418NGrtx5F4RWX8D80','2018-07-21 07:57:08','2018-07-21 07:57:08',NULL,NULL,NULL),(21,2,'willwong','Wilgen','Wong','wilgen04@gmail.com','32031116','$2y$10$xAheYKRoNm7f3yOOCW8PH.wlo54chIzkGY6nrzdGryxHF0dqRGFFe','',6,1,1,0,0,0,'user','52wukl8cWUKeLgubYnWSnmJQtj25zE9Ed8acYM7IeUt7QnojuzDccD9R9sdp','2018-07-21 07:58:43','2018-07-21 07:58:43',NULL,NULL,NULL),(22,1,'rene12018','Irvin','Wong Chavez','irvinwong2013@gmail.com','95725965','$2y$10$uwuaB9gMyvh1uVkYf9x/4uIZrCC3SR/m9ncudjYvNZKRqjLfKkKwa','',21,1,1,0,0,0,'user','aADr1ZWyuaCZJ7mnEtqejP8KkQwfD3UYGtbhNadnVI4Jb1A5rSUtn8Qbb2Gj','2018-07-21 08:00:29','2018-07-21 08:00:29',NULL,NULL,NULL),(23,2,'leo12018','Carlos','Paguada','cabejean@hotmail.com','94716808','$2y$10$.OVzJV6rFGaJiGiYa9PD9eMJ1jmlmz.qug2Zda1hjX6trVXhW.V2y','',21,1,1,0,0,0,'user','WmXp7jkzdC2kSOO31lKZtE2SqHUGxgoIdjGO8la7ynxv5FDWoLScohXZY0oj','2018-07-21 08:02:12','2018-07-21 08:02:12',NULL,NULL,NULL),(24,1,'carlossv12018','carlos','zepeda','carloszepedacornejo@gmail.com','0050370495645','$2y$10$yRU5VMGFZSii0Q3xU20WLuGlNLfTiFWpPsDZ/68ROpfNDTqLqs7ea','',6,1,1,0,0,0,'user','6lGzS0o030obfU0qbCddTZwH2279YQVV7GlvcroBXVyGodxXHBOyScRsnSMu','2018-07-21 08:03:46','2018-07-21 08:03:46',NULL,NULL,NULL),(25,1,'FabianMejia','Fabian','Mejia','fa.mejia@hotmail.com','0050660297473','$2y$10$RU6lc3cmTr61ftseHsyAi.ZxmJkYMs4yMn/nGSx9j.JWzyunKVxwe','',13,1,1,0,0,0,'user','BdNBVPgKWix5UIvCguQryokRpMdCJgh6B9p0F4FTHc8f6kgBMsyoQ1hQxzfU','2018-07-21 08:05:09','2018-07-21 18:08:12',NULL,NULL,NULL),(26,1,'davismimo','David','Fajardo','davismimofajardo@gmail.com','79062925','$2y$10$GHzu0uVYa9QMcHKzsxTqgug6440guBZH2OpB6WI/RLorIZcFkBQH6','',21,1,1,0,0,0,'user','cs7La0OT7fSqxHF0vKGw3SlMHOZuSGdqtu9i4v7nXhm8BjsjhSN5Angz3b4N','2018-07-21 08:06:33','2018-07-21 08:06:33',NULL,NULL,NULL),(27,1,'jiriast01','Victor','Irias','torvicirias@gmail.com','9592-0300','$2y$10$c45YC0GQ9HWooBHOzIYuYOAJqrDiUuB.SMdEC0a3rCD.j.xbGVZPW','',21,1,1,0,0,0,'user','R04lhs0nXk6kdbLgxK60sHLR0kYeYWAeXlFcNKvNrH4snxys5s33fG4zbFse','2018-07-21 08:07:57','2018-07-21 08:07:57',NULL,NULL,NULL),(28,1,'a.fongut','Armando','Fonseca Fonseca','logisticacomercialcr@gmail.com','83026965','$2y$10$vHSfGHlG54yQZq8E5d61jO8Pcf8MqEM0t/bG8A6Twge0J61zJ/vma','',25,1,1,0,0,0,'user','HDqX6pfgQDpiThOFJABuZUv5w8QVDb6kNyX9ZIIC3cPLqpz5IyhyGjoSsoCb','2018-07-21 08:10:12','2018-07-21 08:10:12',NULL,NULL,NULL),(29,1,'pacson777','Marlon','Chavez','pacson777@gmail.com','99573868','$2y$10$Pefw/Ht7bT8jh4EQ9b5TUe/YwsHMpq1ClW1gCQCYUh1wocj1gZjgi','',23,1,1,0,0,0,'user','Owm6rURdUJPPDx8M2DTuFzSFN1TtzEZTn7ZAJZ3f06KImmFR9yWOutz2g7c6','2018-07-21 08:12:11','2018-07-21 08:12:11',NULL,NULL,NULL),(30,1,'MaurenTatiana','Mauren Tatiana','Gonzalez Herrera','mautati2@gmail.com','87101223','$2y$10$62vGb7cNgUPPQFx3QcrlVOi7eGyxw0lp5LOf2a7Pgc6dPXaBKfYgq','',25,1,1,0,0,0,'user','8oeUNOgPRbm1NZYSU4hcW1GFcfPj6v7nAE2Xg6cSq7qVqMqPYja9eAOM7eb7','2018-07-21 08:14:00','2018-07-21 08:14:00',NULL,NULL,NULL),(31,4,'JoseMBit','José','Meléndez','bitcoinaddperu@gmail.com','916431925','$2y$10$0cX8nlYcqdcd0vUU6iBObOsXVCB9bBb/ZxShmfsDwLykQ2CLefbEq','',1,1,1,0,0,0,'user','ZKNDByHAbpnjYwc5A94ipLgiB5wowEA7YG4eY5Z5uOkuRMpdcFUm6bGTCOkp','2018-07-21 08:16:05','2018-07-21 08:16:05',NULL,NULL,NULL),(32,1,'Claudiopips','Claudio','Alvarez','claudioalvarez93@hotmail.com','71469036','$2y$10$8lXQp3eE3fosp/y5kesWduJE2lF84PEcw8B4OMCXEz0KyqYtllMNS','',28,1,1,0,0,0,'user','i6eKKTw8ZBottwVeqjnotNGyknuDYG8KToGtRSh7RaKKySb0Js0JCN45i4vH','2018-07-21 08:19:31','2018-07-21 08:19:31',NULL,NULL,NULL),(33,1,'Giovanoti1','Giovanny Alexander','Cubillos Murillo','giovanoti2@gmail.com','573134054399','$2y$10$5LJ6ul95zCIJ43a26SMuDOEMXdWey/83IMFmPtiJZmgPJY924Vvt2','',1,1,1,0,0,0,'user','mgQMcBNNT0EoAuF4T4J8vHbPZXOn9ilTn14XSE535WVGNqHgpYhMunPbut3P','2018-07-21 08:22:13','2018-07-21 14:47:13',NULL,NULL,NULL),(34,1,'Randall95','Randall','Montero','ranrome95@outlook.com','50684485955','$2y$10$cDt8YQ5GLE84zcDrAJFRpeHj4WqxfbDewXHgvMk8Cwo8.KH5FAf0C','',7,1,1,0,0,0,'user','Gpsig0jdEKIEmndwLObMv0aKCmsisdYTGCI3gOYCdAcKXigYnp8RX7cWV1y7','2018-07-21 08:23:49','2018-07-21 08:23:49',NULL,NULL,NULL),(35,1,'bernyva94','Berny','Vega','berny.big.business@hotmail.com','86441468','$2y$10$1aWSrLqGhjyCYUSqs8kDj.t574lMLB4YkAosKSIa0nBOXEL3/Ahim','',32,1,1,0,0,0,'user','5tIFdnaSCHaOjs3L3NSxDv2zkNL3ncJTX9izuf2cP0zR5QpXzcQCMkI4XhQw','2018-07-21 08:25:26','2018-07-21 08:25:26',NULL,NULL,NULL),(36,1,'raul','Raul','Silva Reategui','silvatkd@yahoo.com','+51 964 278 775','$2y$10$1aWSrLqGhjyCYUSqs8kDj.t574lMLB4YkAosKSIa0nBOXEL3/Ahim','',1,1,1,0,0,0,'user','I3O4DEz1wtN1wFxRDYJzg5hfwPK6kxcn4OncK1vShwx3liaxGWvjPNvQWhiu','2018-07-21 08:26:43','2018-07-21 14:54:26',NULL,NULL,NULL),(37,1,'FabriPro','Fabricio Gonzalo','Pacahuala Garcia','fabriciopg2001@gmail.com','933838650','$2y$10$NUWjP5ETBFkqztAqWo6QVeltvFUd7CpPfSjjfM.zrAD60ny55HLnK','',1,1,1,0,0,0,'user','VRHnlSZLFtJwNecICI0MTLXhx2xj490gpkUXfaPLY5HhijaenkIHu6BUC3VW','2018-07-21 08:27:39','2018-07-21 08:27:39',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-23  1:27:25
