<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Entities\Signal;
use App\Events\SignalEvent;

class TraderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $signals = Signal::latest()->paginate(5);
        return view('dashboard.trader.signals.index',compact('signals'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.trader.signals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $signal = $this->createS($request->all());

        event(new SignalEvent($signal));
        flash('Una nueva señal fue agregada.')->info();

        return redirect()->route('trader.signals.index')
            ->with('success','Signal created successfully.');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'coinname' => 'required',
            'coinsymbol' => 'required',
            'coinimgurl' => 'required',
            'exchange' => 'required',
            'cointradeurl' => 'required',
            'coinprice' => 'required',
            'target1' => 'required',
            'target2' => 'required',
            'target3' => 'required',
            'stoploss' => 'required',
            'membresia_id' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function createS(array $data)
    {
        return Signal::create([
            'coinname' => $data['coinname'],
            'coinsymbol' => $data['coinsymbol'],
            'coinimgurl' => $data['coinimgurl'],
            'exchange' => $data['exchange'],
            'cointradeurl' => $data['cointradeurl'],
            'coinprice' => $data['coinprice'],
            'target1' => $data['target1'],
            'target2' => $data['target2'],
            'target3' => $data['target3'],
            'stoploss' => $data['stoploss'],
            'membresia_id' => $data['membresia_id'],
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $signal = Signal::findOrFail($id);
        return view('dashboard.trader.signals.show')
            ->with('signal', $signal);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $signal = Signal::findOrFail($id);
        return view('dashboard.trader.signals.edit')
            ->with('signal', $signal);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $signal = Signal::findOrFail($id);

        request()->validate([
            'coinname' => 'required',
            'coinsymbol' => 'required',
            'coinimgurl' => 'required',
            'exchange' => 'required',
            'cointradeurl' => 'required',
            'coinprice' => 'required',
            'target1' => 'required',
            'target2' => 'required',
            'target3' => 'required',
            'stoploss' => 'required',
            'membresia_id' => 'required',
        ]);

        $signal->update($request->all());

        event(new SignalEvent($signal));
        flash('La señal fue actualizada.')->info();

        return redirect()->route('trader.signals.index')
            ->with('success','Signal updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $signal = Signal::findOrFail($id);

        $signal->delete();

        //flash()->success('User Deleted');

        return redirect()->route('trader.signals.index')
            ->with('success','Signal deleted successfully');
    }
}
