<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\EduCategoria;
use Yajra\Datatables\Datatables;

class EduCategoriaController extends Controller
{
    public function index()
    {
        $categorias = EduCategoria::where('estado', '=', 1)->get();
        return view('dashboard.admin.educacion.categoria.index', ['categorias' => $categorias]);
    }

    public function listar_categoria_edu()
    {
        $categorias = EduCategoria::where('estado', '=', 1)->get();
        return view('dashboard.admin.educacion.categoria.result.categoria', ['categorias' => $categorias]);
    }

   
    public function registrar_categoria(Request $request)
    {
        $this->validate($request,[
            'nombre' => 'required'
        ]);
        $categoria = new EduCategoria;
        $categoria->nombre = $request->nombre;
        $categoria->descripcion = $request->descripcion;
        $categoria->estado = 1;
        $categoria->usuario_creacion = 1;
        $categoria->usuario_modificacion = 1;
        if ($categoria->save()) {
            return response()->json(['success' => 'true', 'msg' => 'Registro agregado con èxito', 'tipo' => 'success', 'titulo' => 'Categoría Registrada']);
        }
    }

    public function mostrar_categoria($id)
    {
        $categoria = EduCategoria::find($id);
        return response()->json($categoria);
    }

    

    public function actualizar_categoria(Request $request)
    {
        $categoria = EduCategoria::findOrFail($request->id_cat);
        $categoria->nombre = $request->nombre;
        $categoria->descripcion = $request->descripcion;
        $categoria->usuario_modificacion = 1;
        if ($categoria->save()) {
            return response()->json(['success' => 'true', 'msg' => 'Registro actualizado con èxito', 'tipo' => 'success', 'titulo' => 'Categoría Actualizada']);
        }
    }

    public function eliminar_categoria(Request$request)
    {
        $categoria = EduCategoria::findOrFail($request->id);
        $categoria->estado = 0;
        if ($categoria->save()) {
            return response()->json(['success' => 'true', 'msg' => 'Registro eliminado con éxito', 'tipo' => 'success', 'titulo' => 'Categoría Registrada']);
        }
    }
}