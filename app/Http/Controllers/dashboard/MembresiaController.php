<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Item;
use App\Entities\Membresia;
use Yajra\Datatables\Datatables;
use Image;
use Carbon\Carbon;
use File;
use DB;

class MembresiaController extends Controller
{
    public function index()
    {
        $membresias = Membresia::where('estado', '=', 1)->orderBy('id', 'desc')->get();
        return view('dashboard.admin.membresia.index', compact('membresias'));
    }

    public function listar_membresia()
    {
        $membresias = Membresia::where('estado', '=', 1)
        		->select('name', 'precio_mes', 'precio_anual', 'id', 'estado')
                ->orderBy('id', 'desc')
                ->get();
 
        return view('dashboard.admin.membresia.result.membresias', ['membresias' => $membresias]);
    }


    public function registrar_membresia(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'precio_mes' => 'required|numeric|between:0,999.99',
            'precio_anual' => 'required|numeric|between:0,99.99'
        ]);
        
        $membresia = new Membresia;
        $membresia->name = $request->name;
        $membresia->precio_mes = $request->precio_mes;
        $membresia->precio_anual = $request->precio_anual;
        $membresia->estado = 1;
        if ($membresia->save()) {
            return response()->json(['success' => 'true', 'msg' => 'Se guardó correctamente', 'titulo' => 'Membresia Registrada']);
        }       
    }

    public function mostrar_membresia($id)
    {
        $membresia = Membresia::find($id);
        return response()->json($membresia);
    }

    public function actualizar_membresia(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'precio_mes' => 'required|numeric|between:0,999.99',
            'precio_anual' => 'required|numeric|between:0,99.99'
        ]);
        
        $membresia = Membresia::findOrFail($request->id);
        $membresia->name = $request->name;
        $membresia->precio_mes = $request->precio_mes;
        $membresia->precio_anual = $request->precio_anual;
        if ($membresia->save()) {
            return response()->json(['success' => 'true', 'msg' => 'Se guardó correctamente', 'titulo' => 'Membresía Registrada']);
        } 
    }

    public function eliminar_membresia(Request $request)
    {
        $membresia = Membresia::findOrFail($request->id);
        if (DB::table('detalle_membresia')->where('membresia_id', '=',$request->id)->count() > 0) {
        	if (DB::table('detalle_membresia')->where('membresia_id', '=',$request->id)->delete()) {
	            $membresia->estado = 0;
	            $membresia->save();
	            return response()->json(['success' => 'true', 'msg' => 'Registro eliminado con éxito', 'tipo' => 'success', 'titulo' => 'Membresia Eliminada']);
	        }else{
	        	return response()->json(['success' => 'true', 'msg' => 'No se ha podido eliminar', 'tipo' => 'danger', 'titulo' => 'Membresia Error']);
	        }
        }else{
        	$membresia->estado = 0;
        	$membresia->save();
	        return response()->json(['success' => 'true', 'msg' => 'Registro eliminado con éxito', 'tipo' => 'success', 'titulo' => 'Membresia Eliminada']);
        }
        
    }

    //items
    public function listar_items($id_membresia)
    {
    	$items = Item::where('membresia_id', '=', $id_membresia)->orderBy('id','desc')->get();

    	return view('dashboard.admin.membresia.result.items', ['items' => $items]);
    }

    public function registrar_item(Request $request)
    {
        $this->validate($request,[
            'item' => 'required'
        ]);
        
        $item = new Item;
        $item->item = $request->item;
        $item->membresia_id = $request->membresia_id;
        if ($item->save()) {
            return response()->json(['success' => 'true', 'msg' => 'Se guardó correctamente', 'titulo' => 'Membresia Registrada', 'item' => $item]);
        }       
    }

    public function eliminar_item(Request $request)
    {
        $item = Item::findOrFail($request->id);
        if ($item->delete()) {
        	return response()->json(['success' => 'true', 'msg' => 'Registro eliminado con éxito', 'tipo' => 'success', 'titulo' => 'Item Eliminado', 'id' => $request->id]);
        }

        
    }


}
