<?php

namespace App\Http\Controllers\dashboard;

use App\Entities\Log;
use App\Entities\Membresia;
use App\Entities\Pago;
use App\Entities\User;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ActivationController extends Controller
{
    public function index(Request $request)
    {
       
        $membresias = Membresia::all();
    	$usuarios = User::where('estado', '=', 1)->where('role','<>','admin')->paginate(10);
    	if ($request->ajax()) {
    		return response()->json(view('dashboard.admin.activation.result.tabla', ['usuarios' => $usuarios])->render());
    	}
    	return view('dashboard.admin.activation.index', compact('usuarios', 'membresias'));
    }

    public function activar(Request $request)
    {
      
        $usuario = $request->id_user;
        $user = User::find($usuario);
       //return response()->json($user['id']);
        $membresia = Membresia::findOrFail($request->membresia_id);
        //registramos el pago en el api
        $vars = array(
            "prik" => "5b3eb8796868903656fae73a7ebd56bb",
            "pubk" => "4d0b0ed9548dbc01dcd93f5d952e5fb8",
            "m" => "pagos",
            "cmd" => "agregar",
            "id" => $user['id'],
            "fecha" => date('m/d/Y'),
            "plan" => $request->membresia_id,
            "grupo" => 2,//alumno
            "fpago" => 0,
            "sendmail" => TRUE,
            "descripcion" => $request->description
        );
        $urlvars = http_build_query($vars);  
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://grupocreex.net/json-data.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $urlvars);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $resultado_post = curl_exec ($ch);
        curl_close ($ch);
        //fin de api pagos
        $respuesta_post = json_decode($resultado_post, true);

        //$respuesta_post['response'] = true;

        if ($respuesta_post['response'] != false) {
            //registrar pago en nuestro sistema
            $pago = new Pago;
            $pago->monto = $membresia->precio_anual;
            $pago->estado = 1;
            $pago->user_id = $user['id'];
            $pago->membresia_id = $request->membresia_id;               
            $pago->description = $request->description;               
            if ($pago->save()){
                //cambiar role de usuario
                //$user = User::findOrFail($request->id);
                $user->role = 'user';
                $user->membresia_id = $membresia->id;
                if ($user->save()) {
                    return response()->json(['success' => 'true', 'msj' => 'Se ha activado la cuenta', 'id' => $user['id']]);
                }else{
                    $log = new Log;
                    $log->usuario = $user['nickname'];
                    $log->fecha = Carbon::now();
                    $log->descripcion = 'No se ha podido registrar el pago en DB';
                    $log->membresia = $request->membresia_id;
                    $log->ruta = 'url:dashboard/admin/activation';
                    $log->save();
                    return response()->json(['success' => 'false_db_pago', 'msj' => 'No se ha podido registrar el pago en DB']); 
                }
            }
        }else{
            $log = new Log;
            $log->usuario = $user['nickname'];
            $log->fecha = Carbon::now();
            $log->descripcion = 'No se ha podido registrar el pago en el api';
            $log->membresia = $request->membresia_id;
            $log->ruta = 'url:dashboard/admin/activation';
            $log->save();
            return response()->json(['success' => 'false_api_pago', 'msj' => 'No se ha podido registrar el pago en el api']);
        }
	

    }

    public function desactivar(Request $request)
    {
    	//dd($request->id);
    	$user = User::findOrFail($request->id);
    	$user->role = 'invitado';
    	if ($user->save()) {
    		return response()->json(['success' => true, 'msj' => 'Se ha activado la cuenta', 'id' => $request->id]);
    	}

    }
}
