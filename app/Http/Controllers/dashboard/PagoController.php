<?php

namespace App\Http\Controllers\dashboard;

use App\Entities\Log;
use App\Entities\Membresia;
use App\Entities\Pago;
use App\Entities\User;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use Culqi\Culqi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use Redirect;
use Session;
use Validator;

class PagoController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $nickname = Auth::user()->nickname;
        $membresia_id = $user->membresia_id;
        $exp_semestral = "0";
        $exp_mensualidad = "0";

        //dump($user->id);
        //dump($user->membresia_id);
        $pagos = Pago::where('user_id', '=', $user->id)->where('membresia_id', '=', $user->membresia_id)->get();
        $membresiaText = "";

        //dump($pagos);

        if( $user->membresia_id == "1" ){
            $membresiaText = "Aprendiz";
        }else if( $user->membresia_id == "2" ){
            $membresiaText = "Emprendedor";
        }else if( $user->membresia_id == "3" ){
            $membresiaText = "Inversionista";
        }else if( $user->membresia_id == "4" ){
            $membresiaText = "Inversionista VIP";
        }

        //////////////////////////////////////////////
        //API QUE REGISTRA PAGOS EN LA OTRA PLATAFORMA
        //////////////////////////////////////////////

        //CONSULTA DE EXISTENCIA DE USUARIO EN API DE LA OTRA PLATAFORMA
        $vars = array(
            "prik" => "5b3eb8796868903656fae73a7ebd56bb",
            "pubk" => "4d0b0ed9548dbc01dcd93f5d952e5fb8",
            "m" => "usuario",
            "cmd" => "ver",
            "usuario" => $nickname);//Auth::user()->nickname
        # GENERAR UNA CADENA CODIFICADA ESTILO URL
        $urlvars = http_build_query($vars);
        # ENVIANDO POR METODO GET
        $resultado_get = file_get_contents("https://grupocreex.net/json-data.php?$urlvars");
        $userApi = json_decode($resultado_get, true);
        //FIN DE LA CONSULTA


        if ($userApi['response'] != false) {
            //Si el response del API es VALIDA
            //dump($userApi);
            $exp_mensualidad = $userApi['exp_mensualidad'];
            $exp_semestral = $userApi['expiracion'];

        }else{
            $log = new Log;
            $log->usuario = $nickname;
            $log->fecha = Carbon::now();
            $log->descripcion = 'No se encontró el usuario en el api o la peticion respondió con FALSE';
            $log->membresia = $membresia_id;
            $log->ruta = 'url:dashboard/user/pagos';
            $log->save();
            return response()->json(['success' => 'false_api', 'msj' => 'No se ha podido encontrar tu usuario.']);
        }
        //////////////////////////////////////////////////
        //FIN API QUE REGISTRA PAGOS EN LA OTRA PLATAFORMA
        //////////////////////////////////////////////////

//        if(!empty($pagos)):
//            foreach ($pagos as $i => $pago):
//                $data[$i]['id'] = $pago->id;
//                $data[$i]['membresia_id'] = $pago->membresia_id;
//                $data[$i]['user_id'] = $pago->user_id;
//                $data[$i]['monto'] = $pago->monto;
//                $data[$i]['estado'] = $pago->estado;
//                $data[$i]['created_at'] = $pago->created_at;
//                $data[$i]['updated_at'] = $pago->updated_at;
//                $data[$i]['description'] = $pago->description;
//                $data[$i]['tipo_membresia'] = $pago->tipo_membresia;
//                $data[$i]['fecha_pago'] = $pago->fecha_pago;
//                $data[$i]['fecha_activo_hasta'] = $pago->fecha_activo_hasta;
//                $data[$i]['chargeId'] = $pago->chargeId;
//                $data[$i]['origen_pago'] = $pago->origen_pago;
//                $data[$i]['moneda'] = $pago->moneda;
//                $data[$i]['financiado'] = $pago->financiado;
//            endforeach;
//        endif;
//
        //dump(compact('pagos'));

        return view('dashboard.user.pagos.index', compact('pagos'))
            ->with('i', (request()->input('page', 1) - 1) * 10)
            ->with('membresia', $membresiaText);
    }

    public function pagar_membresia(Request $request)
    {
        
        $membresia = Membresia::findOrFail($request->id);
        $cantidad = 1;//session()->get('cantidad');
        $monto = $membresia->precio_anual + ($membresia->precio_anual*0.1);
        $token = $request->token;
        $monto_final = $monto*100*$cantidad;
        $email = Auth::user()->email;

        //cosultas para su api
        $vars = array(
        "prik" => "5b3eb8796868903656fae73a7ebd56bb",
        "pubk" => "4d0b0ed9548dbc01dcd93f5d952e5fb8",
        "m" => "usuario",
        "cmd" => "id",
        "usuario" => Auth::user()->nickname);//Auth::user()->nickname
        # GENERAR UNA CADENA CODIFICADA ESTILO URL
        $urlvars = http_build_query($vars);
        # ENVIANDO POR METODO GET
        $resultado_get = file_get_contents("https://grupocreex.net/json-data.php?$urlvars");
        $user = json_decode($resultado_get, true);
        
        //fin de consulta al api
        
        if ($user['response'] != false) {
            if($request->ajax()){
                //configurar api key
                //esto es produccion

                $SECRET_KEY = "sk_live_tQU2M0zSCpOiSzuc";//SECRET KEY PRODUCCION

                //$SECRET_KEY = "sk_live_tQU2M0zSCpOiSzuc";//live

                //$SECRET_KEY = "sk_test_OwcbojkEMeKhYxGs";

                //autenticacion
                $culqi = new Culqi(array('api_key' => $SECRET_KEY));
                
                try {
                    // Creando Cargo a una tarjeta
                    $charge = $culqi->Charges->create(
                      array(
                        "amount" => $monto_final,
                        "currency_code" => "USD",
                        "email" => $email,
                        "pedido"=> time(),
                        "source_id" => $token,
                        "antifraud_details" => array(
                            "address" => $membresia->name,
                            "address_city"=> "Lima",
                            "country_code" => "PE",
                            "first_name" => Auth::user()->name,
                            "last_name" => Auth::user()->lastname,
                            "phone_number" => Auth::user()->cellphone
                         )
                      )
                    );
                    
                    if ($charge) {

                        $pago = new Pago;
                        $pago->monto = $monto;
                        $pago->estado = 1;
                        $pago->user_id = Auth::user()->id;
                        $pago->membresia_id = $membresia->id;               
                        $pago->description = 'Pago a traves de culqi';               
                        if ($pago->save()) {
                            //mandamos al api mlm para q regstren pago
                           
                            $vars = array(
                                "prik" => "5b3eb8796868903656fae73a7ebd56bb",
                                "pubk" => "4d0b0ed9548dbc01dcd93f5d952e5fb8",
                                "m" => "pagos",
                                "cmd" => "agregar",
                                "id" => $user['id'],
                                "fecha" => date('m/d/Y'),
                                "plan" => $membresia->id,
                                "grupo" => 2,//alumno
                                "fpago" => 0,
                                "sendmail" => TRUE,
                                "descripcion" => 'Pago con culqi'
                            );
                            $urlvars = http_build_query($vars);  
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL,"https://grupocreex.net/json-data.php");
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $urlvars);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $resultado_post = curl_exec ($ch);
                            curl_close ($ch);
                            //fin de api pagos

                            $respuesta_post = json_decode($resultado_post, true);

                            if ($respuesta_post['response'] != false) {
                                //actualziamos el role
                                $user = User::findOrFail(Auth::user()->id);
                                $user->role = 'user';
                                $user->estado = 1;
                                $user->membresia_id = $membresia->id;
                                $user->save();

                                //oddooooo
                                //if ($this->reg_cli_odoo($membresia->id, $charge->id)) {
                                    echo json_encode($charge);
                                //}
                                //fin de odo
                                
                            }else{
                                $log = new Log;
                                $log->usuario = Auth::user()->nickname;
                                $log->fecha = Carbon::now();
                                $log->descripcion = 'El usuario ya existe en el api o el registro respondio con un false';
                                $log->membresia = $membresia->id;
                                $log->ruta = 'url:dashboard/invitado/payments/membresia/pago';
                                $log->save();
                                return response()->json(['success' => 'false_save_api_user', 'msj' => 'No se pudo guardar usuario, El usuario ya existe!']);
                            }
                        }
                    }
                
                } catch (Exception $e) {
                     echo json_encode($e->getMessage());
                      
                }
            }
        }else{
            $log = new Log;
            $log->usuario = Auth::user()->nickname;
            $log->fecha = Carbon::now();
            $log->descripcion = 'No se encontró el usuario en el api o la peticion respondió con FALSE';
            $log->membresia = $membresia->id;
            $log->ruta = 'url:dashboard/invitado/payments/membresia/pago';
            $log->save();
            return response()->json(['success' => 'false_api', 'msj' => 'No se ha podido encontrar tu usuario']);
        }
    }

    //coinbase
    public function paycoinbase(Request $request)
    {
        $v = Validator::make(["id"=>Input::get("id")],
            ["id"=>"required|integer"]);
        $membresia = Membresia::findOrFail($request->id);
        $monto = $membresia->precio_anual;

        if($v->passes()){

            $pago = new Pago;
            $pago->monto = $monto;
            $pago->estado = 1;
            $pago->user_id = Auth::user()->id;
            $pago->membresia_id = $membresia->id;
            $pago->save();

            $bitcoinRedirectURL = URL::to("/");

            //if(Input::get('type')=="coinbase"){

            try {
                $coinbaseResponse =  $this->coinbaseRequestCurl($membresia->id,$monto);
            } catch (\Exception $e) {
                Session::flash("error_msg",$e->getMessage());
                return Redirect::back();
            }

           // $order->type = "coinbase";
           // $order->status = "Pending";
           // $order->response = serialize($coinbaseResponse);

            $bitcoinRedirectURL = "https://www.coinbase.com/checkouts/".$coinbaseResponse->button->code;

            //}
            Session::flash("error_msg", 'Se esta pagando');
            return Redirect::to($bitcoinRedirectURL);

        }else{

            $response = "";
            $messages = $v->messages()->all();

            foreach($messages as $message){
                $response.="<li style='margin-left:10px;'>{$message}</li>";
            }

            Session::flash("error_msg",$response);
            return Redirect::back()->withInput();
        }
    }

    public function coinbaseRequestCurl($orderId,$priceString){

        //$config = Config::get('coinbase');
        $apikey = '195271cd-59dc-4e71-ae70-7e3458cc871a';
        $apisecret = 'db2be7f4-1dd0-4a46-94d2-fba756a1d7a6';
        $coinbase_notify_url = URL::to('/')."/notify/coinbase";
        $coinbase_url = 'https://coinbase.com/api/v1/buttons';
        $nonce = sprintf('%0.0f',round(microtime(true) * 1000000));

        $url = $coinbase_url."?nonce=" . $nonce;

        $parameters = [];
        $parameters["button"]["name"] = "KodeInfo Checkout Invoice";
        $parameters["button"]["custom"] = $orderId;
        $parameters["button"]["price_string"] = $priceString;
        $parameters["button"]["type"] = "buy_now";
        $parameters["button"]["subscription"] = false;
        $parameters["button"]["price_currency_iso"] = "USD";
        $parameters["button"]["description"] = "KodeInfo Checkout Invoice";
        $parameters["button"]["style"] = "custom_large";
        $parameters["button"]["include_email"] = true;
        $parameters["button"]["callback_url"] = $coinbase_notify_url;
        $parameters = http_build_query($parameters, true);

        $signature = hash_hmac("sha256", $nonce . $url . $parameters, $apisecret);

        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                "ACCESS_KEY: " . $apikey,
                "ACCESS_NONCE: " . $nonce,
                "ACCESS_SIGNATURE: " . $signature
            )));

        curl_setopt_array($ch, array(
            CURLOPT_POSTFIELDS => $parameters,
            CURLOPT_POST => true,
        ));

        $response = curl_exec($ch);
        curl_close($ch);

       $decodeResponse = json_decode($response);

       dd($decodeResponse);

        if(sizeof($decodeResponse->errors)>0){
            Log::alert(Request::all());
            throw new \Exception($decodeResponse->errors[0]);
        }else{
            return $decodeResponse;
        }
    }


    public function reg_cli_odoo($memb, $idtransaccion)
    {
        //variable
        $user = Auth::user();
        $membresia = Membresia::findOrFail($memb);
        $newnombre = $user->name.' '.$user->lastname;
        $email = $user->email;

        //inicio sessionen api de ODO
        $url = 'http://gestioncreex.com/web/session/authenticate';

        //create a new cURL resource
        $ch = curl_init($url);

        //setup request to send json via POST
        $data = array(
            "jsonrpc"=>'2.0',
            "method" => 'call',
            "params" => array(
               "db"=>"creexdb",        
               "login"=>"info@grupocreex.net",        
               "password"=>"1nfocreex*2018"
            )
        );
       
        $payload = json_encode($data);

        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        //set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //execute the POST request
        $result = curl_exec($ch);

        //close cURL resource
        curl_close($ch);

        /*+++++++++fin de inicio session de odoo ++++++++******/

        //registro cliente en Odoo

        $url1 = 'http://gestioncreex.com/web/dataset/call_kw';

        $ch = curl_init($url1);

        $result=json_decode($result);
        //var_dump($result);

        $ses_id= $result->result->session_id;

        //var_dump($ses_id);
        $data1 = array(
            "jsonrpc"=>'2.0',
            "method" => 'call',
             "params"=>array(                  
               "model"=>"res.partner",           
               "method"=>"create",         
               "args"=>array(array("name"=>$newnombre,"email"=>$email)),
               //"kwargs"=>array("vals"=>array("name"=>"Teddy Valdez","email"=>"alexis@dani.com"))
               "kwargs" => (object)[]
             )
        );

        $payload1 = json_encode($data1);

        //var_dump($payload1);

        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload1);    
        $cuki = 'Cookie: website_lang=es_PE; session_id='.$ses_id;

        //set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json',$cuki));
       // curl_setopt($ch, CURLOPT_HTTPHEADER, array($cuki));

        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //execute the POST request
        $result1 = curl_exec($ch);

        //close cURL resource
        curl_close($ch);

        $respuesta = json_decode($result1);

        //obtenemos el id del cliente
        $idcliente = $respuesta->result;


        /*************** enviamos factura ********/
        $url1 = 'http://gestioncreex.com/api/factura/create';

        $ch = curl_init($url1);


        //var_dump($ses_id);
        $data3 = array(
            "jsonrpc"=>'2.0',
            "method" => 'call',
            "params"=>array(
                "data" => array(
                    "cliente_id" => $idcliente,
                    "lines" => array(
                        array(
                            "id" => (int)$membresia->id,
                            "descripción"=> $membresia->name,
                            "cantidad" => 1,
                            "precio" => floatval($membresia->precio_anual),
                            "descuento" => 0
                        )
                    ),
                    "moneda" => 'USD',
                    "journal_id" => 1,
                    "tipo_operacion" => "01",
                    "pago"  => array(
                        "monto" => floatval($membresia->precio_anual),
                        "procedencia" => 'culqi',
                        "detalle" => 'pago por membresia',
                        "id_transaccion" => $idtransaccion
                    )
                )
            )
        );

        $payload3 = json_encode($data3);

        //$hola = json_decode($payload3);
        //var_dump($hola);

        //attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload3);    
        $cuki = 'Cookie: website_lang=es_PE; session_id='.$ses_id;

        //set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json',$cuki));
       // curl_setopt($ch, CURLOPT_HTTPHEADER, array($cuki));

        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //execute the POST request
        $result3 = curl_exec($ch);

        //close cURL resource
        curl_close($ch);

        return $result3;

    }


    // public function reg_fac_odoo($idcliente, $idmembresia, $idtransaccion, $idsession)
    // {
    //     $membresia = Membresia::findOrFail($idmembresia);

    //     $url1 = 'http://34.213.233.225:9850/api/factura/create';

    //     $ch = curl_init($url1);


    //     //var_dump($ses_id);
    //     $data3 = array(
    //         "jsonrpc"=>'2.0',
    //         "method" => 'call',
    //         "params"=>array(
    //             "data" => array(
    //                 "cliente_id" => $idcliente,
    //                 "lines" => array(
    //                     array(
    //                         "id" => $membresia->id,
    //                         "descripción"=> $membresia->name,
    //                         "cantidad" => 1,
    //                         "precio" => $membresia->precio_anual,
    //                         "descuento" => 0
    //                     )
    //                 ),
    //                 "moneda" => 'USD',
    //                 "journal_id" => 1,
    //                 "tipo_operacion" => "01",
    //                 "pago"  => array(
    //                     "monto" => $membresia->precio_anual,
    //                     "procedencia" => 'culqi',
    //                     "detalle" => 'pago por membresia',
    //                     "id_transaccion" => $idtransaccion
    //                 )
    //             )
    //         )
    //     );

    //     $payload3 = json_encode($data3);

    //     //$hola = json_decode($payload3);
    //     //var_dump($hola);

    //     //attach encoded JSON string to the POST fields
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $payload3);    
    //     $cuki = 'Cookie: website_lang=es_PE; session_id='.$idsession;

    //     //set the content type to application/json
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json',$cuki));
    //    // curl_setopt($ch, CURLOPT_HTTPHEADER, array($cuki));

    //     //return response instead of outputting
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    //     //execute the POST request
    //     $result3 = curl_exec($ch);

    //     //close cURL resource
    //     curl_close($ch);

    //     //var_dump($result3);
    // }

}
