<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Curso;
use App\Entities\Archivo;
use App\Entities\Leccion;
use Yajra\Datatables\Datatables;
use Image;
use Carbon\Carbon;
use File;

class ArchivoController extends Controller
{
    public function mostrar_archivo($id)
    {
        $archivos = Archivo::where('leccion_id', '=', $id)
                ->orderBy('id', 'desc')
                ->get();
 
        return view('dashboard.admin.educacion.leccion.result.archivos', ['archivos' => $archivos]);
    }

    public function mostrar_leccion_archivo($id)
    {
        $leccion = Leccion::find($id);
        return response()->json($leccion);
    }

    public function subir_archivo(Request $request)
    {
        $this->validate($request,[
            'leccion_id' => 'required'
        ]);
        
        $curso = new Archivo;
        $curso->estado = 1;
        $curso->leccion_id = $request->leccion_id;
        $curso->usuario_creacion = 1;
        $curso->usuario_modificacion = 1;
        
        if ($request->file('archivo')) {
            $tiempo = Carbon::now();
            $user = 1; //Auth::user()
            $extension = $request->file('archivo')->getClientOriginalExtension();
            $dividir = explode('.', $extension);
            //return response()->json($dividir[1]);
            if ($dividir[0] == 'pdf' || $dividir[0] == 'doc' || $dividir[0] == 'docx' || $dividir[0] == 'xls' || $dividir[0] == 'xlsx' || $dividir[0] == 'ppt' || $dividir[0] == 'pptx' || $dividir[0] == 'txt' || $dividir[0] == 'zip') {
                $file_name = 'arc'.$tiempo->second.$tiempo->minute.$tiempo->hour.$user.'.'.$extension;
            
           
                $path = public_path('uploads/archivos/');
                \Request::file('archivo')->move($path, $file_name);
                $curso->archivo = $file_name;
                if ($curso->save()) {
                    return response()->json(['success' => true, 'msg' => 'Se guardó correctamente', 'titulo' => 'Archivo Subido', 'tipo' => 'success']);
                }
            }else{
                return response()->json(['success' => 'error', 'msg' => 'Los Archivos tiene que ser de formato: xls, xlsx, doc, docx, ppt, pptx, txt, pdf, zip', 'titulo' => 'No se pudo subir', 'tipo' => 'error']);
            }
            
        }        
    }

    public function eliminar_archivo(Request $request)
    {
        $archivo = Archivo::findOrFail($request->id);
        $path = public_path('uploads/archivos/');
        if (File::delete(public_path() . '/uploads/archivos/'.$archivo->archivo)) {
            $archivo->delete();
            return response()->json(['success' => 'true', 'msg' => 'Registro eliminado con éxito', 'tipo' => 'success', 'titulo' => 'Categoría Registrada', 'archivo' => $archivo]);
        }   
    }
}
