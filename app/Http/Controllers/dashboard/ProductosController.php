<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Productos;
use App\Entities\Membresia;
use Image;
use Carbon\Carbon;
use File;
use Culqi\Culqi;
use DB;
use Auth;

class ProductosController extends Controller
{
    public function index(Request $request)
    {
    	$productos = Productos::where('estado', '=', 'activado')->paginate(10);
    	$membresias = Membresia::all();
    	if ($request->ajax()) {
    		return response()->json(view('dashboard.admin.productos.result.tabla', ['productos' => $productos, 'membresias' => $membresias])->render());
    	}
    	return view('dashboard.admin.productos.index', ['productos' => $productos, 'membresias' => $membresias]);
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
            'imagen' => 'required|image',
            'nombre' => 'required',
            'precio' => 'required',
            'membresia' => 'required'
        ]);

        
        //return response()->json($descripcion);
        $producto = new Productos;
        $producto->nombre = $request->nombre;
        $producto->precio = $request->precio;
        $producto->estado = 'activado';
        $producto->membresia = $request->membresia;
        $producto->descripcion = $request->descripcion;
        $producto->resumen = $request->resumen;
        
        if ($request->file('imagen')) {
            $tiempo = Carbon::now();
            $user = Auth::user();
            $extension = $request->file('imagen')->getClientOriginalExtension();
            $file_name = 'prod'.$tiempo->second.$tiempo->minute.$tiempo->hour.$user->id.'.'.$extension;
            
            //para servicios
            $path = public_path('uploads/productos/220x220/'.$file_name);
            Image::make($request->file('imagen'))->fit(220, 220)->save($path);
            //para la interna
            $path2 = public_path('uploads/productos/'.$file_name);
            Image::make($request->file('imagen'))->save($path2);
            $producto->imagen = $file_name;
            if ($producto->save()) {
                return response()->json(['success' => 'true', 'msj' => 'Se guardó correctamente', 'titulo' => 'Exitoso!']);
            }
        }
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $membresia = array();
        $producto = Productos::findOrFail($id);
        $membresia = explode(',', $producto->membresia);
        $cant_memb = DB::table('membresia')->get()->count();
        return ['producto' => $producto, 'membresia' => $membresia,'cant_memb' => $cant_memb];
    }

    public function desactivar(Request $request)
    {
        $producto = Productos::findOrFail($request->id);
        $producto->estado = 'desactivado';
        if ($producto->save()) {
            return response()->json(['success' => true, 'msj' => 'Se ha desactivado correctamente', 'id' => $request->id]);
        }
    }
}
