<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use GuzzleHttp\Client;
use App\Entities\Portafolio;
use App\Entities\Moneda;
use App\Entities\Operaciones;
use DB;
use Auth;

class OperacionesController extends Controller
{
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://coincap.io/',
            //'base_uri' => 'https://api.coinmarketcap.com/v1/'
            //'timeout' => 2.0,
        ]);

        $this->response = $this->client->request('GET', 'front');//front, ticker
        $this->response = json_decode($this->response->getbody()->getcontents());  

        //precio actual del btc
        $this->precio_actual_btc = $this->getMonedaUni('BTC');  
    }

    public function listar_moneda($portafolio_id)
    {
        
        $monedas = Moneda::where('estado', '=', 1)->where('portafolio_id', '=', $portafolio_id)
                ->orderBy('created_at', 'DESC')
                ->get();

        $data = array();
        foreach ($monedas as $key => $moneda) {
            $id_moneda = $moneda->id; 
            $moneda_short = $moneda->moneda; 
            $precio_actual = $this->getPrecioActual($moneda->moneda);
            $hold =$this->getHold($moneda->moneda, $moneda->id);
            $precio_avg =$this->getPrecio_avg($moneda->id);
            
            //$profit = $this->getProfit_parcial($moneda->moneda, $moneda->id);

            //cantidad
            $sum_cant_holt = Operaciones::where('moneda_id', '=', $id_moneda)->where('operacion', '=', 0)->sum('cantidad');//hold
            $sum_cant_buy= Operaciones::where('moneda_id', '=', $id_moneda)->where('operacion', '=', 1)->sum('cantidad');//buy
            $cantidad = $sum_cant_holt - $sum_cant_buy;

            array_push($data, ['precio_actual' => $precio_actual, 'hold' => $hold, 'precio_avg' => $precio_avg, 'cantidad' => $cantidad, 'moneda_short' => $moneda_short, 'id_moneda' => $id_moneda]);

        }

        
        //dd($total_moneda);

        return view('dashboard.user.portafolio.tables.monedas', ['data' => $data]);

    }

    public function getPrecio_avg($id_moneda)
    {
        //cantidad total por moneda
        $c_hold = Operaciones::where('operacion', '=', 0)->where('estado', '=', 1)->where('moneda_id', '=', $id_moneda)->sum('cantidad');
        $c_sell = Operaciones::where('operacion', '=', 1)->where('estado', '=', 1)->where('moneda_id', '=', $id_moneda)->sum('cantidad');
        $cant = $c_hold - $c_sell; //cantidad total hasta el momento

        $ope_hold = Operaciones::where('operacion', '=', 0)->where('estado', '=', 1)->where('moneda_id', '=', $id_moneda)->get();
        $ope_sell = Operaciones::where('operacion', '=', 1)->where('estado', '=', 1)->where('moneda_id', '=', $id_moneda)->get();
        $usd_lote_hold = 0;
        $usd_lote_sell = 0;
        foreach ($ope_hold as $key => $value) {
            $usd_lote_hold = $usd_lote_hold + ($value->cantidad * $value->precio);
        }

        //
        foreach ($ope_sell as $key => $value) {
            $usd_lote_sell = $usd_lote_sell + ($value->cantidad * $value->precio);
        }

        $usd_total = $usd_lote_hold - $usd_lote_sell;
        if ($cant > 0) {
            $pre_base_usd = $usd_total / $cant;
            
        }else{
            $pre_base_usd = 0;
        }

        $btc = $this->getMonedaUni('BTC');
        $pre_base_btc = $pre_base_usd/$btc;


        return ['pre_base_usd' => $pre_base_usd, 'pre_base_btc' => $pre_base_btc];
    }

    

    

    public function getHold($moneda, $id_moneda)
    {
        $precio = $this->getMonedaUni($moneda);
        $sum_cant_holt = Operaciones::where('moneda_id', '=', $id_moneda)->where('operacion', '=', 0)->sum('cantidad');//hold
        $sum_cant_buy= Operaciones::where('moneda_id', '=', $id_moneda)->where('operacion', '=', 1)->sum('cantidad');//buy

        $sum_cant = $sum_cant_holt - $sum_cant_buy;

        $btc = $this->getMonedaUni('BTC'); 
        $p_usd = $precio * $sum_cant;
        $p_btc = $p_usd/$btc;
        return 'USD '.number_format($p_usd, 6).'<br>'.'BTC '.number_format($p_btc,6);
    }

    public function getPrecioActual($moneda)
    {
        $precio = $this->getMonedaUni($moneda);
        $btc = $this->getMonedaUni('BTC');                
        $p_en_btc = $precio/$btc;               
        return ['precio_usd' => $precio, 'precio_btc' => $p_en_btc];//'USD '.number_format($precio, 6).'<br>'.'BTC '.number_format($p_en_btc, 6);
    }
   

    public function add_moneda(Request $request)
    {
    	$this->validate($request,[
            'moneda' => 'required'
        ]);
    	try {
    		//DB::beginTransaction();
	        $moneda = new Moneda;
	        $moneda->moneda = $request->moneda;
	        $moneda->estado = 1;
	        $moneda->usuario_creacion = Auth::user()->id;
	        $moneda->usuario_modificacion = Auth::user()->id;
	        $moneda->portafolio_id = $request->portafolio_id;
	        $moneda->save();

	        $ope = new Operaciones;
	        $ope->cantidad = 0;
	        $ope->precio = 0;
	        $ope->fecha = '2018-04-21';
	        $ope->estado = 1;
	        $ope->usuario_creacion = Auth::user()->id;
	        $ope->usuario_modificacion = Auth::user()->id;
	        $ope->moneda_id = $moneda->id;

	        if ($ope->save()) {
	        	return response()->json(['success' => 'true', 'msg' => 'Se guardó correctamente', 'moneda' => $moneda]);
	        }
        	//DB::commit();
    	} catch (Exception $e) {
    		//DB::rollBack();
    	}
    }

    public function getMonedaUni($moneda)
    {
        foreach ($this->response as $value) {
            if ($value->short == $moneda) {
                return $value->price;
            }
        }       
    }

    //eliminar moneda
    public function eliminar_moneda(Request $request)
    {
        $id = $request->id;
        $moneda = Moneda::findOrFail($id);
        if (DB::table('operaciones')->where('moneda_id', $id)->delete()) { 
            $moneda->estado = 0;
            if ($moneda->save()) {
                return response()->json(['success' => true, 'moneda' => $moneda]);
            }
        }else{
            $moneda->estado = 0;
            if ($moneda->save()) {
                return response()->json(['success' => true, 'moneda' => $moneda]);
            }
        }
    }
    

    //operaciones

    public function add_operacion(Request $request)
    {
        $this->validate($request,[
            'operacion' => 'required',
            'cantidad' => 'required',
            'moneda' => 'required',
            'precio' => 'required',
            'fecha' => 'required',
            'moneda_id' => 'required'
        ]);

        //obtenemps el precio actual del btc
        $precio_btc = $this->precio_actual_btc;
        try {
            $ope = new Operaciones;
            $ope->operacion = $request->operacion;
            $ope->cantidad = $request->cantidad;
            if ($request->moneda == 0) {
                $ope->precio = $request->precio*$precio_btc;//pasamos en dolares
            }else{
                $ope->precio = $request->precio;
                
            }
            $ope->fecha = $request->fecha;
            $ope->moneda = $request->moneda;
            $ope->estado = 1;
            $ope->usuario_creacion = Auth::user()->id;
            $ope->usuario_modificacion = Auth::user()->id;
            $ope->moneda_id = $request->moneda_id;

            if ($ope->save()) {
                //return response()->json(['success' => 'true', 'msg' => 'Se guardó correctamente', 'ope' => $ope]);
                return redirect()->back();
            }
            //DB::commit();
        } catch (Exception $e) {
            //DB::rollBack();
            return redirect()->back();
        }
    }

    public function mostrar_operaciones($id_moneda)
    {
        $operaciones = Operaciones::where('estado','=',1)->where('cantidad', '>', 0)->where('moneda_id', '=', $id_moneda)->get();
        return view('dashboard.user.portafolio.tables.mostrar_operaciones', ['operaciones' => $operaciones]);
    }
}
