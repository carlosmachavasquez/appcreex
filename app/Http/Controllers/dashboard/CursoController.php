<?php

namespace App\Http\Controllers\dashboard;

use App\Entities\Archivo;
use App\Entities\Curso;
use App\Entities\EduCategoria;
use App\Entities\Leccion;
use App\Entities\Leccionuser;
use App\Entities\Membresia;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Image;
use Yajra\Datatables\Datatables;

class CursoController extends Controller
{
    //user
    public function index_user()
    {
        $cursos_prin = Curso::Join('educategoria', 'educategoria.id', '=', 'curso.educategoria_id')
            ->select('curso.id as id', 'titulo', 'nivel','imagen', 'curso.descripcion', 'educategoria.nombre as categoria','curso.created_at as created_at', 'curso.estado as estado', 'educategoria.id as id_cat')
            ->where('curso.estado', '=', 1);

        $categorias = EduCategoria::where('estado', '=', 1)->get();

        $user = Auth::user();
        $membresiaId = $user->membresia_id;
        
        if ($membresiaId == 1 || $membresiaId == 5 || $membresiaId == 10 || $membresiaId == 11) { //aprendiz y creexiendo
            $cursos = $cursos_prin->where('membresia', '<=', 1)->orderBy('id', 'desc')->get();
        }else if($membresiaId== 2 || $membresiaId == 6){                                        //emprendedor
            $cursos = $cursos_prin->where('membresia', '<=', 2)->orderBy('id', 'desc')->get();
        }else if($membresiaId == 3 || $membresiaId == 7){                                       //inversionista
            $cursos = $cursos_prin->where('membresia', '<=', 3)->orderBy('id', 'desc')->get();  
        }else if($membresiaId == 4 || $membresiaId == 8){                                       //inversionista vip
            $cursos = $cursos_prin->where('membresia', '<=', 4)->orderBy('id', 'desc')->get();
        }

        

        return view('dashboard.user.educacion.curso.index', compact('categorias', 'cursos', 'membresias'));
    }


    public function mostrar_archivos($id)
    {
        $archivos = Archivo::where('leccion_id', '=', $id)->get();
        return view('dashboard.user.educacion.curso.result.mostrar_archivos',['archivos' => $archivos]);
    }

    //admin

    public function index_admin(){
        $categorias = EduCategoria::where('estado', '=', 1)->get();
        $cursos = Curso::Join('educategoria', 'educategoria.id', '=', 'curso.educategoria_id')
            ->select('curso.id as id', 'titulo', 'nivel','imagen', 'educategoria.nombre as categoria','curso.created_at as created_at', 'curso.estado as estado', 'educategoria.id as id_cat')
            ->where('curso.estado', '=', 1)
            ->orderBy('id', 'desc')
            ->get();
        $membresias = Membresia::where('estado','=', 1)->get();

        return view('dashboard.admin.educacion.curso.index', compact('categorias', 'cursos', 'membresias'));
    }

    public function listar_curso()
    {
        $cursos = Curso::Join('educategoria', 'educategoria.id', '=', 'curso.educategoria_id')
                ->select('curso.id as id', 'titulo', 'nivel','imagen', 'nombre as categoria','curso.created_at as created_at', 'curso.estado as estado')
                ->where('curso.estado', '=', 1)
                ->orderBy('id', 'desc')
                ->get();
 
        return view('dashboard.admin.educacion.curso.result.cursos', ['cursos' => $cursos]);
    }


    public function registrar_curso(Request $request)
    {
        $this->validate($request,[
            'titulo' => 'required',
            'categoria_id' => 'required',
            'nivel' => 'required'
        ]);
        
        $curso = new Curso;
        $curso->titulo = $request->titulo;
        $curso->membresia = $request->membresia;
        $curso->nivel = $request->nivel;
        $curso->educategoria_id = $request->categoria_id;
        $curso->descripcion = $request->descripcion;
        $curso->video = $request->video;
        $curso->estado = 1;
        $curso->usuario_creacion = 1;
        $curso->usuario_modificacion = 1;
        
        if ($request->file('imagen')) {
            $tiempo = Carbon::now();
            $user = 1; //Auth::user()
            $extension = $request->file('imagen')->getClientOriginalExtension();
            $file_name = 'conf'.$tiempo->second.$tiempo->minute.$tiempo->hour.$user.'.'.$extension;
            
            //para servicios
            $path = public_path('uploads/60x60/'.$file_name);
            Image::make($request->file('imagen'))->fit(60, 60)->save($path);
            //para la interna
            $path2 = public_path('uploads/cursos/'.$file_name);
            Image::make($request->file('imagen'))->save($path2);
            $curso->imagen = $file_name;
            if ($curso->save()) {
                return response()->json(['success' => 'true', 'msg' => 'Se guardó correctamente', 'titulo' => 'Curso Registrado']);
            }
        }else{
            $curso->imagen = 'default.png';
            if ($curso->save()) {
                return response()->json(['success' => 'true', 'msg' => 'Se guardó correctamente', 'titulo' => 'Curso Registrado']);
            }
        }        
    }

    public function mostrar_curso($id)
    {
        $curso = Curso::find($id);
        return response()->json($curso);
    }

    public function actualizar_curso(Request $request)
    {
        $this->validate($request,[
            'titulo' => 'required',
            'categoria_id' => 'required',
            'nivel' => 'required'
        ]);
        
        $curso = Curso::findOrFail($request->id_curso);
        $curso->titulo = $request->titulo;
        $curso->nivel = $request->nivel;
        $curso->membresia = $request->membresia;
        $curso->educategoria_id = $request->categoria_id;
        $curso->descripcion = $request->descripcion;
        $curso->video = $request->video;
        $curso->usuario_modificacion = 1;
        
        if ($request->file('imagen')) {
            $tiempo = Carbon::now();
            $user = 1; //Auth::user()
            $extension = $request->file('imagen')->getClientOriginalExtension();
            $file_name = 'conf'.$tiempo->second.$tiempo->minute.$tiempo->hour.$user.'.'.$extension;
            
            //para servicios
            $path = public_path('uploads/60x60/'.$file_name);
            Image::make($request->file('imagen'))->fit(60, 60)->save($path);
            //para la interna
            $path2 = public_path('uploads/cursos/'.$file_name);
            Image::make($request->file('imagen'))->save($path2);
            $curso->imagen = $file_name;
            
        }
        if ($curso->save()) {
            return response()->json(['success' => 'true', 'msg' => 'Se guardó correctamente', 'titulo' => 'Curso Actualizado']);
        }
    }

    public function eliminar_curso(Request$request)
    {
        $curso = Curso::findOrFail($request->id);
        $curso->estado = 0;
        if ($curso->save()) {
            return response()->json(['success' => 'true', 'msg' => 'Registro eliminado con éxito', 'tipo' => 'success', 'titulo' => 'Categoría Registrada']);
        }
    }

    //user
    public function interna_curso($id)
    {
        $curso = Curso::findOrFail($id);
        $lecciones = Leccion::where('curso_id','=', $id)->where('estado', '=', 1)->get();
        //dd($lecciones);
        return view('dashboard.user.educacion.curso.interna', compact('curso', 'lecciones'));
    }

    public function ver_video($id)
    {
        $leccion = Leccion::findOrFail($id);

        $leccuser = new Leccionuser;
        $leccuser->visto = 1;
        $leccuser->usuario = Auth::user()->id;
        $leccuser->estado = 1;
        $leccuser->usuario_creacion = Auth::user()->id;
        $leccuser->usuario_modificacion = Auth::user()->id;
        $leccuser->leccion_id = $leccion->id;
        $leccuser->save();

        return response()->json($leccion);
    }

   
}
