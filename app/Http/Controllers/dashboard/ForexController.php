<?php

namespace App\Http\Controllers\dashboard;

use App\Entities\Forex;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Mail\Consultas;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mail;


class ForexController extends Controller
{

  //trader

	public function index()
	{
		if (Auth::user()->role == 'trader') {
      $forex = Forex::orderBy('id', 'Desc')->paginate(5);
      return view('dashboard.trader.forex.index', ['forex' => $forex]);
    }elseif(Auth::user()->role == 'user'){
      $forex = Forex::orderBy('id', 'Desc')->paginate(5);
      return view('dashboard.user.forex.index', ['forex' => $forex]);
    }
    
	}

  public function create()
  {
    return view('dashboard.trader.forex.create');
  }

  public function store(Request $request)
  {
    $this->validator($request->all())->validate();

    Forex::create($request->all());
    flash('Se ha registrado Correctamente.')->info();
    return redirect()->route('trader.forex.index');

  }

  protected function validator(array $data)
  {
    return Validator::make($data, [
        'pair' => 'required',
        'order_type' => 'required',
        'market_execution' => 'required|numeric|between:0,99999.999',
        'sell_limit' => 'required|numeric|between:0,99999.999',
        'stop_loss' => 'required|numeric|between:0,99999.999',
        'take_prof1' => 'numeric|between:0,99999.999',
        'take_prof2' => 'numeric|between:0,99999.999',
        'take_prof3' => 'numeric|between:0,99999.999',
    ]);
  }

  public function show($id)
  {
      $forex = Forex::findOrFail($id);
      return view('dashboard.trader.forex.show')
        ->with('forex', $forex);
  }

  public function edit($id)
  {
      $forex = Forex::findOrFail($id);      
      return view('dashboard.trader.forex.edit')
        ->with('forex', $forex);
  }

  public function update(Request $request, $id)
  {
      $forex = Forex::findOrFail($id);

      $this->validator($request->all())->validate();

      $forex->update($request->all());

      flash('El registro fue actualizado.')->info();

      return redirect()->route('trader.forex.index');
  }

   public function destroy($id)
    {
      $forex = Forex::findOrFail($id);
      $forex->delete();
      flash('El registro fue eliminado.')->info();
      return redirect()->route('trader.forex.index');
    }



/******************************************************************************************/

  //user

  public function enviar_correo(Request $request)
  {
    $data['nombre'] = $request->nombre;
    $data['email'] = $request->email;
    $data['usuario'] = $request->usuario;
    $data['password'] = $request->password;
    $data['servidor'] = $request->servidor;
    $data['observacion'] = $request->observacion;

    if (Mail::to('forexgrupocreex@gmail.com')->send(new Consultas($data))) {
      return response()->json(['success'=> 'true', 'msj' => 'Sus Datos se enviaron correctamente']);
    }else{
      return response()->json(['success'=> 'false', 'msj' => 'envio ok']);
    }
    
  }
}