<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Curso;
use App\Entities\EduCategoria;
use App\Entities\Leccion;
use Yajra\Datatables\Datatables;
use Image;
use Carbon\Carbon;
use File;
use DB;

class LeccionController extends Controller
{
    public function index()
    {
        $cursos = Curso::where('estado', '=', 1)->get();
        $lecciones = Leccion::Join('curso', 'curso.id', '=', 'leccion.curso_id')
                ->select('leccion.id as id', 'leccion.titulo as titulo', 'curso.titulo as curso','duracion', 'leccion.created_at as created_at', 'leccion.estado as estado')
                ->where('leccion.estado', '=', 1)
                ->orderBy('leccion.id', 'desc')
                ->get();
        return view('dashboard.admin.educacion.leccion.index', compact('cursos', 'lecciones'));
    }

    public function listar_leccion()
    {
        $lecciones = Leccion::Join('curso', 'curso.id', '=', 'leccion.curso_id')
                ->select('leccion.id as id', 'leccion.titulo as titulo', 'curso.titulo as curso','duracion', 'leccion.created_at as created_at', 'leccion.estado as estado')
                ->where('leccion.estado', '=', 1)
                ->orderBy('leccion.id', 'desc')
                ->get();
 
        return view('dashboard.admin.educacion.leccion.result.lecciones', compact('lecciones'));
    }


    public function registrar_leccion(Request $request)
    {
        $this->validate($request,[
            'titulo' => 'required',
            'curso_id' => 'required',
            'video' => 'required'
        ]);
        
        $curso = new Leccion;
        $curso->titulo = $request->titulo;
        $curso->curso_id = $request->curso_id;
        $curso->descripcion = $request->descripcion;
        $curso->duracion = $request->duracion;
        $curso->video = $request->video;
        $curso->estado = 1;
        $curso->usuario_creacion = 1;
        $curso->usuario_modificacion = 1;
        if ($curso->save()) {
            return response()->json(['success' => 'true', 'msg' => 'Se guardó correctamente', 'titulo' => 'Lección Registrado']);
        }       
    }

    public function mostrar_leccion($id)
    {
        $leccion = Leccion::find($id);
        return response()->json($leccion);
    }

    public function actualizar_leccion(Request $request)
    {
        $this->validate($request,[
            'titulo' => 'required',
            'curso_id' => 'required',
            'video' => 'required'
        ]);
        
        $curso = Leccion::findOrFail($request->id_leccion);
        $curso->titulo = $request->titulo;
        $curso->curso_id = $request->curso_id;
        $curso->descripcion = $request->descripcion;
        $curso->duracion = $request->duracion;
        $curso->video = $request->video;
        $curso->estado = 1;
        $curso->usuario_creacion = 1;
        $curso->usuario_modificacion = 1;
        if ($curso->save()) {
            return response()->json(['success' => 'true', 'msg' => 'Se guardó correctamente', 'titulo' => 'Lección Registrado']);
        } 
    }

    public function eliminar_leccion(Request $request)
    {
        $leccion = Leccion::findOrFail($request->id);
        
        if (DB::table('archivo')->where('leccion_id', '=',$request->id)->get()->count() > 0) {
            DB::table('archivo')->where('leccion_id', '=',$request->id)->delete();
            $leccion->delete();
            return response()->json(['success' => 'true', 'msg' => 'Registro eliminado con éxito', 'tipo' => 'success', 'titulo' => 'Categoría Eliminada']);
        }else{
            $leccion->delete();
            return response()->json(['success' => 'true', 'msg' => 'Registro Eliminado.', 'tipo' => 'success', 'titulo' => 'Categoría Eliminada']);
        }
    }
}
