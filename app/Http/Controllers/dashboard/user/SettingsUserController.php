<?php

namespace App\Http\Controllers\dashboard\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\User;
use Illuminate\Support\Facades\Hash;
use Auth;

class SettingsUserController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        return view('dashboard.user.settings.settings')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */

    public function updateprofile(Request $request)
    {

        $user = Auth::user();

         request()->validate([
            'name' => 'required',
            'lastname' => 'required',
            'cellphone' => 'required'
        ]);

        $user->update($request->all());

        flash('El usuario fue actualizado.')->success();

        return redirect()->route('user.settingsuser.index')->with('user', $user);
    }

    public function updateprofilepassword(Request $request)
    {
        $user = Auth::user();

        if (!(Hash::check($request->get('currentpassword'), $user->password))) {
            // The passwords matches
            flash('La contraseña ingresada no coincide con tu contraseña actual. Por favor intenta de nuevo.')->error();

            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
 
        if(strcmp($request->get('currentpassword'), $request->get('newpassword')) == 0){
            //Current password and new password are same
            flash('La contraseña ingresada no puede ser igual que la contraseña antigua. Por favor ingrese otra contraseña diferente.')->error();
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = request()->validate([
            'currentpassword' => 'required',
            'newpassword' => 'required|string|min:6',
            'newpasswordconfirm' => 'same:newpassword'
        ]);

        //Change Password
        if ($validatedData) {
            $user->password = bcrypt($request->get('newpassword'));
            $user->save();
            
            flash('La contraseña fue cambiada satisfactoriamente.')->success();
            return redirect()->back()->with("success","Password changed successfully !");
        }
    }

    public function updateprofilewallet(Request $request)
    {

        $user = Auth::user();

         request()->validate([
            'wallet' => 'string'
        ]);

        $user->update($request->all());

        flash('Tu número Wallet fue actualizado.')->success();

        return redirect()->route('user.settingsuser.index')->with('user', $user);
    }
}