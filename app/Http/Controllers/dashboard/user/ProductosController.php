<?php

namespace App\Http\Controllers\dashboard\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Productos;
use App\Entities\Membresia;
use Image;
use Carbon\Carbon;
use File;
use Culqi\Culqi;
use DB;
use Auth;

class ProductosController extends Controller
{
    public function index(Request $request)
    {
    	$productos = Productos::where('estado', '=', 'activado')->paginate(10);
    	return view('dashboard.user.productos.index', ['productos' => $productos]);
    }


    public function pagar(Request $request)
    {
    	$user = Auth::user();
    	$producto = Productos::findOrFail($request->id);

    	if($request->ajax()){
                //configurar api key
                //esto es produccion
                //$SECRET_KEY = "sk_live_tQU2M0zSCpOiSzuc";//live
                $SECRET_KEY = "sk_test_OwcbojkEMeKhYxGs";

                //autenticacion
                $culqi = new Culqi(array('api_key' => $SECRET_KEY));
                
                try {
                    //Creando Cargo a una tarjeta
                    $charge = $culqi->Charges->create(
                      array(
                        "amount" => $producto->precio,
                        "currency_code" => "USD",
                        "email" => $user->email,
                        "pedido"=> time(),
                        "source_id" => $token,
                        "antifraud_details" => array(
                            "address" => $producto->nombre,
                            "address_city"=> "Lima",
                            "country_code" => "PE",
                            "first_name" => $user->name,
                            "last_name" => $user->lastname,
                            "phone_number" => $user->cellphone
                         )
                      )
                    );
                    
                    if ($charge) {

                        $pago = new Pago;
                        $pago->monto = $monto;
                        $pago->estado = 1;
                        $pago->user_id = Auth::user()->id;
                        $pago->membresia_id = $membresia->id;               
                        $pago->description = 'Pago a traves de culqi';               
                        if ($pago->save()) {
                            echo json_encode($charge);
                        }
                    }
                
                } catch (Exception $e) {
                     echo json_encode($e->getMessage());
                      
                }
            }
    }

}
