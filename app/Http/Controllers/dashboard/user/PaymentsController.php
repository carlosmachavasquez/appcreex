<?php

namespace App\Http\Controllers\dashboard\user;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Membresia;
use App\Entities\Pago;
use Carbon\Carbon;
use Auth;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\Nullable;
use Redirect;
use cryptonator\MerchantAPI;
use Coinbase;

class PaymentsController extends Controller
{
    public function index()
    {
        $vars = array(
            "prik" => "5b3eb8796868903656fae73a7ebd56bb",
            "pubk" => "4d0b0ed9548dbc01dcd93f5d952e5fb8",
            "m" => "login",
            "cmd" => "agregarx",
            "usuario" => Auth::user()->nickname,
        );
        $urlvars = http_build_query($vars);
        # ENVIANDO POR METODO GET
        $resultado_get = file_get_contents("http://grupocreex.net/json-data.php?$urlvars");
        $user = json_decode($resultado_get, true);

        if ($user['response'] != false) {
            $hash = $user['session_hash'];
        }else{
            $hash = 'false';
        }

        //dd($user);

        //return $user['session_hash'];
        return view('dashboard.user.payments.index', ['hash' => $hash]);
        //->with('user', $user);
    }

    //login en api
    public function inicio_session(Request $request)
    {

        if ($request->isMethod('post')) {
            //hacer login en el api
            $vars = array(
                "prik" => "5b3eb8796868903656fae73a7ebd56bb",
                "pubk" => "4d0b0ed9548dbc01dcd93f5d952e5fb8",
                "m" => "login",
                "cmd" => "agregar",
                "usuario" => Auth::user()->nickname,
                "password" => $request->pass
            );
            $urlvars = http_build_query($vars);
            # ENVIANDO POR METODO GET
            $resultado_get = file_get_contents("http://grupocreex.net/json-data.php?$urlvars");
            $response = json_decode($resultado_get, true); 

           // print_r($response['session_hash']);
           if (isset($response['session_hash'])) {
               return view('dashboard.user.payments.index', ['hash' => $response['session_hash']]);
           }else{
                return Redirect::back();
           }
        }else{
            return redirect()->route('user.payments.index');
        }
    }

    //
    public function buy()
    {
        // $user = Auth::user();
        // $membresia = Membresia::find(1);

        // $vars = array(
        //     "prik" => "5b3eb8796868903656fae73a7ebd56bb",
        //     "pubk" => "4d0b0ed9548dbc01dcd93f5d952e5fb8",
        //     "m" => "pagos",
        //     "cmd" => "agregar",
        //     "id" => $user['id'],
        //     "fecha" => date('m/d/Y'),
        //     "plan" => $membresia->idsssss,
        //     "grupo" => 2,//alumno
        //     "fpago" => 0,
        //     "sendmail" => TRUE,
        // );
        // $urlvars = http_build_query($vars);  
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL,"https://grupocreex.net/json-data.php");
        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $urlvars);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // $resultado_post = curl_exec ($ch);
        // curl_close ($ch);
        // //fin de api pagos

        // $respouesta = json_decode($resultado_post, true);

        //return response()->json($respouesta['response']);
        
        /*******************************************/

        $membresia = Membresia::where('estado', '=', 1)->get();

        return view('dashboard.user.payments.buy', ['membresia' => $membresia]);
       
    }

    public function pago_membresia($id)
    {
        $user = Auth::user();
        $membresia = Membresia::findOrFail($id);
        $nickname = $user->nickname;
        $name = $membresia->name;
        $membresia_id = $membresia->id;
        $precio_mes = $membresia->precio_mes;
        $precio_semestral = $membresia->precio_anual;
        $estadoPago = 0;
        $descripcion = 'Pago con CoinBase';
        $tipo_membresia = 1;//valor 0 significa mensual, valor 1 significa semestral
        $fecha_pago = NULL;
        $fecha_activo_hasta = NULL;
        $chargeId = NULL;
        $lastIdInserted = 0;
        $wasPay = false;
        $infopay = '';
        $data = [];
        $dataNoPagados = [];
        $dataPagados = [];
        $paymentsCount = Pago::where('membresia_id', '=', $id)->where('user_id', '=', $user->id)->count();
        //dump($paymentsCount);

        //VEZFNRY3 LITECOIN
        //2JXFFY5W BITCOIN
        //$charge = Coinbase::getCharge("M3NQB8D9");

        if($paymentsCount == 0){
            //Creacion de pago semestral INICIAL
            DB::table('pagos')->insert(
                [   'membresia_id' => $id,
                    'user_id' => $user->id,
                    'monto' => $precio_semestral,
                    'estado' => $estadoPago,
                    'description' => $descripcion,
                    'tipo_membresia' => $tipo_membresia,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'fecha_pago' => $fecha_pago,
                    'fecha_activo_hasta' => $fecha_activo_hasta,
                    'chargeId' => $chargeId,
                    'origen_pago' => 'Coinbase',
                    'moneda' => '',
                    'financiado' => 0
                ]);
        }

        $paymentsNoPagados = Pago::where('membresia_id', '=', $id)->where('estado', '=', 0)->where('user_id', '=', $user->id)->exists();

        if($paymentsNoPagados){
            //Si existe registros de pagos para esta membresia, en estado 0 y el usuario logeado, osea existen pagos pendientes
            $dataNoPagados = [];
            $pagos = Pago::where('membresia_id', '=', $id)->where('estado', '=', 0)->where('user_id', '=', $user->id)->get();
            //dump($pagos);

            if(!empty($pagos)):
                foreach ($pagos as $i => $pago):
                    $dataNoPagados[$i]['id'] = $pago['id'];
                    $dataNoPagados[$i]['membresia_id'] = $pago['membresia_id'];
                    $dataNoPagados[$i]['user_id'] = $pago['user_id'];
                    $dataNoPagados[$i]['monto'] = $pago['monto'];
                    $dataNoPagados[$i]['estado'] = $pago['estado'];
                    if($pago['estado'] == 0){
                        $dataNoPagados[$i]['class'] = 'badge-danger';
                        $dataNoPagados[$i]['estado_text'] = 'pendiente';
                    }else if($pago['estado'] == 1){
                        $dataNoPagados[$i]['class'] = 'badge-success';
                        $dataNoPagados[$i]['estado_text'] = 'pagado';
                    }
                    $dataNoPagados[$i]['description'] = $pago['description'];
                    $dataNoPagados[$i]['tipo_membresia'] = $pago['tipo_membresia'];
                    $dataNoPagados[$i]['created_at'] = $pago['created_at'];
                    $dataNoPagados[$i]['updated_at'] = $pago['updated_at'];
                    $dataNoPagados[$i]['fecha_pago'] = $pago['fecha_pago'];
                    $dataNoPagados[$i]['fecha_activo_hasta'] = is_null($pago['fecha_activo_hasta']) ? '' : $pago['fecha_activo_hasta'];
                    $dataNoPagados[$i]['chargeId'] = $pago['chargeId'];

                    //ACA EMPEZAMOS A VALIDAR EL PAGO POR EL CHARGEID
                    //1ro buscamos que el pago este correcto en el ultimo chargeId cargao
                    $showCharge = Coinbase::getCharge($pago['chargeId']);

                    if(!empty($showCharge)):
                        $timeline = (isset($showCharge['data']['timeline'])) ? $showCharge['data']['timeline'] : [];

                        foreach ($timeline as $i => $time):
                            if($time['status'] == "UNRESOLVED"){
                                if($time['context'] == "UNDERPAID"){
                                    $infopay = 'Estimado cliente, usted a pagado menos del monto válido para la membresía '.$name.'.';
                                }
                                if($time['context'] == "DELAYED"){
                                    $infopay = 'Estimado cliente, al parecer la transacción de su membresía '.$name.' demoró mas de lo normal y por eso se quedó en estado DELAYED. Por favor envie un correo a esta dirección pagos@grupocreex.com para poder ayudarlo en la brevedad posible.';
                                }
                            }
                            if($time['status'] == "PENDING"){
                                $infopay = 'Su proceso de compra esta siendo validado en estos momentos, por favor espere unos minutos.';
                            }
                            if($time['status'] == "COMPLETED"){
                                $wasPay = true;
                                $dataNoPagados[$i]['class'] = 'badge-success';
                                $dataNoPagados[$i]['estado_text'] = 'pagado';
                                $timePaymentComplete = Carbon::parse($time['time']);
                                $infopay = 'El proceso de pago para la membresia '.$name.' fue exitosa. Por favor presione F5 para actualizar su navegador o espere 10 segundos para que se actualize de manera automática.';
                                if($pago['tipo_membresia']==1){//membresia semestral
                                    $fechaActivoHasta = $timePaymentComplete->copy()->addDays(183);
                                }elseif($pago['tipo_membresia']==0){//membresia mensual
                                    $fechaActivoHasta = $timePaymentComplete->copy()->addDays(31);
                                }
                                //////////////////////////////////////////////
                                //API QUE REGISTRA PAGOS EN LA OTRA PLATAFORMA
                                //////////////////////////////////////////////

                                //CONSULTA DE EXISTENCIA DE USUARIO EN API DE LA OTRA PLATAFORMA
                                $vars = array(
                                    "prik" => "5b3eb8796868903656fae73a7ebd56bb",
                                    "pubk" => "4d0b0ed9548dbc01dcd93f5d952e5fb8",
                                    "m" => "usuario",
                                    "cmd" => "id",
                                    "usuario" => $nickname);//Auth::user()->nickname
                                # GENERAR UNA CADENA CODIFICADA ESTILO URL
                                $urlvars = http_build_query($vars);
                                # ENVIANDO POR METODO GET
                                $resultado_get = file_get_contents("https://grupocreex.net/json-data.php?$urlvars");
                                $userApi = json_decode($resultado_get, true);
                                //FIN DE LA CONSULTA

                                if ($userApi['response'] != false) {
                                    //Si el response del API es VALIDA

                                    $vars = array(
                                        "prik" => "5b3eb8796868903656fae73a7ebd56bb",
                                        "pubk" => "4d0b0ed9548dbc01dcd93f5d952e5fb8",
                                        "m" => "pagos",
                                        "cmd" => "agregar",
                                        "id" => $userApi['id'],
                                        "fecha" => date('m/d/Y'),
                                        "plan" => $membresia_id,
                                        "grupo" => 2,//alumno
                                        "fpago" => 0,
                                        "sendmail" => TRUE,
                                        "descripcion" => 'Pago con CoinBase'
                                    );
                                    $urlvars = http_build_query($vars);
                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_URL,"https://grupocreex.net/json-data.php");
                                    curl_setopt($ch, CURLOPT_POST, 1);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $urlvars);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $resultado_post = curl_exec ($ch);
                                    curl_close ($ch);
                                    //Cerramos el envio del pago para el API

                                    $respuesta_post = json_decode($resultado_post, true);

                                    if ($respuesta_post['response'] != false) {
                                        //Actualizamos las tablas correspondientes
                                        $moneda = (isset($showCharge['data']['payments'][0]['network'])) ? $showCharge['data']['payments'][0]['network'] : '';

                                        DB::table('pagos')->where('id', $pago['id'])->update(array(
                                            'estado' => 1,
                                            'fecha_pago' => $timePaymentComplete,
                                            'fecha_activo_hasta' => $fechaActivoHasta,
                                            'origen_pago' => 'Coinbase',
                                            'moneda' => $moneda,
                                            'financiado' => 0
                                        ));

                                        DB::table('users')->where('id', $user->id)->update(array(
                                            'role' => 'user',
                                            'membresia_id' => $membresia_id,
                                            'estado' => 1
                                        ));

                                    }else{
                                        $log = new Log;
                                        $log->usuario = $nickname;
                                        $log->fecha = Carbon::now();
                                        $log->descripcion = 'El usuario ya existe en el api o el registro respondió con un false.';
                                        $log->membresia = $membresia_id;
                                        $log->ruta = 'url:dashboard/invitado/payments/membresia/pago';
                                        $log->save();
                                        return response()->json(['success' => 'false_save_api_user', 'msj' => 'No se pudo guardar usuario, pues este ya existe!']);
                                    }
                                }else{
                                    $log = new Log;
                                    $log->usuario = $nickname;
                                    $log->fecha = Carbon::now();
                                    $log->descripcion = 'No se encontró el usuario en el api o la peticion respondió con FALSE';
                                    $log->membresia = $membresia_id;
                                    $log->ruta = 'url:dashboard/invitado/payments/membresia/pago';
                                    $log->save();
                                    return response()->json(['success' => 'false_api', 'msj' => 'No se ha podido encontrar tu usuario.']);
                                }
                                //////////////////////////////////////////////////
                                //FIN API QUE REGISTRA PAGOS EN LA OTRA PLATAFORMA
                                //////////////////////////////////////////////////
                            }

                        endforeach;
                    endif;

                    if(!$wasPay){
                        //Segunda verificacion donde buscamos en nuestra tabla de cargos
                        $cargos = DB::table('cargos')->where('membresia_id', $pago['membresia_id'])->where('user_id', $pago['user_id'])->get();
                        //dump($cargos);

                        if(!empty($cargos)):
                            foreach ($cargos as $i => $cargo):
                                $chargeCargo = Coinbase::getCharge($cargo->code_charge);
                                //dump($chargeCargo);

                                if(!empty($chargeCargo)):
                                    $timeline = (isset($chargeCargo['data']['timeline'])) ? $chargeCargo['data']['timeline'] : [];
                                    //dump($timeline);
                                    foreach ($timeline as $i => $time):
//                                        if($time['status'] == "EXPIRED"){
//                                            $infopay = 'EXPIRADOOO';
//                                        }
                                        if($time['status'] == "UNRESOLVED"){
                                            if($time['context'] == "UNDERPAID"){
                                                $infopay = 'Estimado cliente, usted a pagado menos del monto válido para la membresía '.$name.'.';
                                            }
                                            if($time['context'] == "DELAYED"){
                                                $infopay = 'Estimado cliente, al parecer la transacción de su membresía '.$name.' demoró mas de lo normal y por eso se quedó en estado DELAYED. Por favor envie un correo a esta dirección pagos@grupocreex.com para poder ayudarlo en la brevedad posible.';
                                            }
                                        }
                                        if($time['status'] == "PENDING"){
                                            $infopay = 'Su proceso de compra esta siendo validado en estos momentos, por favor espere unos minutos.';
                                        }
                                        if($time['status'] == "COMPLETED"){
                                            $wasPay = true;
                                            $dataNoPagados[$i]['class'] = 'badge-success';
                                            $dataNoPagados[$i]['estado_text'] = 'pagado';
                                            $timePaymentComplete = Carbon::parse($time['time']);
                                            $infopay = 'El proceso de pago para la membresia '.$name.' fue exitosa. Por favor presione F5 para actualizar su navegador o espere 10 segundos para que se actualize de manera automática.';
                                            if($pago['tipo_membresia']==1){//membresia semestral
                                                $fechaActivoHasta = $timePaymentComplete->copy()->addDays(183);
                                            }elseif($pago['tipo_membresia']==0){//membresia mensual
                                                $fechaActivoHasta = $timePaymentComplete->copy()->addDays(31);
                                            }

                                            //////////////////////////////////////////////
                                            //API QUE REGISTRA PAGOS EN LA OTRA PLATAFORMA
                                            //////////////////////////////////////////////

                                            //CONSULTA DE EXISTENCIA DE USUARIO EN API DE LA OTRA PLATAFORMA
                                            $vars = array(
                                                "prik" => "5b3eb8796868903656fae73a7ebd56bb",
                                                "pubk" => "4d0b0ed9548dbc01dcd93f5d952e5fb8",
                                                "m" => "usuario",
                                                "cmd" => "id",
                                                "usuario" => $nickname);//Auth::user()->nickname
                                            # GENERAR UNA CADENA CODIFICADA ESTILO URL
                                            $urlvars = http_build_query($vars);
                                            # ENVIANDO POR METODO GET
                                            $resultado_get = file_get_contents("https://grupocreex.net/json-data.php?$urlvars");
                                            $userApi = json_decode($resultado_get, true);
                                            //FIN DE LA CONSULTA

                                            if ($userApi['response'] != false) {
                                                //Si el response del API es VALIDA

                                                $vars = array(
                                                    "prik" => "5b3eb8796868903656fae73a7ebd56bb",
                                                    "pubk" => "4d0b0ed9548dbc01dcd93f5d952e5fb8",
                                                    "m" => "pagos",
                                                    "cmd" => "agregar",
                                                    "id" => $userApi['id'],
                                                    "fecha" => date('m/d/Y'),
                                                    "plan" => $membresia_id,
                                                    "grupo" => 2,//alumno
                                                    "fpago" => 0,
                                                    "sendmail" => TRUE,
                                                    "descripcion" => 'Pago con CoinBase'
                                                );
                                                $urlvars = http_build_query($vars);
                                                $ch = curl_init();
                                                curl_setopt($ch, CURLOPT_URL,"https://grupocreex.net/json-data.php");
                                                curl_setopt($ch, CURLOPT_POST, 1);
                                                curl_setopt($ch, CURLOPT_POSTFIELDS, $urlvars);
                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                $resultado_post = curl_exec ($ch);
                                                curl_close ($ch);
                                                //Cerramos el envio del pago para el API

                                                $respuesta_post = json_decode($resultado_post, true);

                                                if ($respuesta_post['response'] != false) {
                                                    //Actualizamos las tablas correspondientes
                                                    $moneda = (isset($chargeCargo['data']['payments'][0]['network'])) ? $chargeCargo['data']['payments'][0]['network'] : '';

                                                    DB::table('pagos')->where('id', $pago['id'])->update(array(
                                                        'estado' => 1,
                                                        'fecha_pago' => $timePaymentComplete,
                                                        'fecha_activo_hasta' => $fechaActivoHasta,
                                                        'origen_pago' => 'Coinbase',
                                                        'moneda' => $moneda,
                                                        'financiado' => 0
                                                    ));

                                                    DB::table('users')->where('id', $user->id)->update(array(
                                                        'role' => 'user',
                                                        'membresia_id' => $membresia_id,
                                                        'estado' => 1
                                                    ));

                                                }else{
                                                    $log = new Log;
                                                    $log->usuario = $nickname;
                                                    $log->fecha = Carbon::now();
                                                    $log->descripcion = 'El usuario ya existe en el api o el registro respondió con un false.';
                                                    $log->membresia = $membresia_id;
                                                    $log->ruta = 'url:dashboard/invitado/payments/membresia/pago';
                                                    $log->save();
                                                    return response()->json(['success' => 'false_save_api_user', 'msj' => 'No se pudo guardar usuario, pues este ya existe!']);
                                                }
                                            }else{
                                                $log = new Log;
                                                $log->usuario = $nickname;
                                                $log->fecha = Carbon::now();
                                                $log->descripcion = 'No se encontró el usuario en el api o la peticion respondió con FALSE';
                                                $log->membresia = $membresia_id;
                                                $log->ruta = 'url:dashboard/invitado/payments/membresia/pago';
                                                $log->save();
                                                return response()->json(['success' => 'false_api', 'msj' => 'No se ha podido encontrar tu usuario.']);
                                            }
                                            //////////////////////////////////////////////////
                                            //FIN API QUE REGISTRA PAGOS EN LA OTRA PLATAFORMA
                                            //////////////////////////////////////////////////
                                        }

                                    endforeach;
                                endif;

                            endforeach;
                        endif;
                    }

                endforeach;
            endif;
        }

        $paymentsPagados = Pago::where('membresia_id', '=', $id)->where('estado', '=', 1)->where('user_id', '=', $user->id)->exists();

        if($paymentsPagados){
            //si no existe ningun registro de pago para esta membresia en estado 0 y para el usuario logeado, , osea NO existen pagos pendientes

            $dataPagados = [];
            $pagos = Pago::where('membresia_id', '=', $id)->where('estado', '=', 1)->where('user_id', '=', $user->id)->get();
            //dump($pagos);

            if(!empty($pagos)):
                foreach ($pagos as $i => $pago):
                    $dataPagados[$i]['id'] = $pago['id'];
                    $dataPagados[$i]['membresia_id'] = $pago['membresia_id'];
                    $dataPagados[$i]['user_id'] = $pago['user_id'];
                    $dataPagados[$i]['monto'] = $pago['monto'];
                    $dataPagados[$i]['estado'] = $pago['estado'];
                    if($pago['estado'] == 0){
                        $dataPagados[$i]['class'] = 'badge-danger';
                        $dataPagados[$i]['estado_text'] = 'pendiente';
                    }else if($pago['estado'] == 1){
                        $dataPagados[$i]['class'] = 'badge-success';
                        $dataPagados[$i]['estado_text'] = 'pagado';

                        //Si encontramos un registro pagado
                        if($pago['tipo_membresia']==1){//membresia semestral
                            $datePago = Carbon::parse($pago['fecha_pago']);
                            $now = Carbon::now();
                            $diffdays = $datePago->diffInDays($now);

                            if( $diffdays > 183 ){//183 dias o 6 meses
                                //Creacion de pago semestral
                                DB::table('pagos')->insert(
                                    [   'membresia_id' => $id,
                                        'user_id' => $user->id,
                                        'monto' => $precio_semestral,
                                        'estado' => 0,
                                        'description' => 'Semestral',
                                        'tipo_membresia' => 1,
                                        'created_at' => Carbon::now(),
                                        'updated_at' => Carbon::now(),
                                        'fecha_pago' => NULL,
                                        'fecha_activo_hasta' => NULL,
                                        'chargeId' => NULL,
                                        'origen_pago' => 'Coinbase',
                                        'moneda' => '',
                                        'financiado' => 0
                                    ]);
                            }
                        }
                        //Generamos el pago mensual
                        $datePago = Carbon::parse($pago['fecha_pago']);
                        $now = Carbon::now();
                        $diffdays = $datePago->diffInDays($now);

                        if( $diffdays > 31 ){
                            //Creacion de pago mensual
                            DB::table('pagos')->insert(
                                [   'membresia_id' => $id,
                                    'user_id' => $user->id,
                                    'monto' => $precio_mes,
                                    'estado' => 0,
                                    'description' => 'Mensual',
                                    'tipo_membresia' => 0,
                                    'created_at' => Carbon::now(),
                                    'updated_at' => Carbon::now(),
                                    'fecha_pago' => NULL,
                                    'fecha_activo_hasta' => NULL,
                                    'chargeId' => NULL,
                                    'origen_pago' => 'Coinbase',
                                    'moneda' => '',
                                    'financiado' => 0
                                ]);
                        }

                        //
                    }
                    $dataPagados[$i]['description'] = $pago['description'];
                    $dataPagados[$i]['tipo_membresia'] = $pago['tipo_membresia'];
                    $dataPagados[$i]['created_at'] = $pago['created_at'];
                    $dataPagados[$i]['updated_at'] = $pago['updated_at'];
                    $dataPagados[$i]['fecha_pago'] = $pago['fecha_pago'];
                    $dataPagados[$i]['chargeId'] = $pago['chargeId'];
                    $dataPagados[$i]['fecha_activo_hasta'] = is_null($pago['fecha_activo_hasta']) ? '' : $pago['fecha_activo_hasta'];

                endforeach;
            endif;
            //dump($data):
        }


        $pagosTotales = Pago::where('membresia_id', '=', $id)->where('user_id', '=', $user->id)->get();

        if(!empty($pagosTotales)):
            foreach ($pagosTotales as $i => $pay):
                $data[$i]['id'] = $pay['id'];
                $data[$i]['membresia_id'] = $pay['membresia_id'];
                $data[$i]['user_id'] = $pay['user_id'];
                $data[$i]['monto'] = $pay['monto'];
                $data[$i]['estado'] = $pay['estado'];
                if($pay['estado'] == 0){
                    $data[$i]['class'] = 'badge-danger';
                    $data[$i]['estado_text'] = 'pendiente';
                }else if($pay['estado'] == 1){
                    $data[$i]['class'] = 'badge-success';
                    $data[$i]['estado_text'] = 'pagado';
                }
                $data[$i]['description'] = $pay['description'];
                $data[$i]['tipo_membresia'] = $pay['tipo_membresia'];
                $data[$i]['created_at'] = $pay['created_at'];
                $data[$i]['updated_at'] = $pay['updated_at'];
                $data[$i]['fecha_pago'] = $pay['fecha_pago'];
                $data[$i]['fecha_activo_hasta'] = is_null($pay['fecha_activo_hasta']) ? '' : $pay['fecha_activo_hasta'];
                $data[$i]['chargeId'] = $pay['chargeId'];
            endforeach;
        endif;

        //dump($data);

        return view('dashboard.user.payments.pago')
            ->with('name', $name)
            ->with('precio_mes', $precio_mes)
            ->with('precio_semestral', $precio_semestral)
            ->with('data', $data)
            ->with('membresia_id', $membresia_id)
            ->with('infopay', $infopay)
            ->with('wasPay', $wasPay);
    }

    public function gen_charges($membresia_id){
        $user = Auth::user();
        $membresia = Membresia::findOrFail($membresia_id);
        $name = $membresia->name;
        //$precio_semestral = $membresia->precio_anual;
        $currentCharge = [];
        $pagos = Pago::where('membresia_id', '=', $membresia_id)->where('estado', '=', 0)->where('user_id', '=', $user->id)->get();
        //dump($pagos);

        if(!empty($pagos)):
            foreach ($pagos as $i => $pago):
                $currentCharge = Coinbase::createCharge([
                    'name' => $name,
                    'description' => 'Pago por membresia de tipo '.$name,
                    'local_price' => [
                        'amount' => $pago['monto'],
                        'currency' => 'USD',
                    ],
                    'pricing_type' => 'fixed_price',
                ]);
                $codeCharge = $currentCharge['data']['code'];
                DB::table('pagos')->where('id', $pago['id'])->update(array('chargeId' => $codeCharge));
                DB::table('cargos')->insert(
                    [   'membresia_id' => $membresia_id,
                        'user_id' => $user->id,
                        'code_charge' => $codeCharge,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()]);
            endforeach;
        endif;

        return $currentCharge;
    }

    public function mostrar_membresia($id)
    {
        $membresia = Membresia::findOrFail($id);
        return view('dashboard.user.payments.result.payments', ['membresia' => $membresia]);
    }



    //ejemplos para coinbase
    public function coinbase()
    {
        //$charges = Coinbase::getCharges();
        //dd($charges);
        //$events = Coinbase::getEvents();
        //dd($events);
        //$chargeId = '67845LWD'; PAGO VALIDO
        //$chargeId = 'M3NQB8D9'; PAGO IMCOMPLETO DE 2 DOLARES
        //$charge = Coinbase::getCharge($chargeId);
        //dd($charge);


//        $chargeId = 'ZP3HM74A';
//        $charge = Coinbase::getCharge($chargeId);
//
//
//
//        $events = Coinbase::getEvents();
        //$eventId = 'a573ea79-c537-43da-95ab-a100b51ee816';
        //$event = Coinbase::getEvent($eventId);
        //dd($charges);

        // $charge = Coinbase::createCharge([
        //     'name' => 'Aprendiz3',
        //     'description' => 'Description de aprendiz3',
        //     'local_price' => [
        //         'amount' => 16,
        //         'currency' => 'USD',
        //     ],
        //     'pricing_type' => 'fixed_price',
        // ]);

        //checkout
        // $checkout = Coinbase::createCheckout([
        //     'name' => 'Name',
        //     'description' => 'Description',
        //     'local_price' => [
        //         'amount' => 100,
        //         'currency' => 'USD',
        //     ],
        //     'pricing_type' => 'fixed_price',
        // ]);
        //$checkouts = Coinbase::getCheckouts();


       //return $charge;
    }

    

}
