<?php

namespace App\Http\Controllers\dashboard\user;

use App\Entities\Membresia;
use App\Http\Controllers\Controller;
use Auth;



class DashboardController extends Controller
{
    public function index()
    {

        
        
        $membresias = Membresia::where('estado','=',1)->get();
        $user = Auth::user();
        $link_referidor = 'https://www.creex.net/register/?user='.$user->nickname;
        return view('dashboard.templates.dashboard.basic', ['membresias'=>$membresias, 'link_referidor' => $link_referidor]);

        //no 
        //return redirect()->route('dashboard.templates.dashboard.basic');
    }

    // public function basic()
    // {
    //     return view('dashboard.templates.dashboard.basic');
    // }

    // public function ecommerce()
    // {
    //     return view('admin.dashboard.ecommerce');
    // }

    // public function finance()
    // {
    //     return view('admin.dashboard.finance');
    // }
}
