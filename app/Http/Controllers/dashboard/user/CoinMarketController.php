<?php

namespace App\Http\Controllers\dashboard\user;

use App\Http\Controllers\Controller;
use App\Entities\Signal;
use App\Services\CoinMarketService;
use GuzzleHttp\Client;

class CoinMarketController extends Controller
{

    public function index()
    {
        return view('dashboard.user.coinmarket.index');
    }

    public function single($coinsymbol, $id)
    {
        $coinMarketService = new CoinMarketService();
        $signal = Signal::findOrFail($id);
        $idCoin = "";

        // Create a client with a base URI
        $client = new Client(['base_uri' => 'https://api.coinmarketcap.com/v2/']);
        // Send a request to https://foo.com/api/test
        $response = $client->request('GET', 'listings');
        $response = json_decode($response->getbody()->getcontents());
        $response = $response->data;

        foreach ($response as $key => $value) {
            if ($value->symbol == $coinsymbol) {
                $idCoin = $value->id;
                break;
            }        
        }

        $client2 = new Client(['base_uri' => 'https://api.coinmarketcap.com/v2/ticker/']);
        $response2 = $client2->request('GET', $idCoin.'/?convert=BTC');
        $response2 = json_decode($response2->getbody()->getcontents());
        $response2 = $response2->data;

        $marketCap = $response2->quotes->USD->market_cap;
        $marketCap = number_format($marketCap,2);
        $percentChange1h = $response2->quotes->USD->percent_change_1h;
        $percentChange24h = $response2->quotes->USD->percent_change_24h;
        $percentChange7d = $response2->quotes->USD->percent_change_7d;
        $priceUsd = number_format($response2->quotes->USD->price,8);
        $priceBtc = number_format($response2->quotes->BTC->price,8);
        $volumen24Usd = number_format($response2->quotes->USD->volume_24h, 2);
        $timepassed = $coinMarketService->getFormatSinceTime($signal->created_at);
        $target1Percent = $coinMarketService->getPercentByValue($signal->coinprice, $signal->target1, true);
        $target2Percent = $coinMarketService->getPercentByValue($signal->coinprice, $signal->target2, true);
        $target3Percent = $coinMarketService->getPercentByValue($signal->coinprice, $signal->target3, true);
        $stopLossPercent = $coinMarketService->getPercentByValue($signal->coinprice, $signal->stoploss, true);

        $priceTimeSignal = $coinMarketService->setPriceByTimePassWithId($signal);

    	return view('dashboard.user.coinmarket.single')
			->with('signal', $signal)
            ->with('timepassed', $timepassed)
            ->with('target1Percent', $target1Percent)
            ->with('target2Percent', $target2Percent)
            ->with('target3Percent', $target3Percent)
            ->with('stopLossPercent', $stopLossPercent)
            ->with('marketCap', $marketCap)
            ->with('percentChange1h', $percentChange1h)
            ->with('percentChange24h', $percentChange24h)
            ->with('percentChange7d', $percentChange7d)
            ->with('priceTimeSignal', $priceTimeSignal)
            ->with('priceUsd', $priceUsd)
            ->with('priceBtc', $priceBtc)
            ->with('volumen24Usd', $volumen24Usd);
    }

    public function getRequestForTimestamp($coinsymbol, $passto30mtimestamp)
    {
        $client = new Client(['base_uri' => 'https://min-api.cryptocompare.com/data/']);
        $response = $client->request('GET', 'pricehistorical?fsym='.$coinsymbol.'&tsyms=BTC&ts='.$passto30mtimestamp);
        $response = json_decode($response->getbody()->getcontents());
        $response = $response->VIB->BTC;
        $response = number_format($response,8);
        return $response;
    }

}