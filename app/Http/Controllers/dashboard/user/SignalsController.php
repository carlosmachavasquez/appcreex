<?php

namespace App\Http\Controllers\dashboard\user;

use App\Http\Controllers\Controller;
use App\Entities\Signal;
use Auth;
use App\Services\SignalsService;

class SignalsController extends Controller
{
    public function __construct()
    {

    }
    public function index()
    {
        $user = Auth::user();
        $roluser = $user->role;
        $membresiaId = $user->membresia_id;
        $view = '';

        //$signals = Signal::all();
        $signalsService = new SignalsService();

        if ($membresiaId == 1 || $membresiaId == 5) {
            $signals = Signal::where('membresia_id', '=', 1)->latest()->paginate(10);
            $pricesTimesSignals = $signalsService->setPricesByTimePass($signals);
        }else if($membresiaId== 2 || $membresiaId == 6){
            $signals = Signal::where('membresia_id', '<=', 2)->latest()->paginate(10);
            $pricesTimesSignals = $signalsService->setPricesByTimePass($signals);
        }else if($membresiaId == 3 || $membresiaId == 7){
            $signals = Signal::where('membresia_id', '<=', 3)->latest()->paginate(10);
            $pricesTimesSignals = $signalsService->setPricesByTimePass($signals);
        }else if($membresiaId == 4 || $membresiaId == 8){
            $signals = Signal::where('membresia_id', '<=', 4)->latest()->paginate(10);
            $pricesTimesSignals = $signalsService->setPricesByTimePass($signals);
        }

        if($roluser == 'user'){
            $view = 'dashboard.user.signals.index';
        }else if($roluser == 'trader'){
            $view = 'dashboard.trader.livesignals.index';
        }

        return view( $view ,compact('signals'))
            ->with('i', (request()->input('page', 1) - 1) * 10)
            ->with('membresiaUserId', $membresiaId)
            ->with('pricesTimesSignals', $pricesTimesSignals);
    }

    public function signaltable() {
    	return Signal::all(['id', 'coinname', 'coinsymbol', 'coinimgurl', 'exchange', 'cointradeurl', 'coinprice','membresia_id']);
    }
}