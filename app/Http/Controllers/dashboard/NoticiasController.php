<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Feeds;
use App\Entities\Noticia;
use App\Http\Controllers\Controller;
use App\Mail\Consultas;
use Mail;
use App\Entities\User;
use Auth;

class NoticiasController extends Controller
{
    
    public function index()
    {
    	//$data = $this->getrss('https://es.cointelegraph.com/rss', 'https://cointelegraph.com/rss');
      //$data_en = $data['data_en'];
      //$data_es = $data['data_es'];
      //dd($data_en);
      $noticias = Noticia::where('estado','=',1)->where('tipo','=', 'cryptomonedas')->get();
    	return view('dashboard.user.noticias.index', ['noticias' => $noticias]);

    }

    public function select_tipo($tipo)
    {
      //return $tipo;
      $noticias = Noticia::where('estado','=',1)->where('tipo','=', $tipo)->get();
      return view('dashboard.user.noticias.response.tipo', ['noticias' => $noticias]);
    }


    public function getrss($urles, $urlen)
    {
      $cointelegraph_en = Feeds::make([$urlen], 5,true);
      $cointelegraph_es = Feeds::make([$urles],5,true);

       $data_en = array(
        'title'     => $cointelegraph_en->get_title(),
        'permalink' => $cointelegraph_en->get_permalink(),
        'items'     => $cointelegraph_en->get_items(),
      );

      $data_es = array(
        'title'     => $cointelegraph_es->get_title(),
        'permalink' => $cointelegraph_es->get_permalink(),
        'items'     => $cointelegraph_es->get_items(),
      );

      return ['data_en' => $data_en, 'data_es' => $data_es];
    }

    public function mostrar_noticia($id_noticia)
    {

      $noticia = Noticia::findOrFail($id_noticia);
      $es = $noticia->link_es;
      $en = $noticia->link_en;

      $data = $this->getrss($es, $en);
      
      
      if (count($data['data_en']) > 0) {
        $data_en = $data['data_en'];
        $cont_en = count($data['data_en']['items']);
      }else{
        $cont_en = 0;
      }

      if (count($data['data_es']) > 0) {
        $data_es = $data['data_es'];
        $cont_es = count($data['data_es']['items']);
      }else{
        $data_es = 0;
      }


      return view('dashboard.user.noticias.response.noticias', ['data_en' => $data_en,'data_es' => $data_es, 'cont_en' => $cont_en, 'cont_es' => $cont_es]);
    }

    public function enviar_correo(Request $request)
    {
      $data = User::find(Auth::user()->id);
      Mail::to('ilanvaldez34@gmail.com')->send(new Consultas($data));
      return 'se envio correctamente';
    }

}
