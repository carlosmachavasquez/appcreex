<?php

namespace App\Http\Controllers\dashboard;

use App\Entities\User;
use App\Entities\Membresia;
use App\Entities\Pago;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class UsuarioController extends Controller
{
    public function index()
    {
        $usuarios = User::all();
        //dump($usuarios);
        $data = [];
        $dataPagos = [];

        if(!empty($usuarios)):
            foreach ($usuarios as $i => $usuario):
                //dump($usuario->id_referidor);
                $idRef = (isset($usuario->id_referidor)) ? $usuario->id_referidor : '1';
                $userRef = User::findOrFail($idRef);
                //dump($userRef);
                $name = $userRef->name;
                $lastname = $userRef->lastname;
                $data[$i]['name_referido'] = $name.' '.$lastname;

                $membresiaId = (isset($usuario->membresia_id)) ? $usuario->membresia_id : '1';
                $tipoMemb = Membresia::findOrFail($membresiaId);
                $data[$i]['name_membresia'] = $tipoMemb->name;

                $getAllPagos = Pago::where('user_id', '=', $usuario->id)->get();
                if(!empty($getAllPagos)):
                    foreach ($getAllPagos as $i => $pago):
                        $dataPagos[$i]['id'] = $pago->id;
                        $dataPagos[$i]['membresia_id'] = $pago->membresia_id;
                        $dataPagos[$i]['user_id'] = $pago->user_id;
                        $dataPagos[$i]['monto'] = $pago->monto;
                        $dataPagos[$i]['estado'] = $pago->estado;
                        $dataPagos[$i]['created_at'] = $pago->created_at;
                        $dataPagos[$i]['description'] = $pago->description;
                        $dataPagos[$i]['tipo_membresia'] = $pago->tipo_membresia;
                        $dataPagos[$i]['fecha_pago'] = $pago->fecha_pago;
                        $dataPagos[$i]['fecha_activo_hasta'] = $pago->fecha_activo_hasta;
                        $dataPagos[$i]['chargeId'] = $pago->chargeId;
                        $dataPagos[$i]['origen_pago'] = $pago->origen_pago;
                        $dataPagos[$i]['moneda'] = $pago->moneda;
                        $dataPagos[$i]['financiado'] = $pago->financiado;
                    endforeach;
                endif;
                $data[$i]['pago_user'] = $dataPagos;
            endforeach;
        endif;

//        return view('dashboard.admin.usuarios.index',compact('usuarios'))
//            ->with('i', (request()->input('page', 1) - 1) * 10);

//    				->select('users.*', 'membresia.name as membresia')
//    				->where('role', '=', 'user')->orWhere('role', '=', 'invitado')->get();
        //dump($data);
    	return view('dashboard.admin.usuarios.index', compact('usuarios'))
            ->with('data', $data);

//        return view('dashboard.admin.usuarios.index', compact('usuarios'));
    }

    public function export()
    {

        /*$users = User::join('membresia', 'users.membresia_id', '=', 'membresia.id')
                ->select('users.id', 'membresia.name as membresia_id','users.nickname', 'users.name','users.lastname', 'users.email', 'users.cellphone', 'users.id_referidor', DB::raw("SELECT(CONCAT(`name`, ' ', `lastname`) FROM users AS A WHERE A.id = users.id_referidor) as referidor"), 'users.estado', 'users.role', 'users.created_at')
                ->get();*/
        $users = User::all();
        //dump($users);

    	Excel::create('Users', function($excel) {
            $excel->sheet('users', function($sheet) {
                $users = User::join('membresia', 'users.membresia_id', '=', 'membresia.id')
                ->select('users.id as id', 'membresia.name as membresia_id','users.nickname', 'users.name','users.lastname', 'users.email', 'users.cellphone', 'users.id_referidor', 'users.estado', 'users.role', 'users.created_at')
                ->get();
                $users = User::all();
                $sheet->fromArray($users);
                $sheet->row(1, [
                    'Id', 'membresia_id', 'nickname', 'name', 'lastname', 'email', 'cellphone', 'id_referidor', 'estado', 'role', 'fecha de Registro'
                ]);
                $sheet->row(1, function($row) { $row->setBackground('#03B2C8'); });
                foreach($users as $index => $data) {
                    $sheet->row($index+2, [
                        $data->id, $data->membresia_id, $data->nickname, $data->name,$data->lastname, $data->email, $data->cellphone, $data->id_referidor, ($data->estado == 1) ? 'activo' : 'inactivo', $data->role, $data->created_at
                    ]);
                }
            });
        })->export('xls');
    }
}
