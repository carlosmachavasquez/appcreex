<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use GuzzleHttp\Client;
use App\Entities\Portafolio;
use App\Entities\Moneda;
use App\Entities\Operaciones;
use Auth;
use DB;

class PortafolioController extends Controller
{

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://coincap.io/',
            //'base_uri' => 'https://api.coinmarketcap.com/v1/'
            //'timeout' => 2.0,
        ]);

        $this->response = $this->client->request('GET', 'front');//front, ticker
        $this->response = json_decode($this->response->getbody()->getcontents());  
        //precio actual del btc
        $this->precio_actual_btc = $this->getMonedaUni('BTC');   
    }

    public function index()
    {
        //inicializamos con 0  
        $data['portafolio_id'] = 0;
        $cant = 0;
        $portafolio = Portafolio::where('estado', '=', 1)->where('usuario_creacion', '=', Auth::user()->id)->orderBy('id', 'desc')->get();

        $total_moneda = $this->total_moneda();
        $monedas = Moneda::where('estado', '=', 1)->get();
        $operaciones = Operaciones::where('estado', '=', 1)->get();

        return view('dashboard.user.portafolio.index', ['portafolio' => $portafolio, 'cant' => $cant, 'total_moneda' => $total_moneda, 'data' => $data, 'total_moneda' => $total_moneda]);


    }

    

    public function registrar(Request $request)
    {
        
        $portafolio = new Portafolio;
        $portafolio->nombre = $request->nombre;
        $portafolio->estado = 1;
        $portafolio->usuario_creacion = Auth::user()->id;
        $portafolio->usuario_modificacion = Auth::user()->id;
        $portafolio->save();
        return redirect()->back();
    }

    public function getMonedas()
    {

        $client = new Client([
            'base_uri' => 'http://coincap.io/',
        ]);

        $response = $client->request('GET', 'front');
        $monedas = json_decode($response->getbody()->getcontents());
        //return $monedas;
        return view('dashboard.user.portafolio.result.monedas', ['monedas' => $monedas]);
    }

    public function mostrar_portafolio($id)
    {
        $cant = Portafolio::where('id', '=', $id)->where('estado', '=', 1)->where('usuario_creacion', '=', Auth::user()->id)->get()->count();
        if ($cant > 0) {
           
            //portafolio
           $moneda = Moneda::where('estado', '=', 1)->where('portafolio_id', '=', $id)->orderBy('id', 'desc')->get();
           $portafolio = Portafolio::where('estado', '=', 1)->where('usuario_creacion', '=', Auth::user()->id)->orderBy('id', 'desc')->get();

           $data['suma_usd'] = Moneda::Join('operaciones', 'monedas.id', '=', 'operaciones.moneda_id')
                ->where('operaciones.estado', '=', 1)->where('monedas.portafolio_id', '=', $id)
                ->get()
                ->sum('precio');
            $data['portafolio_id'] = $id;

            //para traer el total de la moneda
            $total_moneda = $this->total_moneda_portafolio($id);

           

            return view('dashboard.user.portafolio.index', ['moneda' => $moneda, 'portafolio' => $portafolio, 'cant' => $cant, 'data' => $data, 'total_moneda' => $total_moneda]);
        }else{
            return redirect()->route('user.portafolio.index');
        }
        
    }

    public function getPrecio_avg($id_moneda)
    {
        //cantidad total por moneda
        $c_hold = Operaciones::where('operacion', '=', 0)->where('estado', '=', 1)->where('moneda_id', '=', $id_moneda)->sum('cantidad');
        $c_sell = Operaciones::where('operacion', '=', 1)->where('estado', '=', 1)->where('moneda_id', '=', $id_moneda)->sum('cantidad');
        $cant = $c_hold - $c_sell; //cantidad total hasta el momento

        $ope_hold = Operaciones::where('operacion', '=', 0)->where('estado', '=', 1)->where('moneda_id', '=', $id_moneda)->get();
        $ope_sell = Operaciones::where('operacion', '=', 1)->where('estado', '=', 1)->where('moneda_id', '=', $id_moneda)->get();
        $usd_lote_hold = 0;
        $usd_lote_sell = 0;
        foreach ($ope_hold as $key => $value) {
            $usd_lote_hold = $usd_lote_hold + ($value->cantidad * $value->precio);
        }

        //
        foreach ($ope_sell as $key => $value) {
            $usd_lote_sell = $usd_lote_sell + ($value->cantidad * $value->precio);
        }

        $usd_total = $usd_lote_hold - $usd_lote_sell;
        $pre_base_usd = $usd_total / $cant;


        return $pre_base_usd;
    }


    

    public function total_moneda()
    {
        $ope_hold = Operaciones::where('estado', '=', 1)->where('usuario_creacion', '=', Auth::user()->id)->where('operacion', '=', 0)->get();
        $ope_sell = Operaciones::where('estado', '=', 1)->where('usuario_creacion', '=', Auth::user()->id)->where('operacion', '=', 1)->get();
        $total_hold = 0;
        $total_sell = 0;

        foreach ($ope_hold as $key => $value) {
            $short_hold = Moneda::find($value->moneda_id);
            $pre_actual_hold = $this->getMonedaUni($short_hold->moneda);
            $total_hold = $total_hold + ($value->cantidad * $pre_actual_hold );
        }

        foreach ($ope_sell as $key => $value) {
            $short_sell = Moneda::find($value->moneda_id);
            $pre_actual_sell = $this->getMonedaUni($short_sell->moneda);
            $total_sell = $total_sell + ($value->cantidad * $pre_actual_sell );
        }

        $usd_total = $total_hold - $total_sell;

        $btc = $this->getMonedaUni('BTC');
        $btc_total = $usd_total/$btc;
        
        return ['usd_total' => $usd_total, 'btc_total' => $btc_total];
    }

    public function total_moneda_portafolio($id_portafolio)
    {

        $ope_hold = Operaciones::join('monedas', 'monedas.id', '=', 'operaciones.moneda_id')
                   ->where('operaciones.estado', '=', 1)
                   ->where('operacion', '=', 0)
                   ->where('portafolio_id', '=', $id_portafolio)->get();

        $ope_sell = Operaciones::join('monedas', 'monedas.id', '=', 'operaciones.moneda_id')
                    ->where('operaciones.estado', '=', 1)
                    ->where('operacion', '=', 1)
                    ->where('portafolio_id', '=', $id_portafolio)->get();
        $total_hold = 0;
        $total_sell = 0;

        foreach ($ope_hold as $key => $value) {
            $short_hold = Moneda::find($value->moneda_id);
            $pre_actual_hold = $this->getMonedaUni($short_hold->moneda);
            $total_hold = $total_hold + ($value->cantidad * $pre_actual_hold );
        }

        foreach ($ope_sell as $key => $value) {
            $short_sell = Moneda::find($value->moneda_id);
            $pre_actual_sell = $this->getMonedaUni($short_sell->moneda);
            $total_sell = $total_sell + ($value->cantidad * $pre_actual_sell );
        }

        $usd_total = $total_hold - $total_sell;

        $btc = $this->getMonedaUni('BTC');
        $btc_total = $usd_total/$btc;
        
        return ['usd_total' => $usd_total, 'btc_total' => $btc_total];
    }
   

    public function getMonedaUni($moneda)
    {
        foreach ($this->response as $value) {
            if ($value->short == $moneda) {
                return $value->price;
            }
        }       
    }


    public function eliminar_portafolio(Request $request)
    {
        $id = $request->id;
        $port = Portafolio::findOrFail($id);
        if (DB::table('monedas')->where('portafolio_id', $id)->update(array('estado' => 0))) { 
            $port->estado = 0;
            if ($port->save()) {
                return response()->json(['success' => true, 'port' => $port]);
            }
        }else{
            $port->estado = 0;
            if ($port->save()) {
                return response()->json(['success' => true, 'port' => $port]);
            }
        }
    }

    //autocomplete
    public function autocomplete()
    {
        $client = new Client([
            'base_uri' => 'http://coincap.io',
        ]);

        $response = $client->request('GET', 'front');
        $monedas = json_decode($response->getbody()->getcontents());

        $queries = Product::where(function($query)
        {
            $term = Input::get('term');
            $query->where('name', 'like', '%'.$term.'%');
        })->take(6)->get();
        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->id, 'avatar' =>$query->image,'value' => $query->name];
        }
        return Response::json($results);
    }

    
}
