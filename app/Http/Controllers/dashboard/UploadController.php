<?php

namespace App\Http\Controllers\dashboard;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Image;
use File;

class UploadController extends Controller
{

    public function fileUpload(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        
        $title = $request->get('title');
        $image = $request->file('image');
        
        $input['imagename'] = $title .'.'.$image->getClientOriginalExtension();
        
        $destinationPath = public_path('/uploads/images/thumbnail/');
        
        $img = Image::make($image->getRealPath());

        $img->resize(24, 24, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);

        $destinationPath = public_path('/uploads/images/');

        $image->move($destinationPath, $input['imagename']);
        
        //$this->save();
        //$this->postImage->add($input);
        
        return back()
            ->with('success','Image Upload successful')
            ->with('imageName',$input['imagename']);

    }
}
