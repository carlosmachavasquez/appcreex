<?php

namespace App\Http\Controllers\Demo;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    // Layouts
    public function sidebarLayout()
    {
        return view('dashboard.templates.pages.layouts.sidebar');
    }

    public function iconSidebar()
    {
        return view('dashboard.templates.pages.layouts.icon-sidebar');
    }

    public function horizontalMenu()
    {
        return view('dashboard.templates.pages.layouts.horizontal-menu');
    }

    //Basic UI
    public function buttons()
    {
        return view('dashboard.templates.pages.basic.buttons');
    }

    public function typography()
    {
        return view('dashboard.templates.pages.basic.typography');
    }

    public function tabs()
    {
        return view('dashboard.templates.pages.basic.tabs');
    }

    public function cards()
    {
        return view('dashboard.templates.pages.basic.cards');
    }

    public function tables()
    {
        return view('dashboard.templates.pages.basic.tables');
    }

    public function modals()
    {
        return view('dashboard.templates.pages.basic.modals');
    }

    public function progressBars()
    {
        return view('dashboard.templates.pages.basic.progress-bars');
    }

    //Components

    public function notifications()
    {
        return view('dashboard.templates.pages.components.notifications');
    }

    public function graphs()
    {
        return view('dashboard.templates.pages.components.graphs');
    }

    public function datatables()
    {
        return view('dashboard.templates.pages.components.datatables');
    }

    public function imageCropper()
    {
        return view('dashboard.templates.pages.components.imagecropper');
    }

    public function imageZoom()
    {
        return view('dashboard.templates.pages.components.zoom');
    }

    public function calendar()
    {
        return view('dashboard.templates.pages.components.calendar');
    }

    public function ratings()
    {
        return view('dashboard.templates.pages.components.ratings.star-ratings');
    }

    public function barRatings()
    {
        return view('dashboard.templates.pages.components.ratings.bar-ratings');
    }

    //Charts

    public function chartjs()
    {
        return view('dashboard.templates.pages.charts.chartjs');
    }

    public function sparklineCharts()
    {
        return view('dashboard.templates.pages.charts.sparkline');
    }

    public function amCharts()
    {
        return view('dashboard.templates.pages.charts.amchart');
    }

    public function morrisCharts()
    {
        return view('dashboard.templates.pages.charts.morris');
    }

    public function gaugeCharts()
    {
        return view('dashboard.templates.pages.charts.gauge');
    }

    // Forms
    public function general()
    {
        return view('dashboard.templates.pages.forms.general');
    }

    public function advanced()
    {
        return view('dashboard.templates.pages.forms.advanced');
    }

    public function layouts()
    {
        return view('dashboard.templates.pages.forms.layouts');
    }

    public function validation()
    {
        return view('dashboard.templates.pages.forms.validation');
    }

    public function editors()
    {
        return view('dashboard.templates.pages.forms.editors');
    }

    public function wizards()
    {
        return view('dashboard.templates.pages.forms.wizards');
    }

    public function wizards2()
    {
        return view('dashboard.templates.pages.forms.wizards2');
    }

    public function wizards3()
    {
        return view('dashboard.templates.pages.forms.wizards3');
    }

    // Gallery
    public function galleryGrid()
    {
        return view('dashboard.templates.pages.gallery.grid');
    }

    public function galleryMasonryGrid()
    {
        return view('dashboard.templates.pages.gallery.masonry-grid');
    }

    // Login
    public function login2()
    {
        return view('dashboard.templates.sessions.login-2');
    }

    public function login3()
    {
        return view('dashboard.templates.sessions.login-3');
    }

    // Register
    public function register2()
    {
        return view('dashboard.templates.sessions.register-2');
    }

    public function register3()
    {
        return view('dashboard.templates.sessions.register-3');
    }

    public function nestableList()
    {
        return view('dashboard.templates.pages.components.nestable-list');
    }

    public function nestableTree()
    {
        return view('dashboard.templates.pages.components.nestable-tree');
    }

    // Icons
    public function icoMoons()
    {
        return view('dashboard.templates.pages.icons.ico-moon');
    }

    public function evilIcons()
    {
        return view('dashboard.templates.pages.icons.evil');
    }

    public function meteoIcons()
    {
        return view('dashboard.templates.pages.icons.meteo');
    }

    public function lineIcons()
    {
        return view('dashboard.templates.pages.icons.line');
    }

    public function fpsLineIcons()
    {
        return view('dashboard.templates.pages.icons.fps-line');
    }

    public function fontawesomeIcons()
    {
        return view('dashboard.templates.pages.icons.fontawesome');
    }
}
