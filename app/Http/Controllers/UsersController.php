<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\User;
use Carbon\Carbon;

class UsersController extends Controller
{
    // public function index()
    // {
    //     $users = User::where('role', 'user')->get();

    //     return view('dashboard.templates.users.index')->with('users', $users);
    // }

    // /**
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */

    // public function create()
    // {
    //     return view('dashboard.templates.sessions.register');
    // }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function store(Request $request)
    // {
    //     $data = request()->validate([
    //         'name' => 'required',
    //         'lastname' => 'required',
    //         'email' => ['required', 'email', 'unique:users,email'],
    //         'password' => 'required',
    //         'cellphone' => 'required',
    //         'wallet' => 'string'
    //     ], [
    //         'name.required' => 'El campo nombre es obligatorio',
    //         'lastname.required' => 'El campo apellido es obligatorio',
    //         'cellphone.required' => 'El campo celular es obligatorio'
    //     ]);

    //     User::create([
    //         'name' => $data['name'],
    //         'lastname' => $data['lastname'],
    //         'email' => $data['email'],
    //         'password' => bcrypt($data['password']),
    //         'cellphone' => $data['cellphone'],
    //         'wallet' => ''
    //     ]);

    //     return redirect()->route('user.dashboard');
    // }

    // public function show($id)
    // {
    //     $user = User::findOrFail($id);
    //     return view('dashboard.templates.users.show')->with('user', $user);
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  \App\Product  $product
    //  * @return \Illuminate\Http\Response
    //  */

    // public function edit(Product $product)
    // {
    //     //return view('products.edit',compact('product'));
    // }

    // public function destroy($id)
    // {
    //     $user = User::findOrFail($id);

    //     $user->delete();

    //     flash()->success('User Deleted');

    //     return redirect()->back();
    // }
}
