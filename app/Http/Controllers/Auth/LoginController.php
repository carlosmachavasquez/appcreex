<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Auth;
use App\Entities\Pago;
//use Entities\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard/invitado';

    public function redirectTo()
    {
        if(\Auth::user()->role == 'user')
        {
            return '/dashboard/user';
        }elseif (\Auth::user()->role == 'trader') {
            return '/dashboard/trader';
        }elseif(\Auth::user()->role == 'invitado'){
            $paymentsCount = Pago::where('user_id', '=', \Auth::user()->id)->count();

            if($paymentsCount > 0){

                $lastPagoCancelado = Pago::where('user_id', '=', \Auth::user()->id)->where('estado', '=', 1)->get();
                $estadoPagoCancelado = (isset($lastPagoCancelado->estado)) ? $lastPagoCancelado->estado : '';
                if(!empty($estadoPagoCancelado) && $estadoPagoCancelado === 1){
                    $membresia_id_pago = $estadoPagoCancelado->membresia_id;
                    return route('invitado.payments.buy.membresia', ['membresia' => $membresia_id_pago]);
                }else{
                    $lastPagoSinCancelar = Pago::where('user_id', '=', \Auth::user()->id)->latest()->first();
                    $membresia_id_pago = $lastPagoSinCancelar->membresia_id;
                    return route('invitado.payments.buy.membresia', ['membresia' => $membresia_id_pago]);
                }
            }else{
                return '/dashboard/invitado';
            }
        }else {
            return '/dashboard/admin';
        }
    }
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
