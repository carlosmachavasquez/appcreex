<?php

namespace App\Http\Controllers\Auth;

use App\Entities\User;
use App\Entities\Usuario;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard/invitado';

//    protected function redirectTo()
//    {
//        if(\Auth::user()->role == 'user')
//        {
//            return '/dashboard/user';
//        }elseif (\Auth::user()->role == 'trader') {
//            return '/dashboard/trader';
//        }elseif(\Auth::user()->role == 'invitado'){
//            return '/dashboard/invitado';
//        }else {
//            return '/dashboard/admin';
//        }
//    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nickname' => 'required|string|max:255|unique:users',
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'cellphone' => 'required|string|max:255',
            'wallet' => 'string|max:255'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //creamos el usuario para el sistema de referidos
        $vars = array(
            "prik" => "5b3eb8796868903656fae73a7ebd56bb",
            "pubk" => "4d0b0ed9548dbc01dcd93f5d952e5fb8",
            "m" => "registro",
            "cmd" => "agregar",
            "nombre" => $data['name'],
            "apellido" => $data['lastname'],
            "rut" => '',
            "usuario" => $data['nickname'],
            "correo" => $data['email'],
            "password" => $data['password'],
            "repassword" => $data['password'],
            "direccion" => '',
            "ciudad" => '',
            "region" => '',
            "telefono" => $data['cellphone'],
            "pais" => 'PE',//peru
            "referido" => $data['referido'], //su nickname
            "sendmail" => TRUE
        );
        # GENERAR UNA CADENA CODIFICADA ESTILO URL
        $urlvars = http_build_query($vars);                        
        # ENVIANDO POR METODO POST USANDO cURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://grupocreex.net/json-data.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $urlvars);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $resultado_post = curl_exec ($ch);
        curl_close ($ch);

        $respuesta_post = json_decode($resultado_post, true);

        if ($respuesta_post['response'] != false) {
            //crear usuario
            return User::create([
                'nickname' => $data['nickname'],
                'name' => $data['name'],
                'lastname' => $data['lastname'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'cellphone' => $data['cellphone'],
                'wallet' => '',
                'role' => 'invitado',
                'membresia_id' => 1,
                'id_referidor' => $data['id_referidor']
            ]);
        }else{
            return redirect()->back();
        }
    }


    



}
