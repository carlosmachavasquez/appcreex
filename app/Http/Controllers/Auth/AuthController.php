<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Entities\User;
use Socialite;

class AuthController extends Controller
{
    public function login()
    {
        return view('dashboard.templates.sessions.login');
    }

    public function postLogin(Requests\LoginRequest $request)
    {
        if (User::login($request)) {
            flash('Welcome to CreexApp')->success();
            //flash()->success('Welcome to CreexApp');

            if (Auth::user()->isUser()) {
                return redirect()->to('/dashboard/user');
            }elseif(Auth::user()->isInvitado()){
                return redirect()->to('/dashboard/invitado');
            }else {
                return redirect()->to('/');
            }
        }

        //flash()->error('Invalid Login Credentials');
        flash('Invalid Login Credentials')->error();

        return redirect()->back();
    }

    public function logOut()
    {
        Auth::logout();

        return redirect()->to('/login');
    }

    public function register()
    {
        return view('dashboard.templates.sessions.register');
    }

    /**
     * Redirect the user to the authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $provider_user = Socialite::driver($provider)->user();

        $user = $this->findUserByProviderOrCreate($provider, $provider_user);

        auth()->login($user);

        //flash()->success('Welcome to CreexApp.');

        return redirect()->to('/user');
    }

    private function findUserByProviderOrCreate($provider, $provider_user)
    {
        $user = User::where($provider . '_id', $provider_user->token)
            ->orWhere('email', $provider_user->email)
            ->first();


        if (!$user) {
            $user = User::create([
                'name' => $provider_user->name,
                'email' => $provider_user->email,
                $provider . '_id' => $provider_user->token
            ]);
        } else {
            // Update the token on each login request
            $user[$provider . '_id'] = $provider_user->token;
            $user->save();
        }

        return $user;
    }
}
