<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Membresia extends Model
{
    protected $table = 'membresia';

    protected $fillable = ['id','name', 'precio_mes', 'precio_anual'];
}