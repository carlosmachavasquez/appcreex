<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    protected $table = 'productos';

    protected $fillable = ['id','nombre', 'imagen', 'resumen', 'descripcion', 'precio','membresia', 'estado'];
}
