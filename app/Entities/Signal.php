<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Signal extends Model
{
    use Notifiable;
    
    protected $table = 'signals';

    protected $fillable = ['coinname', 'coinsymbol', 'coinimgurl', 'exchange', 'cointradeurl', 'coinprice',
        'target1', 'target2', 'target3', 'stoploss', 'membresia_id', 'pricet30m', 'pricet1h', 'pricet3h', 'pricet6h', 'pricet12h', 'pricet24h', 'pricet48h', 'flag',
        'pricet30mpercent', 'pricet1hpercent', 'pricet3hpercent', 'pricet6hpercent', 'pricet12hpercent', 'pricet24hpercent', 'pricet48hpercent', 'priceAverage', 'priceAveragePercent', 'rangeprice'
    ];

    /**
     * Get the membresia record associated with the signal.
     */
    public function membresia()
    {
        return $this->hasOne('App\Entities\Membresia');
    }
}