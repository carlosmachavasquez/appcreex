<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    protected $table = 'noticia';

    protected $fillable = ['id','titulo','link_es', 'link_en', 'estado', 'usuario_creacion','usuario_modificacion'];
}
