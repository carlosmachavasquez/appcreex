<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $table = 'curso';

    protected $fillable = ['id','membresia','titulo', 'nivel', 'descripcion', 'video', 'imagen', 'estado', 'usuario_creacion','usuario_modificacion', 'educategoria_id'];
}
