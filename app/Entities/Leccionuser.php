<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Leccionuser extends Model
{
    protected $table = 'leccion_usuario';

    protected $fillable = ['id','visto', 'usuario', 'estado', 'usuario_creacion','usuario_modificacion', 'leccion_id'];
}
