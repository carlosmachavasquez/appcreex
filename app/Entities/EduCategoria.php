<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class EduCategoria extends Model
{
    protected $table = 'educategoria';

    	protected $fillable = ['id','nombre','descripcion', 'estado', 'usuario_creacion','usuario_modificacion'];
}
