<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'password', 'cellphone', 'wallet','estado', 'nivel','nickname','id_referidor','cnivel1','cnivel2','cnivel3','role','membresia_id','facebook_id', 'google_id', 'github_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isUser()
    {
        return ($this->role == 'user');
    }

    public function isTrader()
    {
        return ($this->role == 'trader');
    }

    public function isAdmin()
    {
        return ($this->role == 'admin');
    }

    public function isInvitado()
    {
        return ($this->role == 'invitado');
    }

    public static function login($request)
    {
        $remember = $request->remember;
        $email = $request->email;
        $password = $request->password;
        return (\Auth::attempt(['email' => $email, 'password' => $password], $remember));
    }

    /**
     * Get the membresia record associated with the user.
     */
    public function membresia()
    {
        return $this->hasOne('App\Entities\Membresia');
    }
}
