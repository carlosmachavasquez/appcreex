<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Leccion extends Model
{
    protected $table = 'leccion';

    protected $fillable = ['id','titulo', 'descripcion', 'video', 'duracion', 'estado', 'usuario_creacion','usuario_modificacion', 'curso_id'];
}
