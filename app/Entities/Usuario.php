<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuarios';
    public $timestamps = false;

    protected $fillable = ['id','nombre', 'apellido', 'rut','correo', 'usuario','pass', 'sexo', 'imagen', 'direccion', 'ciudad', 'region', 'telefono', 'id_grupo', 'estado'];
}
