<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Operaciones extends Model
{
    protected $table = 'operaciones';

    protected $fillable = ['id','cantidad', 'fecha', 'precio', 'estado', 'usuario_creacion','usuario_modificacion', 'moneda_id'];
}
