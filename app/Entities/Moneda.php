<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Moneda extends Model
{
    protected $table = 'monedas';

    protected $fillable = ['id','moneda', 'estado', 'usuario_creacion','usuario_modificacion', 'portafolio_id'];
}
