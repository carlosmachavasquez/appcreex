<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Archivo extends Model
{
    protected $table = 'archivo';

    protected $fillable = ['id','archivo', 'estado', 'usuario_creacion','usuario_modificacion', 'leccion_id'];
}
