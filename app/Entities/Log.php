<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'log';

    protected $fillable = ['id','usuario', 'fecha', 'ruta', 'descripcion', 'membresia'];
}
