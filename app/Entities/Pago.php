<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $table = 'pagos';

    protected $fillable = ['id','monto', 'estado', 'user_id','membresia_id', 'description', 'tipo_membresia', 'fecha_pago', 'fecha_activo_hasta', 'chargeId', 'origen_pago', 'moneda', 'financiado'];
}
