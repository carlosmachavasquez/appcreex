<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Cargos extends Model
{
    protected $table = 'cargos';

    protected $fillable = ['id','membresia_id', 'user_id', 'code_charge'];
}
