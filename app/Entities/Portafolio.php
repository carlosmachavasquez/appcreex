<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Portafolio extends Model
{
    protected $table = 'portafolio';

    protected $fillable = ['id','nombre', 'estado', 'usuario_creacion','usuario_modificacion'];
}
