<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Forex extends Model
{
    protected $table = 'forex';

    protected $fillable = ['id','pair', 'order_type', 'market_execution', 'sell_limit', 'stop_loss', 'take_prof1', 'take_prof2', 'take_prof3'];
}
