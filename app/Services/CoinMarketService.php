<?php

namespace App\Services;

use \App\Facades\HelperCreex;
use Carbon\Carbon;
use App\Entities\Signal;
use GuzzleHttp\Client;

class CoinMarketService
{
    public function setPriceByTimePassWithId($signal)
    {
        $now = Carbon::now();
        $createAtSignal = $signal->created_at;
        $data = [];
        $diffinminutes = $now->diffInRealMinutes($createAtSignal);
        $diffinhours = $now->diffInRealHours($createAtSignal);
        $arrPrices = [];

        $price30mpasscreate = $price1hpasscreate = $price3hpasscreate = $price6hpasscreate = $price12hpasscreate = $price24hpasscreate = $price48hpasscreate = "--";
        $price30mpasscreatePercent = $price1hpasscreatePercent = $price3hpasscreatePercent = $price6hpasscreatePercent = $price12hpasscreatePercent = $price24hpasscreatePercent = $price48hpasscreatePercent = "--";
        $data['price30mpasscreate'] = $data['price1hpasscreate'] = $data['price3hpasscreate'] = $data['price6hpasscreate'] = $data['price12hpasscreate'] = $data['price24hpasscreate'] = $data['price48hpasscreate'] = "--";
        $data['price30mpasscreatePercent'] = $data['price1hpasscreatePercent'] = $data['price3hpasscreatePercent'] = $data['price6hpasscreatePercent'] = $data['price12hpasscreatePercent'] = $data['price24hpasscreatePercent'] = $data['price48hpasscreatePercent'] = $data['priceAverage'] = $data['priceAveragePercent'] = "--";
        $data['price30mclass'] = $data['price1hclass'] = $data['price3hclass'] = $data['price6hclass'] = $data['price12hclass'] = $data['price24hclass'] = $data['price48hclass']  = $data['priceAverageclass'] = "";

        if ($diffinminutes >= 30) {
            $createAtSignal = $signal->created_at;
            $passto30m = $createAtSignal->addMinutes(30);
            $passto30mtimestamp = $passto30m->timestamp;
            $price30mpasscreate = $this->getRequestForTimestamp($signal->id, $passto30mtimestamp, 'pricet30m', $diffinminutes);
            $price30mpasscreatePercent = $this->getPercentByValue($signal->coinprice, $price30mpasscreate, false);
            $data['price30mpasscreate'] = ($price30mpasscreate == 0) ? '--' : $price30mpasscreate;
            $data['price30mpasscreatePercent'] = ($price30mpasscreatePercent == -100) ? '--' : $price30mpasscreatePercent;
            array_push($arrPrices, $data['price30mpasscreate']);
            if ($price30mpasscreatePercent == 0) {
                $data['price30mclass'] = 'badge-info';
            } else if ($price30mpasscreatePercent > 0) {
                $data['price30mclass'] = 'badge-success';
            } else {
                $data['price30mclass'] = 'badge-danger';
            }

            if ($diffinhours >= 1) {
                $createAtSignal = $signal->created_at;
                $passto1h = $createAtSignal->addHour();
                $diffinminutes = $now->diffInRealMinutes($passto1h);
                $passto1htimestamp = $passto1h->timestamp;
                $price1hpasscreate = $this->getRequestForTimestamp($signal->id, $passto1htimestamp, 'pricet1h', $diffinminutes);
                $price1hpasscreatePercent = $this->getPercentByValue($signal->coinprice, $price1hpasscreate, false);
                $data['price1hpasscreate'] = ($price1hpasscreate == 0) ? '--' : $price1hpasscreate;
                $data['price1hpasscreatePercent'] = ($price1hpasscreatePercent == -100) ? '--' : $price1hpasscreatePercent;
                array_push($arrPrices, $data['price1hpasscreate']);
                if ($price1hpasscreatePercent == 0) {
                    $data['price1hclass'] = 'badge-info';
                } else if ($price1hpasscreatePercent > 0) {
                    $data['price1hclass'] = 'badge-success';
                } else {
                    $data['price1hclass'] = 'badge-danger';
                }

                if ($diffinhours >= 3) {
                    $createAtSignal = $signal->created_at;
                    $passto3h = $createAtSignal->addHours(3);
                    $diffinminutes = $now->diffInRealMinutes($passto3h);
                    $passto3htimestamp = $passto3h->timestamp;
                    $price3hpasscreate = $this->getRequestForTimestamp($signal->id, $passto3htimestamp, 'pricet3h', $diffinminutes);
                    $price3hpasscreatePercent = $this->getPercentByValue($signal->coinprice, $price3hpasscreate, false);
                    $data['price3hpasscreate'] = ($price3hpasscreate == 0) ? '--' : $price3hpasscreate;
                    $data['price3hpasscreatePercent'] = ($price3hpasscreatePercent == -100) ? '--' : $price3hpasscreatePercent;
                    array_push($arrPrices, $data['price3hpasscreate']);
                    if ($price3hpasscreatePercent == 0) {
                        $data['price3hclass'] = 'badge-info';
                    } else if ($price3hpasscreatePercent > 0) {
                        $data['price3hclass'] = 'badge-success';
                    } else {
                        $data['price3hclass'] = 'badge-danger';
                    }

                    if ($diffinhours >= 6) {
                        $createAtSignal = $signal->created_at;
                        $passto6h = $createAtSignal->addHours(6);
                        $diffinminutes = $now->diffInRealMinutes($passto6h);
                        $passto6htimestamp = $passto6h->timestamp;
                        $price6hpasscreate = $this->getRequestForTimestamp($signal->id, $passto6htimestamp, 'pricet6h', $diffinminutes);
                        $price6hpasscreatePercent = $this->getPercentByValue($signal->coinprice, $price6hpasscreate, false);
                        $data['price6hpasscreate'] = ($price6hpasscreate == 0) ? '--' : $price6hpasscreate;
                        $data['price6hpasscreatePercent'] = ($price6hpasscreatePercent == -100) ? '--' : $price6hpasscreatePercent;
                        array_push($arrPrices, $data['price6hpasscreate']);
                        if ($price6hpasscreatePercent == 0) {
                            $data['price6hclass'] = 'badge-info';
                        } else if ($price6hpasscreatePercent > 0) {
                            $data['price6hclass'] = 'badge-success';
                        } else {
                            $data['price6hclass'] = 'badge-danger';
                        }

                        if ($diffinhours >= 12) {
                            $createAtSignal = $signal->created_at;
                            $passto12h = $createAtSignal->addHours(12);
                            $diffinminutes = $now->diffInRealMinutes($passto12h);
                            $passto12htimestamp = $passto12h->timestamp;
                            $price12hpasscreate = $this->getRequestForTimestamp($signal->id, $passto12htimestamp, 'pricet12h', $diffinminutes);
                            $price12hpasscreatePercent = $this->getPercentByValue($signal->coinprice, $price12hpasscreate, false);
                            $data['price12hpasscreate'] = ($price12hpasscreate == 0) ? '--' : $price12hpasscreate;
                            $data['price12hpasscreatePercent'] = ($price12hpasscreatePercent == -100) ? '--' : $price12hpasscreatePercent;
                            array_push($arrPrices, $data['price12hpasscreate']);
                            if ($price12hpasscreatePercent == 0) {
                                $data['price12hclass'] = 'badge-info';
                            } else if ($price12hpasscreatePercent > 0) {
                                $data['price12hclass'] = 'badge-success';
                            } else {
                                $data['price12hclass'] = 'badge-danger';
                            }

                            if ($diffinhours >= 24) {
                                $createAtSignal = $signal->created_at;
                                $passto24h = $createAtSignal->addHours(24);
                                $diffinminutes = $now->diffInRealMinutes($passto24h);
                                $passto24htimestamp = $passto24h->timestamp;
                                $price24hpasscreate = $this->getRequestForTimestamp($signal->id, $passto24htimestamp, 'pricet24h', $diffinminutes);
                                $price24hpasscreatePercent = $this->getPercentByValue($signal->coinprice, $price24hpasscreate, false);
                                $data['price24hpasscreate'] = ($price24hpasscreate == 0) ? '--' : $price24hpasscreate;
                                $data['price24hpasscreatePercent'] = ($price24hpasscreatePercent == -100) ? '--' : $price24hpasscreatePercent;
                                array_push($arrPrices, $data['price24hpasscreate']);
                                if ($price24hpasscreatePercent == 0) {
                                    $data['price24hclass'] = 'badge-info';
                                } else if ($price24hpasscreatePercent > 0) {
                                    $data['price24hclass'] = 'badge-success';
                                } else {
                                    $data['price24hclass'] = 'badge-danger';
                                }

                                if ($diffinhours >= 48) {
                                    $createAtSignal = $signal->created_at;
                                    $passto48h = $createAtSignal->addHours(48);
                                    $diffinminutes = $now->diffInRealMinutes($passto48h);
                                    $passto48htimestamp = $passto48h->timestamp;
                                    $price48hpasscreate = $this->getRequestForTimestamp($signal->id, $passto48htimestamp, 'pricet48h', $diffinminutes);
                                    $price48hpasscreatePercent = $this->getPercentByValue($signal->coinprice, $price48hpasscreate, false);
                                    $data['price48hpasscreate'] = ($price48hpasscreate == 0) ? '--' : $price48hpasscreate;
                                    $data['price48hpasscreatePercent'] = ($price48hpasscreatePercent == -100) ? '--' : $price48hpasscreatePercent;
                                    array_push($arrPrices, $data['price48hpasscreate']);
                                    if ($price48hpasscreatePercent == 0) {
                                        $data['price48hclass'] = 'badge-info';
                                    } else if ($price48hpasscreatePercent > 0) {
                                        $data['price48hclass'] = 'badge-success';
                                    } else {
                                        $data['price48hclass'] = 'badge-danger';
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }

        if (count($arrPrices)) {
            if (is_numeric(max($arrPrices))){
                $data['priceAverage'] = max($arrPrices);
                $data['priceAveragePercent'] = $this->getPercentByValue($signal->coinprice, max($arrPrices), false);
                if ($data['priceAveragePercent'] == 0) {
                    $data['priceAverageclass'] = 'badge-info';
                } else if ($data['priceAveragePercent'] > 0) {
                    $data['priceAverageclass'] = 'badge-success';
                } else {
                    $data['priceAverageclass'] = 'badge-danger';
                }
            }
        }

        return $data;
    }

    public function getRequestForTimestamp($id, $passtimestamp, $namefield, $limit)
    {
        $signal = Signal::findOrFail($id);
        $timeRound = $this->timeRoundInMinutes($passtimestamp);
        //dump($namefield);
        //dump($timeRound);
        $seconds = $this->getSecondsByTime($passtimestamp);
        $limit = $limit + 1;
        //dump($limit);
        if (is_null($signal->$namefield) || ($signal->$namefield == 0) ){

            $client = new Client(['base_uri' => 'https://min-api.cryptocompare.com/data/histominute']);
            $response = $client->request('GET', '?fsym='.$signal->coinsymbol.'&tsym=BTC&limit='.$limit);
            $response = json_decode($response->getbody()->getcontents());
            $minutes = $response->Data;
            //dump($minutes);
            $price = 0;

            foreach ($minutes as $i => $minute):
                if ( $minute->time == $timeRound ){
                    if ($seconds >= 30){
                        $price = $minute->close;
                    }else{
                        $price = $minute->open;
                    }
                    break;
                }
            endforeach;

            $price = number_format($price, 8);
            //Save data in table
            Signal::findOrFail($id)->update([$namefield=>$price, 'flag'=>'B']);
            return $price;

        }else{
            return $signal->$namefield;
        }
    }

    public function getFormatSinceTime($date)
    {
        $data = HelperCreex::formatPassedTime($date);

        return $data;
    }

    public function getPercentByValue($reference, $value, $activate)
    {
        $diff = $value - $reference;

        if ($activate) {
            $diff = abs($value - $reference);
        }

        $percent = ($diff*100)/$reference;
        $percent = number_format($percent, 2);
        return $percent;
        //return round($percent);
    }
    public function timeRoundInMinutes($passtimestamp)
    {
        $dateInCarbon = Carbon::createFromTimestamp($passtimestamp);
        $getSeconds = $dateInCarbon->second;
        if ($getSeconds >= 30){
            $secondsToAdd = 60 - $getSeconds;
            $dateInCarbon-> addSeconds($secondsToAdd);
        }else{
            $secondsToRemove = $getSeconds;
            $dateInCarbon-> subSeconds($secondsToRemove);
        }
        return $dateInCarbon->timestamp;
    }
    public function getSecondsByTime($passtimestamp)
    {
        $dateInCarbon = Carbon::createFromTimestamp($passtimestamp);
        $getSeconds = $dateInCarbon->second;
        return $getSeconds;
    }
}
