<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 5/19/18
 * Time: 11:02 PM
 */

namespace App\Services;

use Carbon\Carbon;
use App\Entities\Signal;
use GuzzleHttp\Client;

class SignalsService
{
    public function setPricesByTimePass($signals)
    {
        $data = [];
        foreach ($signals as $i => $signal):

            $now = Carbon::now();
            $createAtSignal = $signal->created_at;
            //dump($createAtSignal);
            //dump($signal);
            $diffinminutes = $now->diffInRealMinutes($createAtSignal);
            $diffinhours = $now->diffInRealHours($createAtSignal);
            $arrPrices = [];

//            $price30mpasscreate = $price1hpasscreate = $price3hpasscreate = $price6hpasscreate = $price12hpasscreate = $price24hpasscreate = $price48hpasscreate = "--";
//            $price30mpasscreatePercent = $price1hpasscreatePercent = $price3hpasscreatePercent = $price6hpasscreatePercent = $price12hpasscreatePercent = $price24hpasscreatePercent = $price48hpasscreatePercent = "--";
            $data[$i]['price30mpasscreate'] = $data[$i]['price1hpasscreate'] = $data[$i]['price3hpasscreate'] = $data[$i]['price6hpasscreate'] = $data[$i]['price12hpasscreate'] = $data[$i]['price24hpasscreate'] = $data[$i]['price48hpasscreate'] = "--";
            $data[$i]['price30mpasscreatePercent'] = $data[$i]['price1hpasscreatePercent'] = $data[$i]['price3hpasscreatePercent'] = $data[$i]['price6hpasscreatePercent'] = $data[$i]['price12hpasscreatePercent'] = $data[$i]['price24hpasscreatePercent'] = $data[$i]['price48hpasscreatePercent'] = $data[$i]['priceAverage'] = $data[$i]['priceAveragePercent'] = "--";
            $data[$i]['price30mclass'] = $data[$i]['price1hclass'] = $data[$i]['price3hclass'] = $data[$i]['price6hclass'] = $data[$i]['price12hclass'] = $data[$i]['price24hclass'] = $data[$i]['price48hclass']  = $data[$i]['priceAverageclass'] = "";

            if ( $diffinminutes >= 30 ) {
                if( is_null($signal->pricet30m) or ($signal->pricet30m == "--") ){
                    $dataProcess = $this->processPriceData('addMinutes', 'pricet30m', 30, $signal, $diffinminutes);
                    $data[$i]['price30mpasscreate'] = $dataProcess[0];
                    $data[$i]['price30mpasscreatePercent'] = $dataProcess[1];
                }else{
                    $data[$i]['price30mpasscreate'] = $signal->pricet30m;
                    $data[$i]['price30mpasscreatePercent'] = $signal->pricet30mpercent;
                }

                array_push( $arrPrices, $data[$i]['price30mpasscreate'] );

                if ($data[$i]['price30mpasscreatePercent'] == 0) {
                    $data[$i]['price30mclass'] = 'badge-info';
                } else if ($data[$i]['price30mpasscreatePercent'] > 0) {
                    $data[$i]['price30mclass'] = 'badge-success';
                } else {
                    $data[$i]['price30mclass'] = 'badge-danger';
                }
            }

            if ( $diffinminutes >= 60 ) {
                if( is_null($signal->pricet1h) or ($signal->pricet1h == "--") ){

                    $dataProcess = $this->processPriceData('addMinutes', 'pricet1h', 60, $signal, $diffinminutes);
                    //dump($dataProcess);
                    $data[$i]['price1hpasscreate'] = $dataProcess[0];
                    $data[$i]['price1hpasscreatePercent'] = $dataProcess[1];
                }else{
                    $data[$i]['price1hpasscreate'] = $signal->pricet1h;
                    $data[$i]['price1hpasscreatePercent'] = $signal->pricet1hpercent;
                }

                array_push( $arrPrices, $data[$i]['price1hpasscreate'] );

                if ($data[$i]['price1hpasscreatePercent'] == 0) {
                    $data[$i]['price1hclass'] = 'badge-info';
                } else if ($data[$i]['price1hpasscreatePercent'] > 0) {
                    $data[$i]['price1hclass'] = 'badge-success';
                } else {
                    $data[$i]['price1hclass'] = 'badge-danger';
                }
            }

            if ( $diffinhours >= 3 ) {
                if( is_null($signal->pricet3h) or ($signal->pricet3h == "--") ){
                    $dataProcess = $this->processPriceData('addHours', 'pricet3h', 3, $signal, $diffinminutes);
                    $data[$i]['price3hpasscreate'] = $dataProcess[0];
                    $data[$i]['price3hpasscreatePercent'] = $dataProcess[1];
                }else{
                    $data[$i]['price3hpasscreate'] = $signal->pricet3h;
                    $data[$i]['price3hpasscreatePercent'] = $signal->pricet3hpercent;
                }

                array_push( $arrPrices, $data[$i]['price3hpasscreate'] );

                if ($data[$i]['price3hpasscreatePercent'] == 0) {
                    $data[$i]['price3hclass'] = 'badge-info';
                } else if ($data[$i]['price3hpasscreatePercent'] > 0) {
                    $data[$i]['price3hclass'] = 'badge-success';
                } else {
                    $data[$i]['price3hclass'] = 'badge-danger';
                }
            }

            if ( $diffinhours >= 6 ) {
                if( is_null($signal->pricet6h) or ($signal->pricet6h == "--") ){
                    $dataProcess = $this->processPriceData('addHours', 'pricet6h', 6, $signal, $diffinminutes);
                    $data[$i]['price6hpasscreate'] = $dataProcess[0];
                    $data[$i]['price6hpasscreatePercent'] = $dataProcess[1];
                }else{
                    $data[$i]['price6hpasscreate'] = $signal->pricet6h;
                    $data[$i]['price6hpasscreatePercent'] = $signal->pricet6hpercent;
                }

                array_push( $arrPrices, $data[$i]['price6hpasscreate'] );

                if ($data[$i]['price6hpasscreatePercent'] == 0) {
                    $data[$i]['price6hclass'] = 'badge-info';
                } else if ($data[$i]['price6hpasscreatePercent'] > 0) {
                    $data[$i]['price6hclass'] = 'badge-success';
                } else {
                    $data[$i]['price6hclass'] = 'badge-danger';
                }
            }

            if ( $diffinhours >= 12 ) {
                if( is_null($signal->pricet12h) or ($signal->pricet12h == "--") ){
                    $dataProcess = $this->processPriceData('addHours', 'pricet12h', 12, $signal, $diffinminutes);
                    $data[$i]['price12hpasscreate'] = $dataProcess[0];
                    $data[$i]['price12hpasscreatePercent'] = $dataProcess[1];
                }else{
                    $data[$i]['price12hpasscreate'] = $signal->pricet12h;
                    $data[$i]['price12hpasscreatePercent'] = $signal->pricet12hpercent;
                }

                array_push( $arrPrices, $data[$i]['price12hpasscreate'] );

                if ($data[$i]['price12hpasscreatePercent'] == 0) {
                    $data[$i]['price12hclass'] = 'badge-info';
                } else if ($data[$i]['price12hpasscreatePercent'] > 0) {
                    $data[$i]['price12hclass'] = 'badge-success';
                } else {
                    $data[$i]['price12hclass'] = 'badge-danger';
                }
            }

            if ( $diffinhours >= 24 ) {
                if( is_null($signal->pricet24h) or ($signal->pricet24h == "--") ){
                    $dataProcess = $this->processPriceData('addHours', 'pricet24h', 24, $signal, $diffinminutes);
                    $data[$i]['price24hpasscreate'] = $dataProcess[0];
                    $data[$i]['price24hpasscreatePercent'] = $dataProcess[1];
                }else{
                    $data[$i]['price24hpasscreate'] = $signal->pricet24h;
                    $data[$i]['price24hpasscreatePercent'] = $signal->pricet24hpercent;
                }

                array_push( $arrPrices, $data[$i]['price24hpasscreate'] );

                if ($data[$i]['price24hpasscreatePercent'] == 0) {
                    $data[$i]['price24hclass'] = 'badge-info';
                } else if ($data[$i]['price24hpasscreatePercent'] > 0) {
                    $data[$i]['price24hclass'] = 'badge-success';
                } else {
                    $data[$i]['price24hclass'] = 'badge-danger';
                }
            }

            if ( $diffinhours >= 48 ) {
                if( is_null($signal->pricet48h) or ($signal->pricet48h == "--") ){
                    $dataProcess = $this->processPriceData('addHours', 'pricet48h', 48, $signal, $diffinminutes);
                    $data[$i]['price48hpasscreate'] = $dataProcess[0];
                    $data[$i]['price48hpasscreatePercent'] = $dataProcess[1];
                }else{
                    $data[$i]['price48hpasscreate'] = $signal->pricet48h;
                    $data[$i]['price48hpasscreatePercent'] = $signal->pricet48hpercent;
                }

                array_push( $arrPrices, $data[$i]['price48hpasscreate'] );

                if ($data[$i]['price48hpasscreatePercent'] == 0) {
                    $data[$i]['price48hclass'] = 'badge-info';
                } else if ($data[$i]['price48hpasscreatePercent'] > 0) {
                    $data[$i]['price48hclass'] = 'badge-success';
                } else {
                    $data[$i]['price48hclass'] = 'badge-danger';
                }
            }

            if ( count($arrPrices) > 0 ) {
                if (is_numeric(max($arrPrices))){

                    if ($signal->priceAverage != max($arrPrices) ){
                        Signal::findOrFail($signal->id)->update(['priceAverage'=>max($arrPrices)]);
                        $data[$i]['priceAverage'] = max($arrPrices);
                        $data[$i]['priceAveragePercent'] = $this->getPercentByValue($signal->id, $signal->coinprice, 'priceAveragePercent', max($arrPrices), false);
                    }else{
                        $data[$i]['priceAverage'] = $signal->priceAverage;
                        $data[$i]['priceAveragePercent'] = $signal->priceAveragePercent;
                    }

                    if ($data[$i]['priceAveragePercent'] == 0) {
                        $data[$i]['priceAverageclass'] = 'badge-info';
                    } else if ($data[$i]['priceAveragePercent'] > 0) {
                        $data[$i]['priceAverageclass'] = 'badge-success';
                    } else {
                        $data[$i]['priceAverageclass'] = 'badge-danger';
                    }
                }
            }

        endforeach;

        //$data = array_reverse($data);
        return $data;
    }

    public function processPriceData($timetoadd, $namefield, $valuetoadd, $signal, $diffinminutes)
    {
        $passto = '';
        $dataArr = [];
        $createAtSignal = $signal->created_at;

        if($timetoadd=='addMinutes'){
            $passto = $createAtSignal->addMinutes($valuetoadd);
        }else if($timetoadd=='addHours'){
            $passto = $createAtSignal->addHours($valuetoadd);
        }

        $passtotimestamp = $passto->timestamp;

        $pricepasscreate = $this->getRequestForTimestamp($signal->id, $passtotimestamp, $namefield, $diffinminutes);
        $pricepasscreatePercent = $this->getPercentByValue($signal->id, $signal->coinprice, $namefield.'percent', $pricepasscreate, false);

        //$data[$i]['price30mpasscreate'] = ($price30mpasscreate == 0) ? '--' : $price30mpasscreate;
        //$data[$i]['price30mpasscreate'] = $pricepasscreate;
        array_push($dataArr, $pricepasscreate);

        //$data[$i]['price30mpasscreatePercent'] = ($price30mpasscreatePercent == -100) ? '--' : $price30mpasscreatePercent;
        //$data[$i]['price30mpasscreatePercent'] = $pricepasscreatePercent;
        array_push($dataArr, $pricepasscreatePercent);


        if ($pricepasscreatePercent == 0) {
            array_push($dataArr, 'badge-info');
        } else if ($pricepasscreatePercent > 0) {
            array_push($dataArr, 'badge-success');
        } else {
            array_push($dataArr, 'badge-danger');
        }

        return $dataArr;
    }

    public function getRequestForTimestamp($id, $passtimestamp, $namefield, $limit)
    {
        $signal = Signal::findOrFail($id);
        $timeRound = $this->timeRoundInMinutes($passtimestamp);
        //dump($namefield);
        //dump($timeRound);
        $seconds = $this->getSecondsByTime($passtimestamp);
        $limit = $limit + 1;
        //dump($limit);
        if (is_null($signal->$namefield) || ($signal->$namefield == 0) ){

            $client = new Client(['base_uri' => 'https://min-api.cryptocompare.com/data/histominute']);
            $response = $client->request('GET', '?fsym='.$signal->coinsymbol.'&tsym=BTC&limit='.$limit);
            $response = json_decode($response->getbody()->getcontents());
            $minutes = $response->Data;
            $price = 0;

            foreach ($minutes as $i => $minute):
                if ( $minute->time == $timeRound ){
                    if ($seconds >= 30){
                        $price = $minute->close;
                    }else{
                        $price = $minute->open;
                    }
                    break;
                }
            endforeach;

            $price = number_format($price, 8);
            //Save data in table
            $price = ($price == 0) ? '--' : $price;
            Signal::findOrFail($id)->update([$namefield=>$price, 'flag'=>'B']);
            return $price;

        }else{
            return $signal->$namefield;
        }
    }

    public function getPercentByValue($id, $reference, $namefield, $value, $activate)
    {
        if ($value == "--"){
            $value = 0;
        }

        $diff = $value - $reference;

        if ($activate) {
            $diff = abs($value - $reference);
        }

        $percent = ($diff*100)/$reference;
        $percent = number_format($percent, 2);
        $percent = ($percent == -100) ? '--' : $percent;

        Signal::findOrFail($id)->update([$namefield=>$percent]);
        return $percent;
        //return round($percent);
    }

    public function timeRoundInMinutes($passtimestamp)
    {
        $dateInCarbon = Carbon::createFromTimestamp($passtimestamp);
        $getSeconds = $dateInCarbon->second;
        if ($getSeconds >= 30){
            $secondsToAdd = 60 - $getSeconds;
            $dateInCarbon-> addSeconds($secondsToAdd);
        }else{
            $secondsToRemove = $getSeconds;
            $dateInCarbon-> subSeconds($secondsToRemove);
        }
        return $dateInCarbon->timestamp;
    }
    public function getSecondsByTime($passtimestamp)
    {
        $dateInCarbon = Carbon::createFromTimestamp($passtimestamp);
        $getSeconds = $dateInCarbon->second;
        return $getSeconds;
    }
}