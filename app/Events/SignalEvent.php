<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SignalEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $signal;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($signal)
    {
        $this->signal = $signal;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('signaltable');
        //return new PrivateChannel('order.'.$this->update->order_id);
    }

    public function broadcastWith()
    {
        return [
            'coinid' => $this->signal->id,
            'coinname' => $this->signal->coinname,
            'coinsymbol' => $this->signal->coinsymbol,
            'coinimgurl' => $this->signal->coinimgurl,
            'exchange' => $this->signal->exchange,
            'cointradeurl' => $this->signal->cointradeurl,
            'coinprice' => $this->signal->coinprice,
            'target1'  => $this->signal->target1,
            'target2'  => $this->signal->target2,
            'target3'  => $this->signal->target3,
            'stoploss'  => $this->signal->stoploss,
            'membresia_id' => $this->signal->membresia_id,
            'create_at' => $this->signal->created_at->timestamp,
            'pricet30m' => $this->signal->pricet30m,
            'pricet1h' => $this->signal->pricet1h,
            'pricet3h' => $this->signal->pricet3h,
            'pricet6h' => $this->signal->pricet6h,
            'pricet12h' => $this->signal->pricet12h,
            'pricet24h' => $this->signal->pricet24h,
            'pricet48h' => $this->signal->pricet48h,
            'flag' => $this->signal->flag,
            'pricet30mpercent' => $this->signal->pricet30mpercent,
            'pricet1hpercent' => $this->signal->pricet1hpercent,
            'pricet3hpercent' => $this->signal->pricet3hpercent,
            'pricet6hpercent' => $this->signal->pricet6hpercent,
            'pricet12hpercent' => $this->signal->pricet12hpercent,
            'pricet24hpercent' => $this->signal->pricet24hpercent,
            'pricet48hpercent' => $this->signal->pricet48hpercent,
            'priceAverage' => $this->signal->priceAverage,
            'priceAveragePercent' => $this->signal->priceAveragePercent,
            'rangeprice' => $this->signal->rangeprice
        ];
    }
}