<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Entities\User;

class Consultas extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.consulta')->from('forex@grupocreex.com','NUEVA CONSULTA')->subject('consulta')->with([
            'nombre' => $this->data['nombre'],
            'email' => $this->data['email'],
            'usuario' => $this->data['usuario'],
            'password' => $this->data['password'],
            'servidor' => $this->data['servidor'],
            'observacion' => $this->data['observacion'],
        ]);
    }
}
