<?php

namespace App\Facades;
use Illuminate\Support\Facades\Facade;

class HelperCreex extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'helperCreex';
    }
}
