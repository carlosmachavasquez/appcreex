<?php

namespace App\Components;

use Illuminate\Support\Facades\URL;
use Carbon\Carbon;
use Datetime;

class HelperCreex{

    public function formatoDia($fecha){
        $arrayMeses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
        $arrayDias = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');
        list($a, $m, $d) = explode('-', $fecha);

        $fec = $arrayDias[date('N', strtotime($fecha))-1].", ".date($d)." de ".$arrayMeses[date($m)-1]." del ".date($a);
        return $fec;
    }

    public function formatoNewListTag($dt){
        $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $arrayDias = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');
        return $arrayDias[date('N', strtotime($dt))-1]." ".$dt->day." de ".$meses[$dt->month - 1];
    }

    public function formatPassedTime( $date )
    {
        //Our dates and times.
        $then = $date;
        $then = new DateTime($then);
         
        $now = new DateTime();
         
        $sinceThen = $then->diff($now);
        $days = $sinceThen->d;
        $hours = $sinceThen->h;
        $minutes = $sinceThen->i;
        $seconds = $sinceThen->s;

        if($days == 0 && $hours == 0 && $seconds == 0){
            return $seconds.' s';
        }else if($days == 0 && $hours == 0) {
            return $minutes.' m, '.$seconds.' s';
        }else if( $days == 0 ){
            return $hours.' h, '.$minutes.' m ' .$seconds.' s';
        }else if( $days > 0){
            return $days.' d, '.$hours.' h, '.$minutes.' m, '.$seconds.' s';
        }
    }

    public function formatTime( $fecha ){
        $tiempo = explode(" ", $fecha);
        $timestamp = strtotime($fecha);

        $inicio = explode(':', $tiempo[1]);
        $actual = explode(':', date('H:i:s'));

        $diff = time() - (int)$timestamp;
        if($diff < 20){
            $return = 'Hace unos momentos';
        } else if($diff >= 20 AND $diff < 60){
            $return = sprintf('hace %s segundos', $diff);
        } else if($diff >= 60 AND $diff < 120){
            if($actual[2]>=$inicio[2]){
                $seg = $actual[2]-$inicio[2];
            }else{
                $seg = (60-$inicio[2])+($actual[2]);
            }
            $return = sprintf('hace %s minuto', floor($diff/60))." $seg segundo(s)";
        } else if($diff >= 120 AND $diff < 3600){
            if($actual[2]>=$inicio[2]){
                $seg = $actual[2]-$inicio[2];
            }else{
                $seg = (60-$inicio[2])+($actual[2]);
            }
            $return = sprintf('hace %s minutos', floor($diff/60))." $seg segundo(s)";
        } else if($diff >= 3600 AND $diff < 7200){
            if($actual[1]>=$inicio[1]){
                $min = $actual[1]-$inicio[1];
            }else{
                $min = (60-$inicio[1])+($actual[1]);
            }
            $return = sprintf('hace %s hora', floor($diff/3600))." $min minuto(s)";
        }
        else if($diff >= 7200 AND $diff < 86400){
            if($actual[1]>=$inicio[1]){
                $min = $actual[1]-$inicio[1];
            }else{
                $min = (60-$inicio[1])+($actual[1]);
            }
        $return = sprintf('hace %s horas', floor($diff/3600))." $min minuto(s)";
        }
        else if($diff >= 86400 AND $diff < 172800)        {
            $return = sprintf('hace %s dia'        , floor($diff/86400));        }
        else if($diff >= 172800 AND $diff < 604800)        {
            $return = sprintf('hace %s dias'        , floor($diff/86400));        }
        else if($diff >= 604800 AND $diff < 1209600)    {
            $return = sprintf('hace %s semana'    , floor($diff/604800));        }
        else if($diff >= 1209600 AND $diff < 2629744)    {
            $return = sprintf('hace %s semanas'    , floor($diff/604800));        }
        else if($diff >= 2629744 AND $diff < 5259488)    {
            $return = sprintf('hace %s mes'        , floor($diff/2629744));    }
        else if($diff >= 5259488 AND $diff < 31556926)    {
            $return = sprintf('hace %s meses'    , floor($diff/2629744));    }
        else if($diff >= 31556926 AND $diff < 63113852)    {
            $return = sprintf('hace %s año'        , floor($diff/31556926));    }
        else if($diff >= 63113852)                        {
            $return = sprintf('hace %s años'        , floor($diff/31556926));    }
        else                                             {
            $return = date('Y-m-d H:i:s', $timestamp);
        }
        return $return;
    }

    public function getFormatDateSpanishNews($dt){
        $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $arrayDias = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');
        return $arrayDias[date('N', strtotime($dt))-1].", ".$dt->day." de ".$meses[$dt->month - 1]. " del ".$dt->year ." | ".$dt->hour.":".$dt->minute." h";
    }

    public function getFormatDateSpanish($dt){
        $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $arrayDias = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo');
        return $arrayDias[date('N', strtotime($dt))-1].", ".$dt->day." de ".$meses[$dt->month - 1]. " del ".$dt->year;
    }

    public function getFormatDateSpanishMin($dt){
        $arrayDias = array('LUN', 'MAR', 'MIE', 'JUE', 'VIE', 'SAB', 'DOM');
        return $arrayDias[date('N', strtotime($dt))-1].". ".str_pad($dt->day,2,"0", STR_PAD_LEFT)."/".str_pad($dt->month,2,"0", STR_PAD_LEFT);
    }

    public function getMonthLetters($m){
        $mes = '';
        switch($m){
           case 1: $mes="ENERO"; break;
           case 2: $mes="FEBRERO"; break;
           case 3: $mes="MARZO"; break;
           case 4: $mes="ABRIL"; break;
           case 5: $mes="MAYO"; break;
           case 6: $mes="JUNIO"; break;
           case 7: $mes="JULIO"; break;
           case 8: $mes="AGOSTO"; break;
           case 9: $mes="SEPTIEMBRE"; break;
           case 10: $mes="OCTUBRE"; break;
           case 11: $mes="NOVIEMBRE"; break;
           case 12: $mes="DICIEMBRE"; break;
        }
        return $mes;
    }

    public function formatExcerpt($str, $length){
        //Si no se especifica la longitud por defecto es 50
        if ($length == NULL)
            $length = 50;
        //Primero eliminamos las etiquetas html y luego cortamos el string
        $stringDisplay = substr($str, 0, $length);
        //Si el texto es mayor que la longitud se agrega puntos suspensivos
        if ( strlen($str) > $length )
            $stringDisplay .= ' ...';
        return $stringDisplay;
    }

	public function formatExcerptOutTag($text, $max_length = 150, $keep_word = false){
			$cut_off = ' ...';
			$text = strip_tags($text);

			if(strlen($text) <= $max_length) {
					return $text;
			}

			if(strlen($text) > $max_length) {
					if($keep_word) {
							$text = substr($text, 0, $max_length + 1);

							if($last_space = strrpos($text, ' ')) {
									$text = substr($text, 0, $last_space);
									$text = rtrim($text);
									$text .=  $cut_off;
							}
					} else {
							$text = substr($text, 0, $max_length);
							$text = rtrim($text);
							$text .=  $cut_off;
					}
			}

			return $text;
	}

    public function addQueryStringURL($url,$queryString){
        if(!empty($queryString)):
            if(!empty($url) and !is_object($url)):
                if (!parse_url($url, PHP_URL_QUERY)) :
                    $queryString = '?'.$queryString;
                endif;
                return $url.$queryString;
            else:
                return '';
            endif;
        else:
            return $url;
        endif;

    }

    public function getArrayForText($body, $text)
    {
        $html = $body;

        if(!is_object($html)):
            $html = \Sunra\PhpSimple\HtmlDomParser::str_get_html($html);
        endif;
        $all = $html->find('*');
        $position = 0;
        $arrInfo = [];

        foreach ($all as $key => $value) {
            if ( strpos($value->innertext, $text) ) {
                $position = $key;
            };
        }

        $position++;

        for ($i=$position; $i < sizeof($all); $i++) { 
            if ( strpos($all[$i]->tag, 'h3') !== false ) {
                break;
            }else{
                array_push($arrInfo, $all[$i]->plaintext);
            };
        };

        //$elementP = $html->find("p");
        //$html->getAllAttributes();

        //$html -> load();

        // $a = 'How are you?';

        // if (strpos($a, 'are') !== false) {
        //     echo 'true';
        // }

        // $items = $html->find('h3[class=preview]'); 

        //dump($all[0]->plaintext);

        //return $body['doc'];
        return $arrInfo;
    }

    public function getPrepTime($body, $text)
    {
        $html = $body;
        if(!is_object($html)):
            $html = \Sunra\PhpSimple\HtmlDomParser::str_get_html($html);
        endif;
        $all = $html->find('h3');
        //dump($all);
        //dump($all[4]->nodes[0]->innertext);
        $position = 0;
        $infoText = '';
        $prepTime = '';

        for ($i=$position; $i < sizeof($all); $i++) { 
            if ( strpos($all[$i]->nodes[0]->innertext, 'Tiempo de preparación:') !== false ) {
                $infoText = $all[$i]->nodes[0]->innertext;
                break;
            }
        };
        //dump($i);
        //dump($infoText);
        if ($infoText != '') {
            $prepTime = preg_match_all('!\d+!', $infoText, $matches);
            //$matches = $matches[0];
            $prepTime = "PT".$matches[0][0]."M";
        }


        return $prepTime;
    }

    public function removeAccents($string)
    {

        if ( !preg_match('/[\x80-\xff]/', $string) )
                return $string;

            $chars = array(
            // Decompositions for Latin-1 Supplement
            chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
            chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
            chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
            chr(195).chr(135) => 'C', chr(195).chr(136) => 'E',
            chr(195).chr(137) => 'E', chr(195).chr(138) => 'E',
            chr(195).chr(139) => 'E', chr(195).chr(140) => 'I',
            chr(195).chr(141) => 'I', chr(195).chr(142) => 'I',
            chr(195).chr(143) => 'I', chr(195).chr(145) => 'N',
            chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
            chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
            chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
            chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
            chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
            chr(195).chr(159) => 's', chr(195).chr(160) => 'a',
            chr(195).chr(161) => 'a', chr(195).chr(162) => 'a',
            chr(195).chr(163) => 'a', chr(195).chr(164) => 'a',
            chr(195).chr(165) => 'a', chr(195).chr(167) => 'c',
            chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
            chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
            chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
            chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
            chr(195).chr(177) => 'n', chr(195).chr(178) => 'o',
            chr(195).chr(179) => 'o', chr(195).chr(180) => 'o',
            chr(195).chr(181) => 'o', chr(195).chr(182) => 'o',
            chr(195).chr(182) => 'o', chr(195).chr(185) => 'u',
            chr(195).chr(186) => 'u', chr(195).chr(187) => 'u',
            chr(195).chr(188) => 'u', chr(195).chr(189) => 'y',
            chr(195).chr(191) => 'y',
            // Decompositions for Latin Extended-A
            chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
            chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
            chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
            chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
            chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
            chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
            chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
            chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
            chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
            chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
            chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
            chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
            chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
            chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
            chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
            chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
            chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
            chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
            chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
            chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
            chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
            chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
            chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
            chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
            chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
            chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
            chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
            chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
            chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
            chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
            chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
            chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
            chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
            chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
            chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
            chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
            chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
            chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
            chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
            chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
            chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
            chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
            chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
            chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
            chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
            chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
            chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
            chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
            chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
            chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
            chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
            chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
            chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
            chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
            chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
            chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
            chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
            chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
            chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
            chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
            chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
            chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
            chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
            chr(197).chr(190) => 'z', chr(197).chr(191) => 's'
            );

            $string = strtr($string, $chars);

            return $string;
    }

}
