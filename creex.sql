-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-05-2018 a las 23:28:32
-- Versión del servidor: 10.1.29-MariaDB
-- Versión de PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `creex`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivo`
--

CREATE TABLE `archivo` (
  `id` int(10) UNSIGNED NOT NULL,
  `archivo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `leccion_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE `countries` (
  `code` int(3) NOT NULL,
  `alpha2` varchar(2) NOT NULL,
  `alpha3` varchar(3) NOT NULL,
  `langCS` varchar(45) NOT NULL,
  `langDE` varchar(45) NOT NULL,
  `langEN` varchar(45) NOT NULL,
  `langES` varchar(45) NOT NULL,
  `langFR` varchar(45) NOT NULL,
  `langIT` varchar(45) NOT NULL,
  `langNL` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`code`, `alpha2`, `alpha3`, `langCS`, `langDE`, `langEN`, `langES`, `langFR`, `langIT`, `langNL`) VALUES
(4, 'AF', 'AFG', 'Afghanistán', 'Afghanistan', 'Afghanistan', 'Afganistán', 'Afghanistan', 'Afghanistan', 'Afghanistan'),
(8, 'AL', 'ALB', 'Albánie', 'Albanien', 'Albania', 'Albania', 'Albanie', 'Albania', 'Albanië'),
(10, 'AQ', 'ATA', 'Antarctica', 'Antarktis', 'Antarctica', 'Antartida', 'Antarctique', 'Antartide', 'Antarctica'),
(12, 'DZ', 'DZA', 'Alžírsko', 'Algerien', 'Algeria', 'Argelia', 'Algérie', 'Algeria', 'Algerije'),
(16, 'AS', 'ASM', 'Americká Samoa', 'Amerikanisch-Samoa', 'American Samoa', 'Samoa americana', 'Samoa Américaines', 'Samoa Americane', 'Amerikaans Samoa'),
(20, 'AD', 'AND', 'Andorra', 'Andorra', 'Andorra', 'Andorra', 'Andorre', 'Andorra', 'Andorra'),
(24, 'AO', 'AGO', 'Angola', 'Angola', 'Angola', 'Angola', 'Angola', 'Angola', 'Angola'),
(28, 'AG', 'ATG', 'Antigua a Barbuda', 'Antigua und Barbuda', 'Antigua and Barbuda', 'Antigua y Barbuda', 'Antigua-et-Barbuda', 'Antigua e Barbuda', 'Antigua en Barbuda'),
(31, 'AZ', 'AZE', 'Azerbajdžán', 'Aserbaidschan', 'Azerbaijan', 'Azerbaiyán', 'Azerbaïdjan', 'Azerbaijan', 'Azerbeidzjan'),
(32, 'AR', 'ARG', 'Argentina', 'Argentinien', 'Argentina', 'Argentina', 'Argentine', 'Argentina', 'Argentinië'),
(36, 'AU', 'AUS', 'Austrálie', 'Australien', 'Australia', 'Australia', 'Australie', 'Australia', 'Australië'),
(40, 'AT', 'AUT', 'Rakousko', 'Österreich', 'Austria', 'Austria', 'Autriche', 'Austria', 'Oostenrijk'),
(44, 'BS', 'BHS', 'Bahamy', 'Bahamas', 'Bahamas', 'Bahamas', 'Bahamas', 'Bahamas', 'Bahama\'s'),
(48, 'BH', 'BHR', 'Bahrajn', 'Bahrain', 'Bahrain', 'Bahrain', 'Bahreïn', 'Bahrain', 'Bahrein'),
(50, 'BD', 'BGD', 'Bangladéš', 'Bangladesch', 'Bangladesh', 'Bangladesh', 'Bangladesh', 'Bangladesh', 'Bangladesh'),
(51, 'AM', 'ARM', 'Arménie', 'Armenien', 'Armenia', 'Armenia', 'Arménie', 'Armenia', 'Armenië'),
(52, 'BB', 'BRB', 'Barbados', 'Barbados', 'Barbados', 'Barbados', 'Barbade', 'Barbados', 'Barbados'),
(56, 'BE', 'BEL', 'Belgie', 'Belgien', 'Belgium', 'Bélgica', 'Belgique', 'Belgio', 'België'),
(60, 'BM', 'BMU', 'Bermuda', 'Bermuda', 'Bermuda', 'Bermuda', 'Bermudes', 'Bermuda', 'Bermuda'),
(64, 'BT', 'BTN', 'Bhután', 'Bhutan', 'Bhutan', 'Bhutan', 'Bhoutan', 'Bhutan', 'Bhutan'),
(68, 'BO', 'BOL', 'Bolívie', 'Bolivien', 'Bolivia', 'Bolivia', 'Bolivie', 'Bolivia', 'Bolivia'),
(70, 'BA', 'BIH', 'Bosna a Hercegovina', 'Bosnien und Herzegowina', 'Bosnia and Herzegovina', 'Bosnia y Herzegovina', 'Bosnie-Herzégovine', 'Bosnia Erzegovina', 'Bosnië-Herzegovina'),
(72, 'BW', 'BWA', 'Botswana', 'Botswana', 'Botswana', 'Botswana', 'Botswana', 'Botswana', 'Botswana'),
(74, 'BV', 'BVT', 'Bouvet Island', 'Bouvetinsel', 'Bouvet Island', 'Isla Bouvet', 'Île Bouvet', 'Isola di Bouvet', 'Bouvet'),
(76, 'BR', 'BRA', 'Brazílie', 'Brasilien', 'Brazil', 'Brasil', 'Brésil', 'Brasile', 'Brazilië'),
(84, 'BZ', 'BLZ', 'Belize', 'Belize', 'Belize', 'Belize', 'Belize', 'Belize', 'Belize'),
(86, 'IO', 'IOT', 'Britské Indickooceánské teritorium', 'Britisches Territorium im Indischen Ozean', 'British Indian Ocean Territory', 'Territorio Oceánico de la India Británica', 'Territoire Britannique de l\'Océan Indien', 'Territori Britannici dell\'Oceano Indiano', 'British Indian Ocean Territory'),
(90, 'SB', 'SLB', 'Šalamounovy ostrovy', 'Salomonen', 'Solomon Islands', 'Islas Salomón', 'Îles Salomon', 'Isole Solomon', 'Salomonseilanden'),
(92, 'VG', 'VGB', 'Britské Panenské ostrovy', 'Britische Jungferninseln', 'British Virgin Islands', 'Islas Vírgenes Británicas', 'Îles Vierges Britanniques', 'Isole Vergini Britanniche', 'Britse Maagdeneilanden'),
(96, 'BN', 'BRN', 'Brunej', 'Brunei Darussalam', 'Brunei Darussalam', 'Brunei Darussalam', 'Brunéi Darussalam', 'Brunei Darussalam', 'Brunei'),
(100, 'BG', 'BGR', 'Bulharsko', 'Bulgarien', 'Bulgaria', 'Bulgaria', 'Bulgarie', 'Bulgaria', 'Bulgarije'),
(104, 'MM', 'MMR', 'Myanmar', 'Myanmar', 'Myanmar', 'Mianmar', 'Myanmar', 'Myanmar', 'Myanmar'),
(108, 'BI', 'BDI', 'Burundi', 'Burundi', 'Burundi', 'Burundi', 'Burundi', 'Burundi', 'Burundi'),
(112, 'BY', 'BLR', 'Bělorusko', 'Belarus', 'Belarus', 'Belarus', 'Bélarus', 'Bielorussia', 'Wit-Rusland'),
(116, 'KH', 'KHM', 'Kambodža', 'Kambodscha', 'Cambodia', 'Camboya', 'Cambodge', 'Cambogia', 'Cambodja'),
(120, 'CM', 'CMR', 'Kamerun', 'Kamerun', 'Cameroon', 'Camerún', 'Cameroun', 'Camerun', 'Kameroen'),
(124, 'CA', 'CAN', 'Kanada', 'Kanada', 'Canada', 'Canadá', 'Canada', 'Canada', 'Canada'),
(132, 'CV', 'CPV', 'Ostrovy Zeleného mysu', 'Kap Verde', 'Cape Verde', 'Cabo Verde', 'Cap-vert', 'Capo Verde', 'Kaapverdië'),
(136, 'KY', 'CYM', 'Kajmanské ostrovy', 'Kaimaninseln', 'Cayman Islands', 'Islas Caimán', 'Îles Caïmanes', 'Isole Cayman', 'Caymaneilanden'),
(140, 'CF', 'CAF', 'Středoafrická republika', 'Zentralafrikanische Republik', 'Central African', 'República Centroafricana', 'République Centrafricaine', 'Repubblica Centroafricana', 'Centraal-Afrikaanse Republiek'),
(144, 'LK', 'LKA', 'Srí Lanka', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka'),
(148, 'TD', 'TCD', 'Čad', 'Tschad', 'Chad', 'Chad', 'Tchad', 'Ciad', 'Tsjaad'),
(152, 'CL', 'CHL', 'Chile', 'Chile', 'Chile', 'Chile', 'Chili', 'Cile', 'Chili'),
(156, 'CN', 'CHN', 'Čína', 'China', 'China', 'China', 'Chine', 'Cina', 'China'),
(158, 'TW', 'TWN', 'Tchajwan', 'Taiwan', 'Taiwan', 'Taiwán', 'Taïwan', 'Taiwan', 'Taiwan'),
(162, 'CX', 'CXR', 'Christmas Island', 'Weihnachtsinsel', 'Christmas Island', 'Isla Navidad', 'Île Christmas', 'Isola di Natale', 'Christmaseiland'),
(166, 'CC', 'CCK', 'Kokosové ostrovy', 'Kokosinseln', 'Cocos (Keeling) Islands', 'Islas Cocos (Keeling)', 'Îles Cocos (Keeling)', 'Isole Cocos', 'Cocoseilanden'),
(170, 'CO', 'COL', 'Kolumbie', 'Kolumbien', 'Colombia', 'Colombia', 'Colombie', 'Colombia', 'Colombia'),
(174, 'KM', 'COM', 'Komory', 'Komoren', 'Comoros', 'Comoros', 'Comores', 'Comore', 'Comoren'),
(175, 'YT', 'MYT', 'Mayotte', 'Mayotte', 'Mayotte', 'Mayote', 'Mayotte', 'Mayotte', 'Mayotte'),
(178, 'CG', 'COG', 'Konžská republika Kongo', 'Republik Kongo', 'Republic of the Congo', 'Congo', 'République du Congo', 'Repubblica del Congo', 'Republiek Congo'),
(180, 'CD', 'COD', 'Demokratická republika Kongo Kongo', 'Demokratische Republik Kongo', 'The Democratic Republic Of The Congo', 'República Democrática del Congo', 'République Démocratique du Congo', 'Repubblica Democratica del Congo', 'Democratische Republiek Congo'),
(184, 'CK', 'COK', 'Cookovy ostrovy', 'Cookinseln', 'Cook Islands', 'Islas Cook', 'Îles Cook', 'Isole Cook', 'Cookeilanden'),
(188, 'CR', 'CRI', 'Kostarika', 'Costa Rica', 'Costa Rica', 'Costa Rica', 'Costa Rica', 'Costa Rica', 'Costa Rica'),
(191, 'HR', 'HRV', 'Chorvatsko', 'Kroatien', 'Croatia', 'Croacia', 'Croatie', 'Croazia', 'Kroatië'),
(192, 'CU', 'CUB', 'Kuba', 'Kuba', 'Cuba', 'Cuba', 'Cuba', 'Cuba', 'Cuba'),
(196, 'CY', 'CYP', 'Kypr', 'Zypern', 'Cyprus', 'Chipre', 'Chypre', 'Cipro', 'Cyprus'),
(203, 'CZ', 'CZE', 'Česko', 'Tschechische Republik', 'Czech Republic', 'Chequia', 'République Tchèque', 'Repubblica Ceca', 'Tsjechië'),
(204, 'BJ', 'BEN', 'Benin', 'Benin', 'Benin', 'Benin', 'Bénin', 'Benin', 'Benin'),
(208, 'DK', 'DNK', 'Dánsko', 'Dänemark', 'Denmark', 'Dinamarca', 'Danemark', 'Danimarca', 'Denemarken'),
(212, 'DM', 'DMA', 'Dominika', 'Dominica', 'Dominica', 'Dominica', 'Dominique', 'Dominica', 'Dominica'),
(214, 'DO', 'DOM', 'Dominikánská republika', 'Dominikanische Republik', 'Dominican Republic', 'República Dominicana', 'République Dominicaine', 'Repubblica Dominicana', 'Dominicaanse Republiek'),
(218, 'EC', 'ECU', 'Ekvádor', 'Ecuador', 'Ecuador', 'Ecuador', 'Équateur', 'Ecuador', 'Ecuador'),
(222, 'SV', 'SLV', 'Salvador', 'El Salvador', 'El Salvador', 'El Salvador', 'El Salvador', 'El Salvador', 'El Salvador'),
(226, 'GQ', 'GNQ', 'Rovníková Guinea', 'Äquatorialguinea', 'Equatorial Guinea', 'Guinea Ecuatorial', 'Guinée Équatoriale', 'Guinea Equatoriale', 'Equatoriaal Guinea'),
(231, 'ET', 'ETH', 'Etiopie', 'Äthiopien', 'Ethiopia', 'Etiopía', 'Éthiopie', 'Etiopia', 'Ethiopië'),
(232, 'ER', 'ERI', 'Eritrea', 'Eritrea', 'Eritrea', 'Eritrea', 'Érythrée', 'Eritrea', 'Eritrea'),
(233, 'EE', 'EST', 'Estonsko', 'Estland', 'Estonia', 'Estonia', 'Estonie', 'Estonia', 'Estland'),
(234, 'FO', 'FRO', 'Faerské ostrovy', 'Färöer', 'Faroe Islands', 'Islas Faroe', 'Îles Féroé', 'Isole Faroe', 'Faeröer'),
(238, 'FK', 'FLK', 'Falklandské ostrovy', 'Falklandinseln', 'Falkland Islands', 'Islas Malvinas', 'Îles (malvinas) Falkland', 'Isole Falkland', 'Falklandeilanden'),
(239, 'GS', 'SGS', 'Jižní Georgie a Jižní Sandwichovy ostrovy', 'Südgeorgien und die Südlichen Sandwichinseln', 'South Georgia and the South Sandwich Islands', 'Georgia del Sur e Islas Sandwich del Sur', 'Géorgie du Sud et les Îles Sandwich du Sud', 'Sud Georgia e Isole Sandwich', 'Zuid-Georgië en de Zuidelijke Sandwicheilande'),
(242, 'FJ', 'FJI', 'Fidži', 'Fidschi', 'Fiji', 'Fiji', 'Fidji', 'Fiji', 'Fiji'),
(246, 'FI', 'FIN', 'Finsko', 'Finnland', 'Finland', 'Finlandia', 'Finlande', 'Finlandia', 'Finland'),
(248, 'AX', 'ALA', 'Åland Islands', 'Åland-Inseln', 'Åland Islands', 'IslasÅland', 'Îles Åland', 'Åland Islands', 'Åland Islands'),
(250, 'FR', 'FRA', 'Francie', 'Frankreich', 'France', 'Francia', 'France', 'Francia', 'Frankrijk'),
(254, 'GF', 'GUF', 'Francouzská Guayana', 'Französisch-Guayana', 'French Guiana', 'Guinea Francesa', 'Guyane Française', 'Guyana Francese', 'Frans-Guyana'),
(258, 'PF', 'PYF', 'Francouzská Polynésie', 'Französisch-Polynesien', 'French Polynesia', 'Polinesia Francesa', 'Polynésie Française', 'Polinesia Francese', 'Frans-Polynesië'),
(260, 'TF', 'ATF', 'Francouzská jižní teritoria', 'Französische Süd- und Antarktisgebiete', 'French Southern Territories', 'Territorios Sureños de Francia', 'Terres Australes Françaises', 'Territori Francesi del Sud', 'Franse Zuidelijke en Antarctische gebieden'),
(262, 'DJ', 'DJI', 'Džibutsko', 'Dschibuti', 'Djibouti', 'Djibouti', 'Djibouti', 'Gibuti', 'Djibouti'),
(266, 'GA', 'GAB', 'Gabon', 'Gabun', 'Gabon', 'Gabón', 'Gabon', 'Gabon', 'Gabon'),
(268, 'GE', 'GEO', 'Gruzínsko', 'Georgien', 'Georgia', 'Georgia', 'Géorgie', 'Georgia', 'Georgië'),
(270, 'GM', 'GMB', 'Gambie', 'Gambia', 'Gambia', 'Gambia', 'Gambie', 'Gambia', 'Gambia'),
(275, 'PS', 'PSE', 'Palestinská území', 'Palästinensische Autonomiegebiete', 'Occupied Palestinian Territory', 'Palestina', 'Territoire Palestinien Occupé', 'Territori Palestinesi Occupati', 'Palestina'),
(276, 'DE', 'DEU', 'Německo', 'Deutschland', 'Germany', 'Alemania', 'Allemagne', 'Germania', 'Duitsland'),
(288, 'GH', 'GHA', 'Ghana', 'Ghana', 'Ghana', 'Ghana', 'Ghana', 'Ghana', 'Ghana'),
(292, 'GI', 'GIB', 'Gibraltar', 'Gibraltar', 'Gibraltar', 'Gibraltar', 'Gibraltar', 'Gibilterra', 'Gibraltar'),
(296, 'KI', 'KIR', 'Kiribati', 'Kiribati', 'Kiribati', 'Kiribati', 'Kiribati', 'Kiribati', 'Kiribati'),
(300, 'GR', 'GRC', 'Řecko', 'Griechenland', 'Greece', 'Grecia', 'Grèce', 'Grecia', 'Griekenland'),
(304, 'GL', 'GRL', 'Grónsko', 'Grönland', 'Greenland', 'Groenlandia', 'Groenland', 'Groenlandia', 'Groenland'),
(308, 'GD', 'GRD', 'Grenada', 'Grenada', 'Grenada', 'Granada', 'Grenade', 'Grenada', 'Grenada'),
(312, 'GP', 'GLP', 'Guadeloupe', 'Guadeloupe', 'Guadeloupe', 'Guadalupe', 'Guadeloupe', 'Guadalupa', 'Guadeloupe'),
(316, 'GU', 'GUM', 'Guam', 'Guam', 'Guam', 'Guam', 'Guam', 'Guam', 'Guam'),
(320, 'GT', 'GTM', 'Guatemala', 'Guatemala', 'Guatemala', 'Guatemala', 'Guatemala', 'Guatemala', 'Guatemala'),
(324, 'GN', 'GIN', 'Guinea', 'Guinea', 'Guinea', 'Guinea', 'Guinée', 'Guinea', 'Guinee'),
(328, 'GY', 'GUY', 'Guyana', 'Guyana', 'Guyana', 'Guayana', 'Guyana', 'Guyana', 'Guyana'),
(332, 'HT', 'HTI', 'Haiti', 'Haiti', 'Haiti', 'Haití', 'Haïti', 'Haiti', 'Haiti'),
(334, 'HM', 'HMD', 'Heardův ostrov a McDonaldovy ostrovy', 'Heard und McDonaldinseln', 'Heard Island and McDonald Islands', 'Islas Heard e Islas McDonald', 'Îles Heard et Mcdonald', 'Isola Heard e Isole McDonald', 'Heard- en McDonaldeilanden'),
(336, 'VA', 'VAT', 'Vatikán', 'Vatikanstadt', 'Vatican City State', 'Estado Vaticano', 'Saint-Siège (état de la Cité du Vatican)', 'Città del Vaticano', 'Vaticaanstad'),
(340, 'HN', 'HND', 'Honduras', 'Honduras', 'Honduras', 'Honduras', 'Honduras', 'Honduras', 'Honduras'),
(344, 'HK', 'HKG', 'Hong Kong', 'Hongkong', 'Hong Kong', 'Hong Kong', 'Hong-Kong', 'Hong Kong', 'Hongkong'),
(348, 'HU', 'HUN', 'Maďarsko', 'Ungarn', 'Hungary', 'Hungría', 'Hongrie', 'Ungheria', 'Hongarije'),
(352, 'IS', 'ISL', 'Island', 'Island', 'Iceland', 'Islandia', 'Islande', 'Islanda', 'IJsland'),
(356, 'IN', 'IND', 'Indie', 'Indien', 'India', 'India', 'Inde', 'India', 'India'),
(360, 'ID', 'IDN', 'Indonésie', 'Indonesien', 'Indonesia', 'Indonesia', 'Indonésie', 'Indonesia', 'Indonesië'),
(364, 'IR', 'IRN', 'Írán', 'Islamische Republik Iran', 'Islamic Republic of Iran', 'Irán', 'République Islamique d\'Iran', 'Iran', 'Iran'),
(368, 'IQ', 'IRQ', 'Irák', 'Irak', 'Iraq', 'Irak', 'Iraq', 'Iraq', 'Irak'),
(372, 'IE', 'IRL', 'Irsko', 'Irland', 'Ireland', 'Irlanda', 'Irlande', 'Eire', 'Ierland'),
(376, 'IL', 'ISR', 'Izrael', 'Israel', 'Israel', 'Israel', 'Israël', 'Israele', 'Israël'),
(380, 'IT', 'ITA', 'Itálie', 'Italien', 'Italy', 'Italia', 'Italie', 'Italia', 'Italië'),
(384, 'CI', 'CIV', 'Pobřeží slonoviny', 'Côte d\'Ivoire', 'Côte d\'Ivoire', 'Costa de Marfil', 'Côte d\'Ivoire', 'Costa d\'Avorio', 'Ivoorkust'),
(388, 'JM', 'JAM', 'Jamajka', 'Jamaika', 'Jamaica', 'Jamaica', 'Jamaïque', 'Giamaica', 'Jamaica'),
(392, 'JP', 'JPN', 'Japonsko', 'Japan', 'Japan', 'Japón', 'Japon', 'Giappone', 'Japan'),
(398, 'KZ', 'KAZ', 'Kazachstán', 'Kasachstan', 'Kazakhstan', 'Kazajstán', 'Kazakhstan', 'Kazakhistan', 'Kazachstan'),
(400, 'JO', 'JOR', 'Jordánsko', 'Jordanien', 'Jordan', 'Jordania', 'Jordanie', 'Giordania', 'Jordanië'),
(404, 'KE', 'KEN', 'Keňa', 'Kenia', 'Kenya', 'Kenia', 'Kenya', 'Kenya', 'Kenia'),
(408, 'KP', 'PRK', 'Severní Korea', 'Demokratische Volksrepublik Korea', 'Democratic People\'s Republic of Korea', 'Corea', 'République Populaire Démocratique de Corée', 'Corea del Nord', 'Noord-Korea'),
(410, 'KR', 'KOR', 'Jižní Korea', 'Republik Korea', 'Republic of Korea', 'Corea', 'République de Corée', 'Corea del Sud', 'Zuid-Korea'),
(414, 'KW', 'KWT', 'Kuvajt', 'Kuwait', 'Kuwait', 'Kuwait', 'Koweït', 'Kuwait', 'Koeweit'),
(417, 'KG', 'KGZ', 'Kyrgyzstán', 'Kirgisistan', 'Kyrgyzstan', 'Kirgistán', 'Kirghizistan', 'Kirghizistan', 'Kirgizië'),
(418, 'LA', 'LAO', 'Laos', 'Demokratische Volksrepublik Laos', 'Lao People\'s Democratic Republic', 'Laos', 'République Démocratique Populaire Lao', 'Laos', 'Laos'),
(422, 'LB', 'LBN', 'Libanon', 'Libanon', 'Lebanon', 'Líbano', 'Liban', 'Libano', 'Libanon'),
(426, 'LS', 'LSO', 'Lesotho', 'Lesotho', 'Lesotho', 'Lesoto', 'Lesotho', 'Lesotho', 'Lesotho'),
(428, 'LV', 'LVA', 'Lotyšsko', 'Lettland', 'Latvia', 'Letonia', 'Lettonie', 'Lettonia', 'Letland'),
(430, 'LR', 'LBR', 'Libérie', 'Liberia', 'Liberia', 'Liberia', 'Libéria', 'Liberia', 'Liberia'),
(434, 'LY', 'LBY', 'Libye', 'Libysch-Arabische Dschamahirija', 'Libyan Arab Jamahiriya', 'Libia', 'Jamahiriya Arabe Libyenne', 'Libia', 'Libië'),
(438, 'LI', 'LIE', 'Lichtenštejnsko', 'Liechtenstein', 'Liechtenstein', 'Liechtenstein', 'Liechtenstein', 'Liechtenstein', 'Liechtenstein'),
(440, 'LT', 'LTU', 'Litva', 'Litauen', 'Lithuania', 'Lituania', 'Lituanie', 'Lituania', 'Litouwen'),
(442, 'LU', 'LUX', 'Lucembursko', 'Luxemburg', 'Luxembourg', 'Luxemburgo', 'Luxembourg', 'Lussemburgo', 'Groothertogdom Luxemburg'),
(446, 'MO', 'MAC', 'Macao', 'Macao', 'Macao', 'Macao', 'Macao', 'Macao', 'Macao'),
(450, 'MG', 'MDG', 'Madagaskar', 'Madagaskar', 'Madagascar', 'Madagascar', 'Madagascar', 'Madagascar', 'Madagaskar'),
(454, 'MW', 'MWI', 'Malawi', 'Malawi', 'Malawi', 'Malawi', 'Malawi', 'Malawi', 'Malawi'),
(458, 'MY', 'MYS', 'Malajsie', 'Malaysia', 'Malaysia', 'Malasia', 'Malaisie', 'Malesia', 'Maleisië'),
(462, 'MV', 'MDV', 'Maledivy', 'Malediven', 'Maldives', 'Maldivas', 'Maldives', 'Maldive', 'Maldiven'),
(466, 'ML', 'MLI', 'Mali', 'Mali', 'Mali', 'Mali', 'Mali', 'Mali', 'Mali'),
(470, 'MT', 'MLT', 'Malta', 'Malta', 'Malta', 'Malta', 'Malte', 'Malta', 'Malta'),
(474, 'MQ', 'MTQ', 'Martinik', 'Martinique', 'Martinique', 'Martinica', 'Martinique', 'Martinica', 'Martinique'),
(478, 'MR', 'MRT', 'Mauretánie', 'Mauretanien', 'Mauritania', 'Mauritania', 'Mauritanie', 'Mauritania', 'Mauritanië'),
(480, 'MU', 'MUS', 'Mauricius', 'Mauritius', 'Mauritius', 'Mauricio', 'Maurice', 'Maurizius', 'Mauritius'),
(484, 'MX', 'MEX', 'Mexiko', 'Mexiko', 'Mexico', 'México', 'Mexique', 'Messico', 'Mexico'),
(492, 'MC', 'MCO', 'Monako', 'Monaco', 'Monaco', 'Mónaco', 'Monaco', 'Monaco', 'Monaco'),
(496, 'MN', 'MNG', 'Mongolsko', 'Mongolei', 'Mongolia', 'Mongolia', 'Mongolie', 'Mongolia', 'Mongolië'),
(498, 'MD', 'MDA', 'Moldavsko', 'Moldawien', 'Republic of Moldova', 'Moldavia', 'République de Moldova', 'Moldavia', 'Republiek Moldavië'),
(500, 'MS', 'MSR', 'Montserrat', 'Montserrat', 'Montserrat', 'Montserrat', 'Montserrat', 'Montserrat', 'Montserrat'),
(504, 'MA', 'MAR', 'Maroko', 'Marokko', 'Morocco', 'Marruecos', 'Maroc', 'Marocco', 'Marokko'),
(508, 'MZ', 'MOZ', 'Mosambik', 'Mosambik', 'Mozambique', 'Mozambique', 'Mozambique', 'Mozambico', 'Mozambique'),
(512, 'OM', 'OMN', 'Omán', 'Oman', 'Oman', 'Omán', 'Oman', 'Oman', 'Oman'),
(516, 'NA', 'NAM', 'Namíbie', 'Namibia', 'Namibia', 'Namibia', 'Namibie', 'Namibia', 'Namibië'),
(520, 'NR', 'NRU', 'Nauru', 'Nauru', 'Nauru', 'Nauru', 'Nauru', 'Nauru', 'Nauru'),
(524, 'NP', 'NPL', 'Nepál', 'Nepal', 'Nepal', 'Nepal', 'Népal', 'Nepal', 'Nepal'),
(528, 'NL', 'NLD', 'Nizozemsko', 'Niederlande', 'Netherlands', 'Holanda', 'Pays-Bas', 'Paesi Bassi', 'Nederland'),
(530, 'AN', 'ANT', 'Nizozemské Antily', 'Niederländische Antillen', 'Netherlands Antilles', 'Antillas Holandesas', 'Antilles Néerlandaises', 'Antille Olandesi', 'Nederlandse Antillen'),
(533, 'AW', 'ABW', 'Aruba', 'Aruba', 'Aruba', 'Aruba', 'Aruba', 'Aruba', 'Aruba'),
(540, 'NC', 'NCL', 'Nová Kaledonie', 'Neukaledonien', 'New Caledonia', 'Nueva Caledonia', 'Nouvelle-Calédonie', 'Nuova Caledonia', 'Nieuw-Caledonië'),
(548, 'VU', 'VUT', 'Vanuatu', 'Vanuatu', 'Vanuatu', 'Vanuatu', 'Vanuatu', 'Vanuatu', 'Vanuatu'),
(554, 'NZ', 'NZL', 'Nový Zéland', 'Neuseeland', 'New Zealand', 'Nueva Zelanda', 'Nouvelle-Zélande', 'Nuova Zelanda', 'Nieuw-Zeeland'),
(558, 'NI', 'NIC', 'Nikaragua', 'Nicaragua', 'Nicaragua', 'Nicaragua', 'Nicaragua', 'Nicaragua', 'Nicaragua'),
(562, 'NE', 'NER', 'Niger', 'Niger', 'Niger', 'Níger', 'Niger', 'Niger', 'Niger'),
(566, 'NG', 'NGA', 'Nigérie', 'Nigeria', 'Nigeria', 'Nigeria', 'Nigéria', 'Nigeria', 'Nigeria'),
(570, 'NU', 'NIU', 'Niue', 'Niue', 'Niue', 'Niue', 'Niué', 'Niue', 'Niue'),
(574, 'NF', 'NFK', 'Norfolk Island', 'Norfolkinsel', 'Norfolk Island', 'Islas Norfolk', 'Île Norfolk', 'Isola Norfolk', 'Norfolkeiland'),
(578, 'NO', 'NOR', 'Norsko', 'Norwegen', 'Norway', 'Noruega', 'Norvège', 'Norvegia', 'Noorwegen'),
(580, 'MP', 'MNP', 'Severomariánské ostrovy', 'Nördliche Marianen', 'Northern Mariana Islands', 'Islas de Norte-Mariana', 'Îles Mariannes du Nord', 'Isole Marianne Settentrionali', 'Noordelijke Marianen'),
(581, 'UM', 'UMI', 'United States Minor Outlying Islands', 'Amerikanisch-Ozeanien', 'United States Minor Outlying Islands', 'Islas Ultramarinas de Estados Unidos', 'Îles Mineures Éloignées des États-Unis', 'Isole Minori degli Stati Uniti d\'America', 'United States Minor Outlying Eilanden'),
(583, 'FM', 'FSM', 'Mikronésie', 'Mikronesien', 'Federated States of Micronesia', 'Micronesia', 'États Fédérés de Micronésie', 'Stati Federati della Micronesia', 'Micronesië'),
(584, 'MH', 'MHL', 'Marshallovy ostrovy', 'Marshallinseln', 'Marshall Islands', 'Islas Marshall', 'Îles Marshall', 'Isole Marshall', 'Marshalleilanden'),
(585, 'PW', 'PLW', 'Palau', 'Palau', 'Palau', 'Palau', 'Palaos', 'Palau', 'Palau'),
(586, 'PK', 'PAK', 'Pakistán', 'Pakistan', 'Pakistan', 'Pakistán', 'Pakistan', 'Pakistan', 'Pakistan'),
(591, 'PA', 'PAN', 'Panama', 'Panama', 'Panama', 'Panamá', 'Panama', 'Panamá', 'Panama'),
(598, 'PG', 'PNG', 'Papua Nová Guinea', 'Papua-Neuguinea', 'Papua New Guinea', 'Papúa Nueva Guinea', 'Papouasie-Nouvelle-Guinée', 'Papua Nuova Guinea', 'Papoea-Nieuw-Guinea'),
(600, 'PY', 'PRY', 'Paraguay', 'Paraguay', 'Paraguay', 'Paraguay', 'Paraguay', 'Paraguay', 'Paraguay'),
(604, 'PE', 'PER', 'Peru', 'Peru', 'Peru', 'Perú', 'Pérou', 'Perù', 'Peru'),
(608, 'PH', 'PHL', 'Filipíny', 'Philippinen', 'Philippines', 'Filipinas', 'Philippines', 'Filippine', 'Filippijnen'),
(612, 'PN', 'PCN', 'Pitcairn', 'Pitcairninseln', 'Pitcairn', 'Pitcairn', 'Pitcairn', 'Pitcairn', 'Pitcairneilanden'),
(616, 'PL', 'POL', 'Polsko', 'Polen', 'Poland', 'Polonia', 'Pologne', 'Polonia', 'Polen'),
(620, 'PT', 'PRT', 'Portugalsko', 'Portugal', 'Portugal', 'Portugal', 'Portugal', 'Portogallo', 'Portugal'),
(624, 'GW', 'GNB', 'Guinea-Bissau', 'Guinea-Bissau', 'Guinea-Bissau', 'Guinea-Bissau', 'Guinée-Bissau', 'Guinea-Bissau', 'Guinee-Bissau'),
(626, 'TL', 'TLS', 'Východní Timor', 'Timor-Leste', 'Timor-Leste', 'Timor Leste', 'Timor-Leste', 'Timor Est', 'Oost-Timor'),
(630, 'PR', 'PRI', 'Portoriko', 'Puerto Rico', 'Puerto Rico', 'Puerto Rico', 'Porto Rico', 'Porto Rico', 'Puerto Rico'),
(634, 'QA', 'QAT', 'Katar', 'Katar', 'Qatar', 'Qatar', 'Qatar', 'Qatar', 'Qatar'),
(638, 'RE', 'REU', 'Reunion', 'Réunion', 'Réunion', 'Reunión', 'Réunion', 'Reunion', 'Réunion'),
(642, 'RO', 'ROU', 'Rumunsko', 'Rumänien', 'Romania', 'Rumanía', 'Roumanie', 'Romania', 'Roemenië'),
(643, 'RU', 'RUS', 'Rusko', 'Russische Föderation', 'Russian Federation', 'Rusia', 'Fédération de Russie', 'Federazione Russa', 'Rusland'),
(646, 'RW', 'RWA', 'Rwanda', 'Ruanda', 'Rwanda', 'Ruanda', 'Rwanda', 'Ruanda', 'Rwanda'),
(654, 'SH', 'SHN', 'Svatá Helena', 'St. Helena', 'Saint Helena', 'Santa Helena', 'Sainte-Hélène', 'Sant\'Elena', 'Sint-Helena'),
(659, 'KN', 'KNA', 'Svatý Kitts a Nevis', 'St. Kitts und Nevis', 'Saint Kitts and Nevis', 'Santa Kitts y Nevis', 'Saint-Kitts-et-Nevis', 'Saint Kitts e Nevis', 'Saint Kitts en Nevis'),
(660, 'AI', 'AIA', 'Anguilla', 'Anguilla', 'Anguilla', 'Anguilla', 'Anguilla', 'Anguilla', 'Anguilla'),
(662, 'LC', 'LCA', 'Svatá Lucie', 'St. Lucia', 'Saint Lucia', 'Santa Lucía', 'Sainte-Lucie', 'Santa Lucia', 'Saint Lucia'),
(666, 'PM', 'SPM', 'Svatý Pierre a Miquelon', 'St. Pierre und Miquelon', 'Saint-Pierre and Miquelon', 'San Pedro y Miquelon', 'Saint-Pierre-et-Miquelon', 'Saint Pierre e Miquelon', 'Saint-Pierre en Miquelon'),
(670, 'VC', 'VCT', 'Svatý Vincenc a Grenadiny', 'St. Vincent und die Grenadinen', 'Saint Vincent and the Grenadines', 'San Vincente y Las Granadinas', 'Saint-Vincent-et-les Grenadines', 'Saint Vincent e Grenadine', 'Saint Vincent en de Grenadines'),
(674, 'SM', 'SMR', 'San Marino', 'San Marino', 'San Marino', 'San Marino', 'Saint-Marin', 'San Marino', 'San Marino'),
(678, 'ST', 'STP', 'Svatý Tomáš a Princův ostrov', 'São Tomé und Príncipe', 'Sao Tome and Principe', 'Santo Tomé y Príncipe', 'Sao Tomé-et-Principe', 'Sao Tome e Principe', 'Sao Tomé en Principe'),
(682, 'SA', 'SAU', 'Saudská Arábie', 'Saudi-Arabien', 'Saudi Arabia', 'Arabia Saudí', 'Arabie Saoudite', 'Arabia Saudita', 'Saoedi-Arabië'),
(686, 'SN', 'SEN', 'Senegal', 'Senegal', 'Senegal', 'Senegal', 'Sénégal', 'Senegal', 'Senegal'),
(690, 'SC', 'SYC', 'Seychely', 'Seychellen', 'Seychelles', 'Seychelles', 'Seychelles', 'Seychelles', 'Seychellen'),
(694, 'SL', 'SLE', 'Sierra Leone', 'Sierra Leone', 'Sierra Leone', 'Sierra Leona', 'Sierra Leone', 'Sierra Leone', 'Sierra Leone'),
(702, 'SG', 'SGP', 'Singapur', 'Singapur', 'Singapore', 'Singapur', 'Singapour', 'Singapore', 'Singapore'),
(703, 'SK', 'SVK', 'Slovensko', 'Slowakei', 'Slovakia', 'Eslovaquia', 'Slovaquie', 'Slovacchia', 'Slowakije'),
(704, 'VN', 'VNM', 'Vietnam', 'Vietnam', 'Vietnam', 'Vietnam', 'Viet Nam', 'Vietnam', 'Vietnam'),
(705, 'SI', 'SVN', 'Slovinsko', 'Slowenien', 'Slovenia', 'Eslovenia', 'Slovénie', 'Slovenia', 'Slovenië'),
(706, 'SO', 'SOM', 'Somálsko', 'Somalia', 'Somalia', 'Somalia', 'Somalie', 'Somalia', 'Somalië'),
(710, 'ZA', 'ZAF', 'Jižní Afrika', 'Südafrika', 'South Africa', 'Sudáfrica', 'Afrique du Sud', 'Sud Africa', 'Zuid-Afrika'),
(716, 'ZW', 'ZWE', 'Zimbabwe', 'Simbabwe', 'Zimbabwe', 'Zimbabue', 'Zimbabwe', 'Zimbabwe', 'Zimbabwe'),
(724, 'ES', 'ESP', 'Španělsko', 'Spanien', 'Spain', 'España', 'Espagne', 'Spagna', 'Spanje'),
(732, 'EH', 'ESH', 'Západní Sahara', 'Westsahara', 'Western Sahara', 'Sáhara Occidental', 'Sahara Occidental', 'Sahara Occidentale', 'Westelijke Sahara'),
(736, 'SD', 'SDN', 'Súdán', 'Sudan', 'Sudan', 'Sudán', 'Soudan', 'Sudan', 'Sudan'),
(738, 'SS', 'SSD', 'Jižní Súdán', 'Südsudan', 'South Sudan', 'Sudán del Sur', 'Soudan du Sud', 'Sudan del Sud', 'Zuid-Soedan'),
(740, 'SR', 'SUR', 'Surinam', 'Suriname', 'Suriname', 'Surinám', 'Suriname', 'Suriname', 'Suriname'),
(744, 'SJ', 'SJM', 'Špicberky a Jan Mayen', 'Svalbard and Jan Mayen', 'Svalbard and Jan Mayen', 'Esvalbard y Jan Mayen', 'Svalbard etÎle Jan Mayen', 'Svalbard e Jan Mayen', 'Svalbard'),
(748, 'SZ', 'SWZ', 'Svazijsko', 'Swasiland', 'Swaziland', 'Suazilandia', 'Swaziland', 'Swaziland', 'Swaziland'),
(752, 'SE', 'SWE', 'Švédsko', 'Schweden', 'Sweden', 'Suecia', 'Suède', 'Svezia', 'Zweden'),
(756, 'CH', 'CHE', 'Švýcarsko', 'Schweiz', 'Switzerland', 'Suiza', 'Suisse', 'Svizzera', 'Zwitserland'),
(760, 'SY', 'SYR', 'Sýrie', 'Arabische Republik Syrien', 'Syrian Arab Republic', 'Siria', 'République Arabe Syrienne', 'Siria', 'Syrië'),
(762, 'TJ', 'TJK', 'Tadžikistán', 'Tadschikistan', 'Tajikistan', 'Tajikistán', 'Tadjikistan', 'Tagikistan', 'Tadzjikistan'),
(764, 'TH', 'THA', 'Thajsko', 'Thailand', 'Thailand', 'Tailandia', 'Thaïlande', 'Tailandia', 'Thailand'),
(768, 'TG', 'TGO', 'Togo', 'Togo', 'Togo', 'Togo', 'Togo', 'Togo', 'Togo'),
(772, 'TK', 'TKL', 'Tokelau', 'Tokelau', 'Tokelau', 'Tokelau', 'Tokelau', 'Tokelau', 'Tokelau -eilanden'),
(776, 'TO', 'TON', 'Tonga', 'Tonga', 'Tonga', 'Tongo', 'Tonga', 'Tonga', 'Tonga'),
(780, 'TT', 'TTO', 'Trinidad a Tobago', 'Trinidad und Tobago', 'Trinidad and Tobago', 'Trinidad y Tobago', 'Trinité-et-Tobago', 'Trinidad e Tobago', 'Trinidad en Tobago'),
(784, 'AE', 'ARE', 'Spojené Arabské Emiráty', 'Vereinigte Arabische Emirate', 'United Arab Emirates', 'EmiratosÁrabes Unidos', 'Émirats Arabes Unis', 'Emirati Arabi Uniti', 'Verenigde Arabische Emiraten'),
(788, 'TN', 'TUN', 'Tunisko', 'Tunesien', 'Tunisia', 'Túnez', 'Tunisie', 'Tunisia', 'Tunesië'),
(792, 'TR', 'TUR', 'Turecko', 'Türkei', 'Turkey', 'Turquía', 'Turquie', 'Turchia', 'Turkije'),
(795, 'TM', 'TKM', 'Turkmenistán', 'Turkmenistan', 'Turkmenistan', 'Turmenistán', 'Turkménistan', 'Turkmenistan', 'Turkmenistan'),
(796, 'TC', 'TCA', 'Turks a ostrovy Caicos', 'Turks- und Caicosinseln', 'Turks and Caicos Islands', 'Islas Turks y Caicos', 'Îles Turks et Caïques', 'Isole Turks e Caicos', 'Turks- en Caicoseilanden'),
(798, 'TV', 'TUV', 'Tuvalu', 'Tuvalu', 'Tuvalu', 'Tuvalu', 'Tuvalu', 'Tuvalu', 'Tuvalu'),
(800, 'UG', 'UGA', 'Uganda', 'Uganda', 'Uganda', 'Uganda', 'Ouganda', 'Uganda', 'Oeganda'),
(804, 'UA', 'UKR', 'Ukrajina', 'Ukraine', 'Ukraine', 'Ucrania', 'Ukraine', 'Ucraina', 'Oekraïne'),
(807, 'MK', 'MKD', 'Makedonie', 'Ehem. jugoslawische Republik Mazedonien', 'The Former Yugoslav Republic of Macedonia', 'Macedonia', 'L\'ex-République Yougoslave de Macédoine', 'Macedonia', 'Macedonië'),
(818, 'EG', 'EGY', 'Egypt', 'Ägypten', 'Egypt', 'Egipto', 'Égypte', 'Egitto', 'Egypte'),
(826, 'GB', 'GBR', 'Velká Británie', 'Vereinigtes Königreich von Großbritannien und', 'United Kingdom', 'Reino Unido', 'Royaume-Uni', 'Regno Unito', 'Verenigd Koninkrijk'),
(833, 'IM', 'IMN', 'Ostrov Man', 'Insel Man', 'Isle of Man', 'Isla de Man', 'Île de Man', 'Isola di Man', 'Eiland Man'),
(834, 'TZ', 'TZA', 'Tanzánie', 'Vereinigte Republik Tansania', 'United Republic Of Tanzania', 'Tanzania', 'République-Unie de Tanzanie', 'Tanzania', 'Tanzania'),
(840, 'US', 'USA', 'USA', 'Vereinigte Staaten von Amerika', 'United States', 'Estados Unidos', 'États-Unis', 'Stati Uniti d\'America', 'Verenigde Staten'),
(850, 'VI', 'VIR', 'Americké Panenské ostrovy', 'Amerikanische Jungferninseln', 'U.S. Virgin Islands', 'Islas Vírgenes Estadounidenses', 'Îles Vierges des États-Unis', 'Isole Vergini Americane', 'Amerikaanse Maagdeneilanden'),
(854, 'BF', 'BFA', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso'),
(858, 'UY', 'URY', 'Uruguay', 'Uruguay', 'Uruguay', 'Uruguay', 'Uruguay', 'Uruguay', 'Uruguay'),
(860, 'UZ', 'UZB', 'Uzbekistán', 'Usbekistan', 'Uzbekistan', 'Uzbekistán', 'Ouzbékistan', 'Uzbekistan', 'Oezbekistan'),
(862, 'VE', 'VEN', 'Venezuela', 'Venezuela', 'Venezuela', 'Venezuela', 'Venezuela', 'Venezuela', 'Venezuela'),
(876, 'WF', 'WLF', 'Wallis a Futuna', 'Wallis und Futuna', 'Wallis and Futuna', 'Wallis y Futuna', 'Wallis et Futuna', 'Wallis e Futuna', 'Wallis en Futuna'),
(882, 'WS', 'WSM', 'Samoa', 'Samoa', 'Samoa', 'Samoa', 'Samoa', 'Samoa', 'Samoa'),
(887, 'YE', 'YEM', 'Jemen', 'Jemen', 'Yemen', 'Yemen', 'Yémen', 'Yemen', 'Jemen'),
(891, 'CS', 'SCG', 'Serbia and Montenegro', 'Serbien und Montenegro', 'Serbia and Montenegro', 'Serbia y Montenegro', 'Serbie-et-Monténégro', 'Serbia e Montenegro', 'Servië en Montenegro'),
(894, 'ZM', 'ZMB', 'Zambie', 'Sambia', 'Zambia', 'Zambia', 'Zambie', 'Zambia', 'Zambia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE `curso` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nivel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `educategoria_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `curso`
--

INSERT INTO `curso` (`id`, `titulo`, `nivel`, `descripcion`, `video`, `imagen`, `estado`, `usuario_creacion`, `usuario_modificacion`, `created_at`, `updated_at`, `educategoria_id`) VALUES
(1, 'Programacion', 'Intermedio', '', 'sdasasdasdasd', 'default.png', 1, 1, 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_membresia`
--

CREATE TABLE `detalle_membresia` (
  `id` int(10) UNSIGNED NOT NULL,
  `membresia_id` int(10) UNSIGNED NOT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `detalle_membresia`
--

INSERT INTO `detalle_membresia` (`id`, `membresia_id`, `item`, `created_at`, `updated_at`) VALUES
(1, 1, 'Clases Grupales de Blockchain en VIVO.', '2018-05-13 03:32:54', '2018-05-13 03:32:54'),
(2, 1, '5 Señales de Trading al día.', '2018-05-13 03:32:57', '2018-05-13 03:32:57'),
(3, 1, '1 Señal de ICO a la semana', '2018-05-13 03:33:09', '2018-05-13 03:33:09'),
(4, 1, '4 Revisiones Grupales de las Señales de Trading en VIVO.', '2018-05-13 03:33:18', '2018-05-13 03:33:18'),
(5, 1, 'Clases Grupales de ICOs en VIVO', '2018-05-13 03:33:25', '2018-05-13 03:33:25'),
(6, 4, 'Clases Grupales de Blockchain en VIVO.', '2018-05-13 03:34:53', '2018-05-13 03:34:53'),
(7, 4, '7 Señales de Trading al día.', '2018-05-13 03:34:59', '2018-05-13 03:34:59'),
(8, 4, '1 Señal de ICO a la semana.', '2018-05-13 03:35:03', '2018-05-13 03:35:03'),
(9, 4, '4 Revisiones Grupales de las Señales de Trading en VIVO', '2018-05-13 03:35:08', '2018-05-13 03:35:08'),
(10, 4, 'Clases Grupales de ICOs en VIVO', '2018-05-13 03:35:12', '2018-05-13 03:35:12'),
(11, 4, '2 Señales de Minería POS al mes', '2018-05-13 03:35:16', '2018-05-13 03:35:16'),
(12, 4, '1 Revisión Personal de tu Portafolio en VIVO al mes', '2018-05-13 03:35:20', '2018-05-13 03:35:20'),
(13, 4, '10% de Comisión de tu 1era Línea de Referidos', '2018-05-13 03:35:25', '2018-05-13 03:35:25'),
(14, 4, '10% de Comisión de tu 2da Línea de Referidos', '2018-05-13 03:35:30', '2018-05-13 03:35:30'),
(15, 4, '5% de Comisión de tu 3era Línea de Referidos', '2018-05-13 03:35:34', '2018-05-13 03:35:34'),
(16, 5, 'Clases Grupales de Blockchain en VIVO.', '2018-05-13 03:36:44', '2018-05-13 03:36:44'),
(17, 5, '10 Señales de Trading al día.', '2018-05-13 03:36:50', '2018-05-13 03:36:50'),
(18, 5, '2 Señal de ICO a la semana.', '2018-05-13 03:36:54', '2018-05-13 03:36:54'),
(19, 5, '4 Revisiones Grupales de las Señales de Trading en VIVO', '2018-05-13 03:36:58', '2018-05-13 03:36:58'),
(20, 5, 'Clases Grupales de ICOs en VIVO', '2018-05-13 03:37:02', '2018-05-13 03:37:02'),
(21, 5, '4 Señales de Minería POS al mes', '2018-05-13 03:37:07', '2018-05-13 03:37:07'),
(22, 5, '1 Revisión Personal de tu Portafolio en VIVO al mes', '2018-05-13 03:37:11', '2018-05-13 03:37:11'),
(23, 5, 'Clases Grupales de Minería POS en VIVO', '2018-05-13 03:37:16', '2018-05-13 03:37:16'),
(24, 5, 'Clases Grupales de Masternodes en VIVO', '2018-05-13 03:37:20', '2018-05-13 03:37:20'),
(25, 5, 'Clases Grupales de Minería POW en VIVO', '2018-05-13 03:37:27', '2018-05-13 03:37:27'),
(26, 5, 'Compartir las Actualizaciones de un Portafolio Elite', '2018-05-13 03:37:32', '2018-05-13 03:37:32'),
(27, 5, 'Aplicar a un Crowfunding para tu Start-up', '2018-05-13 03:37:36', '2018-05-13 03:37:36'),
(28, 5, 'Cripto-Tarjeta', '2018-05-13 03:37:40', '2018-05-13 03:37:40'),
(29, 5, '10% de Comisión de tu 1era Línea de Referidos', '2018-05-13 03:37:45', '2018-05-13 03:37:45'),
(30, 5, '10% de Comisión de tu 2da Línea de Referidos', '2018-05-13 03:37:49', '2018-05-13 03:37:49'),
(31, 5, '5% de Comisión de tu 3era Línea de Referidos', '2018-05-13 03:37:53', '2018-05-13 03:37:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `educategoria`
--

CREATE TABLE `educategoria` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL,
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `educategoria`
--

INSERT INTO `educategoria` (`id`, `nombre`, `descripcion`, `estado`, `usuario_creacion`, `usuario_modificacion`, `created_at`, `updated_at`) VALUES
(1, 'Computacion', '', 1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `rut` varchar(255) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  `tema` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `ciudad` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `cpostal` varchar(255) NOT NULL,
  `pais` varchar(255) NOT NULL,
  `dominio` varchar(255) NOT NULL,
  `directorio` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id`, `nombre`, `rut`, `correo`, `telefono`, `tema`, `logo`, `direccion`, `ciudad`, `region`, `cpostal`, `pais`, `dominio`, `directorio`) VALUES
(1, 'Grupo Creex', '111', 'mail@domain.tld', '+51 (1) 234567', 'adminlte', 'sep-content/multimedia/logo-creex-new.png', 'NA', 'NA', 'NA', '1', '604', 'grupocreex.net', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `ids_menu` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`id`, `nombre`, `ids_menu`) VALUES
(1, 'Administrador', '1,44,41,45,46,16,18,19,20,48,49,50,51,61,53,54,55,42,43,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,13,2,3,5,6,7,12,65'),
(2, 'Alumno', '1,42,43,26,27,29,30,31,32,33,34,35,36,38,39'),
(3, 'Influencer', '1,42,43,26,27,29,30,31,32,33,34,35,36,38,39,65'),
(4, 'Inactivo', '1,44,42,43,38,39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `leccion`
--

CREATE TABLE `leccion` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duracion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visto` tinyint(1) NOT NULL DEFAULT '0',
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `curso_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `leccion`
--

INSERT INTO `leccion` (`id`, `titulo`, `descripcion`, `video`, `duracion`, `visto`, `estado`, `usuario_creacion`, `usuario_modificacion`, `created_at`, `updated_at`, `curso_id`) VALUES
(1, 'introduccion a la programacion', 'aprenderas mucho', 'sdasasdasdasd', '2h 40min', 0, 1, 1, 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `leccion_usuario`
--

CREATE TABLE `leccion_usuario` (
  `id` int(10) UNSIGNED NOT NULL,
  `visto` tinyint(1) NOT NULL DEFAULT '0',
  `usuario` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `leccion_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `usuario` varchar(255) NOT NULL,
  `icono` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL DEFAULT 'navy',
  `asunto` varchar(255) NOT NULL,
  `mensaje` text NOT NULL,
  `tiempo` varchar(255) NOT NULL,
  `dia` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `logs`
--

INSERT INTO `logs` (`id`, `usuario`, `icono`, `color`, `asunto`, `mensaje`, `tiempo`, `dia`, `mes`, `year`) VALUES
(1, 'creex', 'user', 'purple', ' ha ingresado al sistema', '', '1526671108', 18, 5, 2018),
(2, 'creex', 'user', 'fuchsia', ' ha ingresado al sistema', '', '1526688191', 19, 5, 2018),
(3, 'creex', 'user', 'light-blue-active', ' ha ingresado al sistema', '', '1526702767', 19, 5, 2018),
(4, 'creex', 'user', 'orange-active', ' ha ingresado al sistema', '', '1526703052', 19, 5, 2018),
(5, 'soporte', 'user', 'light-blue-active', ' ha ingresado al sistema', '', '1526704118', 19, 5, 2018),
(6, 'gabriel', 'user', 'fuchsia', ' ha ingresado al sistema', '', '1526704183', 19, 5, 2018),
(7, 'creex', 'user', 'light-blue-active', ' ha ingresado al sistema', '', '1526704381', 19, 5, 2018),
(8, 'soporte', 'user', 'blue', ' ha ingresado al sistema', '', '1526704492', 19, 5, 2018),
(9, 'creex', 'building-o', 'orange', 'ha creado el nivel <b>Nivel 1</b>', '', '1526704588', 19, 5, 2018),
(10, 'creex', 'building-o', 'navy', 'ha creado el nivel <b>Nivel 2</b>', '', '1526704593', 19, 5, 2018),
(11, 'creex', 'building-o', 'maroon', 'ha creado el nivel <b>Nivel 3</b>', '', '1526704600', 19, 5, 2018),
(12, 'soporte', 'user', 'teal-active', ' ha ingresado al sistema', '', '1526705399', 19, 5, 2018),
(13, 'soporte2', 'user', 'fuchsia-active', ' ha ingresado al sistema', '', '1526705823', 19, 5, 2018),
(14, 'soporte', 'user', 'teal-active', ' ha ingresado al sistema', '', '1526706294', 19, 5, 2018),
(15, 'creex', 'cog', 'purple', 'ha modificado los datos de la empresa', '', '1526708715', 19, 5, 2018),
(16, 'soporte2', 'user', 'light-blue-active', ' ha ingresado al sistema', '', '1526748593', 19, 5, 2018),
(17, 'creex', 'user', 'light-blue', ' ha ingresado al sistema', '', '1526755275', 19, 5, 2018),
(18, 'creex', 'user', 'teal-active', ' ha ingresado al sistema', '', '1526783808', 20, 5, 2018),
(19, 'creex', 'user', 'light-blue-active', ' ha ingresado al sistema', '', '1526791043', 20, 5, 2018),
(20, 'jhoann1', 'user', 'green-active', ' ha ingresado al sistema', '', '1526791115', 20, 5, 2018),
(21, 'soporte', 'user', 'teal', ' ha ingresado al sistema', '', '1526827589', 20, 5, 2018),
(22, 'soporte2', 'user', 'black', ' ha ingresado al sistema', '', '1526866310', 21, 5, 2018),
(23, 'soporte', 'user', 'aqua', ' ha ingresado al sistema', '', '1526866412', 21, 5, 2018),
(24, 'soporte', 'user', 'blue', ' ha ingresado al sistema', '', '1526883857', 21, 5, 2018),
(25, 'soporte', 'user', 'lime-active', ' ha ingresado al sistema', '', '1526909935', 21, 5, 2018),
(26, 'soporte', 'user', 'maroon-active', ' ha ingresado al sistema', '', '1526918505', 21, 5, 2018),
(27, 'creex', 'user', 'navy', ' ha ingresado al sistema', '', '1526923413', 21, 5, 2018),
(28, 'creex', 'user', 'blue', ' ha ingresado al sistema', '', '1526924494', 21, 5, 2018),
(29, 'soporte', 'user', 'orange', ' ha ingresado al sistema', '', '1527003300', 22, 5, 2018),
(30, 'creex', 'user', 'maroon', ' ha ingresado al sistema', '', '1527044567', 23, 5, 2018),
(31, 'creex', 'user', 'red-active', ' ha ingresado al sistema', '', '1527048034', 23, 5, 2018),
(32, 'soporte', 'user', 'lime', ' ha ingresado al sistema', '', '1527048317', 23, 5, 2018);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mail`
--

CREATE TABLE `mail` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mail`
--

INSERT INTO `mail` (`id`, `id_usuario`, `correo`, `password`) VALUES
(1, 1, '@', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `membresia`
--

CREATE TABLE `membresia` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Aprendiz',
  `precio_mes` decimal(10,2) NOT NULL DEFAULT '0.00',
  `precio_anual` decimal(10,2) NOT NULL DEFAULT '0.00',
  `estado` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `membresia`
--

INSERT INTO `membresia` (`id`, `name`, `precio_mes`, `precio_anual`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'APRENDIZ', '49.75', '597.00', 1, NULL, '2018-05-13 02:35:56'),
(4, 'EMPRENDEDOR', '91.50', '1097.00', 1, '2018-05-13 02:35:13', '2018-05-13 02:35:13'),
(5, 'INVERSIONISTA', '166.50', '1997.00', 1, '2018-05-13 02:35:41', '2018-05-13 02:35:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(255) NOT NULL DEFAULT 'system',
  `nombre` varchar(255) NOT NULL,
  `icono` varchar(255) NOT NULL DEFAULT 'fa fa-circle-o',
  `archivo` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `orden` int(11) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id`, `id_menu`, `alias`, `nombre`, `icono`, `archivo`, `link`, `orden`, `activo`) VALUES
(1, 0, 'system', 'Dashboard', 'fa fa-dashboard', 'index', '', 0, 1),
(2, 0, 'system', 'Usuarios', 'fa fa-group', '', '', 100, 1),
(3, 2, 'system', 'Ver usuarios', 'fa fa-users', 'usuarios', '', 1, 1),
(4, 2, 'system', 'Crear usuario', 'fa fa-user-plus', 'newusr', '', 2, 0),
(5, 2, 'system', 'Grupos de usuarios', 'fa fa-users', 'grupos', '', 3, 1),
(6, 0, 'system', 'Configuraci&oacute;n del sistema', 'fa fa-cogs', '', '', 101, 1),
(7, 6, 'system', 'Empresa', 'glyphicon glyphicon-briefcase', 'empresa', '', 1, 1),
(8, 6, 'system', 'Plugins', 'glyphicon glyphicon-download-alt', 'plugins', '', 2, 1),
(9, 6, 'system', 'Widgets', 'glyphicon glyphicon-th-large', 'widgets', '', 3, 1),
(10, 6, 'system', 'Respaldos', 'fa fa-archive', 'respaldos', '', 4, 1),
(11, 6, 'system', 'Actualizaciones', 'glyphicon glyphicon-refresh', 'actualizaciones', '', 5, 1),
(12, 6, 'system', 'Registros', 'glyphicon glyphicon-list-alt', 'logs', '', 6, 1),
(13, 0, 'system', 'Multimedia', 'fa fa-film', 'multimedia', '', 98, 1),
(14, 0, 'system', 'Configuraci&oacute;n de Plugins', 'fa fa-cog', '', '', 103, 1),
(15, 0, 'system', 'Mensajer&iacute;a', 'fa fa-envelope-o', 'mailbox', '', 99, 0),
(16, 0, 'mlmsys', 'Administraci&oacute;n', 'fa fa-line-chart', '', '', 20, 1),
(17, 16, 'mlmsys', 'Ajustes de pagos', 'fa fa-credit-card', 'mlmpagos', '', 1, 1),
(18, 16, 'mlmsys', 'Ajustes de correo*', 'fa fa-envelope-o', 'mlmcorreo', '', 2, 1),
(19, 16, 'mlmsys', 'Ajustes de paquetes*', 'fa fa-dropbox', 'mlmpaquetes', '', 3, 1),
(20, 16, 'mlmsys', 'Ajustes de niveles*', 'fa fa-building-o', 'mlmniveles', '', 4, 1),
(21, 16, 'mlmsys', 'Publicaciones', 'fa fa-newspaper-o', 'mlmpub', '', 5, 1),
(22, 16, 'mlmsys', 'Pagos v&iacute;a Paypal', 'fa fa-paypal', 'mlmpaypal', '', 6, 1),
(23, 16, 'mlmsys', 'Pedidos C.O.D', 'fa fa-folder', 'mlmcod', '', 7, 1),
(24, 16, 'mlmsys', 'Solicitudes de pago', 'fa fa-money', 'mlmspay', '', 8, 1),
(25, 16, 'mlmsys', 'Solicitudes de renovaci&oacute;n', 'fa fa-refresh', 'mlmren', '', 9, 1),
(26, 0, 'mlmsys', 'Equipo', 'fa fa-group', '', '', 30, 1),
(27, 26, 'mlmsys', 'Ver lista', 'fa fa-list-ol', 'mlmevlista', '', 1, 1),
(28, 26, 'mlmsys', '&Aacute;rbol geneal&oacute;gico', 'fa fa-sitemap', 'mlmarbolg', '', 2, 1),
(29, 0, 'mlmsys', 'Comisiones', 'fa fa-balance-scale', '', '', 31, 1),
(30, 29, 'mlmsys', 'Bonos', 'fa fa-dollar', 'mlmbonos', '', 1, 1),
(31, 29, 'mlmsys', 'Historial', 'fa fa-list', 'mlmhistorial', '', 2, 1),
(32, 0, 'mlmsys', 'Mis compras', 'fa fa-shopping-cart', '', '', 32, 1),
(33, 32, 'mlmsys', 'Estado de cuenta', 'fa fa-line-chart', 'mlmecuenta', '', 1, 1),
(34, 32, 'mlmsys', 'Historal', 'fa fa-list', 'mlmmchistorial', '', 2, 1),
(35, 0, 'mlmsys', 'EWallet', 'fa fa-shopping-bag', '', '', 33, 1),
(36, 35, 'mlmsys', 'Cobrar Comisiones', 'fa fa-bank', 'mlmccomisiones', '', 1, 1),
(37, 35, 'mlmsys', 'Pago factura', 'fa fa-handshake-o', 'mlmpfactura', '', 2, 1),
(38, 0, 'mlmsys', 'Soporte', 'fa fa-info-circle', 'mlmsoporte', '', 35, 1),
(39, 38, 'mlmsys', 'Formulario de contacto', 'fa fa-envelope', 'mlmfcontacto', '', 1, 1),
(40, 0, 'system', 'OPCIONES DEL SISTEMA', '', '', '', 97, 1),
(41, 0, 'mlmsys', 'ADMINISTRADORES', '', '', '', 19, 1),
(42, 0, 'mlmsys', 'CLIENTES', '', '', '', 28, 1),
(43, 0, 'mlmsys', 'Perfil', 'fa fa-user-circle', 'perfil', '', 29, 1),
(44, 0, 'mlmsys', 'Realizar Pago', 'fa fa-credit-card', 'mlmregpagos', '', 16, 1),
(45, 0, 'mlmsys', 'Usuarios', 'fa fa-users', '', '', 19, 1),
(46, 45, 'mlmsys', 'Ver Usuarios', 'fa fa-circle-o', 'mlmusuarios', '', 1, 1),
(47, 45, 'mlmsys', 'Crear Usuario', 'fa fa-circle-o', 'mlmnewusr', '', 2, 1),
(48, 0, 'mlmsys', 'Balance', 'fa fa-balance-scale', '', '', 21, 1),
(49, 48, 'mlmsys', 'Ingresos', 'fa fa-indent', 'mlmingresos', '', 1, 1),
(50, 48, 'mlmsys', 'Egresos', 'fa fa-outdent', 'mlmegresos', '', 2, 1),
(51, 48, 'mlmsys', 'Comisiones pagadas', 'fa fa-circle-o', 'mlmcompag', '', 3, 1),
(52, 0, 'mlmsys', 'Auto/VPS', 'fa fa-server', '', '', 22, 0),
(53, 0, 'mlmsys', 'Solicitudes de comisi&oacute;n', 'fa fa-list', 'mlmsolcomi', '', 23, 1),
(54, 0, 'mlmsys', 'Generar pago', 'fa fa-dollar', 'mlmgenpag', '', 24, 1),
(55, 0, 'mlmsys', 'Generar comisi&oacute;n', 'fa fa-dollar', 'mlmgencom', '', 25, 1),
(56, 0, 'mlmsys', 'Formaci&oacute;n', 'fa fa-book', '', 'formacion/wp-login.php', 35, 1),
(57, 0, 'mlmsys', 'Autotrader', 'fa fa-server', 'autotrader', '', 34, 0),
(58, 52, 'mlmsys', 'Servidores', 'fa fa-server', 'mlmservidores', '', 1, 0),
(59, 52, 'mlmsys', 'VPS', 'fa fa-server', 'mlmvps', '', 2, 0),
(60, 52, 'mlmsys', 'Solicitudes Autotrader', 'fa fa-server', 'mlmsautotrader', '', 3, 0),
(61, 48, 'mlmsys', 'Historial comisiones', 'fa fa-list', 'mlmhistcom', '', 4, 1),
(62, 57, 'mlmsys', 'Usuarios Enlazados', 'fa fa-link', 'autotrader_link', '', 1, 0),
(63, 57, 'mlmsys', 'Configurar Autotrader', 'fa fa-cogs', 'autotrader', '', 1, 0),
(64, 57, 'mlmsys', 'Operativa Live', 'fa fa-signal', 'oplive', '', 3, 0),
(65, 0, 'gcreex', 'influencer', 'fa fa-group', 'influencer', '', 26, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2018_04_13_051412_create_table_edu_categoria', 1),
(3, '2018_04_13_165157_create_table_curso', 1),
(4, '2018_04_15_161708_create_table_leccion', 1),
(5, '2018_04_15_184126_create_table_archivo', 1),
(6, '2018_04_16_215047_create_table_leccion_usuario', 1),
(7, '2018_04_17_230802_create_settings_table', 1),
(8, '2018_04_20_191547_create_table_portafolio', 1),
(9, '2018_04_20_192137_create_table_monedas', 1),
(10, '2018_04_21_094217_create_table_operaciones', 1),
(11, '2018_05_02_214255_create_table_noticias', 1),
(12, '2018_05_05_200903_create_membresia_table', 1),
(13, '2018_05_05_223228_create_users_table', 1),
(14, '2018_05_05_223802_add_providers_to_users_table', 1),
(15, '2018_05_05_224742_create_signals_table', 1),
(16, '2018_05_12_120242_create_table_detalle_membresia', 1),
(17, '2018_05_19_000604_add_prices_to_signals_table', 1),
(18, '2018_05_22_171803_create_table_pagos', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mlm_autotrader`
--

CREATE TABLE `mlm_autotrader` (
  `id` int(11) NOT NULL,
  `experto` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_ctrading` varchar(255) NOT NULL,
  `passtrading` varchar(255) NOT NULL,
  `servidor` varchar(255) NOT NULL,
  `valor` varchar(255) NOT NULL,
  `lotaje` varchar(255) NOT NULL,
  `id_vps` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `clave` varchar(255) NOT NULL,
  `detalles` varchar(255) NOT NULL,
  `factivo` int(11) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mlm_chistorial`
--

CREATE TABLE `mlm_chistorial` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `us_ref` varchar(255) NOT NULL,
  `monto` float(8,2) NOT NULL,
  `fecha` int(11) NOT NULL,
  `dia` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `semana` int(11) NOT NULL,
  `plan` int(11) NOT NULL,
  `bono` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mlm_chistorial`
--

INSERT INTO `mlm_chistorial` (`id`, `id_usuario`, `us_ref`, `monto`, `fecha`, `dia`, `mes`, `year`, `semana`, `plan`, `bono`) VALUES
(1, 1, 'jhoann1', 199.70, 1526601600, 18, 5, 2018, 20, 3, 'bir'),
(2, 1, 'soporte', 199.70, 1526601600, 18, 5, 2018, 20, 3, 'bir'),
(3, 4, 'soporte2', 109.70, 1526688000, 19, 5, 2018, 20, 2, 'bir'),
(4, 1, 'soporte2', 54.85, 1526688000, 19, 5, 2018, 20, 2, 'bir'),
(5, 1, 'gabriel', 109.70, 1526601600, 18, 5, 2018, 20, 2, 'bir'),
(6, 3, 'gabriel2', 59.70, 1526688000, 19, 5, 2018, 20, 1, 'bir'),
(7, 1, 'gabriel2', 29.85, 1526688000, 19, 5, 2018, 20, 1, 'bir'),
(8, 3, 'gabriel2', 199.70, 1526688000, 19, 5, 2018, 20, 3, 'bir'),
(9, 1, 'gabriel2', 99.85, 1526688000, 19, 5, 2018, 20, 3, 'bir'),
(10, 6, 'soporte3', 59.70, 1526860800, 21, 5, 2018, 21, 1, 'bir'),
(11, 4, 'soporte3', 59.70, 1526860800, 21, 5, 2018, 21, 1, 'bir'),
(12, 1, 'soporte3', 29.85, 1526860800, 21, 5, 2018, 21, 1, 'bir'),
(13, 6, 'soporte3', 59.70, 1526601600, 18, 5, 2018, 20, 1, 'bir'),
(14, 4, 'soporte3', 59.70, 1526601600, 18, 5, 2018, 20, 1, 'bir'),
(15, 1, 'soporte3', 29.85, 1526601600, 18, 5, 2018, 20, 1, 'bir'),
(16, 6, 'soporte3', 109.70, 1526688000, 19, 5, 2018, 20, 2, 'bir'),
(17, 4, 'soporte3', 109.70, 1526688000, 19, 5, 2018, 20, 2, 'bir'),
(18, 1, 'soporte3', 54.85, 1526688000, 19, 5, 2018, 20, 2, 'bir'),
(19, 6, 'soporte3', 199.70, 1526688000, 19, 5, 2018, 20, 3, 'bir'),
(20, 4, 'soporte3', 199.70, 1526688000, 19, 5, 2018, 20, 3, 'bir'),
(21, 1, 'soporte3', 99.85, 1526688000, 19, 5, 2018, 20, 3, 'bir');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mlm_comisiones`
--

CREATE TABLE `mlm_comisiones` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `detalles` varchar(255) NOT NULL,
  `fecha` int(11) NOT NULL,
  `monto` int(11) NOT NULL,
  `fecha_pago` int(11) NOT NULL,
  `dia` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `semana` int(11) NOT NULL,
  `procesado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mlm_correo`
--

CREATE TABLE `mlm_correo` (
  `id` int(11) NOT NULL,
  `correo` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `dominio` varchar(255) CHARACTER SET latin1 NOT NULL,
  `puerto` int(11) NOT NULL,
  `cssl` int(11) NOT NULL,
  `asunto_act` varchar(255) CHARACTER SET latin1 NOT NULL,
  `txt_act` text CHARACTER SET latin1 NOT NULL,
  `asunto_pag` varchar(255) CHARACTER SET latin1 NOT NULL,
  `txt_pag` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mlm_correo`
--

INSERT INTO `mlm_correo` (`id`, `correo`, `password`, `dominio`, `puerto`, `cssl`, `asunto_act`, `txt_act`, `asunto_pag`, `txt_pag`) VALUES
(1, 'no-reply@grupocreex.net', '3Lsam1wffNa8', 'mail.grupocreex.net', 26, 1, 'Activación de cuenta en Empresa', '<b></b>Hola {nombre} {apellidos},<b></b><div><br></div><div>Usted ha creado la cuenta {usuario} en nuestro sitio web.</div><div><b></b></div><blockquote><div><b>Usuario</b>: {usuario}</div><div><b>contraseÃ±a</b>: {password}</div><div><b>correo</b>: {correo}</div></blockquote><div></div><div><br></div><div>Para activar su cuenta debe hacer clic en el siguiente link:</div><div>{link_act}</div><div><br></div><div>DespuÃ©s de realizar la activación debe elegir un plan y realizar el pago correspondiente al plan.</div><div><br></div><div>ATTE.</div><div><b>Trading Thor</b></div>', 'EnvÃ­o de pago Trading Thor', '<p>Hola {usuario},</p><p>se ha procesado un pago realizado el dÃ­a {fecha}</p><p><br></p><p>ATTE.<br><b>Trading Thor</b><b></b></p>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mlm_correo_vars`
--

CREATE TABLE `mlm_correo_vars` (
  `id` int(11) NOT NULL,
  `code` varchar(255) CHARACTER SET latin1 NOT NULL,
  `titulo` varchar(255) CHARACTER SET latin1 NOT NULL,
  `asunto` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `mensaje` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mlm_correo_vars`
--

INSERT INTO `mlm_correo_vars` (`id`, `code`, `titulo`, `asunto`, `mensaje`) VALUES
(1, 'registro', 'Registros', 'ConfirmaciÃ³n de su nueva cuenta', '<b></b>Hola {nombre} {apellido},<b></b><div><br></div><div>Usted ha creado la cuenta {usuario} en nuestro sitio web.</div><div><b></b></div><blockquote><div><b>Usuario</b>: {usuario}</div><div><b>contraseÃ±a</b>: {password}</div><div><b>correo</b>: {correo}</div></blockquote><div></div><div><br></div><div>Su cuenta se encuentra actualmente activa, debe realizar el pago de inscripciÃ³n para acceder a todas las funciones.</div><div><b><i></i></b><i>ï»¿</i><b><i></i></b><a target=\"_blank\" rel=\"nofollow\" href=\"http://www.grupocreex.net\" title=\"Link: http://www.grupocreex.net\">http://www.grupocreex.net</a><b></b></div><div><br></div><div>ATTE.</div><div><b>GrupoCreex</b></div>'),
(2, 'repass', 'Contrase&ntilde;a', 'RecuperaciÃ³n de contraseÃ±a', 'Hola {nombre} {apellido},<div><br></div><div>Se ha solicitado la recuperaciÃ³n de su contraseÃ±a.</div><div><br></div><blockquote><div><b>Usuario:</b> {usuario}</div></blockquote><blockquote><div><b>ContraseÃ±a nueva:</b> {password}</div></blockquote><div><br></div><div>ATTE.</div><div><b>{e_nombre}</b></div>'),
(3, 'genpag', 'Pagos', 'NotificaciÃ³n de su pago', 'Hola {usuario},\n\nhemos recibido su pago por el monto de {monto} USD.\n\nATTE.\n{e_nombre}'),
(4, 'retcom', 'Ret. Com.', 'Comisiones retiradas', '<p>Hola {nombre} {apellido},</p><p>&nbsp;Se ha {estado} el pago de las comisiones por un monto solicitado de ${monto} USD.</p><blockquote><p><b>Monto pagado</b>: ${monto} USD<br><b>Fecha de pago</b>: {fpago}</p></blockquote><p>Pagado a:</p><blockquote><p><b>Nombre del banco</b>: {nbanco}<br><b>Nombre de cuenta</b>: {ncuenta}<br><b>NÃºmero de cuenta</b>: {nucuenta}</p></blockquote><p>Si se ha rechazado su pago por favor omitir algunos datos de este correo.</p><p>ATTE<br><b>{e_nombre}</b></p>'),
(5, 'apcom', 'Ap. Com.', 'Estado solicitud de Pago de comisiones', 'Hola {nombre} {apellido},<div><br></div><div>&nbsp;Se ha procesado y <b>{estado}</b> su solicitud por el monto de <b>{monto} USD</b>.<b></b></div><div><br></div><blockquote><div><b>Fecha de pago</b>: {fpago}</div></blockquote><br><div>ATTE.</div><div><b>{e_nombre}</b></div>'),
(6, 'pagfac', 'Pag. Fac.', 'Confirmaci+on de pago', '<p>Hola {nombre} {apellido},</p><p>&nbsp;Se ha confirmado el pago del plan {npaquete} por el monto de {monto} USD.</p><p>Fecha de pago: {fpago}</p><p>Fecha prÃ³ximo pago: {fppago}</p><p><br></p><p>ATTE.</p><p>{e_nombre}</p>'),
(7, 'genpin', 'Gen. Pin.', 'Se ha generado un nuevo PIN', 'Hola {u_nombre} {u_apellido},<br />\r\n<br />\r\nSe ha solicitado que se genere un nuevo pin para su cuenta desde el panel de contol.<br />\r\nPIN: {pin}<br />\r\n<br />\r\nATTE.<br />\r\n{e_nombre}'),
(8, 'campass', 'Cam. Contra.', 'Cambio de contraseÃ±a', 'Hola {u_nombre} {u_apellido},<br>\n<br>\nLe avisamos que se ha cambiado su contrase?a desde el panel de control.<br>\nSi usted no ha realizado cambios intente recuperar la contrase?a o contacte a un administrador.<br>\n<br>\nATTE.<br>\n{e_nombre}'),
(9, 'sussrv', 'Susp. Serv.', 'Suspensiï¿½n de servicio Autotrader', 'Hola {nombre} {apellido},<br>\r\n<br>\r\nLe enviamos este correo para avisarle que su servicio en EMPRESA ha sido suspendido, esto quiere decir que ya no podrÃ¡ hacer uso del sistema Autotrader.<br>\r\n<br>\r\n<b>Usuario</b>: {usuario}<br>\r\n<br>\r\n<br>\r\nSaludos Cordiales.<br>\r\n<br>\r\nATTE.<br>\r\n{e_nombre}'),
(10, 'avipag', 'Aviso Pago', 'Aviso de Pago de Cuenta', 'Hola {} {},<br>\n<br>\nLe enviamos este correo para avisarle que quedan {dias} para realizar el pago, de lo contrario el servicio de AutoTrading serÃ¡ suspendido.<br>\n<b>USUARIO</b>: {usuario}<br>\n<br>\nSaludos Cordiales.<br>\n<br>\n<br>\nATTE.<br>\n{e_nombre}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mlm_egresos`
--

CREATE TABLE `mlm_egresos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `detalles` text NOT NULL,
  `monto` float(8,2) NOT NULL,
  `fecha` int(11) NOT NULL,
  `dia` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `semana` int(11) NOT NULL,
  `comprobante` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mlm_empresa`
--

CREATE TABLE `mlm_empresa` (
  `id` int(11) NOT NULL,
  `saldo` float(8,2) NOT NULL,
  `saldo_total` float(14,2) NOT NULL,
  `comisiones` float(8,2) NOT NULL,
  `com_pagadas` float(14,2) NOT NULL,
  `dolar` float(6,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mlm_empresa`
--

INSERT INTO `mlm_empresa` (`id`, `saldo`, `saldo_total`, `comisiones`, `com_pagadas`, `dolar`) VALUES
(1, 10935.25, 10935.25, 2134.75, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mlm_factura`
--

CREATE TABLE `mlm_factura` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_paquete` int(11) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `respuesta` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `metodo` varchar(255) NOT NULL,
  `fecha` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mlm_factura`
--

INSERT INTO `mlm_factura` (`id`, `id_usuario`, `id_paquete`, `hash`, `respuesta`, `estado`, `metodo`, `fecha`) VALUES
(1, 1, 3, 'Tp4uZA8nvitcB0KmXNY9ryWFfPjqLd5wbRoVga6IG', 0, 0, 'payplus', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mlm_formsec`
--

CREATE TABLE `mlm_formsec` (
  `id` int(11) NOT NULL,
  `modulo` varchar(255) NOT NULL,
  `reporte` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mlm_mchistorial`
--

CREATE TABLE `mlm_mchistorial` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `monto` float(8,2) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `plan` int(11) NOT NULL,
  `fecha` int(11) NOT NULL,
  `dia` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `semana` int(11) NOT NULL,
  `fechaexp` int(11) NOT NULL,
  `fecha_pago` int(11) NOT NULL,
  `id_factura` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mlm_mchistorial`
--

INSERT INTO `mlm_mchistorial` (`id`, `id_usuario`, `monto`, `descripcion`, `plan`, `fecha`, `dia`, `mes`, `year`, `semana`, `fechaexp`, `fecha_pago`, `id_factura`) VALUES
(1, 2, 1997.00, 'Pago aprobado por administrador', 3, 1526601600, 18, 5, 2018, 20, 1558137600, 0, 0),
(2, 4, 1997.00, 'Pago aprobado por administrador', 3, 1526601600, 18, 5, 2018, 20, 1558137600, 0, 0),
(3, 6, 1097.00, 'Pago aprobado por administrador', 2, 1526688000, 19, 5, 2018, 20, 1558224000, 0, 0),
(4, 3, 1097.00, 'Pago aprobado por administrador', 2, 1526601600, 18, 5, 2018, 20, 1558137600, 0, 0),
(5, 5, 597.00, 'Pago aprobado por administrador', 1, 1526688000, 19, 5, 2018, 20, 1558224000, 0, 0),
(6, 5, 1997.00, 'Pago aprobado por administrador', 3, 1526688000, 19, 5, 2018, 20, 1558224000, 0, 0),
(7, 7, 597.00, 'Pago aprobado por administrador', 1, 1526860800, 21, 5, 2018, 21, 1558396800, 0, 0),
(8, 7, 597.00, 'Pago aprobado por administrador', 1, 1526601600, 18, 5, 2018, 20, 1558137600, 0, 0),
(9, 7, 1097.00, 'Pago aprobado por administrador', 2, 1526688000, 19, 5, 2018, 20, 1558224000, 0, 0),
(10, 7, 1997.00, 'Pago aprobado por administrador', 3, 1526688000, 19, 5, 2018, 20, 1558224000, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mlm_niveles`
--

CREATE TABLE `mlm_niveles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mlm_niveles`
--

INSERT INTO `mlm_niveles` (`id`, `nombre`) VALUES
(1, 'Nivel 1'),
(2, 'Nivel 2'),
(3, 'Nivel 3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mlm_paquetes`
--

CREATE TABLE `mlm_paquetes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `precio` float(8,2) NOT NULL,
  `impuesto` float(8,2) NOT NULL,
  `pmin` float(8,2) NOT NULL,
  `moneda` int(11) NOT NULL,
  `duracion` int(11) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `niveles` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mlm_paquetes`
--

INSERT INTO `mlm_paquetes` (`id`, `nombre`, `descripcion`, `precio`, `impuesto`, `pmin`, `moneda`, `duracion`, `tipo`, `niveles`) VALUES
(1, 'Aprendiz', 'Aprendiz', 597.00, 0.00, 0.00, 0, 365, 'bir', '10;5;5'),
(2, 'Emprendedor', 'Emprendedor', 1097.00, 0.00, 0.00, 0, 365, 'bir', '10;5;5'),
(3, 'Inversionista', 'Inversionista', 1997.00, 0.00, 0.00, 0, 365, 'bir', '10;10;10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mlm_servidores`
--

CREATE TABLE `mlm_servidores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `libre` int(11) NOT NULL DEFAULT '0',
  `ocupado` int(11) NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mlm_usuarios`
--

CREATE TABLE `mlm_usuarios` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `referido` varchar(255) NOT NULL,
  `pin` varchar(11) NOT NULL DEFAULT '0576',
  `idiml` varchar(255) NOT NULL,
  `pais` varchar(255) NOT NULL,
  `freg` int(11) NOT NULL,
  `cactiva` varchar(255) NOT NULL,
  `monto` float(8,2) NOT NULL,
  `mtotal` float(8,2) NOT NULL,
  `retirado` float(8,2) NOT NULL DEFAULT '0.00',
  `rango` int(11) NOT NULL,
  `id_paquete` int(11) NOT NULL,
  `expiracion` int(11) NOT NULL,
  `nbanco` varchar(255) NOT NULL,
  `ncuenta` varchar(255) NOT NULL,
  `nucuenta` varchar(255) NOT NULL,
  `tcuenta` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mlm_usuarios`
--

INSERT INTO `mlm_usuarios` (`id`, `id_usuario`, `referido`, `pin`, `idiml`, `pais`, `freg`, `cactiva`, `monto`, `mtotal`, `retirado`, `rango`, `id_paquete`, `expiracion`, `nbanco`, `ncuenta`, `nucuenta`, `tcuenta`) VALUES
(1, 1, '', '0576', '0', 'PE', 1526671002, '0', 908.05, 908.05, 0.00, 0, 2, 0, '', '', '', '1'),
(2, 2, 'creex', '0576', '', 'NZ', 1526671146, '1', 0.00, 0.00, 0.00, 0, 3, 1558137600, '', '', '', '1'),
(3, 3, 'creex', '0576', '', 'CL', 1526704068, '1', 259.40, 259.40, 0.00, 0, 2, 1558137600, '', '', '', '1'),
(4, 4, 'creex', '5079', '', 'CL', 1526704082, '1', 538.50, 538.50, 0.00, 0, 3, 1558137600, '', '', '', '1'),
(5, 5, 'gabriel', '0576', '', 'CL', 1526704305, '1', 0.00, 0.00, 0.00, 0, 3, 1558224000, '', '', '', '1'),
(6, 6, 'soporte', '0576', '', 'CL', 1526704365, '1', 428.80, 428.80, 0.00, 0, 2, 1558224000, '', '', '', '1'),
(7, 7, 'soporte2', '0576', '', 'CL', 1526705385, '1', 0.00, 0.00, 0.00, 0, 3, 1558224000, '', '', '', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mlm_vps`
--

CREATE TABLE `mlm_vps` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `usuario` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `gserv` int(11) NOT NULL,
  `adm` int(11) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monedas`
--

CREATE TABLE `monedas` (
  `id` int(10) UNSIGNED NOT NULL,
  `moneda` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `portafolio_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticia`
--

CREATE TABLE `noticia` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_es` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `noticia`
--

INSERT INTO `noticia` (`id`, `titulo`, `link_es`, `link_en`, `estado`, `usuario_creacion`, `usuario_modificacion`, `created_at`, `updated_at`) VALUES
(1, 'Cointelegraph', 'https://es.cointelegraph.com/rss', 'https://cointelegraph.com/rss', 1, 1, 1, NULL, NULL),
(2, 'Bitcoin', 'https://noticias.bitcoin.com/feed/', 'https://news.bitcoin.com/feed/', 1, 1, 1, NULL, NULL),
(3, 'CoinStaker', 'https://www.coinstaker.com/es/feed/', 'https://www.coinstaker.com/feed/', 1, 1, 1, NULL, NULL),
(4, 'Bitcoinmagazine', '', 'https://bitcoinmagazine.com/feed', 1, 1, 1, NULL, NULL),
(5, 'Newsbtc.com', NULL, 'https://www.newsbtc.com/feed/', 1, 1, 1, NULL, NULL),
(6, 'Cryptocurrencynews.com', NULL, 'https://cryptocurrencynews.com/feed/', 1, 1, 1, NULL, NULL),
(7, 'CCN.com', NULL, 'https://www.ccn.com/feed/', 1, 1, 1, NULL, NULL),
(8, 'CryptoNoticias.com', 'https://www.criptonoticias.com/feed/', NULL, 1, 1, 1, NULL, NULL),
(9, 'ElBitcoin.org', 'https://elbitcoin.org/feed/', NULL, 1, 1, 1, NULL, NULL),
(10, 'SobreBitCoin', 'http://sobrebitcoin.com/feed/', NULL, 1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operaciones`
--

CREATE TABLE `operaciones` (
  `id` int(10) UNSIGNED NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `operacion` tinyint(1) NOT NULL DEFAULT '0',
  `moneda` tinyint(1) NOT NULL DEFAULT '0',
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `moneda_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id` int(10) UNSIGNED NOT NULL,
  `membresia_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  `estado` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`id`, `membresia_id`, `user_id`, `monto`, `estado`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '49.75', 1, '2018-05-22 23:06:23', '2018-05-22 23:06:23'),
(2, 1, 2, '49.75', 1, '2018-05-22 23:11:10', '2018-05-22 23:11:10'),
(3, 1, 2, '49.75', 1, '2018-05-22 23:26:02', '2018-05-22 23:26:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portafolio`
--

CREATE TABLE `portafolio` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ref_temp`
--

CREATE TABLE `ref_temp` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `nivel` int(11) NOT NULL,
  `id_usr` int(11) NOT NULL,
  `cactiva` int(11) NOT NULL,
  `freg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessions`
--

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL,
  `usuario` varchar(255) NOT NULL,
  `session_hash` varchar(255) NOT NULL,
  `fin` varchar(255) NOT NULL,
  `lockscreen` int(11) NOT NULL,
  `activo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sessions`
--

INSERT INTO `sessions` (`id`, `usuario`, `session_hash`, `fin`, `lockscreen`, `activo`) VALUES
(1, 'creex', '391094c6334a6d97a56e71eac7beff72', '1526714308', 1, 1),
(2, 'creex', 'fdc00aeb5f6717bb7e979bb16585c31c', '1526731391', 1, 1),
(3, 'creex', '2a48246ce633055cb2ff094aa34048eb', '1526745967', 1, 1),
(7, 'creex', 'a3bed4c5b6435415e3ce87dab41d7533', '1526747581', 0, 1),
(13, 'creex', '3af4f316a380f1c18cde29ac4a0ba90f', '1526798475', 1, 1),
(14, 'creex', 'f6df4980e0e0e850bbae1bb1c93dd61e', '1526827008', 1, 1),
(15, 'creex', '7ed175491210ac5d8cb1e004c4019bc9', '1526834243', 1, 1),
(16, 'jhoann1', '5e581fcaace2454eeab4e4df61e6b4ac', '1526834315', 0, 1),
(19, 'soporte', '9c3090b1c7eecc6b3f930d99b7b1f80f', '1526909612', 0, 1),
(20, 'soporte', '2918dbb21294b0a03fad7adf5a7c7cd3', '1526927057', 1, 1),
(22, 'soporte', '582b1d7f44fc508e32098e8fd6f61fba', '1526961705', 0, 1),
(23, 'creex', '9314e37f2321aaba0d42d2405f3cd173', '1526966613', 0, 1),
(24, 'creex', '8589b7c37d39171991e93d43189e06b6', '1526967694', 1, 1),
(25, 'soporte', '2134b93cd128afb36eb09307a6bb6baf', '1527046500', 0, 1),
(27, 'creex', 'd40fcdb94180e0086d6fd4dafdec1208', '1527091234', 0, 1),
(28, 'soporte', 'd181415db31a34a2006220d57ac5f2d3', '1527091517', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `signals`
--

CREATE TABLE `signals` (
  `id` int(10) UNSIGNED NOT NULL,
  `membresia_id` int(10) UNSIGNED NOT NULL,
  `coinname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coinsymbol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coinimgurl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exchange` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cointradeurl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coinprice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stoploss` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pricet30m` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet1h` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet3h` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet6h` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet12h` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet24h` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricet48h` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `membresia_id` int(10) UNSIGNED NOT NULL,
  `nickname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cellphone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wallet` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `id_referidor` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '0',
  `nivel` int(11) NOT NULL DEFAULT '1',
  `cnivel1` int(11) NOT NULL DEFAULT '0',
  `cnivel2` int(11) NOT NULL DEFAULT '0',
  `cnivel3` int(11) NOT NULL DEFAULT '0',
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `facebook_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `github_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `membresia_id`, `nickname`, `name`, `lastname`, `email`, `cellphone`, `password`, `wallet`, `id_referidor`, `estado`, `nivel`, `cnivel1`, `cnivel2`, `cnivel3`, `role`, `remember_token`, `created_at`, `updated_at`, `facebook_id`, `google_id`, `github_id`) VALUES
(2, 1, 'ilanalexis', 'Alexis', 'Vaquez', 'alexis@diferenciarte.pe', '123456', '$2y$10$E/GKT0M93D/1BYnBiYqfl.j7lDc4x6Mnf/ExYrbOxxvWk6vSAWISa', '', NULL, 0, 1, 0, 0, 0, 'user', NULL, '2018-05-13 00:54:30', '2018-05-22 23:26:02', NULL, NULL, NULL),
(3, 1, 'josejose', 'jose', 'roberto', 'jose@gmail.com', '123456789', '$2y$10$WqYvK/HLPN4DQz3YH1GZge.wdgFbYGWswuyfDM8okc6AnNFhhuHUW', '', NULL, 0, 1, 0, 0, 0, 'invitado', NULL, '2018-05-24 21:19:50', '2018-05-24 21:19:50', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `rut` varchar(255) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `usuario` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `sexo` varchar(255) NOT NULL DEFAULT 'masculino',
  `imagen` varchar(255) NOT NULL DEFAULT './imagenes/admin.png',
  `direccion` varchar(255) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `id_grupo` int(11) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellido`, `rut`, `correo`, `usuario`, `pass`, `sexo`, `imagen`, `direccion`, `ciudad`, `region`, `telefono`, `id_grupo`, `estado`) VALUES
(1, 'Administrador', 'Master', '0000000-0', 'administracion@grupocreex.com', 'creex', 'd69037257ad5efa6dedbf4e226f0ae48', 'masculino', 'sep-includes/adminlte/imagenes/admin.png', 'NA', 'NA', 'NA', '', 1, 1),
(2, 'jhoan', 'pacahuala', '87632', 'jhoan@gmail.com', 'jhoann1', '5f99c627364865f1e93eb2844afef5e3', 'masculino', 'sep-includes/adminlte/imagenes/admin.png', 'ima', 'lima', 'lima', '', 3, 1),
(3, 'Gabriel', 'Rodriguez', '007', 'gabriel@tal.cl', 'gabriel', '25d55ad283aa400af464c76d713c07ad', 'masculino', 'sep-includes/adminlte/imagenes/admin.png', 'Sady ZaÃ±artu', 'Taltal', 'AN', '', 2, 1),
(4, 'Soporte', 'soporte', '1111111', 'forex-chile@hotmail.com', 'soporte', 'ddbaabbd45b3891210ef476e743e4c8e', 'masculino', './sep-content/multimedia/soporte-perfil.jpg', 'estacion', 'antofa', 'antofa', '', 1, 1),
(5, 'Gabriel', 'Rodriguez', '007', 'gabriel2@tal.cl', 'gabriel2', '25d55ad283aa400af464c76d713c07ad', 'masculino', 'sep-includes/adminlte/imagenes/admin.png', 'Sady ZaÃ±artu', 'Taltal', 'AN', '', 2, 1),
(6, 'Alex', 'Rodriguez', '222222222', 'elazouniverteam@gmail.com', 'soporte2', 'ddbaabbd45b3891210ef476e743e4c8e', 'masculino', './sep-content/multimedia/soporte2-perfil.jpeg', 'estacion', 'antfo', 'antofq', '', 2, 1),
(7, 'soporte3', 'soporte3', '333333333', 'elazo@demo.cl', 'soporte3', 'ddbaabbd45b3891210ef476e743e4c8e', 'masculino', 'sep-includes/adminlte/imagenes/admin.png', 'esta', 'antofa', 'antofa', '', 2, 1),
(8, 'jose', 'roberto', '12345678', 'jose@gmail.com', 'josejose', '$2y$10$AHvoSSJmdsOAibZvAd1hDujtPtjnRP.Gzbu3WjKJwnSguLDOXx.ra', 'masculino', './imagenes/admin.png', NULL, NULL, NULL, NULL, 2, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `archivo`
--
ALTER TABLE `archivo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `archivo_leccion_id_foreign` (`leccion_id`);

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`code`),
  ADD UNIQUE KEY `alpha2` (`alpha2`),
  ADD UNIQUE KEY `alpha3` (`alpha3`);

--
-- Indices de la tabla `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `curso_educategoria_id_foreign` (`educategoria_id`);

--
-- Indices de la tabla `detalle_membresia`
--
ALTER TABLE `detalle_membresia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detalle_membresia_membresia_id_foreign` (`membresia_id`);

--
-- Indices de la tabla `educategoria`
--
ALTER TABLE `educategoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `leccion`
--
ALTER TABLE `leccion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leccion_curso_id_foreign` (`curso_id`);

--
-- Indices de la tabla `leccion_usuario`
--
ALTER TABLE `leccion_usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leccion_usuario_leccion_id_foreign` (`leccion_id`);

--
-- Indices de la tabla `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mail`
--
ALTER TABLE `mail`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `membresia`
--
ALTER TABLE `membresia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mlm_autotrader`
--
ALTER TABLE `mlm_autotrader`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mlm_chistorial`
--
ALTER TABLE `mlm_chistorial`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mlm_comisiones`
--
ALTER TABLE `mlm_comisiones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mlm_correo`
--
ALTER TABLE `mlm_correo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mlm_correo_vars`
--
ALTER TABLE `mlm_correo_vars`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mlm_egresos`
--
ALTER TABLE `mlm_egresos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mlm_empresa`
--
ALTER TABLE `mlm_empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mlm_factura`
--
ALTER TABLE `mlm_factura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mlm_formsec`
--
ALTER TABLE `mlm_formsec`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mlm_mchistorial`
--
ALTER TABLE `mlm_mchistorial`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mlm_niveles`
--
ALTER TABLE `mlm_niveles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mlm_paquetes`
--
ALTER TABLE `mlm_paquetes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mlm_servidores`
--
ALTER TABLE `mlm_servidores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mlm_usuarios`
--
ALTER TABLE `mlm_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mlm_vps`
--
ALTER TABLE `mlm_vps`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `monedas`
--
ALTER TABLE `monedas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `monedas_portafolio_id_foreign` (`portafolio_id`);

--
-- Indices de la tabla `noticia`
--
ALTER TABLE `noticia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `operaciones`
--
ALTER TABLE `operaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `operaciones_moneda_id_foreign` (`moneda_id`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pagos_membresia_id_foreign` (`membresia_id`),
  ADD KEY `pagos_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `portafolio`
--
ALTER TABLE `portafolio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ref_temp`
--
ALTER TABLE `ref_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `signals`
--
ALTER TABLE `signals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `signals_membresia_id_foreign` (`membresia_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_nickname_unique` (`nickname`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_membresia_id_foreign` (`membresia_id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `archivo`
--
ALTER TABLE `archivo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `curso`
--
ALTER TABLE `curso`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `detalle_membresia`
--
ALTER TABLE `detalle_membresia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `educategoria`
--
ALTER TABLE `educategoria`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `grupo`
--
ALTER TABLE `grupo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `leccion`
--
ALTER TABLE `leccion`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `leccion_usuario`
--
ALTER TABLE `leccion_usuario`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `mail`
--
ALTER TABLE `mail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `membresia`
--
ALTER TABLE `membresia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `mlm_autotrader`
--
ALTER TABLE `mlm_autotrader`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mlm_chistorial`
--
ALTER TABLE `mlm_chistorial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `mlm_comisiones`
--
ALTER TABLE `mlm_comisiones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mlm_correo`
--
ALTER TABLE `mlm_correo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `mlm_correo_vars`
--
ALTER TABLE `mlm_correo_vars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `mlm_egresos`
--
ALTER TABLE `mlm_egresos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mlm_empresa`
--
ALTER TABLE `mlm_empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `mlm_factura`
--
ALTER TABLE `mlm_factura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `mlm_formsec`
--
ALTER TABLE `mlm_formsec`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mlm_mchistorial`
--
ALTER TABLE `mlm_mchistorial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `mlm_niveles`
--
ALTER TABLE `mlm_niveles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `mlm_paquetes`
--
ALTER TABLE `mlm_paquetes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `mlm_servidores`
--
ALTER TABLE `mlm_servidores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mlm_usuarios`
--
ALTER TABLE `mlm_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `mlm_vps`
--
ALTER TABLE `mlm_vps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `monedas`
--
ALTER TABLE `monedas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `noticia`
--
ALTER TABLE `noticia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `operaciones`
--
ALTER TABLE `operaciones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `portafolio`
--
ALTER TABLE `portafolio`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ref_temp`
--
ALTER TABLE `ref_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT de la tabla `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `signals`
--
ALTER TABLE `signals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `archivo`
--
ALTER TABLE `archivo`
  ADD CONSTRAINT `archivo_leccion_id_foreign` FOREIGN KEY (`leccion_id`) REFERENCES `leccion` (`id`);

--
-- Filtros para la tabla `curso`
--
ALTER TABLE `curso`
  ADD CONSTRAINT `curso_educategoria_id_foreign` FOREIGN KEY (`educategoria_id`) REFERENCES `educategoria` (`id`);

--
-- Filtros para la tabla `detalle_membresia`
--
ALTER TABLE `detalle_membresia`
  ADD CONSTRAINT `detalle_membresia_membresia_id_foreign` FOREIGN KEY (`membresia_id`) REFERENCES `membresia` (`id`);

--
-- Filtros para la tabla `leccion`
--
ALTER TABLE `leccion`
  ADD CONSTRAINT `leccion_curso_id_foreign` FOREIGN KEY (`curso_id`) REFERENCES `curso` (`id`);

--
-- Filtros para la tabla `leccion_usuario`
--
ALTER TABLE `leccion_usuario`
  ADD CONSTRAINT `leccion_usuario_leccion_id_foreign` FOREIGN KEY (`leccion_id`) REFERENCES `leccion` (`id`);

--
-- Filtros para la tabla `monedas`
--
ALTER TABLE `monedas`
  ADD CONSTRAINT `monedas_portafolio_id_foreign` FOREIGN KEY (`portafolio_id`) REFERENCES `portafolio` (`id`);

--
-- Filtros para la tabla `operaciones`
--
ALTER TABLE `operaciones`
  ADD CONSTRAINT `operaciones_moneda_id_foreign` FOREIGN KEY (`moneda_id`) REFERENCES `monedas` (`id`);

--
-- Filtros para la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD CONSTRAINT `pagos_membresia_id_foreign` FOREIGN KEY (`membresia_id`) REFERENCES `membresia` (`id`),
  ADD CONSTRAINT `pagos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `signals`
--
ALTER TABLE `signals`
  ADD CONSTRAINT `signals_membresia_id_foreign` FOREIGN KEY (`membresia_id`) REFERENCES `membresia` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_membresia_id_foreign` FOREIGN KEY (`membresia_id`) REFERENCES `membresia` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
