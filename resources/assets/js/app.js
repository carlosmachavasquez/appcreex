
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('Leaderboard', require('./components/Leaderboard.vue'));

// axios.get('/signaltable')
// 	.then((response) => {
    	// console.log("leader AXIOS");
    	//this.users = response.data;
    	// console.log(response);
// })

Echo.channel('signaltable')
	.listen('SignalEvent', (e) => {
	    // console.log("listenECHO");
	    // console.log(e);
	    // console.log(e.membresia_id);
	   	var rowSignals = document.getElementsByClassName('row-signal');
	   	var update = false;
	   	var targetRow = {};
	    //Notification
		for (var i = 0; i < rowSignals.length; i++) {
			if ( parseInt(rowSignals[i].id) == parseInt(e.coinid) ){
				update = true;
				targetRow = rowSignals[i];
			}
		}
		if (update){
			toastr.info('La señal para la criptomoneda '+e.coinname+' | '+e.coinsymbol+' fue actualizada')
		}else{
			toastr.info('Una nueva señal para la criptomoneda '+e.coinname+' | '+e.coinsymbol+' fue agregada');
		}
	    

	    var membresiaUserId = parseInt(document.getElementById('membresiaUserId').innerHTML);
	    var auth = false;

	    if (membresiaUserId == 1){
			if (e.membresia_id == 1){
				auth = true;
			}
	    }else if(membresiaUserId == 2){
			if (e.membresia_id <= 2){
				auth = true;
			}	
	    }else if(membresiaUserId == 3){
			if (e.membresia_id <= 3){
				auth = true;
			}
	    }

	    if (auth){

	    	if (update){
	    		targetRow.remove();
	    	}

		    //Update Table
		    //{coinname: "Stratis", coinsymbol: "STRAT", coinimgurl: "https://www.cryptocompare.com/media/351303/stratis-logo.png", exchange: "Binance", cointradeurl: "https://bittrex.com/Market/Index?MarketName=BTC-STRAT", …}
		    var tableRef = document.getElementById('userSignals').getElementsByTagName('tbody')[0];
			var row = tableRef.insertRow(0);
			var cell1 = row.insertCell(0);
			var cell2 = row.insertCell(1);
			cell2.className = 'font-14 dropdown-coin-td';
			cell2.setAttribute("scope", "row");
			var cell3 = row.insertCell(2);
			var cell4 = row.insertCell(3);

			var cell5 = row.insertCell(4);
			var cell6 = row.insertCell(5);
			var cell7 = row.insertCell(6);
			var cell8 = row.insertCell(7);
			var cell9 = row.insertCell(8);
			var cell10 = row.insertCell(9);
			var cell11 = row.insertCell(10);
			var cell12 = row.insertCell(11);
			var cell13 = row.insertCell(12);

			function ElapsedTime () {
				var nTotalDiff = Math.round((new Date()).getTime() / 1000) - e.create_at;
				if (nTotalDiff >= 0) {
					var oDiff = {};
					oDiff.days = Math.floor(nTotalDiff / 86400);
					nTotalDiff -= oDiff.days * 86400;
					oDiff.hours = Math.floor(nTotalDiff / 3600);
					nTotalDiff -= oDiff.hours * 3600;
					oDiff.minutes = Math.floor(nTotalDiff / 60);
					nTotalDiff -= oDiff.minutes * 60;
					oDiff.seconds = Math.floor(nTotalDiff);
					return oDiff;
				} else {
					console('nTotalDiff still not >=0, so must be before the specified date');
				}
			}

			function TimePassed() {
				oDiff = ElapsedTime();
				if(oDiff) {
					if(oDiff.days == 0 && oDiff.hours == 0 && oDiff.seconds == 0){
						return oDiff.seconds + ' s';
					}else if(oDiff.days == 0 && oDiff.hours == 0){
						return oDiff.minutes + ' m, ' + oDiff.seconds + ' s';
					}else if(oDiff.days == 0){
						return oDiff.hours + ' h, ' + oDiff.minutes + ' m, ' + oDiff.seconds + ' s';
					}else if(oDiff.days > 0){
						return oDiff.days + ' d, ' + oDiff.hours + ' h, ' + oDiff.minutes + ' m, ' + oDiff.seconds + ' s';
					}
				}
			}

			cell1.innerHTML = TimePassed();
			
			cell2.innerHTML = "<div class=\"btn-group\"><a aria-expanded=\"false\" aria-haspopup=\"true\" class=\"btn dropdown-toggle\" data-toggle=\"dropdown\"><span data-original-title=\""+e.coinname+"\" data-placement=\"top\" data-toggle=\"tooltip\" title=\"" +e.coinname+ "\"><img src=\""+e.coinimgurl+"?anchor=center&mode=crop&width=32&height=32\" width=\"24\"><b>" +e.coinsymbol+ "</b></span></a><div class=\"dropdown-menu dropdown-menu-right\" style=\"position: absolute; transform: translate3d(100px, 38px, 0px); top: 0px; left: 0px; will-change: transform;\"><a class=\"dropdown-item\" href=\"/dashboard/user/coinmarket/"+e.coinsymbol+"/"+e.coinid+"\">Ver Detalles</a> <a class=\"dropdown-item\" href=\"#\">Marcar Señal</a> <a class=\"dropdown-item\" href=\""+e.cointradeurl+"\" target=\"_blank\">Tradear Señal</a></div></div>";
			cell3.innerHTML = e.exchange;
			cell4.innerHTML = e.coinprice;

			cell5.innerHTML = "";
			cell6.innerHTML = "<b><div id=\"price30mpasscreatePercent_"+e.coinsymbol+"\" class=\"badge badge-default font-12\"> -- %</div></b>";
			cell7.innerHTML = "<b><div id=\"price1hpasscreatePercent_"+e.coinsymbol+"\" class=\"badge badge-default font-12\"> -- %</div></b>";
			cell8.innerHTML = "<b><div id=\"price3hpasscreatePercent_"+e.coinsymbol+"\" class=\"badge badge-default font-12\"> -- %</div></b>";
			cell9.innerHTML = "<b><div id=\"price6hpasscreatePercent_"+e.coinsymbol+"\" class=\"badge badge-default font-12\"> -- %</div></b>";
			cell10.innerHTML = "<b><div id=\"price12hpasscreatePercent_"+e.coinsymbol+"\" class=\"badge badge-default font-12\"> -- %</div></b>";
			cell11.innerHTML = "<b><div id=\"price24hpasscreatePercent_"+e.coinsymbol+"\" class=\"badge badge-default font-12\"> -- %</div></b>";
			cell12.innerHTML = "<b><div id=\"price48hpasscreatePercent_"+e.coinsymbol+"\" class=\"badge badge-default font-12\"> -- %</div></b>";
			cell13.innerHTML = "<b><div id=\"bestpercentpricePercent_"+e.coinsymbol+"\" class=\"badge badge-success font-12\"> -- %</div></b>";

			setTimeout(function() {
				location.reload();
			},10000);	
	    }
})

const app = new Vue({
    el: '#app'
});
