@foreach($categorias as $data)
<tr>
    <th>{{ $data->id }}</th>
    <th>{{ $data->nombre }}</th>
    <th>
        @if ($data->estado == 1)
            <span class="btn btn-xs btn-success">Activo</span>
        @endif
    </th>
    <th>
        <div class="cont_btn_actions" data-toggle="tooltip" data-placement="top" title="Editar">
            <button type="button" class="btn btn-outline-primary" onclick="mostrar_datos({{ $data->id }})">Editar</button>
            <button type="button" class="btn btn-outline-danger" onclick="eliminar_categoria({{ $data->id }})">Eliminar</button>
        </div>
    </th>
</tr>
@endforeach