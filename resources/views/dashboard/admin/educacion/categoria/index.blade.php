@extends('dashboard.layouts.principal')
@section('styles')

    <style>
        .table th{
            color: #000;
        }
        
        .thead-dark{
            font-weight: bold;
            background: #212529;
        }

        .thead-dark th{
            color: #fff;
        }
    </style>
@endsection
@section('scripts')
    <script>
        

        //listar
        //datatable
        function listar_categoria_edu()
        {   
            var route = "{{ url('dashboard/admin/education/category/listar_categoria_edu') }}";
             $.get(route, function(value){
                $("#result_categoria").html(value);
                
            });
        }

        function agregar()
        {
            $('.form_editar').hide();
            $('.form_listar').hide();
            $('.form_agregar').show();
        }

        function editar()
        {
            $('.form_listar').hide();
            $('.form_agregar').hide();
            $('.form_editar').show();
        }

        function listar()
        {
            $('.form_editar').hide();
            $('.form_agregar').hide();
            $('.form_listar').show();
        }

        $('#form_cat_reg').submit(function(e){
                e.preventDefault();
                var form = $(this);
                var data = form.serialize()
                var token = $('#token').val();
                var route = "{{url('dashboard/admin/education/category/register')}}";     
         
                $.ajax({
                    url: route,
                    headers: { 'X-CSRF-TOKEN': token },
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    success:function(data){
                        if (data.success == 'true') {                   
                            $('.input').val('');
                            $('#result_categoria').empty();
                            listar_categoria_edu();
                            listar();
                            toastr.success(data.msg, data.titulo);
                        }
                    },

                    error:function(msj){         
                        console.log('no se pudo jejej');
                        //if (msj.responseJSON.nombre != '') {
                            //$('.val_nom_ag').append('holaaaa ...');
                        //}                
                    }
                }); 
        });

        function mostrar_datos(data)
        {
            $('.input').val('');
            editar();
            var route = "{{url('dashboard/admin/education/category/show')}}/"+data;
            //alert(route); 
            $.get(route, function(value){
                $("#nombre_catedu_ed").val(value.nombre);
                $("#descripcion_catedu_ed").val(value.descripcion);
                $('#id_catedu_ed').val(value.id);
                
            });
        }

        $('#form_cat_act').submit(function(e){
            e.preventDefault();
            var form = $(this);
            var data = form.serialize()
            var token = $('#token').val();
            var route = "{{url('dashboard/admin/education/category/update')}}";
            $.ajax({
                url: route,
                headers: { 'X-CSRF-TOKEN': token },
                type: 'POST',
                dataType: 'json',
                data: data,
                success:function(data){
                    if (data.success == 'true') {
                        $('#result_categoria').empty();
                        listar_categoria_edu();
                        listar();
                        toastr.success(data.msg, data.titulo);
                    }else{
                       toastr.danger('Hubo un inconveniente, consultar con el desarrollador', 'Opps!!');

                    }
                },

                error:function(msj){ 

                    //if (msj.responseJSON.nombre != '') {
                        $('.val_nom_ag').empty().append(msj.errors);
                    //}
                }
            }); 
        });

        function eliminar_categoria(data)
        {
            swal({
              title: "¿Estas seguro deeliinar?",
              text: "El registro seeliminará!",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                var id = data;
                var token = $('#token').val();
                var route = "{{url('dashboard/admin/education/category/remove')}}";       
                $.ajax({
                    url: route,
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id, _token: token},
                    success:function(data){
                        if (data.success == 'true') {
                            $('#result_categoria').empty();
                            listar_categoria_edu();
                            listar();
                            swal("Poof! ha sido eliminado con éxito!", {
                              icon: "success",
                            });
                        }
                    },

                    error:function(data){         
                        console.log('no se pudo');
                    }
                });
              } else {
                swal("Your imaginary file is safe!");
              }
            });
        }
    </script>
@endsection

@section('content')
    <div class="main-content page-category form_listar">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">Categoría</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Educación</a></li>
                <li class="breadcrumb-item active">Categoría</li>
            </ol>
        </div>
        <div class="card">
            <div class="card-header">
               <button type="button" class="btn btn-outline-info" onclick="agregar();">Agregar</button>
            </div>
            <div class="card-block">
                <table id="table_cat_edu" class="table table-striped table-bordered " cellspacing="0"
                       width="100%">
                    <thead class="thead-dark">
                    <tr>
                        <th>id</th>
                        <th>nombre</th>
                        <th>estado</th>
                        <th>Accion</th>
                    </tr>
                    </thead>
                    <tbody id="result_categoria">
                       @include('dashboard.admin.educacion.categoria.result.categoria')
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    
    @include('dashboard.admin.educacion.categoria.forms.add')
    @include('dashboard.admin.educacion.categoria.forms.edit')
@endsection