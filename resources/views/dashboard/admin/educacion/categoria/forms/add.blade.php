<div class="main-content form_agregar" style="display: none;">
    <div class="card">
        <div class="card-header">
           <h2 style="margin-bottom: 0; color: 808080;">Agregar categoría</h2>
        </div>
        <div class="card-block">
            <form id="form_cat_reg" class="form_cat_reg">
                <div class="modal-body">                    
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                    <div class="form-group">
                        <label>Nombre de Categoría</label>
                        <input type="text" class="form-control input" name="nombre"
                               placeholder="Categoría">
                               <span class="val_nom_ag"></span>
                    </div>
                    <div class="form-group">
                        <label>Descripción</label>
                        <textarea name="descripcion" class="form-control input" placeholder="Descrición"></textarea>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="listar()">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>


