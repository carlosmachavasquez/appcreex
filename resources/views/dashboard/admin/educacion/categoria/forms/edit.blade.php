<div class="main-content form_editar" style="display: none;">
    <div class="card">
        <div class="card-header">
           <h2 style="margin-bottom: 0; color: 808080;">Editar categoría</h2>
        </div>
        <div class="card-block">
            <form id="form_cat_act" class="form_cat_act" autocomplete="off">
                <div class="modal-body">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                    <input type="hidden" name="id_cat" value="" id="id_catedu_ed">
                    <div class="form-group">
                        <label>Nombre de Categoría</label>
                        <input type="text" class="form-control input" id="nombre_catedu_ed" name="nombre"
                               placeholder="Categoría">
                    </div>
                    <div class="form-group">
                        <label>Descripción</label>
                        <textarea name="descripcion" class="form-control input" id="descripcion_catedu_ed" placeholder="Descrición"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="listar();">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>