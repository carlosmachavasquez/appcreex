@extends('dashboard.layouts.principal')
@section('styles')
    <style>
        #result_lecciones th, #archivos_list th{
            color: #2E2D2D!important;
        }
        .table th{
            color: #000;
        }
        
        .thead-dark{
            font-weight: bold;
            background: #212529;
        }

        .thead-dark th{
            color: #fff;
        }
    </style>
@endsection
@section('content')
<div class="main-content page-course">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">Lección</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Educación</a></li>
                <li class="breadcrumb-item active">Lección</li>
            </ol>
        </div>
    </div>
<div class="card form_list_leccion">
    <div class="card-header">
       <button type="button" class="btn btn-outline-info" onclick="abrir_form_add()">Agregar</button>
    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
    <div class="card-block">
        <table id="table_leccion" class="table table-striped table-bordered " cellspacing="0"
               width="100%">
            <thead class="thead-dark">
            <tr>
                <th>Curso</th>
                <th>Lección</th>
                <th>Duración</th>
                <th>estado</th>
                <th>Accion</th>
            </tr>
            </thead>
            <tbody id="result_lecciones">
                @include('dashboard.admin.educacion.leccion.result.lecciones')
            </tbody>
        </table>
    </div>
</div>

@include('dashboard.admin.educacion.leccion.formularios.add')
@include('dashboard.admin.educacion.leccion.formularios.edit')
@include('dashboard.admin.educacion.leccion.modal.add_archivo')
@endsection

@section('scripts')
<script src="http://malsup.github.io/jquery.blockUI.js"></script>
<script>
	

    function abrir_form_add()
    {
        //selectCategoria();
        $('.form_list_leccion').hide();
        $('.form_edit_leccion').hide();
        $('.form_add_leccion').show();
    }

    function abrir_form_edit()
    {
        //selectCategoria();
        $('.form_list_leccion').hide();
        $('.form_add_leccion').hide();
        $('.form_edit_leccion').show();
        //mostrar_leccion(data);
    }

    function abrir_list_leccion()
    {
        $('.form_add_leccion').hide();
        $('.form_edit_leccion').hide();
        $('.form_list_leccion').show();        
    }


	//listar
	//datatable
	function listar_leccion()
    {   
        var route = "{{ url('dashboard/admin/education/leccion/listar_leccion') }}";
        $.get(route, function(value){
            $('#result_lecciones').html(value);
        });
    }



    function reset_input()
    {
        $('.input').val('');
        $("#descripcion").summernote('code', '');
    }

    $('#form_leccion_reg').submit(function(e){
        e.preventDefault();
        var curso_id = $('#lec_curso_id_add').val();
        var titulo = $('#lec_titulo_add').val();
        var descripcion = $('#lec_descripcion_add').summernote('code');//summernote
        var duracion = $('#lec_duracion_add').val();
        var video = $('#lec_video_add').val();
        var token = $('#token').val();
        
        var route = "{{url('dashboard/admin/education/leccion/registrar')}}";

        $.ajax({
            url: route,
            headers: { 'X-CSRF-TOKEN': token },
            type: 'POST',
            dataType: 'json',
            data: {curso_id: curso_id, titulo:titulo, descripcion:descripcion, duracion:duracion, video:video},
            success:function(data){
                if (data.success = 'true') {
                    $('.response_errors').hide();
                    reset_input();
                    $('#result_lecciones').empty();
                    listar_leccion();
                    toastr.success(data.msg, data.titulo);
                    abrir_list_leccion();
                }
            },
            error:function(data){         
                 $.unblockUI();
                 $('.response_errors').show();
                 $('.errors').empty();
                 var errors = data.responseJSON.errors;
                 $.each(errors, function (key, value) {
                    $('.errors').append('<li>'+value+'</li>');
                })             
            }
        }); 
    });

    function mostrar_leccion(data)
    {
        abrir_form_edit();
        var route = "{{url('dashboard/admin/education/leccion/mostrar')}}/"+data;
        //alert(route);
        $.get(route, function(value){
            $('#lec_id_leccion_ed').val(value.id);
            $('#lec_curso_id_ed').val(value.curso_id);
            $('#lec_titulo_ed').val(value.titulo);
            $('#lec_duracion_ed').val(value.duracion);
            $('#lec_video_ed').val(value.video);
            $('#lec_descripcion_ed').summernote("code",value.descripcion);
            
        });
    }

    $('#form_leccion_act').submit(function(e){
        e.preventDefault();
        var id_leccion = $('#lec_id_leccion_ed').val();
        var curso_id = $('#lec_curso_id_ed').val();
        var titulo = $('#lec_titulo_ed').val();
        var descripcion = $('#lec_descripcion_ed').summernote('code');//summernote
        var duracion = $('#lec_duracion_ed').val();
        var video = $('#lec_video_ed').val();
        var token = $('#token').val();
        
        var route = "{{url('dashboard/admin/education/leccion/actualizar')}}";

        $.ajax({
            url: route,
            headers: { 'X-CSRF-TOKEN': token },
            type: 'POST',
            dataType: 'json',
            data: {id_leccion: id_leccion,curso_id: curso_id, titulo:titulo, descripcion: descripcion, duracion:duracion, video:video},
            success:function(data){
                if (data.success = 'true') {
                    $('.response_errors').hide();
                    reset_input();
                    $('#result_lecciones').empty();
                    listar_leccion();
                    toastr.success(data.msg, data.titulo);
                    abrir_list_leccion();
                }
            },
            error:function(data){         
                 $.unblockUI();
                 $('.response_errors').show();
                 $('.errors').empty();
                 var errors = data.responseJSON.errors;
                 $.each(errors, function (key, value) {
                    $('.errors').append('<li>'+value+'</li>');
                })             
            }
        }); 
    });



    function eliminar_leccion(data)
    {
        swal({
          title: "¿Estas seguro de eliminar?",
          text: "El registro seeliminará!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            var id = data;
            var token = $('#token').val();
            var route = "{{url('dashboard/admin/education/leccion/eliminar')}}";       
            $.ajax({
                url: route,
                type: 'POST',
                dataType: 'json',
                data: {id: id, _token: token},
                success:function(data){
                    if (data.success == 'true') {
                        $('#result_lecciones').empty();
                        listar_leccion();
                        swal("Poof! ha sido eliminado con éxito!", {
                          icon: "success",
                        });
                    }
                },

                error:function(data){         
                    console.log('no se pudo');
                }
            });
          } else {
            swal("Your imaginary file is safe!");
          }
        });
        
    }

    //archivos
    function get_leccion(data)
    {
        var route = "{{url('dashboard/admin/education/leccion/mostrar_leccion_archivo')}}/"+data;
        //alert(route);
        $.get(route, function(value){
            $('#id_leccion_archivo').val(value.id);
            
        });
    }

    function mostrar_archivo(data)
    {
        //llamamos a la funcion para obtener el id de a leccion
        get_leccion(data);
        //destruimos la tabla antes de llenarla.
        //$('#table_archivos').empty();
        var route = "{{ url('dashboard/admin/education/leccion/mostrar_archivo') }}/"+data;

        $.get(route, function(value){
            $('#archivos_list').html(value);
            
        });
    }

    
    $('#archivo_leccion').change(function(e){
        e.preventDefault();
        $.blockUI({
            theme: false,
            baseZ: 2000,
            message: '<h2 style="font-size: 20px!important;"><img src="{{ asset('dashboard/load.gif') }}" style="width: 45px;" alt="load"/> Subiendo Archivo...</h2>'
        });

        var inputFileImage = document.getElementById("archivo_leccion");
        var file = inputFileImage.files[0];
        var data = new FormData(); 

        var leccion_id = $('#id_leccion_archivo').val();

        //agregamos al formdata
        data.append('archivo',file);
        data.append('leccion_id',leccion_id);
        var token = $('#token').val();
        
        var route = "{{url('dashboard/admin/education/leccion/subir_archivo')}}";

        $.ajax({
            async: true,
            headers: { 'X-CSRF-TOKEN': token },
            url: route,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            success:function(data){
                $.unblockUI();
                reset_input();
                $('#archivos_list').empty();
                mostrar_archivo(leccion_id);
                if (data.tipo == 'success') {
                    toastr.success(data.msg, data.titulo);
                }else{
                    toastr.error(data.msg, data.titulo);
                }
                
            },
            error:function(data){         
                 $.unblockUI();
                 $('.response_errors_file').show();
                 $('.errors').empty();
                 var errors = data.responseJSON.errors;
                 $.each(errors, function (key, value) {
                    $('.errors').append('<li>'+value+'</li>');
                })             
            }
        }); 
    });

    function eliminar_archivo(data)
    {
        var data = data;
        swal({
          title: "¿Estas seguro de eliminar?",
          text: "El registro seeliminará!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            var token = $('#token').val();
            var route = "{{url('dashboard/admin/education/leccion/eliminar_archivo')}}";       
            $.ajax({
                url: route,
                headers: { 'X-CSRF-TOKEN': token },
                type: 'POST',
                dataType: 'json',
                data: {id: data},
                success:function(data){
                    if (data.success == 'true') {
                        $('#archivos_list').empty();
                        mostrar_archivo(data.archivo.leccion_id);
                        swal("Poof! ha sido eliminado con éxito!", {
                          icon: "success",
                        });
                    }
                },

                error:function(data){         
                    console.log('no se pudo');
                }
            });
          } else {
            swal("Se canseló!");
          }
        });
        
    }
</script>
@endsection