@foreach($archivos as $data)
<tr>
    <th>{{ $data->archivo }}</th>
    <th>
        @if ($data->estado == 1)
            <span class="btn btn-xs btn-success">Activo</span>
        @endif
    </th>
    <th>
        <div class="cont_btn_actions">
            <button type="button" class="btn btn-outline-danger" onclick="eliminar_archivo({{ $data->id }})">Eliminar</button>
        </div>
    </th>
</tr>
@endforeach
