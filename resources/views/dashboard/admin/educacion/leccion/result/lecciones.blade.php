@foreach($lecciones as $data)
<tr>
    <th>{{ $data->curso }}</th>
    <th>{{ $data->titulo }}</th>
    <th>{{ $data->duracion }}</th>
    <th>
        @if ($data->estado == 1)
            <span class="btn btn-xs btn-success">Activo</span>
        @endif
    </th>
    <th>
        <div class="cont_btn_actions" title="Editar">
            <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target=".modal_archivo" onclick="mostrar_archivo({{ $data->id }})">Archivo</button>
            <button type="button" class="btn btn-outline-primary" onclick="mostrar_leccion({{ $data->id }})">Editar</button>
            <button type="button" class="btn btn-outline-danger" onclick="eliminar_leccion({{ $data->id }})">Eliminar</button>
        </div>
    </th>
</tr>
@endforeach