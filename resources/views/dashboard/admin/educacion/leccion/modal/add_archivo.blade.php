<div class="modal fade modal_archivo" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title cb" id="exampleModalLabel">Agregar Archivo</h5>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="hidden" id="id_leccion_archivo">
                                    <input type="file" class="form-control-file input" id="archivo_leccion">
                                </div>
                            </div>
                            <div class="linea"></div>
                            <div class="col-md-12">
                                <table id="table_archivos" class="table table-striped table-bordered " cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>Archivo</th>
                                    <th>estado</th>
                                    <th>Accion</th>
                                </tr>
                                </thead>
                                <tbody id="archivos_list">
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>