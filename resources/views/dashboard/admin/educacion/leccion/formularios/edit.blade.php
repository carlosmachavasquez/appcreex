<div class="card form_edit_leccion" style="display: none;">
    <div class="card-header">
       <h4>Editar Lección</h4>
    </div>
    <div class="card-block">
        <div class="alert alert-danger response_errors" style="display: none;">
            <div class="errors"></div>
        </div>
        <form id="form_leccion_act" class="form_leccion_act"> 
            <input type="hidden" id="lec_id_leccion_ed" name="id_leccion"> 
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Curso</label>
                        <select class="form-control input" name="curso_id" id="lec_curso_id_ed">
                            <option value="">Seleccione ...</option>
                            @foreach ($cursos as $data)
                                <option value="{{$data->id}}">{{$data->titulo}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Titulo de la Lección</label>
                        <input type="text" class="form-control input" name="titulo" id="lec_titulo_ed" 
                               placeholder="Título">
                    </div>  
                </div>
            </div>
                      
            <div class="form-group">
                <label>Descripción</label>
                <div class="ls-summernote" id="lec_descripcion_ed">
                    Descripción de la lección
                </div>
            </div>
            <div class="form-group">
                <label>Link de video</label>
                <input type="text" class="form-control input" name="video" id="lec_video_ed">
            </div>
            <div class="form-group">
                <label>Duración</label>
                <input type="text" class="form-control input" name="duracion" id="lec_duracion_ed" 
                       placeholder="Duración">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="abrir_list_leccion();">
                    Close
                </button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </form>
    </div>
</div>

