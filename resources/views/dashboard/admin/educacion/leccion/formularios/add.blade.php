<div class="card form_add_leccion" style="display: none;">
    <div class="card-header">
       <h4>Agregar Lección</h4>
    </div>
    <div class="card-block">
        <div class="alert alert-danger response_errors" style="display: none;">
            <div class="errors"></div>
        </div>
        <form id="form_leccion_reg" class="form_leccion_reg">  
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Curso</label>
                        <select class="form-control input" name="curso_id" id="lec_curso_id_add">
                            <option value="">Seleccione ...</option>
                            @foreach ($cursos as $data)
                                <option value="{{$data->id}}">{{$data->titulo}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Titulo de la Lección</label>
                        <input type="text" class="form-control input" name="titulo" id="lec_titulo_add" 
                               placeholder="Título">
                    </div>  
                </div>
            </div>
                      
            <div class="form-group">
                <label>Descripción</label>
                <div class="ls-summernote" id="lec_descripcion_add">
                    Descripción de la lección
                </div>
            </div>
            <div class="form-group">
                <label>Link de video</label>
                <input type="text" class="form-control input" name="video" id="lec_video_add" 
                       placeholder="Link de video">
            </div>
            <div class="form-group">
                <label>Duración</label>
                <input type="text" class="form-control input" name="duracion" id="lec_duracion_add" 
                       placeholder="Duración">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="abrir_list_leccion();">
                    Close
                </button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </form>
    </div>
</div>

