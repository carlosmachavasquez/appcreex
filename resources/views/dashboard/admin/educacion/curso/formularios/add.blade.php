<div class="card form_add_curso" style="display: none;">
    <div class="card-header">
       <h4>Agregar Curso</h4>
    </div>
    <div class="card-block">
        <div class="alert alert-danger response_errors" style="display: none;">
            <div class="errors"></div>
        </div>
        <form id="form_curso_reg" class="form_curso_reg" autocomplete="off">                           
            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Categoría del Curso</label>
                        <select class="form-control input" name="categoria_id" id="educategoria_id">
                            <option value="">Seleccione ...</option>
                            @foreach ($categorias as $data)
                                <option value="{{$data->id}}">{{$data->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Membresía</label><br>
                        <select class="form-control input" name="membresia" id="membresia_add">
                            <option value="">Seleccione ...</option>
                            @foreach ($membresias as $m)
                                <option value="{{$m->id}}">{{$m->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Titulo de Curso</label>
                        <input type="text" class="form-control input" name="titulo"
                               placeholder="Título">
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nivel del Curso</label>
                        <select class="form-control input" name="nivel" id="nivel">
                            <option value="">Seleccione ...</option>
                            <option value="basico">Básico</option>
                            <option value="Intermedio">Intermedio</option>
                            <option value="avanzado">Avanzado</option>
                        </select>
                    </div>
                </div>
            </div>                       
            <div class="form-group">
                <label>Descripción</label>
                <div class="ls-summernote" id="descripcion">
                   
                </div>
            </div>
            <div class="form-group">
                <label>Link de video</label>
                <input type="text" class="form-control input" name="video"
                       placeholder="Link de video">
            </div>
            <div class="form-group">
                <label for="exampleInputFile">Imagen</label>
                <input type="file" class="form-control-file input" id="img_conf_add">
                <small class="text-muted">This is some placeholder block-level help text for the above
                    input. It's a bit lighter and easily wraps to a new line.
                </small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="abrir_list_curso();">
                    Close
                </button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </form>
    </div>
</div>

