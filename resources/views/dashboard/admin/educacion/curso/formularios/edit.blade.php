<div class="card form_edit_curso" style="display: none;">
    <div class="card-header">
       <h4>Editar Curso</h4>
    </div>
    <div class="card-block">
        <div class="alert alert-danger response_errors" style="display: none;">
            <div class="errors"></div>
        </div>
        <form id="form_curso_edit" class="form_curso_edit">                           
            <input type="hidden" name="_token" value="{{ csrf_token() }}" class="token">
            <input type="hidden" name="id_curso" id="id_curso_ed">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Categoría del Curso</label>
                        <select class="form-control input" name="categoria_id" id="educategoria_id_ed">
                            <option value="">Seleccione ...</option>
                            @foreach ($categorias as $data)
                                <option value="{{$data->id}}">{{$data->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Membresía</label>
                        <select class="form-control input" name="membresia" id="membresia_ed" required>
                            <option value="">Seleccione ...</option>
                            @foreach ($membresias as $m)
                                <option value="{{$m->id}}">{{$m->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Titulo de Curso</label>
                        <input type="text" class="form-control input" name="titulo" id="titulo_ed">
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nivel del Curso</label>
                        <select class="form-control input" name="nivel" id="nivel_ed">
                            <option value="">Seleccione ...</option>
                            <option value="basico">Básico</option>
                            <option value="Intermedio">Intermedio</option>
                            <option value="avanzado">Avanzado</option>
                        </select>
                    </div>
                </div>
            </div>                       
            <div class="form-group">
                <label>Descripción</label>
                <div class="ls-summernote" id="descripcion_ed">
                </div>
            </div>
            <div class="form-group">
                <label>Link de video</label>
                <input type="text" class="form-control input" name="video" id="video_ed">
            </div>
            <div class="form-group">
                <label for="exampleInputFile">Imagen</label><br>
                <img src="" alt="imagen" style="width: 200px" id="imagen_ed">
                
                <input type="file" class="form-control-file input" id="img_conf_ed">
                <small class="text-muted">
                </small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="abrir_list_curso();">
                    Close
                </button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </form>
    </div>
</div>

