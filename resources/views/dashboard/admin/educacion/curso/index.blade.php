@extends('dashboard.layouts.principal')
@section('styles')
    <style>
        #result_cursos th{
            color: #2E2D2D!important;
        }
        .table th{
            color: #000;
        }
        
        .thead-dark{
            font-weight: bold;
            background: #212529;
        }

        .thead-dark th{
            color: #fff;
        }
    </style>
@endsection
@section('scripts')
    <script src="{{asset('dashboard/blockui/jquery.blockUI.js')}}"></script>
    <script>
        

        function abrir_form_add()
        {
            //selectCategoria();
            reset_input();
            $('.form_list_curso').hide();
            $('.form_edit_curso').hide();
            $('.form_add_curso').show();
        }

        function abrir_form_edit(data)
        {
            //selectCategoria();
            $('.form_list_curso').hide();
            $('.form_add_curso').hide();
            $('.form_edit_curso').show();
            //mostrar_curso(data);
        }

        function abrir_list_curso()
        {
            $('.form_add_curso').hide();
            $('.form_edit_curso').hide();
            $('.form_list_curso').show();        
        }


        //listar
        //datatable
        function listar_curso()
        {   
            var route = "{{ url('dashboard/admin/education/course/listcourse') }}";
            $.get(route, function(value){
                $('#result_cursos').html(value);
                
            });
        }

        

        function reset_input()
        {
            $('.input').val('');
            $("#descripcion").summernote('code', '');
        }

        $('#form_curso_reg').submit(function(e){
            e.preventDefault();
            $.blockUI({
                theme: false,
                baseZ: 2000,
                message: '<h2 style="font-size: 20px!important;"><img src="{{ asset('dashboard/load.gif') }}" style="width: 45px;" alt="load"/> Subiendo Archivo...</h2>'
            });

            var inputFileImage = document.getElementById("img_conf_add");
            var file = inputFileImage.files[0];
            var data = new FormData();        

            var titulo = $('input[name="titulo"]').val();
            var categoria_id = $('#educategoria_id').val();
            var nivel = $('#nivel').val();
            var descripcion = $('#descripcion').summernote('code');//summernote
            var video = $('input[name="video"]').val();
            var membresia = $('#membresia_add').val();

            //agregamos al formdata
            data.append('imagen',file);
            data.append('titulo',titulo);
            data.append('categoria_id',categoria_id);
            data.append('nivel',nivel);
            data.append('video',video);
            data.append('membresia',membresia);
            data.append('descripcion',descripcion);
            var token = $('#token').val();
            
            var route = "{{url('dashboard/admin/education/course/register')}}";

            $.ajax({
                async: true,
                headers: { 'X-CSRF-TOKEN': token },
                url: route,
                type: 'POST',
                contentType: false,
                data: data,
                processData: false,
                success:function(data){
                    if (data.success = 'true') {
                        $('.response_errors').hide();
                        reset_input();
                        $.unblockUI();
                        $('#result_cursos').empty();
                        listar_curso();
                        toastr.success(data.msg, data.titulo);
                        abrir_list_curso();
                    }
                },
                error:function(data){         
                     $.unblockUI();
                     $('.response_errors').show();
                     $('.errors').empty();
                     var errors = data.responseJSON.errors;
                     $.each(errors, function (key, value) {
                        $('.errors').append('<li>'+value+'</li>');
                    })             
                }
            }); 
        });

        function mostrar_curso(data)
        {
            abrir_form_edit();
            var route = "{{url('dashboard/admin/education/course/show')}}/"+data;
            //alert(route);
            $.get(route, function(value){
                var ruta = '{{url('uploads')}}/cursos/';
                $('#id_curso_ed').val(value.id);
                $('input[name="titulo"]').val(value.titulo);
                $('#educategoria_id_ed').val(value.educategoria_id);
                $('#nivel_ed').val(value.nivel);
                $('input[name="video"]').val(value.video);
                $('#imagen_ed').attr('src', ruta+value.imagen);
                $("#descripcion_ed").summernote("code",value.descripcion);
                $("#membresia_ed").val(value.membresia);
                
            });
        }

        $('#form_curso_edit').submit(function(e){
            e.preventDefault();
            $.blockUI({
                theme: false,
                baseZ: 2000,
                message: '<h2 style="font-size: 20px!important;"><img src="{{ asset('dashboard/load.gif') }}" style="width: 45px;" alt="load"/> Subiendo Archivo...</h2>'
            });

            var inputFileImage = document.getElementById("img_conf_ed");
            var file = inputFileImage.files[0];
            var data = new FormData();        

            var titulo = $('#titulo_ed').val();
            var categoria_id = $('#educategoria_id_ed').val();
            var nivel = $('#nivel_ed').val();
            var id_curso = $('#id_curso_ed').val();
            var descripcion = $('#descripcion_ed').summernote('code');//summernote
            var video = $('#video_ed').val();
            var membresia = $('#membresia_ed').val();  
            

            //agregamos al formdata
            data.append('imagen',file);
            data.append('id_curso',id_curso);
            data.append('titulo',titulo);
            data.append('categoria_id',categoria_id);
            data.append('nivel',nivel);
            data.append('video',video);
            data.append('membresia',membresia);
            data.append('descripcion',descripcion);
            var token = $('.token').val();
            
            var route = "{{url('dashboard/admin/education/course/update')}}";

            $.ajax({
                async: true,
                headers: { 'X-CSRF-TOKEN': token },
                url: route,
                type: 'POST',
                contentType: false,
                data: data,
                processData: false,
                success:function(data){
                    if (data.success = 'true') {
                        $('.response_errors').hide();
                        reset_input();
                        $.unblockUI();
                        $('#result_cursos').empty();
                        listar_curso();
                        toastr.success(data.msg, data.titulo);
                        abrir_list_curso();
                    }
                },
                error:function(data){         
                     $.unblockUI();
                     $('.response_errors').show();
                     $('.errors').empty();
                     var errors = data.responseJSON.errors;
                     $.each(errors, function (key, value) {
                        $('.errors').append('<li>'+value+'</li>');
                    })             
                }
            }); 
        });



        function eliminar_curso(data)
        {
            swal({
              title: "¿Estas seguro de eliminar?",
              text: "El registro seeliminará!",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                var id = data;
                var token = $('#token').val();
                var route = "{{url('dashboard/admin/education/course/remove')}}";     
                $.ajax({
                    url: route,
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id, _token: token},
                    success:function(data){
                        if (data.success == 'true') {
                            $('#result_cursos').empty();
                            listar_curso();
                            swal("Poof! ha sido eliminado con éxito!", {
                              icon: "success",
                            });
                        }
                    },

                    error:function(data){         
                        console.log('no se pudo');
                    }
                });
              } else {
                swal("Your imaginary file is safe!");
              }
            });
            
        }
    </script>
@endsection

@section('content')
    <div class="main-content page-course">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">Curso</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Educación</a></li>
                <li class="breadcrumb-item active">Curso</li>
            </ol>
        </div>
    </div>
    <div class="card form_list_curso">
        <div class="card-header">
           <button type="button" class="btn btn-outline-info" onclick="abrir_form_add()">Agregar</button>
        </div>
        <div class="card-block">
            <table id="table_curso" class="table table-striped table-bordered " cellspacing="0"
                   width="100%">
                <thead class="thead-dark">
                <tr>
                    <th>Categoría</th>
                    <th>Curso</th>
                    <th>Nivel</th>
                    <th>Imagen</th>
                    <th>Estado</th>
                    <th>Accion</th>
                </tr>
                </thead>
                <tbody id="result_cursos">
                    @include('dashboard.admin.educacion.curso.result.cursos')
                </tbody>
            </table>
        </div>
    </div>
    @include('dashboard.admin.educacion.curso.formularios.add')
    @include('dashboard.admin.educacion.curso.formularios.edit')
@endsection