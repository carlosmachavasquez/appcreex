@foreach($cursos as $data)
<tr>
    <th>{{ $data->categoria }}</th>
    <th>{{ $data->titulo }}</th>
    <th>{{ $data->nivel }}</th>
    <th>
        <img src="{{ asset('uploads/cursos') }}/{{ $data->imagen }}" alt="{{ $data->titulo }}" style="width: 60px;">
    </th>
    <th>
        @if ($data->estado == 1)
            <span class="btn btn-xs btn-success">Activo</span>
        @endif
    </th>
    <th>
        <div class="cont_btn_actions" data-toggle="tooltip" data-placement="top" title="Editar">
            <button type="button" class="btn btn-outline-primary" onclick="mostrar_curso({{ $data->id }})">Editar</button>
            <button type="button" class="btn btn-outline-danger" onclick="eliminar_curso({{ $data->id }})">Eliminar</button>
        </div>
    </th>
</tr>
@endforeach