@extends('dashboard.layouts.principal')

@section('styles')
    <link href="{{ asset('/assets/admin/css/signals/show.css') }}" rel="stylesheet" type="text/css">
@stop

@section('content')
	<div class="main-content page-signals-show">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">Signals</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('admin.signals.index')}}">Signals</a></li>
                <li class="breadcrumb-item active"> Show Signal</li>
            </ol>
        </div>
        
		<div class="row">
			<div class="col-lg-12 margin-tb">
				<div class="pull-left">
					<h2></h2>
				</div>
				<div class="pull-right">
					<a class="btn btn-primary" href="{{ route('admin.signals.index') }}"> Back</a>
				</div>
			</div>
		</div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <strong>Coin Name:</strong>
                                            {{ $signal->coinname }}
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <strong>Coin Symbol:</strong>
                                            {{ $signal->coinsymbol }}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <strong>Url image:</strong>
                                            {{ $signal->coinimgurl }}
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <strong>Exchange:</strong>
                                            {{ $signal->exchange }}
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <strong>Target por membresía:</strong>
			                              	@if( $signal->membresia_id == 1 )
			                                    Aprendiz
			                                @elseif( $signal->membresia_id == 2 )
			                                    Emprendedor
			                                @elseif( $signal->membresia_id == 3 )
			                                    Inversionista
			                                @endif
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <strong>Link Tradear Señal:</strong>
                                            {{ $signal->cointradeurl }}
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <strong>Precio sugerido de compra:</strong>
                                            {{ $signal->coinprice }}
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <strong>Target 1:</strong>
                                            {{ $signal->target1 }}
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <strong>Target 2:</strong>
                                            {{ $signal->target2 }}
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <strong>Target 3:</strong>
                                            {{ $signal->target3 }}
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <strong>Stop loss:</strong>
                                            {{ $signal->stoploss }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col container-card-img">
                    <div class="card">
                        <div class="card-block">
                            <img id="img-coin" src="{{ $signal->coinimgurl }}" width="300" height="300">
                        </div>
                    </div>
                </div>
            </div>  
        </div>
	</div>
@endsection