@extends('dashboard.layouts.principal')

@section('styles')
    <link href="{{ asset('/assets/admin/css/signals/signals.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.24.4/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <style>
        table{
            margin: 0 auto;
            width: 100% !important;
            clear: both;
            border-collapse: collapse;
            table-layout: fixed;
            word-wrap:break-word;
        }
        .dataTables_scrollFootInner{
            width: 100% !important;
        }
    </style>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.24.4/sweetalert2.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function(){
            //$('#').dataTable();
            $('#users-datatable').DataTable( {
                // initComplete: function () {
                //     this.api().columns().every( function () {
                //         var column = this;
                //         var select = $('<select><option value=""></option></select>')
                //             .appendTo( $(column.footer()).empty() )
                //             .on( 'change', function () {
                //                 var val = $.fn.dataTable.util.escapeRegex(
                //                     $(this).val()
                //                 );
                //
                //                 column
                //                     .search( val ? '^'+val+'$' : '', true, false )
                //                     .draw();
                //             } );
                //
                //         column.data().unique().sort().each( function ( d, j ) {
                //             select.append( '<option value="'+d+'">'+d+'</option>' )
                //         } );
                //     } );
                // },
                fixedColumns: true,
                scrollX:        true,
                scrollCollapse: true,
                autoWidth:         true,
                paging:         true,
                columnDefs: [
                    { "width": "88px", "targets": [0] },
                    { "width": "96px", "targets": [4] },
                    { "width": "76px", "targets": [5] },
                    { "width": "116px", "targets": [6] }
                ]
            } );

            $(".btn-modal").click(function(e){
                console.log("ENTRO AL MODAL");
                var index = e.currentTarget.attributes[3].value;
                console.log("Index: ", index);
                console.log(pagosJson);
                var getDataByUser = pagosJson[parseInt(index)];
                console.log(getDataByUser);
                // var modaltemplate = document.getElementsByClassName('modaltemplate')[0];
                // modaltemplate.id = "modal-" + idUser;
                // $("#" + "modal-" + idUser).modal();
            });
        });
    </script>
@endsection

@section('content')
    <div class="main-content page-signals">
        @include('flash::message')
        <div class="page-header" style="position: relative;">
            <h3 class="page-title">Usuarios</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item active">Usuarios</li>
            </ol>
            <div class="botones" style="position: absolute; top: 1em; right: 1em;">
                <a href="{{route('admin.usuarios.export')}}" class="btn btn-outline-success">Exportar a Excel</a>
            </div>
        </div>
        <div class="alert app_alert">
            <h2 class="app_titulo"></h2>
        </div>
        
        <div class="table-responsive users">
            <table id="users-datatable" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                <thead class="thead-dark">
                    <tr>
                        <th style="width: 88px;">Nickname</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Membresia</th>
                        <th style="width: 96px;">Referido de</th>
                        <th style="width: 76px;">Celular</th>
                        <th style="width: 116px;">Rol de usuario</th>
                        <th>Pagos</th>
                    </tr>
                </thead>
                <tbody id="result_activacion">
                    <script type="text/javascript">
                        var pagosJson = [];
                    </script>
                    @foreach ($usuarios as $key => $user)
                        <tr id="{{$user->id}}" class="row-signal">
                            @php
                                $dataPago = (isset($data[$key]['pago_user'])) ? $data[$key]['pago_user'] : '';
                                //$dataPago = json_encode($dataPago);
                            @endphp
                            <script type="text/javascript">
                                var index = '{{ $key }}';
                                index = parseInt(index);
                                var dataPhp = '<?php echo json_encode($dataPago); ?>';
                                pagosJson[index] = dataPhp;
                                console.log(pagosJson[index]);
                            </script>
                            <td>{{$user->nickname}}</td>
                            <td>{{$user->name}} {{$user->lastname}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{ $data[$key]['name_membresia'] }}</td>
                            <td>{{ $data[$key]['name_referido'] }}</td>
                            <td>{{$user->cellphone}}</td>
                            <td>
                                @if($user->role == 'user')
                                <span class="badge badge-primary">{{$user->role}}</span>
                                @else
                                <span class="badge badge-warning">{{$user->role}}</span>
                                @endif
                            </td>
                            <td>
                                <button type="button" class="btn-modal btn btn-primary" data-toggle="modal"
                                        data-index="{{$key}}" data-target="#modal-{{$user->id}}">
                                    Ver Pagos
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- Modal -->
    @php
        //dump($data['14']['pago_user']);
    @endphp
    <div class="modaltemplate modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p>This is a large modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop
