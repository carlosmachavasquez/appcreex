@extends('dashboard.layouts.principal')
@section('contenido')


<div class="col-sm-12">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h6>Agregar ico recomenda</h6>
                </div>
                <div class="card-block">
                    <form class="form-inline">
                        <div class="form-group">
                            <label for="nameInput" class="control-label">Moneda</label>      
                            <select id="nameInput"  class="form-control ls-select2">
                                <option value="AK">BTC</option>
                                <option value="HI">Hawaii</option>
                                <option value="CA">California</option>
                                <option value="NV">Nevada</option>
                                <option value="OR">Oregon</option>
                                <option value="WA">Washington</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleSelect1" class="control-label">Categoria</label>
                            <select class="form-control" id="exampleSelect1">
                                    <option>Todos</option>
                                    <option>Map Silver</option>
                                    <option>Mao Bronce</option>
                                    <option>Map Yuneor</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="checkbox-inline"> 
                                <input type="checkbox" id="inlineCheckbox1" value="option1">
                                Muy Recomenda
                            </label>
                        </div>


                        <button type="submit" class="btn btn-primary">Agregar Moneda</button>
                    </form>
                </div>
            </div>
        </div>    
    </div>
</div>


<div class="col-sm-12">                
    <div class="card">
        <div class="card-block">
            <!-- Tabla de señales-->
            <table  class="table table-hover" width="100%">
                <thead>                                                                   
                    <td>ICO</td>
                    <td>Moneda</td>                                    
                    <td>F.Inicio</td>
                    <td>F.Cierre</td>
                    <td>Calif</td>
                </thead>                                        
                <tbody>
                    <tr>                                  
                      <td>
                            <a href=""><img style="display: inline-block" src="http://162.243.196.93/josh/cryptoera/public/assets/images/coins/btc.jpg"></a>
                            
                      </td>
                      <td>
                         <div style="display: inline-block">
                                <a href=""><strong>Crowd Machine</strong></a>
                                <br>
                                <span  class="descripcion-ico" style="color: grey">Crowd Machine is powering the next generation of decentralized blockchain applications.</span>
                            </div>
                      </td>
                      <td>
                          28/04/2018
                      </td>
                      <td>
                        22/05/2019
                      </td>
                      <td class="calif">
                          3.8
                      </td>
                    </tr>
                    <tr>                                  
                      <td>
                            <img style="display: inline-block" src="http://162.243.196.93/josh/cryptoera/public/assets/images/coins/btc.jpg">
                            
                      </td>
                      <td>
                         <div style="display: inline-block">
                                <a href=""><strong>Crowd Machine</strong></a>
                                <br>
                                <span style="color: grey">Crowd Machine is powering the next generation of decentralized blockchain applications.</span>
                            </div>
                      </td>
                      <td>
                          28/04/2018
                      </td>
                      <td>
                        22/05/2019
                      </td>
                      <td>
                          3.8
                      </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection

@section('scripts')

@endsection

