@extends('dashboard.layouts.principal')

@section('styles')
    <link href="{{ asset('/assets/admin/css/signals/signals.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.24.4/sweetalert2.min.css">

    <style>
        .app_edit_prod, .app_new_prod{
            display: none;
        }

        .app_checkbox label{
            margin-right: 1em;
        }

        .app_img_btn{
            background: #007bff;
            border-color: #007bff;
            z-index: 1000;
        }
    </style>
@stop

@section('scripts')
    <script src="/assets/admin/js/coinmarket/common.js"></script>
    <script src="/assets/admin/js/coinmarket/ccc-streamer-utilities.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.24.4/sweetalert2.min.js"></script>

    <script type="text/javascript">
        $(document).on('click', '.pagination a', function(e){
            e.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            var route = "{{route('admin.activation.index')}}";
            $.ajax({
                url: route,
                data: {page},
                type: 'GET',
                dataType: 'json',
                success: function(data)
                {
                    $(".users").html(data);
                }

            });
        });

       

        function listar_productos()
        {
            abrirForm('productos', 'list');
            var route = "{{route('admin.productos.index')}}";
            $.ajax({
                url: route,
                data: {page:1},
                type: 'GET',
                dataType: 'json',
                success: function(data)
                {
                    $(".users").html(data);
                }

            });
        }

        function limpiarcampos()
        {
            $('.input').val('');
            $('.checkbox').prop('checked', false);
            $('#descripcion').summernote('code', '');
            $('#imagenprod').val('');
        }

        {{-- ++++++++++ --}}
        function abrirForm(criterio, tipo, id = 0){
            switch(tipo) {
                case 'new':
                    limpiarcampos();
                    $('.img_prod_edit').hide();
                    $('.app_prod').hide();
                    $('.app_new_prod').show();
                    break;
                case 'edit':
                    mostrar_prod(id);
                    $('.app_prod').hide();
                    $('.img_prod_edit').show();
                    $('.app_new_prod').show();
                    break;
                case 'list':
                    $('.app_prod').hide();
                    $('.app_list_prod').show();
                    break;
            }
        }

        function mostrar_prod(idproducto)
        {
            limpiarcampos();
            var route = "{{route('admin.productos.edit')}}";
            $.ajax({
                url: route,
                data: {id:idproducto},
                type: 'GET',
                dataType: 'json',
                success: function(data)
                {   
                    var url = 'http://localhost:8000/uploads/productos/'+data.producto.imagen;
                    $('.img_prod_edit').attr('src', url);
                    $('#nombre_add').val(data.producto.nombre);
                    $('#precio_add').val(data.producto.precio);
                    $('#resumen_add').val(data.producto.resumen);
                    $('#descripcion_add').val(data.producto.descripcion);
                    //$('#descripcion').summernote('code',data.producto.descripcion);

                    for (var i = 0; i < data.cant_memb; i++) {
                        //alert(data.membresia[i]);
                        if (i == parseInt(data.membresia[i])) {
                            $('input[type=checkbox][value='+i+']').prop('checked', true);
                            console.log(data.membresia[i]);
                        }
                    }
                }

            });
        }


        $('#form_prod_reg').submit(function(e){
            e.preventDefault();
            var inputFileImage = document.getElementById("imagenprod");
            var file = inputFileImage.files[0];
            var data = new FormData();        

            var nombre = $('#nombre_add').val();
            var precio = $('#precio_add').val();
            //membresia
            var yourArray = [];
            $("input:checkbox[name=membresia]:checked").each(function(){
                yourArray.push(parseInt($(this).val()));
            });
            //fin de membresia
            var membresia = yourArray;
            alert(membresia);
            var resumen = $('#resumen_add').val();
            var descripcion = $('#descripcion_add').val();
            //var descripcion = $('#descripcion').summernote('code');
            //agregamos al formdata
            data.append('imagen',file);
            data.append('nombre',nombre);
            data.append('precio',precio);
            data.append('membresia',membresia);
            data.append('resumen',resumen);
            data.append('descripcion',descripcion);
            
            var route = "{{route('admin.productos.registrar')}}";

            $.ajax({
                async: true,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url: route,
                type: 'POST',
                contentType: false,
                data: data,
                processData: false,
                success:function(data){
                    if (data.success = true) {
                        limpiarcampos();
                        listar_productos();
                    }else{

                    }
                },
                error:function(msj){ 
                    if (msj.responseJSON.imagen != '') {
                        $('.val_error_imagen_up').empty().append(msj.responseJSON.imagen);
                        
                    }            
                }
            }); 
        });

        function desactivar_producto(data)
        {
            var id = data;
            var route = "{{route('admin.productos.desactivar')}}"; 
            const swalWithBootstrapButtons = swal.mixin({
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-danger',
              buttonsStyling: false,
            })

            swalWithBootstrapButtons({
              title: 'Estas seguro de Desactivar?',
              text: 'Se desactivara este producto',
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Si, Desactivar',
              cancelButtonText: 'No, cancelar!',
              reverseButtons: true
            }).then((result) => {
              if (result.value) {
                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                    success:function(data){
                       swalWithBootstrapButtons(
                                  'Desactivado!',
                                  'Se ha Desactivado con éxito!',
                                  'success'
                                )
                            listar_productos();
                            //$('#'+data.id).remove();
                    },

                    error:function(data){         
                        console.log('no se pudo');
                    }
                });
                
              } else if (
                // Read more about handling dismissals
                result.dismiss === swal.DismissReason.cancel
              ) {
                swalWithBootstrapButtons(
                  'Cancelado',
                  'Ha cancelado la Desactivación :)',
                  'error'
                )
              }
            }) 
            
        }





        

        
        
        

        $('.app_btn_new').click(function(){
            var yourArray = [];
            $("input:checkbox[name=membresia]:checked").each(function(){
                yourArray.push(parseInt($(this).val()));
            });
        });



    </script>
@stop

@section('content')
    {{-- list --}}
    <div class="app_prod app_list_prod">
        @include('dashboard.admin.productos.includes.list')
    </div>
    
    {{-- new --}}
    <div class="app_prod app_new_prod">
        @include('dashboard.admin.productos.includes.new')
    </div>
@stop
