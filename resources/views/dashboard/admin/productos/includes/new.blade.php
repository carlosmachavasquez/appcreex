<div class="main-content page-signals app_cont_edit">
        <div class="page-header">
            <h3 class="page-title">Agregar Productos</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item active">Add Productos</li>
            </ol>
        </div>
        <div class="alert app_alert">
            <h2 class="app_titulo"></h2>
        </div>
        
        <div class="table-responsive">
            <form id="form_prod_reg" class="form_prod_reg" autocomplete="off">       
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="text" class="input form-control input" name="nombre"
                               placeholder="Nombre" id="nombre_add">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Precio</label>
                        <input type="text" class="input form-control input" name="precio"
                               placeholder="Precio" id="precio_add">
                    </div> 
                </div>
                <div class="col-md-12">
                    <div class="form-group ">
                        <label>Membresías</label><br>
                        @foreach($membresias as $key => $mem)
                        <label class="checkbox-inline">
                            <input type="checkbox" value="{{$mem->id}}" id="{{$mem->id}}" name="membresia" class="checkbox">{{$mem->name}}
                        </label>
                        @endforeach
                    </div>
                </div>
                
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Resumen</label>
                        <textarea name="resumen" class="input form-control" id="resumen_add"></textarea>
                    </div>
                </div>
            </div>                       
            <div class="form-group">
                <label>Descripción</label>
                <textarea name="descripcion" class="input form-control" id="descripcion_add" style="height: 10em"></textarea>
            </div>
            <div class="form-grosup">
                <label>Imagen</label><br>
                <img src="" class="img_prod_edit" alt="imagen" style="width: 300px"><br>
                <div class="custom-file app_img_btn btn btn-primary">
                    <input type="file" class="custom-file-input" id="imagenprod">
                    <label class="custom-file-label" for="imagenprod">Selecciona imagen...</label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="abrirForm('productos', 'list');">
                    Close
                </button>
                <button type="submit" class="btn btn-primary app_btn_new">Guardar</button>
            </div>
        </form>
        </div>        
    </div>