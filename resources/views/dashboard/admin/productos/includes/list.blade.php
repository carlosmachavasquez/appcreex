<div class="main-content page-signals app_cont_list">
        <div class="page-header">
            <h3 class="page-title">Productos</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item active">Productos</li>
            </ol>
            <div class="botones" style="position: absolute; top: 1em; right: 1em;">
                <button class="btn btn-outline-success" onclick="abrirForm('producto', 'new')">Agregar Producto</button>
            </div>
        </div>
        <div class="alert app_alert">
            <h2 class="app_titulo"></h2>
        </div>
        
        <div class="table-responsive users">
            <table id="userSignals" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                <thead class="thead-dark">
                    <tr>
                        <th>Nombre</th>
                        <th>Membresia</th>
                        <th>Precio</th>
                        <th>Imagen</th>
                        <th>Estado</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody id="result_activacion">
                    @foreach ($productos as $i => $prod)
                        <tr id="{{$prod->id}}" class="row-signal">                        
                            <td>{{$prod->nombre}}</td>
                            <td>{{$prod->membresia}}</td>
                            <td>{{$prod->precio}}</td>
                            <td><img src="{{ url('uploads/productos') }}/{{$prod->imagen}}" alt="{{$prod->nombre}}" style="width: 100px;"></td>
                            <td>
                                @if($prod->estado == 'activado')
                                <span class="badge badge-primary">{{$prod->estado}}</span>
                                @else
                                <span class="badge badge-warning">{{$prod->estado}}</span>
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-success" data-toggle="modal" data-target="#activar" data-dismiss="modal" onclick="abrirForm('producto', 'edit', {{$prod->id}})"><i class="icon-fa icon-fa-edit"></i></button>
                                    
                                <button class="btn btn-danger" data-toggle="modal" data-target="#activar" data-dismiss="modal" onclick="desactivar_producto({{$prod->id}})"><i class="icon-fa icon-fa-times"></i></button>                                
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $productos->render() !!}
        </div>        
    </div>