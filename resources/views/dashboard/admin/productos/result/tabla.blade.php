<table id="userSignals" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
    <thead class="thead-dark">
        <tr>
            <th>Nombre</th>
            <th>Membresia</th>
            <th>Precio</th>
            <th>Imagen</th>
            <th>Estado</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody id="result_activacion">
        @foreach ($productos as $i => $prod)
            <tr id="{{$prod->id}}" class="row-signal">                        
                <td>{{$prod->nombre}}</td>
                <td>{{$prod->membresia}}</td>
                <td>{{$prod->precio}}</td>
                <td><img src="{{ url('uploads/productos') }}/{{$prod->imagen}}" alt="{{$prod->nombre}}" style="width: 100px;"></td>
                <td>
                    @if($prod->estado == 'activado')
                    <span class="badge badge-primary">{{$prod->estado}}</span>
                    @else
                    <span class="badge badge-warning">{{$prod->estado}}</span>
                    @endif
                </td>
                <td>
                    <button class="btn btn-success" data-toggle="modal" data-target="#activar" data-dismiss="modal" onclick="abrirForm('producto', 'edit')"><i class="icon-fa icon-fa-edit"></i></button>
                    <button class="btn btn-warning" data-toggle="modal" data-target="#activar" data-dismiss="modal" onclick=""><i class="icon-fa icon-fa-eye"></i></button>
                    <button class="btn btn-danger" data-toggle="modal" data-target="#activar" data-dismiss="modal" onclick="desactivar_producto({{$prod->id}})"><i class="icon-fa icon-fa-times"></i></button>                                
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
{!! $productos->render() !!}