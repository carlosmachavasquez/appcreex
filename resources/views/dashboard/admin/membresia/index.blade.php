@extends('dashboard.layouts.principal')

@section('styles')
<style>
    #result_membresia th{
        color: #2d2d2d;
    }
</style>
@endsection
@section('content')
<div class="main-content page-course">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">Membresia</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Home</a></li>
                <li class="breadcrumb-item active">Membresia</li>
            </ol>
        </div>
    </div>
<div class="card form_list_membresia">
    <div class="card-header">
       <button type="button" class="btn btn-outline-info" onclick="abrir_form_add()">Agregar</button>
    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
    <div class="card-block">
        <table id="table_membresia" class="table table-striped table-bordered " cellspacing="0"
               width="100%">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Precio Mes</th>
                <th>Precio Anual</th>
                <th>Estado</th>
                <th>Accion</th>
            </tr>
            </thead>
            <tbody id="result_membresia">
                @include('dashboard.admin.membresia.result.membresias');
            </tbody>
        </table>
    </div>
</div>

@include('dashboard.admin.membresia.formularios.add')
@include('dashboard.admin.membresia.formularios.edit')
@include('dashboard.admin.membresia.modal.add_items')
@endsection

@section('scripts')
<script>
	

    function abrir_form_add()
    {
        //selectCategoria();
        $('.form_list_membresia').hide();
        $('.form_edit_membresia').hide();
        $('.form_add_membresia').show();
    }

    function abrir_form_edit(data)
    {
        //selectCategoria();
        $('.form_list_membresia').hide();
        $('.form_add_membresia').hide();
        $('.form_edit_membresia').show();
       //mostrar_membresia(data);
    }

    function abrir_list_membresia()
    {
        $('.form_add_membresia').hide();
        $('.form_edit_membresia').hide();
        $('.form_list_membresia').show();        
    }


	//listar
	//datatable
	function listar_membresia()
    {   

        var route = "{{ url('dashboard/admin/membresia/listar_membresia') }}";
        $.get(route, function(value){
            $('#result_membresia').html(value);            
        });
    }



    function reset_input()
    {
        $('.input').val('');
        $("#descripcion").summernote('code', '');
    }

    $('#form_membresia_reg').submit(function(e){
        e.preventDefault();
        var form = $(this);
        var data = form.serialize();
        var token = $('#token').val();    
        var route = "{{url('dashboard/admin/membresia/registrar')}}";

        $.ajax({
            url: route,
            headers: { 'X-CSRF-TOKEN': token },
            type: 'POST',
            dataType: 'json',
            data: data,
            success:function(data){
                if (data.success = 'true') {
                    $('.response_errors').hide();
                    reset_input();
                    $('#result_membresia').empty();
                    listar_membresia();
                    toastr.success(data.msg, data.titulo);
                    abrir_list_membresia();
                }
            },
            error:function(data){  
                 $('.response_errors').show();
                 $('.errors').empty();
                 var errors = data.responseJSON.errors;
                 $.each(errors, function (key, value) {
                    $('.errors').append('<li>'+value+'</li>');
                })             
            }
        }); 
    });

    function mostrar_membresia(data)
    {
        abrir_form_edit();
        var route = "{{url('dashboard/admin/membresia/mostrar')}}/"+data;
        //alert(route);
        $.get(route, function(value){
            $('#mem_id_edit').val(value.id);
            $('#mem_titulo_edit').val(value.name);
            $('#mem_precio_mes_edit').val(value.precio_mes);
            $('#mem_precio_anual_edit').val(value.precio_anual);            
        });
    }

    $('#form_membresia_act').submit(function(e){
        e.preventDefault();
        
        var token = $('#token').val();
        var form = $(this);
        var data = form.serialize();
        var route = "{{url('dashboard/admin/membresia/actualizar')}}";

        $.ajax({
            url: route,
            headers: { 'X-CSRF-TOKEN': token },
            type: 'POST',
            dataType: 'json',
            data: data,
            success:function(data){
                if (data.success = 'true') {
                    $('.response_errors').hide();
                    reset_input();
                    $('#result_membresia').empty();
                    listar_membresia();
                    toastr.success(data.msg, data.titulo);
                    abrir_list_membresia();
                }
            },
            error:function(data){  
                 $('.response_errors').show();
                 $('.errors').empty();
                 var errors = data.responseJSON.errors;
                 $.each(errors, function (key, value) {
                    $('.errors').append('<li>'+value+'</li>');
                })             
            }
        }); 
    });



    function eliminar_membresia(data)
    {
        swal({
          title: "¿Estas seguro de eliminar?",
          text: "El registro seeliminará!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            var id = data;
            var token = $('#token').val();
            var route = "{{url('dashboard/admin/membresia/eliminar')}}";       
            $.ajax({
                url: route,
                type: 'POST',
                dataType: 'json',
                data: {id: id, _token: token},
                success:function(data){
                    if (data.success == 'true') {
                        $('#result_membresia').empty();
                        listar_membresia();
                        swal("Poof! ha sido eliminado con éxito!", {
                          icon: "success",
                        });
                    }
                },

                error:function(data){         
                    $('.response_errors').show();
                    $('.errors').empty();
                     var errors = data.responseJSON.errors;
                     $.each(errors, function (key, value) {
                        $('.errors').append('<li>'+value+'</li>');
                    })  
                }
            });
          } else {
            swal("Your imaginary file is safe!");
          }
        });
        
    }

    //detalle membresia
    function mostrar_items(id_membresia)
    {
        var route = "{{url('dashboard/admin/membresia/mostrar_items')}}/"+id_membresia;
        //alert(route);
        $.get(route, function(value){
            $('#table_items').empty().append('Cargando ....');
            $('#table_items').html(value);
            $('#add_membresia_id').val(id_membresia);
        });
    }

    

    
    $('#form_add_item').submit(function(e){
        e.preventDefault();
        var form = $(this);
        var data = form.serialize();
        var token = $('#token').val();        
        var route = "{{url('dashboard/admin/membresia/add_item')}}";

        $.ajax({
            url: route,
            headers: { 'X-CSRF-TOKEN': token },
            type: 'POST',
            dataType: 'json',
            data: data,
            success:function(data){
                toastr.success(data.msg, data.titulo);
                reset_input();
                $('#item_cuerpo').prepend('<tr class="registro'+data.item.id+'">'+
                                        '<td>'+data.item.item+'</td>'+
                                        '<td width="30">'+
                                          '<button class="btn btn-danger" onclick="eliminar_item('+data.item.id+');">Eliminar</button>'+
                                        '</td>'+
                                      '</tr>');

                
            },
            error:function(data){ 
                 $('.response_errors_file').show();
                 $('.errors').empty();
                 var errors = data.responseJSON.errors;
                 $.each(errors, function (key, value) {
                    $('.errors').append('<li>'+value+'</li>');
                })             
            }
        });
    });

    function eliminar_item(data)
    {
        var data = data;
        swal({
          title: "¿Estas seguro de eliminar?",
          text: "El registro seeliminará!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            var token = $('#token').val();
            var route = "{{url('dashboard/admin/membresia/eliminar_item')}}";       
            $.ajax({
                url: route,
                headers: { 'X-CSRF-TOKEN': token },
                type: 'POST',
                dataType: 'json',
                data: {id: data},
                success:function(data){
                    toastr.success(data.msg, data.titulo);
                    $('.registro'+data.id).remove();
                },

                error:function(data){         
                    console.log('no se pudo');
                }
            });
          } else {
            swal("Se canseló!");
          }
        });
        
    }

    
</script>
@endsection