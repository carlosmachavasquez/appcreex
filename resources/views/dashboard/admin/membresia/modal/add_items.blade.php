<div class="modal fade modal_items" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title cb" id="exampleModalLabel">Agregar Items</h5>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form_add_item">
                        <div class="row">                            
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="hidden" name="membresia_id" id="add_membresia_id">
                                    <input type="text" class="form-control input" name="item" id="add_item" 
                                           placeholder="Item" required>
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success" id="btn_add_item">Guardar</button>
                                </div>
                            </div>
                        
                            <div class="linea"></div>
                            <div class="col-md-12">
                                <div class="result-table" id="table_items" style="height: 400px; overflow: overlay;">
                                    
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>