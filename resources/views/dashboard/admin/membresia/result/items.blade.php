<table class="table table-hover tabla_items" >
    <thead>
      <tr>
        <th>Item</th>
        <th>Acción</th>
      </tr>
    </thead>
    <tbody id="item_cuerpo">
      @foreach ($items as $data)
      <tr class="registro{{ $data->id }}">
        <td>{{ $data->item }}</td>
        <td width="30">
          <button class="btn btn-danger" onclick="eliminar_item({{ $data->id }});" type="button">Eliminar</button>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

  <script>$('.tabla_items').DataTable({
    "pageLength": 5
    });</script>