@foreach($membresias as $data)
<tr>
    <th>{{ $data->name }}</th>
    <th>{{ $data->precio_mes }}</th>
    <th>{{ $data->precio_anual }}</th>
    <th>
        @if ($data->estado == 1)
            <span class="btn btn-xs btn-success">Activo</span>
        @endif
    </th>
    <th>
        <div class="cont_btn_actions" data-toggle="tooltip" data-placement="top" title="Editar">
            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target=".modal_items" onclick="mostrar_items({{ $data->id }})">Item</button>
            <button type="button" class="btn btn-outline-primary" onclick="mostrar_membresia({{ $data->id }})">Editar</button>
            <button type="button" class="btn btn-outline-danger" onclick="eliminar_membresia({{ $data->id }})">Eliminar</button>
        </div>
    </th>
</tr>
@endforeach