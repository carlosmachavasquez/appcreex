<div class="card form_edit_membresia" style="display: none;">
    <div class="card-header">
       <h4>Editar Membresía</h4>
    </div>
    <div class="card-block">
        <div class="alert alert-danger response_errors" style="display: none;">
            <div class="errors"></div>
        </div>
        <form id="form_membresia_act" class="form_membresia_act"> 
            <input type="hidden" id="mem_id_edit" name="id"> 
            <div class="form-group">
                <label>Título de la Menbresía</label>
                <input type="text" class="form-control input" name="name" id="mem_titulo_edit" 
                       placeholder="Título" required>
            </div> 
            <div class="form-group">
                <label>Precio Mensual</label>
                <input type="text" class="form-control input" name="precio_mes" id="mem_precio_mes_edit" 
                       placeholder="Precio Mensual" required>
            </div>
            <div class="form-group">
                <label>Precio Anual</label>
                <input type="text" class="form-control input" name="precio_anual" id="mem_precio_anual_edit" 
                       placeholder="Precio Anual" required>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="abrir_list_membresia();">
                    Close
                </button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </form>
    </div>
</div>

