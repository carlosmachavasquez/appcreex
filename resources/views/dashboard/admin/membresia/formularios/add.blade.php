<div class="card form_add_membresia" style="display: none;">
    <div class="card-header">
       <h4>Agregar Menbresía</h4>
    </div>
    <div class="card-block">
        <div class="alert alert-danger response_errors" style="display: none;">
            <div class="errors"></div>
        </div>
        <form id="form_membresia_reg" class="form_membresia_reg">  
            
            <div class="form-group">
                <label>Título de la Menbresía</label>
                <input type="text" class="form-control input" name="name" id="mem_titulo_add" 
                       placeholder="Título" required>
            </div> 
            <div class="form-group">
                <label>Precio Mensual</label>
                <input type="text" class="form-control input" name="precio_mes" id="mem_video_add" 
                       placeholder="Precio Mensual" required>
            </div>
            <div class="form-group">
                <label>Precio Anual</label>
                <input type="text" class="form-control input" name="precio_anual" id="mem_duracion_add" 
                       placeholder="Precio Anual" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="abrir_list_membresia();">
                    Close
                </button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </form>
    </div>
</div>

