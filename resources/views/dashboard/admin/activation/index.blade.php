@extends('dashboard.layouts.principal')

@section('styles')
    <link href="{{ asset('/assets/admin/css/signals/signals.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.24.4/sweetalert2.min.css">
@stop

@section('scripts')
    <script src="/assets/admin/js/coinmarket/common.js"></script>
    <script src="/assets/admin/js/coinmarket/ccc-streamer-utilities.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.24.4/sweetalert2.min.js"></script>

    <script type="text/javascript">
        $(document).on('click', '.pagination a', function(e){
            e.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            var route = "{{route('admin.activation.index')}}";
            $.ajax({
                url: route,
                data: {page},
                type: 'GET',
                dataType: 'json',
                success: function(data)
                {
                    $(".users").html(data);
                }

            });
        });

        function limpiar_modal()
        {
            $('.form-control').val('');
            $('#id_user').val('');
            $('#activar').modal('hide');
            if ($('.modal-backdrop').is(':visible')) {
                $('body').removeClass('modal-open'); 
                $('.modal-backdrop').remove(); 
            };
        }

        function listar_usuarios()
        {
            var route = "{{route('admin.activation.index')}}";
            $.ajax({
                url: route,
                data: {page:1},
                type: 'GET',
                dataType: 'json',
                success: function(data)
                {
                    $(".users").html(data);
                }

            });
        }

        function asignar_id(id){
          $('#id_user').val(id);
        }


        $('#form_activar_user').submit(function(e){
          e.preventDefault();
          var form = $(this);
          var data = form.serialize();
          var route = "{{route('admin.activation.activar')}}";

          $.ajax({
            url: route,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: 'POST',
            dataType: 'json',
            data: data,
            success:function(data){
              if (data.success == 'true') {
                $('#activar').modal('toggle');
                toastr.success(data.msj);
                limpiar_modal();
                listar_usuarios();
              }else if(data.success == 'false_db_pago'){
                $('#activar').modal('toggle');
                toastr.error(data.msj);
                limpiar_modal();
              }else if(data.success == 'false_api_pago'){
                $('#activar').modal('toggle');
                toastr.error(data.msj);
                limpiar_modal();
              }
            },

            error:function(data){         
                console.log('no se pudo');
            }
          });
        });

        function desactivar_usuario(data)
        {
            var id = data;
            var route = "{{route('admin.activation.desactivar')}}"; 
            const swalWithBootstrapButtons = swal.mixin({
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-danger',
              buttonsStyling: false,
            })

            swalWithBootstrapButtons({
              title: 'Estas seguro de Desactivar?',
              text: 'El usuario NO tendra acceso de nivel "USER"',
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Si, Desactivar',
              cancelButtonText: 'No, cancelar!',
              reverseButtons: true
            }).then((result) => {
              if (result.value) {
                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                    success:function(data){
                       swalWithBootstrapButtons(
                                  'Desactivado!',
                                  'Se ha Desactivado con éxito!',
                                  'success'
                                )
                            listar_usuarios();
                            //$('#'+data.id).remove();
                    },

                    error:function(data){         
                        console.log('no se pudo');
                    }
                });
                
              } else if (
                // Read more about handling dismissals
                result.dismiss === swal.DismissReason.cancel
              ) {
                swalWithBootstrapButtons(
                  'Cancelado',
                  'Ha cancelado la Desactivación :)',
                  'error'
                )
              }
            }) 
            
        }

    </script>
@stop

@section('content')
    <div class="main-content page-signals">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">Activation</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item active">Activation</li>
            </ol>
        </div>
        <div class="alert app_alert">
            <h2 class="app_titulo"></h2>
        </div>
        
        <div class="table-responsive users">
            <table id="userSignals" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                <thead class="thead-dark">
                    <tr>
                        <th>Usuario</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Fecha</th>
                        <th>Estado</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody id="result_activacion">
                    @foreach ($usuarios as $i => $user)
                        <tr id="{{$user->id}}" class="row-signal">                        
                            <td>{{$user->nickname}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->lastname}}</td>
                            <td>{{$user->created_at}}</td>
                            <td>
                                @if($user->role == 'user')
                                <span class="badge badge-primary">{{$user->role}}</span>
                                @else
                                <span class="badge badge-warning">{{$user->role}}</span>
                                @endif
                            </td>
                            <td>
                                @if($user->role == 'user')
                                <button class="btn btn-danger" onclick="desactivar_usuario({{$user->id}})"><i class="icon-fa icon-fa-times"></i></button>
                                @else
                                <button class="btn btn-success" data-toggle="modal" data-target="#activar" data-dismiss="modal" onclick="asignar_id({{$user->id}})"><i class="icon-fa icon-fa-check"></i></button>
                                @endif                                
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $usuarios->render() !!}
        </div>        
    </div>
    @include('dashboard.admin.activation.modal.activar')
@stop
