<table id="userSignals" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
    <thead class="thead-dark">
        <tr>
            <th>Usuario</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Fecha</th>
            <th>Estado</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody id="result_activacion">
        @foreach ($usuarios as $i => $user)
            <tr id="{{$user->id}}" class="row-signal">                        
                <td>{{$user->nickname}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->lastname}}</td>
                <td>{{$user->created_at}}</td>
                <td>
                    @if($user->role == 'user')
                    <span class="badge badge-primary">{{$user->role}}</span>
                    @else
                    <span class="badge badge-warning">{{$user->role}}</span>
                    @endif
                </td>
                <td>
                    @if($user->role == 'user')
                    <button class="btn btn-danger" onclick="desactivar_usuario({{$user->id}})"><i class="icon-fa icon-fa-times"></i></button>
                    @else 
                    <button class="btn btn-success" data-toggle="modal" data-target="#activar" onclick="asignar_id({{$user->id}})"><i class="icon-fa icon-fa-check"></i></button>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
{!! $usuarios->render() !!}