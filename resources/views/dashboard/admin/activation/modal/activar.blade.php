<!-- Modal -->
<div class="modal" id="activar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Activar Usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" id="form_activar_user">
          @csrf
          <input type="hidden" id="id_user" name="id_user">
          <div class="form-group">
            <label for="membresia_id">Membresia</label>
            <select name="membresia_id" class="form-control" required>
              <option value="">Selecciona...</option> 
              @foreach($membresias as $data)              
              <option value="{{$data->id}}">{{$data->name}}</option>
              @endforeach 
            </select>
          </div>
          <div class="form-group">
            <label for="description">Descripción</label>
            <textarea class="form-control" name="description" required></textarea>
          </div>
          <div class="form-group text-center">
            <button type="submit" class="btn btn-info">Activar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>