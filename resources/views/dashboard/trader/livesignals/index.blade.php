@extends('dashboard.layouts.principal')

@section('socketio')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
@stop

@section('styles')
    <link href="{{ asset('/assets/admin/css/signals/signals.css') }}" rel="stylesheet" type="text/css">
@stop

@section('scripts')
    <script src="/assets/admin/js/coinmarket/common.js"></script>
    <script src="/assets/admin/js/coinmarket/ccc-streamer-utilities.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            dropdownFix();
            streaming();
        });

        function dropdownFix() {
            $('[data-toggle="tooltip"]').tooltip();
            $(document).click(function(event) {
                var dropdownMenu = document.getElementsByClassName('dropdown-menu');
                for (var i = 0; i < dropdownMenu.length; i++) {
                    dropdownMenu[i].className = "dropdown-menu";
                }
            });
        }
        function streaming()
        {
            var coins = document.getElementsByClassName('coinsymbol-container-value');
            var subscriptionCoins = [];
            for (var i = 0; i < coins.length; i++) {
                subscriptionCoins[i] = '5~CCCAGG~'+coins[i].textContent+'~BTC';
            }

            var currentPrice = {};
            var socket = io.connect('https://streamer.cryptocompare.com/');
            var subscription =  subscriptionCoins;
            //var subscription = ['5~CCCAGG~'+coinsymbol+'~BTC', '5~CCCAGG~'+coinsymbol+'~USD'];
            //var subscription = ['5~CCCAGG~BTC~USD','5~CCCAGG~ETH~USD','5~CCCAGG~XRP~USD','5~CCCAGG~BCH~USD','5~CCCAGG~EOS~USD','5~CCCAGG~ADA~USD','5~CCCAGG~LTC~USD','5~CCCAGG~XLM~USD','5~CCCAGG~TRX~USD','5~CCCAGG~MIOTA~USD','5~CCCAGG~NEO~USD','5~CCCAGG~XMR~USD','5~CCCAGG~DASH~USD','5~CCCAGG~XEM~USD','5~CCCAGG~USDT~USD','5~CCCAGG~VEN~USD','5~CCCAGG~ETC~USD','5~CCCAGG~QTUM~USD','5~CCCAGG~OMG~USD','5~CCCAGG~ICX~USD','5~CCCAGG~BNB~USD','5~CCCAGG~BTG~USD','5~CCCAGG~LSK~USD','5~CCCAGG~AE~USD','5~CCCAGG~ZEC~USD','5~CCCAGG~STEEM~USD','5~CCCAGG~XVG~USD','5~CCCAGG~BCN~USD','5~CCCAGG~SC~USD','5~CCCAGG~NANO~USD','5~CCCAGG~BTM~USD','5~CCCAGG~WAN~USD','5~CCCAGG~BCD~USD','5~CCCAGG~ONT~USD','5~CCCAGG~PPT~USD','5~CCCAGG~ZIL~USD','5~CCCAGG~BTS~USD','5~CCCAGG~BTCP~USD','5~CCCAGG~WAVES~USD','5~CCCAGG~STRAT~USD','5~CCCAGG~MKR~USD','5~CCCAGG~ZRX~USD','5~CCCAGG~XIN~USD','5~CCCAGG~DCR~USD','5~CCCAGG~DOGE~USD','5~CCCAGG~SNT~USD','5~CCCAGG~RHOC~USD','5~CCCAGG~HSR~USD','5~CCCAGG~DGD~USD','5~CCCAGG~AION~USD'];
            socket.emit('SubAdd', { subs: subscription });
            socket.on("m", function(message) {
                var messageType = message.substring(0, message.indexOf("~"));
                var res = {};
                if (messageType == CCC.STATIC.TYPE.CURRENTAGG) {
                    res = CCC.CURRENT.unpack(message);
                    dataUnpack(res);
                }
            });

            var dataUnpack = function(data) {
                var from = data['FROMSYMBOL'];
                var to = data['TOSYMBOL'];
                var fsym = CCC.STATIC.CURRENCY.getSymbol(from);
                var tsym = CCC.STATIC.CURRENCY.getSymbol(to);
                var pair = from + to;
                // console.log(data);

                if (!currentPrice.hasOwnProperty(pair)) {
                    currentPrice[pair] = {};
                }
                for (var key in data) {
                    currentPrice[pair][key] = data[key];
                }
                if (currentPrice[pair]['LASTTRADEID']) {
                    currentPrice[pair]['LASTTRADEID'] = parseInt(currentPrice[pair]['LASTTRADEID']).toFixed(0);
                }
                currentPrice[pair]['CHANGE24HOUR'] = CCC.convertValueToDisplay(tsym, (currentPrice[pair]['PRICE'] - currentPrice[pair]['OPEN24HOUR']));
                currentPrice[pair]['CHANGE24HOURPCT'] = ((currentPrice[pair]['PRICE'] - currentPrice[pair]['OPEN24HOUR']) / currentPrice[pair]['OPEN24HOUR'] * 100).toFixed(2) + "%";
                displayData(currentPrice[pair], from, tsym, fsym);
            };

            var displayData = function(current, from, tsym, fsym) {
                //console.log(current);
                var color = 'red';
                var priceDirection = current.FLAGS;
                for (var key in current) {
                    if (key == 'CHANGE24HOURPCT') {
                        if(current[key] != 'NaN%') {
                            if(current[key].slice(0, -1) > 0) {
                                color = 'green';
                            }
                            //$('#' + key + '_' + from).html("<span style='color: "+color+"'>"+current[key]+"</span>");
                        }
                    }
                    else if (key == 'LASTVOLUMETO' || key == 'VOLUME24HOURTO') {
                        //$('#' + key + '_' + from).text(CCC.convertValueToDisplay(tsym, current[key]));
                    }
                    else if (key == 'LASTVOLUME' || key == 'VOLUME24HOUR' || key == 'OPEN24HOUR' || key == 'OPENHOUR' || key == 'HIGH24HOUR' || key == 'HIGHHOUR' || key == 'LOWHOUR' || key == 'LOW24HOUR') {
                        //$('#' + key + '_' + from).text(CCC.convertValueToDisplay(fsym, current[key]));
                    }
                    else if(key == 'PRICE') {
                        var decimal = 5;
                        var amount = current[key]*1;
                        // current['PRICE']*$(".top-currency-dropdown .selected").attr('value');
                        if (amount >= 1) {
                            decimal = 2;
                        }
                        var formatted_amount = '$' + "" + formatAmount(amount, decimal, false);

                        $('#' + key + '_' + from).text(current['PRICE']);

                    }
                }

                $('#PRICE_' + from).removeClass("up").removeClass("down").removeClass("unchanged");
                if (priceDirection & 1) {
                    $('#PRICE_' + from).addClass("up");
                } else if (priceDirection & 2) {
                    $('#PRICE_' + from).addClass("down");
                } else if (priceDirection & 4) {
                    $('#PRICE_' + from).addClass("unchanged");
                }
            };
        }

    </script>
@stop

@section('content')
    <div class="main-content page-signals">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">Signals</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('user.signals.index')}}">Signals</a></li>
                <li class="breadcrumb-item active">Signals</li>
            </ol>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div id="membresiaUserId" style="display: none;">{{$membresiaUserId}}</div>
        <div class="table-responsive">
            <table id="userSignals" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                <thead class="thead-dark">
                <tr>
                    <th>Hace</th>
                    <th>Moneda</th>
                    <th>Exchange</th>
                    <th data-toggle="tooltip" data-placement="top" title="" data-original-title="Precio sugerido de compra al emitir la señal">
                        Precio <i class="icon-fa icon-fa-btc"></i>
                    </th>
                    <th data-toggle="tooltip" data-placement="top" title="" data-original-title="Precio actual en tiempo real" style="width: 135px;">
                        Precio actual <i class="icon-fa icon-fa-btc"></i>
                    </th>
                    <th>30m</th>
                    <th>1h</th>
                    <th>3h</th>
                    <th>6h</th>
                    <th>12h</th>
                    <th>24h</th>
                    <th>48h</th>
                    <th>Mejor</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($signals as $i => $signal)
                    <tr id="{{$signal->id}}" class="row-signal">
                        <td style="width: 170px;">
                            @php
                                //Our dates and times.
                                $then = $signal->created_at;
                                $then = new DateTime($then);

                                $now = new DateTime();

                                $sinceThen = $then->diff($now);
                                $days = $sinceThen->d;
                                $hours = $sinceThen->h;
                                $minutes = $sinceThen->i;
                                $seconds = $sinceThen->s;

                                if($days == 0 && $hours == 0 && $seconds == 0){
                                    $seconds.'s';
                                }else if($days == 0 && $hours == 0) {
                                    echo $minutes.'m, '.$seconds.'s';
                                }else if( $days == 0 ){
                                    echo $hours.'h, '.$minutes.'m ' .$seconds.'s';
                                }else if( $days > 0){
                                    echo $days.'d, '.$hours.'h, '.$minutes.'m, '.$seconds.'s';
                                }
                            @endphp
                        </td>
                        <td class="font-14 dropdown-coin-td" scope="row">
                            <div class="dropdown">
                                <a class="btn dropdown-toggle" href="#" role="button" id="dropdownCoin-{{$signal->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span data-original-title="{{ $signal->coinname }}" data-placement="top" data-toggle="tooltip" title="{{ $signal->coinname }}">
                                        <img src="{{ $signal->coinimgurl }}?anchor=center&mode=crop&width=32&height=32" width="24">
                                        <b class="coinsymbol-container-value">{{ $signal->coinsymbol }}</b>
                                    </span>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownCoin-{{$signal->id}}">
                                    <a class="dropdown-item" href="/dashboard/trader/coinmarket/{{$signal->coinsymbol}}/{{$signal->id}}">Ver Detalles</a>
                                    {{--
                                    <a class="dropdown-item" href="#">Marcar Señal</a>
                                    --}}
                                    <a class="dropdown-item" href="{{ $signal->cointradeurl }}" target="_blank">Tradear Señal</a>
                                </div>
                            </div>
                        </td>
                        <td>{{ $signal->exchange }}</td>
                        <td>{{ $signal->coinprice }}</td>
                        <td id="PRICE_{{$signal->coinsymbol}}">--</td>
                        <td id="price30mpasscreate_{{$signal->coinsymbol}}" data-toggle="tooltip" data-placement="top" title="{{$pricesTimesSignals[$i]['price30mpasscreate']}}" data-original-title="{{$pricesTimesSignals[$i]['price30mpasscreate']}}">     <b><div id="price30mpasscreatePercent_{{$signal->coinsymbol}}" class="badge badge-default {{$pricesTimesSignals[$i]['price30mclass']}} font-12">{{$pricesTimesSignals[$i]['price30mpasscreatePercent']}} %</div></b>
                        </td>
                        <td id="price1hpasscreate_{{$signal->coinsymbol}}" data-toggle="tooltip" data-placement="top" title="{{$pricesTimesSignals[$i]['price1hpasscreate']}}" data-original-title="{{$pricesTimesSignals[$i]['price1hpasscreate']}}">     <b><div id="price1hpasscreatePercent_{{$signal->coinsymbol}}" class="badge badge-default {{$pricesTimesSignals[$i]['price1hclass']}} font-12">{{$pricesTimesSignals[$i]['price1hpasscreatePercent']}} %</div></b>
                        </td>
                        <td id="price3hpasscreate_{{$signal->coinsymbol}}" data-toggle="tooltip" data-placement="top" title="{{$pricesTimesSignals[$i]['price3hpasscreate']}}" data-original-title="{{$pricesTimesSignals[$i]['price3hpasscreate']}}">     <b><div id="price3hpasscreatePercent_{{$signal->coinsymbol}}" class="badge badge-default {{$pricesTimesSignals[$i]['price3hclass']}} font-12">{{$pricesTimesSignals[$i]['price3hpasscreatePercent']}} %</div></b>
                        </td>
                        <td id="price6hpasscreate_{{$signal->coinsymbol}}" data-toggle="tooltip" data-placement="top" title="{{$pricesTimesSignals[$i]['price6hpasscreate']}}" data-original-title="{{$pricesTimesSignals[$i]['price6hpasscreate']}}">     <b><div id="price6hpasscreatePercent_{{$signal->coinsymbol}}" class="badge badge-default {{$pricesTimesSignals[$i]['price6hclass']}} font-12">{{$pricesTimesSignals[$i]['price6hpasscreatePercent']}} %</div></b>
                        </td>
                        <td id="price12hpasscreate_{{$signal->coinsymbol}}" data-toggle="tooltip" data-placement="top" title="{{$pricesTimesSignals[$i]['price12hpasscreate']}}" data-original-title="{{$pricesTimesSignals[$i]['price12hpasscreate']}}">     <b><div id="price12hpasscreatePercent_{{$signal->coinsymbol}}" class="badge badge-default {{$pricesTimesSignals[$i]['price12hclass']}} font-12">{{$pricesTimesSignals[$i]['price12hpasscreatePercent']}} %</div></b>
                        </td>
                        <td id="price24hpasscreate_{{$signal->coinsymbol}}" data-toggle="tooltip" data-placement="top" title="{{$pricesTimesSignals[$i]['price24hpasscreate']}}" data-original-title="{{$pricesTimesSignals[$i]['price24hpasscreate']}}">     <b><div id="price24hpasscreatePercent_{{$signal->coinsymbol}}" class="badge badge-default {{$pricesTimesSignals[$i]['price24hclass']}} font-12">{{$pricesTimesSignals[$i]['price24hpasscreatePercent']}} %</div></b>
                        </td>
                        <td id="price48hpasscreate_{{$signal->coinsymbol}}" data-toggle="tooltip" data-placement="top" title="{{$pricesTimesSignals[$i]['price48hpasscreate']}}" data-original-title="{{$pricesTimesSignals[$i]['price48hpasscreate']}}">     <b><div id="price48hpasscreatePercent_{{$signal->coinsymbol}}" class="badge badge-default {{$pricesTimesSignals[$i]['price48hclass']}} font-12">{{$pricesTimesSignals[$i]['price48hpasscreatePercent']}} %</div></b>
                        </td>
                        <td id="bestpercentprice_{{$signal->coinsymbol}}" data-toggle="tooltip" data-placement="top" title="{{$pricesTimesSignals[$i]['priceAverage']}}" data-original-title="{{$pricesTimesSignals[$i]['priceAverage']}}">      <b><div id="bestpercentpricePercent_{{$signal->coinsymbol}}" class="badge badge-success {{$pricesTimesSignals[$i]['priceAverageclass']}} font-12">{{$pricesTimesSignals[$i]['priceAveragePercent']}} %</div></b>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{--
        {!! $signals->render() !!}
        --}}
        {!! $signals->links() !!}
    </div>
@stop
