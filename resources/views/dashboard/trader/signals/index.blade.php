@extends('dashboard.layouts.principal')

@section('socketio')
{{--
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
--}}
@stop

@section('styles')
    <link href="{{ asset('/assets/admin/css/signals/signals.css') }}" rel="stylesheet" type="text/css">
@stop

@section('scripts')
{{--
    <script src="/assets/admin/js/signals/common.js"></script>
    <script src="/assets/admin/js/signals/ccc-streamer-utilities.js"></script>
    <script src="/assets/admin/js/signals/signals.js"></script>
--}}
@stop

@section('content')
    <div class="main-content page-signals">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">Crear Señales</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('trader.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('trader.signals.index')}}">Señales</a></li>
                <li class="breadcrumb-item active">Creación de Señales</li>
            </ol>
        </div>

        <div class="row">
            <div class="col-lg-12 margin-tb container-new-signal">
                <div class="pull-left">
                    <h2></h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('trader.signals.create') }}"> Create New Signal</a>
                </div>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="table-responsive">  
            <table id="adminSignals" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                <thead class="thead-dark">
                    <tr>
                        <th>Hace</th>
                        <th>Moneda</th>
                        <th>Precio <i class="icon-fa icon-fa-btc"></i></th>
                        <th>Exchange</th>
                        <th>Target Membresía</th>
                        <th width="490px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($signals as $signal)
                        <tr>
                            <td>
                                @php
                                    //Our dates and times.
                                    $then = $signal->created_at;
                                    $then = new DateTime($then);
                                     
                                    $now = new DateTime();
                                     
                                    $sinceThen = $then->diff($now);
                                    $days = $sinceThen->d;
                                    $hours = $sinceThen->h;
                                    $minutes = $sinceThen->i;
                                    $seconds = $sinceThen->s;

                                    if($days == 0 && $hours == 0 && $seconds == 0){
                                        $seconds.' s';
                                    }else if($days == 0 && $hours == 0) {
                                        echo $minutes.' m, '.$seconds.' s';
                                    }else if( $days == 0 ){
                                        echo $hours.' h, '.$minutes.' m ' .$seconds.' s';
                                    }else if( $days > 0){
                                        echo $days.' d, '.$hours.' h, '.$minutes.' m, '.$seconds.' s';
                                    }
                                @endphp
                            </td>
                            <td class="font-14" scope="row">
                                <div class="dropdown">
                                    <span data-original-title="{{ $signal->coinname }}" data-placement="top" data-toggle="tooltip" title="{{ $signal->coinname }}">
                                        <img src="{{ $signal->coinimgurl }}?anchor=center&mode=crop&width=32&height=32" width="24"> 
                                        <b>{{ $signal->coinsymbol }}</b>
                                    </span>
                                </div>
                            </td>
                            <td>{{ $signal->coinprice }}</td>
                            <td>{{ $signal->exchange }}</td>
                            <td>
                                @if( $signal->membresia_id == 1 )
                                    Aprendiz
                                @elseif( $signal->membresia_id == 2 )
                                    Emprendedor
                                @elseif( $signal->membresia_id == 3 )
                                    Inversionista
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-info" href="{{ route('trader.signals.show',$signal->id) }}">Show</a>
                                <a class="btn btn-primary" href="{{ route('trader.signals.edit',$signal->id) }}">Edit</a>
                                <form class="btn-delete" action="{{ route('trader.signals.destroy',$signal->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-delete-signal">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {!! $signals->links() !!}
    </div>
@stop
