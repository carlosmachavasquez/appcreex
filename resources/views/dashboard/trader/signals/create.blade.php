@extends('dashboard.layouts.principal')

@section('socketio')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
@stop

@section('styles')
    <link href="{{ asset('/assets/admin/css/signals/easy-autocomplete.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/admin/css/signals/create.css') }}" rel="stylesheet" type="text/css">
@stop

@section('scripts')
    <script src="/assets/admin/js/coinmarket/common.js"></script>
    <script src="/assets/admin/js/coinmarket/ccc-streamer-utilities.js"></script>
    <script src="/assets/admin/js/signals/jquery.easy-autocomplete.min.js"></script>
    <script src="/assets/admin/js/signals/validate-create.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var arrNameCoin = [];
            var domain = 'https://www.cryptocompare.com';
            var queStringCropImg = '?anchor=center&mode=crop&width=32&height=32';
            var obj = {};
            var objSelect = {};
            $.ajax({
                url: "https://min-api.cryptocompare.com/data/all/coinlist",
                type: 'GET',
                dataType: 'json',
                async:false,
                success:function(data){
                    var datacoin = data.Data;
                    for(var i in datacoin){
                        obj = {
                            symbol: datacoin[i].Symbol,
                            coinname: datacoin[i].CoinName,
                            findname: datacoin[i].CoinName + ' | ' + datacoin[i].Symbol,
                            img32: domain + datacoin[i].ImageUrl + queStringCropImg,
                            imgori: domain + datacoin[i].ImageUrl,
                            sortOrder: parseInt(datacoin[i].SortOrder)
                        };
                        arrNameCoin.push(obj);
                        arrNameCoin.sort(compare);
                    }

                    function compare(a,b) {
                        if (a.sortOrder < b.sortOrder)
                            return -1;
                        if (a.sortOrder > b.sortOrder)
                            return 1;
                        return 0;
                    }
                    
                    var options = {
                        data: arrNameCoin,
                        getValue: function(element) {
                            objSelect = {
                                "symbol" : element.symbol,
                                "coinname" : element.coinname,
                                "findname" : element.findname,
                                "imgori" : element.imgori
                            }
                            var findname = element.findname;
                            return findname;
                        },
                        list: {
                            match: {
                                enabled: true
                            },
                            showAnimation: {
                                type: "slide"
                            },
                            onSelectItemEvent: function () {
                                var value = $("#coin").getSelectedItemData().findname;
                                var imgCoin = document.getElementById('img-coin');
                                var coinname = document.getElementById('coinname');
                                var coinsymbol = document.getElementById('coinsymbol');
                                var coinimgurl = document.getElementById('coinimgurl');
                                $("#coin").val(value).trigger("change");
                                imgCoin.src=objSelect.imgori;
                                coinname.value = objSelect.coinname;
                                coinsymbol.value = objSelect.symbol;
                                coinimgurl.value = objSelect.imgori;
                            },
                            onChooseEvent: function() {
                                streaming(objSelect);
                            }
                        },
                        //                                        
                        placeholder: "Type Coin...",
                        template: {
                            type: "custom",
                            method: function (value, item) {
                                return "<img src=" + item.img32 + "/> " + value + "";
                            }
                        },
                        theme: "solid"
                    };
                    $("#coin").easyAutocomplete(options);
                },
                error:function(msj){

                }
            });
        });

        function streaming(objSelect)
        {
            var currentPrice = {};
            var socket = io.connect('https://streamer.cryptocompare.com/');
            var currentSymbolCoin = objSelect.symbol;
            var subscription = ['5~CCCAGG~'+currentSymbolCoin+'~BTC'];
            //var subscription = ['5~CCCAGG~BTC~USD','5~CCCAGG~ETH~USD','5~CCCAGG~XRP~USD','5~CCCAGG~BCH~USD','5~CCCAGG~EOS~USD','5~CCCAGG~ADA~USD','5~CCCAGG~LTC~USD','5~CCCAGG~XLM~USD','5~CCCAGG~TRX~USD','5~CCCAGG~MIOTA~USD','5~CCCAGG~NEO~USD','5~CCCAGG~XMR~USD','5~CCCAGG~DASH~USD','5~CCCAGG~XEM~USD','5~CCCAGG~USDT~USD','5~CCCAGG~VEN~USD','5~CCCAGG~ETC~USD','5~CCCAGG~QTUM~USD','5~CCCAGG~OMG~USD','5~CCCAGG~ICX~USD','5~CCCAGG~BNB~USD','5~CCCAGG~BTG~USD','5~CCCAGG~LSK~USD','5~CCCAGG~AE~USD','5~CCCAGG~ZEC~USD','5~CCCAGG~STEEM~USD','5~CCCAGG~XVG~USD','5~CCCAGG~BCN~USD','5~CCCAGG~SC~USD','5~CCCAGG~NANO~USD','5~CCCAGG~BTM~USD','5~CCCAGG~WAN~USD','5~CCCAGG~BCD~USD','5~CCCAGG~ONT~USD','5~CCCAGG~PPT~USD','5~CCCAGG~ZIL~USD','5~CCCAGG~BTS~USD','5~CCCAGG~BTCP~USD','5~CCCAGG~WAVES~USD','5~CCCAGG~STRAT~USD','5~CCCAGG~MKR~USD','5~CCCAGG~ZRX~USD','5~CCCAGG~XIN~USD','5~CCCAGG~DCR~USD','5~CCCAGG~DOGE~USD','5~CCCAGG~SNT~USD','5~CCCAGG~RHOC~USD','5~CCCAGG~HSR~USD','5~CCCAGG~DGD~USD','5~CCCAGG~AION~USD'];
            socket.emit('SubAdd', { subs: subscription });
            socket.on("m", function(message) {
                var messageType = message.substring(0, message.indexOf("~"));
                var res = {};
                if (messageType == CCC.STATIC.TYPE.CURRENTAGG) {
                    res = CCC.CURRENT.unpack(message);
                    dataUnpack(res);
                }
            });

            var dataUnpack = function(data) {
                var from = data['FROMSYMBOL'];
                var to = data['TOSYMBOL'];
                var fsym = CCC.STATIC.CURRENCY.getSymbol(from);
                var tsym = CCC.STATIC.CURRENCY.getSymbol(to);
                var pair = from + to;
                // console.log(data);

                if (!currentPrice.hasOwnProperty(pair)) {
                    currentPrice[pair] = {};
                }
                for (var key in data) {
                    currentPrice[pair][key] = data[key];
                }
                if (currentPrice[pair]['LASTTRADEID']) {
                    currentPrice[pair]['LASTTRADEID'] = parseInt(currentPrice[pair]['LASTTRADEID']).toFixed(0);
                }
                currentPrice[pair]['CHANGE24HOUR'] = CCC.convertValueToDisplay(tsym, (currentPrice[pair]['PRICE'] - currentPrice[pair]['OPEN24HOUR']));
                currentPrice[pair]['CHANGE24HOURPCT'] = ((currentPrice[pair]['PRICE'] - currentPrice[pair]['OPEN24HOUR']) / currentPrice[pair]['OPEN24HOUR'] * 100).toFixed(2) + "%";
                displayData(currentPrice[pair], from, tsym, fsym);
            };

            var displayData = function(current, from, tsym, fsym) {
                var currentPriceShow = document.getElementById('currentpriceshow');
                var currentExchangeShow = document.getElementById('currentexchangeshow');
                currentPriceShow.value = current.PRICE;
                currentExchangeShow.value = current.LASTMARKET;
            };
        }
    </script>
@stop

@section('content')
    <div class="main-content page-signals-create">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">Signals</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('trader.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('trader.signals.index')}}">Signals</a></li>
                <li class="breadcrumb-item active">Add New Signal</li>
            </ol>
        </div>
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2></h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('trader.signals.index') }}"> Back</a>
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Selecciona tu moneda:</strong>
                                <input id="coin" type="text" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <form action="{{ route('trader.signals.store') }}" id="validateCreateForm" method="POST">
                                    @csrf
                                     <div class="row">
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>Coin Name:</strong>
                                                <input id="coinname" type="text" name="coinname" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>Coin Symbol:</strong>
                                                <input id="coinsymbol" type="text" name="coinsymbol" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <strong>Url image:</strong>
                                                <input id="coinimgurl" type="text" name="coinimgurl" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>Exchange:</strong>
                                                <input id="exchange" type="text" name="exchange" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>Target por membresía:</strong>
                                                <select id="membresia_id" name="membresia_id" class="form-control ls-select2">
                                                    <option value="1">Aprendiz</option>
                                                    <option value="2">Emprendedor</option>
                                                    <option value="3">Inversionista</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>Link Tradear Señal:</strong>
                                                <input id="cointradeurl" type="text" name="cointradeurl" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>Precio sugerido de compra:</strong>
                                                <input id="coinprice" type="number" name="coinprice" class="form-control" step=".000000001">
                                            </div>
                                        </div>
                                         <div class="col-xs-12 col-sm-12 col-md-12">
                                             <div class="form-group">
                                                 <strong>Rango de precios (Referencial):</strong>
                                                 <input id="rangeprice" type="text" name="rangeprice" class="form-control" step=".000000001">
                                             </div>
                                         </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>Target 1:</strong>
                                                <input id="target1" type="number" name="target1" class="form-control" step=".000000001">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>Target 2:</strong>
                                                <input id="target2" type="number" name="target2" class="form-control" step=".000000001">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>Target 3:</strong>
                                                <input id="target3" type="number" name="target3" class="form-control" step=".000000001">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>Stop loss:</strong>
                                                <input id="stoploss" type="number" name="stoploss" class="form-control" step=".000000001">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                            <button type="submit" class="btn btn-primary">Crear señal</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col container-card-img">
                    <div class="card">
                        <div class="card-block">
                            <img id="img-coin" src="" width="300" height="300">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <h3>Información en tiempo real de la moneda</h3>
                                        </div>
                                    </div>                                  
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <strong>Precio actual de la moneda (Ƀ):</strong>
                                            <input id="currentpriceshow" type="text" name="currentpriceshow" class="form-control bg-info text-white">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <strong>Ultimo Exchange donde se realizo la transacción:</strong>
                                            <input id="currentexchangeshow" type="text" name="currentexchangeshow" class="form-control bg-info text-white">
                                        </div>
                                    </div>
                                </div>
                             </div>
                        </div> 
                    </div>
                </div>
            </div>  
        </div>
    </div>
@endsection