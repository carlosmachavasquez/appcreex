@extends('dashboard.layouts.principal')



@section('styles')
    <link href="{{ asset('/assets/admin/css/signals/easy-autocomplete.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/admin/css/signals/create.css')}}" rel="stylesheet" type="text/css">
    
@stop

@section('scripts')
    
@stop

@section('content')
    <div class="main-content page-signals-create">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">FOREX</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('trader.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('trader.forex.index')}}">FOREX</a></li>
                <li class="breadcrumb-item active">Add New FOREX</li>
            </ol>
        </div>
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2></h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('trader.forex.index') }}"> Back</a>
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <form action="{{ route('trader.forex.update',$forex->id) }}" id="validateCreateForm" method="POST" autocomplete="off">
                                    @csrf
                                     <div class="row">
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>PAIR:</strong>
                                                <input id="pair" type="text" name="pair" class="form-control" value="{{$forex->pair}}" required>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>ORDER TYPE:</strong>
                                                <select name="order_type" class="form-control" id="order_type" value="{{$forex->order_type}}" required>
                                                    <option value="sell">Sell</option>
                                                    <option value="buy">Buy</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <strong>MARKET EXECUTION AT OR NEAR PRICE:</strong>
                                                <input id="market_execution" type="number" step="any" name="market_execution" class="form-control" value="{{$forex->market_execution}}" required>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>SELL LIMIT:</strong>
                                                <input id="sell_limit" type="number" step="any" name="sell_limit" class="form-control" value="{{$forex->sell_limit}}" required>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>STOP LOSS:</strong>
                                                <input id="stop_loss" type="number" step="any" name="stop_loss" class="form-control" value="{{$forex->stop_loss}}" required>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>TAKE PROFIT 1:</strong>
                                                 <input id="take_prof1" type="number" step="any" name="take_prof1" class="form-control" value="0" min="0">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>TAKE PROFIT 2:</strong>
                                                <input id="take_prof2" type="number" step="any" name="take_prof2" class="form-control" value="0" min="0">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>TAKE PROFIT 3:</strong>
                                                <input id="take_prof3" type="number" step="any" name="take_prof3" class="form-control" value="0" min="0">
                                            </div>
                                        </div>
                                        
                                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                            <button type="submit" class="btn btn-primary">Actualizar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>  
        </div>
    </div>
@endsection