@extends('dashboard.layouts.principal')

@section('styles')
    <style>
        .thead-dark{
             background: #212529;
            border-color: #888;
        }
    </style>
@stop

@section('content')
    <div class="main-content page-signals">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">Forex</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('trader.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('trader.forex.index')}}">Forex</a></li>
                <li class="breadcrumb-item active">Forex</li>
            </ol>
        </div>

        <div class="row">
            <div class="col-lg-12 margin-tb container-new-signal">
                <div class="pull-left">
                    <h2></h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" href="{{ route('trader.forex.create') }}"> Create New FOREX</a>
                </div>
            </div>
        </div>
        
        <div class="table-responsive">  
            <table id="adminSignals" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                <thead class="thead-dark">
                    <tr>
                        <th>PAIR</th>
                        <th>ORDER TYPE</th>
                        <th>MARKET</th>
                        <th>SELL LIMIT</th>
                        <th>STOP LOSS</th>
                        <th>TAKE PROFIT 1</th>
                        <th>TAKE PROFIT 2</th>
                        <th>TAKE PROFIT 3</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($forex as $data)
                        <tr>
                            <td>{{$data->pair}}</td>
                            <td>{{$data->order_type}}</td>
                            <td>{{$data->market_execution}}</td>
                            <td>{{$data->sell_limit}}</td>
                            <td>{{$data->stop_loss}}</td>
                            <td>{{$data->take_prof1}}</td>
                            <td>{{$data->take_prof2}}</td>
                            <td>{{$data->take_prof3}}</td>
                            <td>
                                <a class="btn btn-info" href="{{route('trader.forex.show',$data->id)}}">Show</a>
                                <a class="btn btn-primary" href="{{route('trader.forex.edit',$data->id)}}">Edit</a>
                                <form class="btn-delete" action="{{ route('trader.forex.destroy',$data->id) }}" method="POST" style="width: 100px; display: inline-block;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-delete-signal">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $forex->links() }}
        </div>
    </div>
@stop
