<!DOCTYPE html>
<html>
    <head>
        <meta charset = "utf-8">
        <title>Creex App</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
        <script src="{{asset('dashboard/assets/global/js/core/pace.js')}}"></script>
        <link href="{{asset('dashboard/assets/global/css/laraspace.css')}}" rel="stylesheet" type="text/css">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @include('dashboard.templates.layouts.partials.favicons')
        @yield('styles')
        @yield('socketio')
        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
            
        </script>
{{--         <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
        <script>
          var OneSignal = window.OneSignal || [];
          OneSignal.push(function() {
            OneSignal.init({
              appId: "b615ea09-7eed-44cd-a306-f420a43f4aff",
              autoRegister: false,
              notifyButton: {
                enable: true,
              },
            });
          });
        </script> --}}
    </head>
    <body class="layout-horizontal skin-default pace-done" >
        <div id="app" class="site-wrapper">
            @include('dashboard.templates.layouts.partials.laraspace-notifs')
            @include('dashboard.templates.layouts.partials.header')
            <div class="mobile-menu-overlay"></div>
            @include('dashboard.templates.layouts.partials.header-bottom')
            @yield('content')
            @include('dashboard.templates.layouts.partials.footer')

            {{--
            @include('dashboard.templates.layouts.partials.skintools')
            @include('dashboard.includes.sidebar')
            @yield('nav-search')
            <div class="main-content">
                @yield('breadcrump')
                <div class="row">
                    <div class="col-sm-12">
                        @yield('contenido')
                    </div>
                </div>
            </div>
            --}}
        </div>

        <script src="{{mix('/assets/admin/js/core/plugins.js')}}"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        {{-- <script src="{{asset('dashboard/blockui/jquery.blockUI.js')}}"></script> --}}
        {{-- <script src="{{asset('/assets/admin/js/demo/skintools.js')}}"></script> --}}

        @if(request()->is('dashboard/user/signals'))
            <script src="{{mix('/assets/admin/js/core/app.js')}}"></script>
        @endif

        <script src="{{ asset('dashboard/assets/plugins/summernote/summernote.js') }}"></script>
        <script src="{{ asset('dashboard/assets/pages/editors.js')}}"></script>
        @yield('scripts')
        <script>
            $('#flash-overlay-modal').modal();
        </script>
        <script>
            $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
        </script>
        <script>
            $('.nav-toggle').on('click', function (e) {
                e.preventDefault();
                toggleSideBar();
            });
            var toggleSideBar = function () {
                var icon = $('.hamburger').first();
                $('body').toggleClass('sidebar-open');
                icon.toggleClass('is-active');
            };
        </script>
        <script>
             // Recibimos Token del Culqi.js
        
        </script>
    </body>
</html>
