@extends('dashboard.layouts.principal')

@section('socketio')
    {{--
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
    --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.slim.js"></script>
@stop

@section('styles')
    <link href="{{ asset('x4-crypto-tables/coins/coins.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('x4-crypto-tables/minified/x4-crypto-tables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/admin/css/coinmarket/single.css') }}" rel="stylesheet" type="text/css">
@stop

@section('scripts')
    <script src="/assets/admin/js/coinmarket/common.js"></script>
    <script src="/assets/admin/js/coinmarket/ccc-streamer-utilities.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.min.js"></script>
    <!-- connect X4 Crypto Charts main script -->
    {{--
    <script src="/assets/admin/js/coinmarket/x4-crypto-charts.js"></script>
    --}}
    <script src="/x4-crypto-tables/x4-crypto-tables.js?v=1.0.0"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            streaming();
            setCharts();
            //setTablePercents();
        });

        function streaming()
        {
            var coinsymbol = '{{$signal->coinsymbol}}';
            //console.log(coinsymbol);
            var currentPrice = {};
            var socket = io.connect('https://streamer.cryptocompare.com/');
            var subscription = ['5~CCCAGG~'+coinsymbol+'~BTC', '5~CCCAGG~'+coinsymbol+'~USD'];
            socket.emit('SubAdd', { subs: subscription });
            socket.on("m", function(message) {
                //console.log(message);
                var messageType = message.substring(0, message.indexOf("~"));
                var res = {};
                if (messageType == CCC.STATIC.TYPE.CURRENTAGG) {
                    res = CCC.CURRENT.unpack(message);
                    dataUnpack(res);
                }
            });

            var dataUnpack = function(data) {
                var from = data['FROMSYMBOL'];
                var to = data['TOSYMBOL'];
                var fsym = CCC.STATIC.CURRENCY.getSymbol(from);
                var tsym = CCC.STATIC.CURRENCY.getSymbol(to);
                var pair = from + to;
                // console.log(data);

                if (!currentPrice.hasOwnProperty(pair)) {
                    currentPrice[pair] = {};
                }
                for (var key in data) {
                    currentPrice[pair][key] = data[key];
                }
                if (currentPrice[pair]['LASTTRADEID']) {
                    currentPrice[pair]['LASTTRADEID'] = parseInt(currentPrice[pair]['LASTTRADEID']).toFixed(0);
                }
                currentPrice[pair]['CHANGE24HOUR'] = CCC.convertValueToDisplay(tsym, (currentPrice[pair]['PRICE'] - currentPrice[pair]['OPEN24HOUR']));
                currentPrice[pair]['CHANGE24HOURPCT'] = ((currentPrice[pair]['PRICE'] - currentPrice[pair]['OPEN24HOUR']) / currentPrice[pair]['OPEN24HOUR'] * 100).toFixed(2) + "%";
                displayData(currentPrice[pair], from, tsym, fsym);
            };

            var displayData = function(current, from, tsym, fsym) {
                //console.log(current);
                var color = 'red';
                //$('.crypto-currencies-data tr').removeClass('up').removeClass('down');
                var priceDirection = current.FLAGS;
                for (var key in current) {
                    if (key == 'CHANGE24HOURPCT') {
                        if(current[key] != 'NaN%') {
                            if(current[key].slice(0, -1) > 0) {
                                color = 'green';
                            }
                            //$('#' + key + '_' + from).html("<span style='color: "+color+"'>"+current[key]+"</span>");
                        }
                    }
                    else if (key == 'LASTVOLUMETO' || key == 'VOLUME24HOURTO') {
                        //$('#' + key + '_' + from).text(CCC.convertValueToDisplay(tsym, current[key]));
                        //console.log( key + "= ", CCC.convertValueToDisplay(tsym, current[key]) );
                    }
                    else if (key == 'LASTVOLUME' || key == 'VOLUME24HOUR' || key == 'OPEN24HOUR' || key == 'OPENHOUR' || key == 'HIGH24HOUR' || key == 'HIGHHOUR' || key == 'LOWHOUR' || key == 'LOW24HOUR') {
                        //$('#' + key + '_' + from).text(CCC.convertValueToDisplay(fsym, current[key]));
                        console.log( key + "= ", CCC.convertValueToDisplay(tsym, current[key]) );
                    }
                    else if(key == 'PRICE') {
                        var decimal = 5;
                        var amount = current[key]*1;
                        // current['PRICE']*$(".top-currency-dropdown .selected").attr('value');
                        if (amount >= 1) {
                            decimal = 2;
                        }
                        var formatted_amount = '$' + "" + formatAmount(amount, decimal, false);
                        //$('#' + key + '_' + from).text(formatted_amount);
                        //console.log( key +": "+ from+"= ", current[key] );
                    } else if (key == 'TOSYMBOL'){
                        if (current[key] == 'USD'){
                            if(current['PRICE'] != undefined){
                                $('#PRICE_USD').text('$ ' + current['PRICE'] + ' USD');
                            }
                            if(!isNaN(current['VOLUME24HOUR'])){
                                $('#VOLUME24HOURUSD').text( CCC.convertValueToDisplay(tsym, current['VOLUME24HOUR']) );
                            }

                        }else if(current[key] == 'BTC'){
                            if(current['PRICE'] != undefined){
                                $('#PRICE_BTC').text('Ƀ ' + current['PRICE'] + ' BTC');
                            }
                        }
                    }
                }

                $('#PRICE_USD').removeClass("up").removeClass("down").removeClass("unchanged");
                $('#PRICE_BTC').removeClass("up").removeClass("down").removeClass("unchanged");
                $('#VOLUME24HOURUSD').removeClass("up").removeClass("down").removeClass("unchanged");
                if (priceDirection & 1) {
                    $('#PRICE_USD').addClass("up");
                    $('#PRICE_BTC').addClass("up");
                    $('#VOLUME24HOURUSD').addClass("up");
                } else if (priceDirection & 2) {
                    $('#PRICE_USD').addClass("down");
                    $('#PRICE_BTC').addClass("down");
                    $('#VOLUME24HOURUSD').addClass("down");
                } else if (priceDirection & 4){
                    $('#PRICE_USD').addClass("unchanged");
                    $('#PRICE_BTC').addClass("unchanged");
                    $('#VOLUME24HOURUSD').addClass("unchanged");
                }

            };
        }
        function setCharts() {
            new X4CryptoTable('#grafica', {
                coins: {
                    strategy: 'exclude_all',
                    except: ['{{$signal->coinsymbol}}'],
                },
                fiat: {
                    visible: false,
                },
                perPage: {
                    visible: false,
                },
                search: {
                    visible: false,
                },
                pager: {
                    visible: false,
                },
                font: {
                    family: 'Roboto, sans-serif',
                }
            });
        }
        function setTablePercents() {
            var signalCreateAtTimeStamp = '{{$signal->created_at->timestamp}}';
            var coinprice = '{{$signal->coinprice}}';
            var coinsymbol = '{{$signal->coinsymbol}}';

            function diffInRealMinutes(datetimestamp) {
                var nTotalDiff = Math.round((new Date()).getTime() / 1000) - datetimestamp;
                if (nTotalDiff >= 0) {
                    return Math.floor(nTotalDiff / 60);
                }
            }

            function diffInRealHours(datetimestamp) {
                var nTotalDiff = Math.round((new Date()).getTime() / 1000) - datetimestamp;
                if (nTotalDiff >= 0) {
                    return Math.floor(nTotalDiff / 3600);
                }
            }

            function addMinutesInTimestamp(datetimestamp, number) {
                if (parseInt(number) < 60){
                    return parseInt(datetimestamp) + 60*parseInt(number);
                }
            }

            function addHoursInTimestamp(datetimestamp, number) {
                return parseInt(datetimestamp) + 3600*parseInt(number);
            }

            function getRequestForTimestamp(coinsymbolcryto, passtime) {
                var pricePassTime = 0;
                var url = 'https://min-api.cryptocompare.com/data/pricehistorical?fsym='+coinsymbolcryto+'&tsyms=BTC&ts='+passtime;

                var request = new XMLHttpRequest();
                request.open('GET', url, false);

                request.onload = function() {
                  if (request.status >= 200 && request.status < 400) {
                    // Success!
                    var resp = request.responseText;
                    var respjson = JSON.parse(resp);
                    pricePassTime = respjson[coinsymbolcryto]['BTC'];
                  } else {
                    // We reached our target server, but it returned an error

                  }
                };

                request.onerror = function() {
                  // There was a connection error of some sort
                };

                request.send();

                return pricePassTime;
            }

            function getPercentByValue(reference, value, activate) {
                var diff = value - reference;

                if (activate) {
                    diff = Math.abs(value - reference);
                }
                
                var percent = (diff*100)/reference;
                percent = Math.round(percent * 100) / 100;

                return percent;
            }

            function setBgColorPercent(name,percent) {
                if (percent < 0){
                    document.getElementById(name).classList.add("badge-danger");
                    //$('#'+name).addClass("badge-danger");
                }
                if(percent == 0){
                    //$('#'+name).addClass("badge-info");
                    document.getElementById(name).classList.add("badge-info");
                }
                if(percent > 0){
                    //$('#'+name).addClass("badge-success");
                    document.getElementById(name).classList.add("badge-success");
                }
            }

            var diffinminutes = diffInRealMinutes(signalCreateAtTimeStamp);
            var diffinhours = diffInRealHours(signalCreateAtTimeStamp);

            var price30mpasscreate = price1hpasscreate = price3hpasscreate = price6hpasscreate = price12hpasscreate = price24hpasscreate = price48hpasscreate = "--";
            var price30mpasscreatePercent = price1hpasscreatePercent = price3hpasscreatePercent = price6hpasscreatePercent = price12hpasscreatePercent = price24hpasscreatePercent = price48hpasscreatePercent = "--";

            if (diffinminutes >= 30){
                var passto30mtimestamp = addMinutesInTimestamp(signalCreateAtTimeStamp, 30);
                price30mpasscreate = getRequestForTimestamp(coinsymbol, passto30mtimestamp);
                price30mpasscreatePercent = getPercentByValue(coinprice, price30mpasscreate);
                if (diffinhours >= 1){
                    var passto1htimestamp = addHoursInTimestamp(signalCreateAtTimeStamp, 1);
                    price1hpasscreate = getRequestForTimestamp(coinsymbol, passto1htimestamp);
                    price1hpasscreatePercent = getPercentByValue(coinprice, price1hpasscreate);
                    if (diffinhours >= 3){
                        var passto3htimestamp = addHoursInTimestamp(signalCreateAtTimeStamp, 3);
                        price3hpasscreate = getRequestForTimestamp(coinsymbol, passto3htimestamp);
                        price3hpasscreatePercent = getPercentByValue(coinprice, price3hpasscreate);
                        if (diffinhours >= 6){
                            var passto6htimestamp = addHoursInTimestamp(signalCreateAtTimeStamp, 6);
                            price6hpasscreate = getRequestForTimestamp(coinsymbol, passto6htimestamp);
                            price6hpasscreatePercent = getPercentByValue(coinprice, price6hpasscreate);
                            if (diffinhours >= 12){
                                var passto12htimestamp = addHoursInTimestamp(signalCreateAtTimeStamp, 12);
                                price12hpasscreate = getRequestForTimestamp(coinsymbol, passto12htimestamp);
                                price12hpasscreatePercent = getPercentByValue(coinprice, price12hpasscreate);
                                if (diffinhours >= 24){
                                    var passto24htimestamp = addHoursInTimestamp(signalCreateAtTimeStamp, 24);
                                    price24hpasscreate = getRequestForTimestamp(coinsymbol, passto24htimestamp);
                                    price24hpasscreatePercent = getPercentByValue(coinprice, price24hpasscreate);
                                    if (diffinhours >= 48){
                                        var passto48htimestamp = addHoursInTimestamp(signalCreateAtTimeStamp, 48);
                                        price48hpasscreate = getRequestForTimestamp(coinsymbol, passto48htimestamp);
                                        price48hpasscreatePercent = getPercentByValue(coinprice, price48hpasscreate);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            document.getElementById('price30mpasscreate').innerHTML = price30mpasscreate;
            document.getElementById('price1hpasscreate').innerHTML = price1hpasscreate;
            document.getElementById('price3hpasscreate').innerHTML = price3hpasscreate;
            document.getElementById('price6hpasscreate').innerHTML = price6hpasscreate;
            document.getElementById('price12hpasscreate').innerHTML = price12hpasscreate;
            document.getElementById('price24hpasscreate').innerHTML = price24hpasscreate;
            document.getElementById('price48hpasscreate').innerHTML = price48hpasscreate;

            document.getElementById('price30mpasscreatePercent').innerHTML = price30mpasscreatePercent + ' %';
            setBgColorPercent('price30mpasscreatePercent', price30mpasscreatePercent);

            document.getElementById('price1hpasscreatePercent').innerHTML = price1hpasscreatePercent + ' %';
            setBgColorPercent('price1hpasscreatePercent', price1hpasscreatePercent);

            document.getElementById('price3hpasscreatePercent').innerHTML = price3hpasscreatePercent + ' %';
            setBgColorPercent('price3hpasscreatePercent', price3hpasscreatePercent);

            document.getElementById('price6hpasscreatePercent').innerHTML = price6hpasscreatePercent + ' %';
            setBgColorPercent('price6hpasscreatePercent', price6hpasscreatePercent);

            document.getElementById('price12hpasscreatePercent').innerHTML = price12hpasscreatePercent + ' %';
            setBgColorPercent('price12hpasscreatePercent', price12hpasscreatePercent);

            document.getElementById('price24hpasscreatePercent').innerHTML = price24hpasscreatePercent + ' %';
            setBgColorPercent('price24hpasscreatePercent', price24hpasscreatePercent);

            document.getElementById('price48hpasscreatePercent').innerHTML = price48hpasscreatePercent + ' %';
            setBgColorPercent('price48hpasscreatePercent', price48hpasscreatePercent);
        }
    </script>
@stop

@section('content')
    <div class="main-content page-coinmaker {{$signal->coinsymbol}}">
        @include('flash::message')
        <div class="row">
            <div class="col-lg-3">
                <div class="card m-b-30">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <div class="form-group">
                                <span class="font-12">Hace: {{$timepassed}}</span><br>
                                <span class="font-18">Coin: <b>{{$signal->coinsymbol}}</b></span><br>
                                Exchange: <b>{{$signal->exchange}}</b><br>
                                Buy Price: <b>{{$signal->coinprice}}</b> BTC<br>
                                @php
                                    $rango = is_null($signal->rangeprice) ? 'No se llenó la información' : "<br>"."<b>".$signal->rangeprice."</b> BTC";
                                @endphp
                                Rango de precios: {!!$rango!!}
                            </div>
                            <div class="form-group font-12">
                                 Take Profit {{$target1Percent}}%: <b>{{$signal->target1}}</b> BTC<br>
                                 Take Profit {{$target2Percent}}%: <b>{{$signal->target2}}</b> BTC<br>
                                 Take Profit {{$target3Percent}}%: <b>{{$signal->target3}}</b> BTC<br>
                                 Stop Loss {{$stopLossPercent}}%: <b>{{$signal->stoploss}}</b> BTC<br>
                             </div>
                            <div class="form-group font-14">
                                <table class="table table-hover font-12 m-l-0">
                                    <tbody>
                                        <tr style="height:10px;">
                                            <th>30m:</th>
                                            <th><b><div id="price30mpasscreatePercent" class="badge badge-default {{$priceTimeSignal['price30mclass']}} font-12">{{$priceTimeSignal['price30mpasscreatePercent']}} %</div></b></th>
                                            <th id="price30mpasscreate">{{$priceTimeSignal['price30mpasscreate']}}</th>
                                        </tr>
                                        <tr>
                                            <th>1h:</th>
                                            <th><b><div id="price1hpasscreatePercent" class="badge badge-default {{$priceTimeSignal['price1hclass']}} font-12">{{$priceTimeSignal['price1hpasscreatePercent']}} %</div></b></th>
                                            <th id="price1hpasscreate">{{$priceTimeSignal['price1hpasscreate']}}</th>
                                        </tr>
                                        <tr>
                                            <th>3h:</th>
                                            <th><b><div id="price3hpasscreatePercent" class="badge badge-default {{$priceTimeSignal['price3hclass']}} font-12">{{$priceTimeSignal['price3hpasscreatePercent']}} %</div></b></th>
                                            <th id="price3hpasscreate">{{$priceTimeSignal['price3hpasscreate']}}</th>
                                        </tr>
                                        <tr>
                                            <th>6h:</th>
                                            <th><b><div id="price6hpasscreatePercent" class="badge badge-default {{$priceTimeSignal['price6hclass']}} font-12">{{$priceTimeSignal['price6hpasscreatePercent']}} %</div></b></th>
                                            <th id="price6hpasscreate">{{$priceTimeSignal['price6hpasscreate']}}</th>
                                        </tr>
                                        <tr>
                                            <th>12h:</th>
                                            <th><b><div id="price12hpasscreatePercent" class="badge badge-default {{$priceTimeSignal['price12hclass']}} font-12">{{$priceTimeSignal['price12hpasscreatePercent']}} %</div></b></th>
                                            <th id="price12hpasscreate">{{$priceTimeSignal['price12hpasscreate']}}</th>
                                        </tr>
                                        <tr>
                                            <th>24h:</th>
                                            <th><b><div id="price24hpasscreatePercent" class="badge badge-default {{$priceTimeSignal['price24hclass']}} font-12">{{$priceTimeSignal['price24hpasscreatePercent']}} %</div></b></th>
                                            <th id="price24hpasscreate">{{$priceTimeSignal['price24hpasscreate']}}</th>
                                        </tr>
                                        <tr>
                                            <th>48h:</th>
                                            <th><b><div id="price48hpasscreatePercent" class="badge badge-default {{$priceTimeSignal['price48hclass']}} font-12">{{$priceTimeSignal['price48hpasscreatePercent']}} %</div></b></th>
                                            <th id="price48hpasscreate">{{$priceTimeSignal['price48hpasscreate']}}</th>
                                        </tr>
                                    </tbody>
                                          
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-4 col-xl-4">
                    <div class="mini-stat clearfix bg-white">
                        <div class="mini-stat-info text-right text-secondary">
                            <span class="counter">
                                <span id="PRICE_USD">$ {{$priceUsd}} USD</span>
                                <span id="PRICE_BTC">Ƀ {{$priceBtc}} BTC</span>
                            </span>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-4 col-xl-4">
                        <div class="mini-stat clearfix bg-white">
                            <div class="mini-stat-info text-right text-secondary">
                                Volumen 24 horas:<br>
                                <span id="VOLUME24HOURUSD" class="counter"> ${{$volumen24Usd}}</span>
                                <span id="MARKETCAP" class="marketcap-container">Market Cap: ${{$marketCap}}</span>
                       </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xl-4">
                        <div class="mini-stat clearfix bg-white">
                           <div class="mini-stat-info text-right text-secondary">
                                <div class="m-b-5">Cambio 1H: <div id="PERCENTCHANGE1H" class="badge badge-danger font-14"> {{$percentChange1h}} %</div></div>
                              
                                <div>Cambio 24H: <div id="PERCENTCHANGE24H" class="badge badge-success font-14"> {{$percentChange24h}} %</div></div>
                               
                                <div class="m-t-5">Cambio 7D: <div id="PERCENTCHANGE7D" class="badge badge-danger font-14"> {{$percentChange7d}} %</div></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card m-b-30">
                            <div class="card-body">
                                <div id="grafica"></div>
                                {{--
                                <div id="volume"></div>
                                --}}
                            </div>
                        </div>
                    </div>
                    {{--
                    <div class="col-lg-3">
                        <div class="card m-b-30">
                             <!--<div class="row">-->
                             <div class="card-body">
                                 <ul class="list-group list-group-flush">
                                    <form method="post" action="">
                                        <div class="form-group">
                                            <label>Mostrar por:</label>
                                            <select name="property" class="form-control">
                                                <option value="price">Precio</option>
                                                <option value="volume">Volumen</option>
                                                <option value="mktcap">Cap de Mercado</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Moneda:</label>
                                            <select name="currency" class="form-control">
                                                <option value="USD">USD</option>
                                                <option value="EUR">EUR</option>
                                                <option value="BTC">BTC</option>
                                            
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Periodo</label>
                                            <select name="period" class="form-control">
                                                <option value="1day">1 día</option>
                                                <option value="7day">7 días</option>
                                                <option value="30day" selected="">1 mes</option>
                                                <option value="90day">3 meses</option>
                                                <option value="180day">6 meses</option>
                                                <option value="365day">1 año</option>
                                                <option value="all">Todo</option>
                                            </select>
                                        </div>
                                        <input class="btn btn-primary btn-lg btn-block m-b-30" value="Filtrar" type="submit">
                                    </form>
                                </ul>
                            </div>
                        </div>
                    </div>
                    --}}
                </div>
            </div>
        </div>
    </div>
@stop