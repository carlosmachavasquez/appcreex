@extends('dashboard.layouts.principal')

@section('socketio')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.slim.js"></script>
@stop

@section('styles')
    <link href="{{ asset('x4-crypto-tables/coins/coins.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('x4-crypto-tables/minified/x4-crypto-tables.min.css') }}" rel="stylesheet" type="text/css">
@stop

@section('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.min.js"></script>
    <script src="/x4-crypto-tables/minified/x4-crypto-tables.min.js?v=1.0.0"></script>
    <script>
        new X4CryptoTable('#coinmakertable', {
            fiat: {
                value: 'USD',
            },
            perPage: {
                value: 20,
            },
            font: {
                family: 'Roboto, sans-serif',
            }
        });
    </script>

@stop

@section('content')
    <div class="main-content page-signals">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">CoinMarket</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('user.coinmarket.index')}}">CoinMarket</a></li>
                <li class="breadcrumb-item active">Coin Live Watch</li>
            </ol>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h6>Live updates of all active crypto currencies</h6>
                    </div>
                    <div class="card-block table-responsive">
                        <div id="coinmakertable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop