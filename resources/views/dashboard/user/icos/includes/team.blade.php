@foreach ($data->team as $person)
<div class="col-lg-4">
    <div class="card m-b-30">
        <div class="card-body">

            <div class="media">
                <img class="d-flex mr-3 rounded-circle img-thumbnail thumb-lg" src="{{$person->photo}}" alt="Generic placeholder image">
                <div class="media-body">
                    <h5 class="mt-0 font-18 mb-1">{{$person->name}}</h5>
                    <p class="text-muted font-14">{{$person->title}}</p>

                    <ul class="social-links list-inline mb-0">
                       	<li class="list-inline-item">
                            <a target="_blank" data-placement="top" data-toggle="tooltip" class="tooltips" href="https://www.linkedin.com/in/pavel-shkliaev-88a5b9124/" data-original-title="linkedin"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div> 
@endforeach
 