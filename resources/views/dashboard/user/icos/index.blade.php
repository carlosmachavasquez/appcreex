@extends('dashboard.layouts.principal')
@section('content')
<style>
	.calif{
		font-size: 20px!important;
		font-weight: bold;
	}
	td{
		vertical-align: middle!important;
	}
</style>

<div class="main-content page-portafolio">
  @include('flash::message')
  <div class="page-header">
      <h3 class="page-title">Icos</h3>
      <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Home</a></li>
          <li class="breadcrumb-item active">Icos</li>
      </ol>
  </div>
</div>


<div class="col-sm-12">                
    <div class="card">
        <div class="card-block">
          <div class="table-responsive" >
            <!-- Tabla de señales-->
            @if($data == null || $data == '')
            <div class="alert alert-warning" role="alert">
              El API de ICOBENCH está <strong>temporalmente deshabilitada</strong>, Gracias por su comprensión.
            </div>
            @else
            <table  class="table table-hover" width="100%">
                <thead>                                                                   
                    <td>ICO</td>
                    <td>Moneda</td>                                    
                    <td>F.Inicio</td>
                    <td>F.Cierre</td>
                    <td>Calif</td>
                </thead>                                        
                <tbody  class="container-ico">
                  @foreach ($data->results as $item)
                    <tr>                               
                      <td>
                            <a href="{{ url('dashboard/user/icos/detalle') }}/{{$item->id}}"><img style="display: inline-block" src="{{$item->logo}}"></a>
                            
                      </td>
                      <td>
                         <div style="display: inline-block">
                                <a href="{{ url('dashboard/user/icos/detalle') }}/{{$item->id}}"><strong>{{$item->name}}</strong></a>
                                <br>
                                <span  class="descripcion-ico" style="color: grey">{{$item->desc}}</span>
                            </div>
                      </td>
                      <td>
                          {{$item->dates->icoStart}}
                      </td>
                      <td>
                        {{$item->dates->icoEnd}}
                      </td>
                      <td class="calif">
                          {{$item->rating}}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
            @endif
          </div>
        </div>
    </div>
</div>





@endsection

@section('scripts')

@endsection