@extends('dashboard.layouts.principal')
@section('content')
<style>

</style>
<div class="main-content page-portafolio">
        @include('flash::message')

	<div class="row">
		<div class="col-sm-8 col-md-8"> 
			<div class="row ico-header">
				<div class="image">
					<img src="{{$data->logo}}">
				</div>
				<div class="name">
	            	<a href="/premium" title="Premium" class="premium">&nbsp;</a>
	            	<h1>{{$data->name}}</h1>
					<h3>{{$data->tagline}}</h3>
				</div>				
			</div>

			<p>{{$data->intro}}</p>

		    <div class="card with-tabs">
			    <div class="card-header">
			        <div class="actions tabs-simple">
			            <ul class="nav nav-tabs" role="tablist">
			                <li class="nav-item">
			                    <a class="nav-link active" data-toggle="tab" href="#ranking"
			                       role="tab"><i class="icon-fa icon-fa-file-text-o text-success"></i>Intro</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" data-toggle="tab" href="#empresa" role="tab"><i class="icon-fa icon-fa-building text-success"></i>Sobre la empresa</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" data-toggle="tab" href="#team" role="tab"><i class="icon-fa icon-fa-users text-success"></i>Equipo</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" data-toggle="tab" href="#hitos" role="tab"><i class="icon-fa icon-fa-bar-chart text-success"></i>Hitos</a>
			                </li>
			            </ul>
			    	</div>
				</div>

				<div class="card-block">
				    <!-- Tab panes -->
				    <div class="tab-content">
				        <div class="tab-pane active" id="ranking" role="tabpanel">
				            <div class="embed-responsive embed-responsive-16by9 hs-responsive-embed-youtube">
	                        	<iframe height="auto" class="embed-responsive-item" src="{{$data->links->youtube}}"></iframe>
	                        </div>
				        </div>
				        <div class="tab-pane justificado" id="empresa" role="tabpanel">
				            {!!$data->about!!}
				        </div>
				        <div class="tab-pane" id="team" role="tabpanel">
							<div class="row content">
	                            @include('dashboard.user.icos.includes.team')                      
				        	</div>
				        </div>
				        <div class="tab-pane" id="hitos" role="tabpanel">
				        	@include('dashboard.user.icos.includes.hitos') 
				        </div>
				        <div class="tab-pane" id="misfavoritas" role="tabpanel">
				        </div>

				    </div>
				</div>
			</div>
		</div>

		<div class="col-sm-4 col-md-4">
			<div style="vertical-align: inherit;text-align:center;" class="dashbox">
				<div class="title">
					<h2 style="color:#68b541;font-weight: bold;" >{{$data->rating}}</h2>
					{{-- <a style="    width: 15px; height: 15px;  background: url(https://icobench.com/images/sprite.svg) no-repeat; background-size: 400px 400px; background-position: -49px -95px; display: inline-block; margin-bottom: 20px;"></a> --}}
				</div>

				<div class="row ico-rating">
					<div class="box col-sm-3 col-md-3">
						<div class="wrapper">
							{{$data->ratingProfile}}
							<span>BECH</span>
						</div>
					</div>
					<div class="box col-sm-9 col-md-9">
						<div class="row wrapper">
							<div class=" col-sm-4 col-md-4">
								{{$data->ratingTeam}}
								<span>EQUIPO</span>
							</div>
							<div class="col-sm-4 col-md-4">
								{{$data->ratingVision}}
								<span>VISIÓN</span>
							</div>
							<div class="col-sm-4 col-md-4">
								{{$data->ratingProduct}}
								<span>PRODUCTO</span>
			{{--  --}}				</div>
						</div>
						<span>Puntaje por Expertos</span>
					</div>
				</div>


			</div>
		    <div class="card">
		        <div class="card-header">Información General</div>
		        <div class="card-block">
		        	<div class="row">
		        		<div class="col-sm-3 col-md-3">
		        			<p>Token</p>
		        			<p>Precio</p>
		        			<p>Min</p>
		        			<p>Max</p>
		        			<p>platform</p>
		        			<p>Pais</p>
		        		</div>
		        		<div class="col-sm-9 col-md-9">
		        			<p><strong>: {{$data->finance->token}}</strong></p>
				           	<p><strong>: {{$data->finance->price}}</strong></p>
				           	<p><strong>: {{$data->finance->softcap}}</strong></p>
				           	<p><strong>: {{$data->finance->hardcap}}</strong></p>	           	
				           	<p><strong>: {{$data->finance->platform}}</strong></p>
				           	<p><strong>: {{$data->country}}</strong> </p>
		        		</div>
		        	</div>
		    
		           <div class="">
			           <a href="{{$data->links->whitepaper}}" target="_blank"  class="btn-view btn-primary">
			           	<i class="icon-fa icon-fa-file-pdf-o "></i> Ver Paper</a>
			           <a href="{{$data->links->www}}" target="_blank" class="btn-view btn-primary" >
			           	<i class="icon-fa icon-fa-internet-explorer "></i> Ingresar a Website</a>
		       	   </div>
		        </div>
		    </div>
		</div>

	</div>
</div>


@endsection

@section('scripts')

@endsection