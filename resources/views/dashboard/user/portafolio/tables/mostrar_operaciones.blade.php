<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Cantidad</th>
        <th>Fecha</th>
        <th>Precio</th>
        <th>Operacion</th>
        <th>Moneda</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($operaciones as $element)
          <tr>
            <td>{{$element->cantidad}}</td>
            <td>{{$element->fecha}}</td>
            <td>{{$element->precio}}</td>
            <td>
              @if ($element->operacion == 0)
              <span class="success">Hold</span>
              @else
              <span class="warning">Buy</span>
              @endif
            </td>
            <td>
              @if ($element->moneda == 0 )
              <span class="success">BTC</span>
              @else
              <span class="warning">USD</span>
              @endif
            </td>
          </tr>
      @endforeach
    </tbody>
  </table> 
</div>