<div class="table-responsive">
    <table class="table table-hover">
        <thead>
        <tr>                              
            <td>Acción</td>
            <td>Moneda</td>
            <td>Precio </br>AVG Buy</td>
            <td>Precio Actual</td>
            <td>Cantidad</td>                            
            <td>Hold</td>
            <td>Profit</td>
        </tr>
        </thead>
        <tbody>
            @php
                $ruta = 'http://coincap.io/images/coins';
            @endphp
            @foreach ($data as $item)
    
                <tr class="tr-moneda table-hover col-{{$item['id_moneda']}}">                    
                    <td>
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"data-target="#agregar-posicion" onclick="f_moneda_id({{$item['id_moneda']}})"><i class="icon-fa icon-fa-plus"></i></button>&nbsp;<button type="button" class="btn btn-success btn-sm" data-toggle="modal"data-target="#show-posicion" onclick="mostrar_operaciones({{$item['id_moneda']}})"><i class="icon-fa icon-fa-eye"></i></button>&nbsp;<button type="button" class="btn btn-danger btn-sm" onclick="eliminar_moneda({{$item['id_moneda']}})"><i class="icon-fa icon-fa-trash"></i></button>
                    </td>
                    <td><span class="label label-success">{!!$item['moneda_short']!!}</span></td>
                    <td>USD {{ number_format($item['precio_avg']['pre_base_usd'], 6, ',', ' ')}} <br>BTC {{ number_format($item['precio_avg']['pre_base_btc'], 6, ',', ' ')}}</td>
                    <td>USD {{ number_format($item['precio_actual']['precio_usd'], 6, ',', ' ')}} <br>BTC {{ number_format($item['precio_actual']['precio_btc'], 6, ',', ' ')}} </td>
                    <td>{!!$item['cantidad']!!}</td>
                    <td>{!!$item['hold']!!}</td>                             
                    <td>
                        @php
                            $p_compra = $item['precio_avg']['pre_base_usd'];
                            $p_venta = $item['precio_actual']['precio_usd'];
                            if ($p_compra == 0) {
                                $profit = 0;
                            }else{
                                $profit = (($p_venta-$p_compra)/$p_compra)*100;
                            }
                            
                        @endphp
                        @if ($profit < 0)
                            <label class="badge badge-danger">{{ number_format($profit, 2, ',', ' ')}} %</label>
                        @else
                            <label class="badge badge-success">{{ number_format($profit, 2, ',', ' ')}} %</label>
                        @endif
                    </td>                           
                                                 
                </tr>

            @endforeach
        </tbody>
    </table>
</div>
