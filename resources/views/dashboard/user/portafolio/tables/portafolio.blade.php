<div class="table-responsive">
    <table class="table table-hover">

        <tbody>
            @if($portafolio->count() > 0)
                @foreach ($portafolio as $port)
                    @if ($cant > 0)
                        <tr class="tr-portafolio table-{{ ($port->id == $data['portafolio_id']) ? 'success' : 'default' }} col-{{ $port->id }}">
                            <td class="cont_portafolio">
                                <a href="{{ url('dashboard/user/portafolio/mostrar') }}/{{ $port->id }}"><strong><i class="fa fa-check"></i> {{$port->nombre}}</strong> </a>
                                <i class="fa fa-trash btnportafoliodelete" onclick="eliminarPortafolio({{ $port->id }})"></i>
                            </td>                      
                        </tr>
                    @else
                        <tr class="table-default col-{{ $port->id }}">
                            <td class="cont_portafolio">
                                <a href="{{ url('dashboard/user/portafolio/mostrar') }}/{{ $port->id }}">{{$port->nombre}} </a>
                                <i class="fa fa-trash btnportafoliodelete" onclick="eliminarPortafolio({{ $port->id }})"></i>
                            </td>
                        </tr>
                    @endif
                @endforeach
            @else
                <div class="alert alert-warning">
                    <h2>Crea un portafolio primero!</h2>
                </div>
            @endif
        </tbody>
    </table>
</div>