@extends('dashboard.layouts.principal')
@section('styles')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<link href="{{ asset('/assets/admin/css/signals/easy-autocomplete.min.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.24.4/sweetalert2.min.css">
<style>
    .btnportafoliodelete{
        position: absolute;
        right: 10px;
        top: 19px;
    }

    .btnportafoliodelete:hover{
        color: red;
        cursor: pointer;
    }

    .cont_portafolio{
        position: relative;
    }
    .easy-autocomplete{
        width: 100% !important;
    }
</style>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.24.4/sweetalert2.min.js"></script>
    <script src="/assets/admin/js/coinmarket/common.js"></script>
    <script src="/assets/admin/js/coinmarket/ccc-streamer-utilities.js"></script>
    <script src="/assets/admin/js/signals/jquery.easy-autocomplete.min.js"></script>
    <script src="/assets/admin/js/signals/validate-create.js"></script>

        <script>

        //getMonedas();
        $(document).ready(function() {
            var arrNameCoin = [];
            var domain = 'https://www.cryptocompare.com';
            var queStringCropImg = '?anchor=center&mode=crop&width=32&height=32';
            var obj = {};
            var objSelect = {};
            $.ajax({
                url: "https://min-api.cryptocompare.com/data/all/coinlist",
                type: 'GET',
                dataType: 'json',
                async:false,
                success:function(data){
                    var datacoin = data.Data;
                    for(var i in datacoin){
                        obj = {
                            symbol: datacoin[i].Symbol,
                            coinname: datacoin[i].CoinName,
                            findname: datacoin[i].CoinName + ' | ' + datacoin[i].Symbol,
                            img32: domain + datacoin[i].ImageUrl + queStringCropImg,
                            imgori: domain + datacoin[i].ImageUrl,
                            sortOrder: parseInt(datacoin[i].SortOrder)
                        };
                        arrNameCoin.push(obj);
                        arrNameCoin.sort(compare);
                    }

                    function compare(a,b) {
                        if (a.sortOrder < b.sortOrder)
                            return -1;
                        if (a.sortOrder > b.sortOrder)
                            return 1;
                        return 0;
                    }
                    
                    var options = {
                        data: arrNameCoin,
                        getValue: function(element) {
                            objSelect = {
                                "symbol" : element.symbol,
                                "coinname" : element.coinname,
                                "findname" : element.findname,
                                "imgori" : element.imgori
                            }
                            var findname = element.findname;
                            return findname;
                        },
                        list: {
                            match: {
                                enabled: true
                            },
                            showAnimation: {
                                type: "slide"
                            },
                            onSelectItemEvent: function () {
                                var value = $("#coin").getSelectedItemData().symbol;
                                $("#coin").val(value).trigger("change");
                                //imgCoin.src=objSelect.imgori;
                                //coinname.value = objSelect.coinname;
                                //coinsymbol.value = objSelect.symbol;
                                //coinimgurl.value = objSelect.imgori;
                            },
                            onChooseEvent: function() {
                                //streaming(objSelect);
                            }
                        },
                        //                                        
                        placeholder: "Type Coin...",
                        template: {
                            type: "custom",
                            method: function (value, item) {
                                return "<img src=" + item.img32 + "/> " + value + "";
                            }
                        },
                        theme: "solid"
                    };
                    $("#coin").easyAutocomplete(options);
                },
                error:function(msj){

                }
            });
        });

        

        function formatState (state)
        {
          if (!state.id) { return state.text; }
          var $state = $(
            '<span> ' + state.text + '</span>'
          );
          return $state;
        };

      

        $(document).ready(function() {
            @if ($cant > 0)
                listar_moneda({{ $data['portafolio_id'] }});
            @endif


        });


        //listar
        //datatable
        function listar_moneda(data)
        {   
            $('#table_monedas_op').append('Cargando ...');
            var route = "{{url('dashboard/user/portafolio/listar_moneda')}}/"+data;
            //alert(route);
            $.get(route, function(value){
                $('#table_monedas_op').empty().html(value);
            });
        }

        function f_moneda_id(data)
        {
            $('#moneda_id').val(data);
        }

        //bloqueo de boton
        $('#coin').change(function(){
            //alert('hol');
            $('.btn_registrar_mon').removeAttr('disabled');
        });


        //agregar moneda
        $('#form_add_moneda').submit(function(e){
            e.preventDefault();        
            var form = $(this);
            var data = form.serialize();
            var token = $('#token').val();
            var route = "{{url('dashboard/user/portafolio/add_moneda')}}";     
     
            $.ajax({
                url: route,
                headers: { 'X-CSRF-TOKEN': token },
                type: 'POST',
                dataType: 'json',
                data: data,
                beforeSend: function() {
                    // setting a timeout
                    $('#agregar-moneda').modal('toggle');
                    $.blockUI({ message: '<h5><img src="http://malsup.com/jquery/block/busy.gif" /> Configurando Moneda...</h5>' });
                },
                success:function(data){
                    if (data.success == 'true') {                   
                        //$('.input').val('');
                        $.unblockUI();
                        $('.modal-backdrop').remove();
                       listar_moneda(data.moneda.portafolio_id);
                    }
                },

                error:function(msj){  
                    $.unblockUI();       
                    $('.msj_moneda').empty().append('Debes ingresar una moneda');              
                }
            }); 
        });

        //operaciones por moneda
        $('#form_add_operacion').submit(function(e){
            e.preventDefault();
            var form = $(this);
            var data = form.serialize();
            var token = $('#token').val();
            var route = "{{url('dashboard/user/portafolio/add_operacion')}}";     
     
            $.ajax({
                url: route,
                headers: { 'X-CSRF-TOKEN': token },
                type: 'POST',
                dataType: 'json',
                data: data,
                beforeSend: function() {
                    // setting a timeout
                    $('#agregar-posicion').modal('toggle');
                    $.blockUI({ message: '<h5><img src="http://malsup.com/jquery/block/busy.gif" /> Configurando Operación...</h5>' });
                },
                success:function(data){
                    if (data.success == 'true') {                   
                        //$('.input').val('');
                        $.unblockUI();
                        $('.modal-backdrop').remove();
                       //$('#app_monedas_operaciones').dataTable().fnDestroy();
                      // listar_moneda(data.moneda.portafolio_id);
                    }
                },

                error:function(msj){ 
                $.unblockUI();        
                    console.log('no se pudo jejej');              
                }
            }); 
        });

        //mostrar las operaciones
        function mostrar_operaciones(data)
        {
            $('#mostrar_operaciones').empty().append('Cargando...');
            var route = "{{url('dashboard/user/portafolio/show_operacion')}}/"+data;
            //alert(route);
            $.get(route, function(value){
                
                $('#mostrar_operaciones').empty().html(value);
            });
        }

        //eliminar portafolio
        function eliminarPortafolio(id)
        {
            var token = $('#token').val();
            var route = "{{ route('user.portafolio.eliminar') }}";
            const swalWithBootstrapButtons = swal.mixin({
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-danger',
              buttonsStyling: false,
            })

            swalWithBootstrapButtons({
              title: 'Estas seguro de eliminar?',
              text: "No podras ver nuevamente este portafolio",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Si, Eliminar',
              cancelButtonText: 'No, cancelar!',
              reverseButtons: true
            }).then((result) => {
              if (result.value) {
                $.ajax({
                    url: route,
                    headers: { 'X-CSRF-TOKEN': token },
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                    beforeSend: function() {
                        // setting a timeout
                        $.blockUI({ message: '<h5><img src="http://malsup.com/jquery/block/busy.gif" /> Eliminando...</h5>' });
                    },
                    success:function(data){
                        $.unblockUI();
                        $('.col-'+data.port.id).remove();
                        swalWithBootstrapButtons(
                          'Eliminado!',
                          'Se ha eliminado con éxito!',
                          'success'
                        )
                    },

                    error:function(msj){ 
                        $.unblockUI();        
                        console.log('no se pudo eliminar, contacte al desarrollador');              
                    }
                });
                
              } else if (
                // Read more about handling dismissals
                result.dismiss === swal.DismissReason.cancel
              ) {
                swalWithBootstrapButtons(
                  'Cancelado',
                  'Ha cancelado la eliminación :)',
                  'error'
                )
              }
            })
             
        }

        //eliminar moneda
        function eliminar_moneda(id)
        {
            var token = $('#token').val();
            var route = "{{ route('user.moneda.eliminar') }}";
            const swalWithBootstrapButtons = swal.mixin({
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-danger',
              buttonsStyling: false,
            })

            swalWithBootstrapButtons({
              title: 'Estas seguro de eliminar?',
              text: "No podras ver nuevamente esta Moneda",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Si, Eliminar',
              cancelButtonText: 'No, cancelar!',
              reverseButtons: true
            }).then((result) => {
              if (result.value) {
                $.ajax({
                    url: route,
                    headers: { 'X-CSRF-TOKEN': token },
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                    beforeSend: function() {
                        // setting a timeout
                        $.blockUI({ message: '<h5><img src="http://malsup.com/jquery/block/busy.gif" /> Eliminando...</h5>' });
                    },
                    success:function(data){
                        $.unblockUI();
                        $('.col-'+data.moneda.id).remove();
                        swalWithBootstrapButtons(
                          'Eliminado!',
                          'Se ha eliminado con éxito!',
                          'success'
                        )
                    },

                    error:function(msj){ 
                        $.unblockUI();        
                        console.log('no se pudo eliminar, contacte al desarrollador');              
                    }
                });
                
              } else if (
                // Read more about handling dismissals
                result.dismiss === swal.DismissReason.cancel
              ) {
                swalWithBootstrapButtons(
                  'Cancelado',
                  'Ha cancelado la eliminación :)',
                  'error'
                )
              }
            })
             
        }
    </script>
@endsection

@section('content')
    <div class="main-content page-portafolio">
        @include('flash::message')
        <div class="page-header"></div>
   
    
        

        <section class="row">
            <div class="col-sm-3">
                <div class="card card-border">
                    <div class="card-block">
                        @include('dashboard.user.portafolio.tables.portafolio')
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".ls-example-modal-sm" style="width: 100%;">Agregar Portafolio</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">

                <div  class="row ">
                    
                </div> 
            
                
                <div class="row">
                    <div class="col-xl-12 col-sm-12">
                      <div style="background-color:  #e7eef4;" class="card">
                        <div class="card-block row">     
                                   <div class="col-md-12 col-lg-6">
                                        <a style="width: 100%;padding: 5px;margin-bottom: 2px;" class="dashbox dashbox-circle-progress">                        
                                        <span class="title text-primary">{{ number_format($total_moneda['usd_total'], 6, ',', ' ')}}</span><span class="desc">USD</span>
                                        </a>
                                   </div> 

                                    <div class="col-md-12 col-lg-6">
                                        <a style="width: 100%;padding: 5px; margin-bottom: 2px;"  href="#" class="dashbox dashbox-circle-progress">
                                             <span class="title text-success">{{ number_format($total_moneda['btc_total'], 6, ',', ' ')}}</span>
                                             <span class="desc">BTC</span>
                                        </a>
                                    </div>
                        </div>
                      </div>
                    </div>
                        @if ($cant > 0)
                        <div class="col-sm-12">
                            <div class="card card_monedas">
                                <div class="card-header">
                                    <h3>Mis Monedas</h3>
                                    <div class="actions">
                                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" 
                                            data-target="#agregar-moneda"><i class="icon-fa icon-fa-plus"></i> Agregar Moneda
                                        </button>
                                        
                                    </div>
                                </div>
                                <div class="card-block">
                                   <div id="table_monedas_op">
                                       
                                   </div>

                                </div>
                            </div>
                            <div class="alert alert-warning msg_moneda" style="display: none;">
                                <h4>Selecciona o Crea un portafolio</h4>
                            </div>
                        </div>
                        @endif
                </div>
                
            </div>
        </section>

    

    </div>

    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

    @include('dashboard.user.portafolio.modal.add_portafolio')
    @include('dashboard.user.portafolio.modal.add_moneda')
    @include('dashboard.user.portafolio.modal.add_posicion')
    @include('dashboard.user.portafolio.modal.show_posicion')
@endsection