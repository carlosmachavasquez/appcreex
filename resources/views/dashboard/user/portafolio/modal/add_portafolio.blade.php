<div class="modal fade ls-example-modal-sm" tabindex="-1" role="dialog"
                                         aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nuevo Portafolio</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{url('dashboard/user/portafolio/registrar')}}">
                {{ csrf_field() }}
                <div class="modal-body">                                                    
                    <div class="form-group">
                        <label for="nameInput" class="control-label">Nombre</label>
                        <input type="text" class="form-control" placeholder="ejemplo: Bitrex - Jose" name="nombre" required>
                    </div>                                                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Cerrar
                    </button>
                    <button type="submit" class="btn btn-primary">Crear Portafolio</button>
                </div>
            </form>
        </div>
    </div>
</div>