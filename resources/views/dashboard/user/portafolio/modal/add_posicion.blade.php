<div class="modal fade" id="agregar-posicion"  role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Posición</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form_add_operacion1" action="{{ route('user.portafolio.agregar.operacion') }}" method="POST">
                {{csrf_field()}}
                <input type="hidden" id="moneda_id" name="moneda_id" required>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nameInput" class="control-label">Fecha</label>
                        <input type="date" name="fecha" class="form-control" required>
                    </div> 
                    <div class="form-group">
                        <label for="nameInput" class="control-label">Operación</label>
                        <select name="operacion" class="form-control" required>
                            <option value="0">Hold</option>
                            <option value="1">Sell</option>
                        </select>
                    </div>  
                    <div class="form-group">
                        <label for="nameInput" class="control-label">Cantidad</label>
                        <input type="text" class="form-control" name="cantidad" required>
                    </div> 
                    <div class="form-group">
                        <label for="nameInput" class="control-label">Costo / und.</label>
                        <input type="text" class="form-control" name="precio" required>
                        <span style="color: red" class="msj_posicion_costo">Ingresa un monto válido * (puede ser decimal)</span>
                    </div>

                    <div class="form-group">
                        <label for="nameInput" class="control-label">Moneda</label>
                        <select class="form-control" name="moneda" required>
                            <option value="0">BTC</option>
                            <option value="1">USD</option>
                        </select>
                    </div>                 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary">Registrar</button>
                </div>
            </form>
        </div>
    </div>
</div>