<h4 class="mt-0 header">Actualizar contraseña</h4>
@if ($errors->any())
    <div class="alert alert-danger">
        <h6>Por favor corrige los errores debajo:</h6>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{ route('user.settingsuser.updateprofilepassword') }}" id="updateSettingsPasswordForm" method="post">
    {{ csrf_field() }}
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Password actual</label>
        <div class="col-sm-6">
            <input id="currentpassword" type="password" class="form-control form-control-danger" name="currentpassword" required>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Password nuevo</label>
        <div class="col-sm-6">
            <input id="newpassword" type="password" class="form-control form-control-danger" name="newpassword" required>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Re-Password nuevo</label>
        <div class="col-sm-6">
            <input id="newpasswordconfirm" type="password" class="form-control form-control-danger" name="newpasswordconfirm" required>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label"></label>
        <div class="col-sm-6">   
            <button type="submit" class="btn btn-primary btn-lg btn-block">Actualizar Password</button>
        </div>
    </div>
</form>