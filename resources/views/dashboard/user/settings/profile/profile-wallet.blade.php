<h4 class="mt-0 header">Importante:</h4>
<p class="text-muted m-b-30 font-14">
    <b>Este monedero es de Bitcoin (BTC)</b>,Grupo Creex no se responsabiliza de envios fallidos por usted colocar datos erroneos.</p>
@if ($errors->any())
    <div class="alert alert-danger">
        <h6>Por favor corrige los errores debajo:</h6>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{ route('user.settingsuser.updateprofilewallet') }}" id="updateSettingsWalletForm" method="post">
    @csrf
    @method('PUT')
    <div class="form-group row">
        <label for="example-text-input" class="col-lg-3 col-form-label">Bitcoin:</label>
        <div class="col-lg-6">
            <input class="form-control" id="wallet" name="wallet" placeholder="" value="{{$user->wallet}}" type="text">
        </div>
        <div class="col-lg-2">
            <button type="submit" class="btn btn-primary waves-effect waves-light">Agregar Wallet</button>
        </div>
    </div>
</form>
