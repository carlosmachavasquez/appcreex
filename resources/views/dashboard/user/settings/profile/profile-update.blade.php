<h4 class="mt-0 header"> Importante:</h4>
<p class="text-muted m-b-30 font-14">
     Por favor coloque sus datos reales, recuerde que sus referidos directos podran ver esta informacion, excepto su correo.
 </p>
@if ($errors->any())
    <div class="alert alert-danger">
        <h6>Por favor corrige los errores debajo:</h6>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{ route('user.settingsuser.updateprofile') }}" id="updateSettingsForm" method="post">
    @csrf
    @method('PUT')
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Nombre</label>
        <div class="col-sm-6">
            <input id="name" name="name" class="form-control form-control-danger" value="{{$user->name}}" type="text">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Apellido</label>
        <div class="col-sm-6">
            <input id="lastname" name="lastname" class="form-control form-control-danger" value="{{$user->lastname}}" type="text">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-6">
            <input class="form-control" value="{{$user->email}}" readonly="" data-toggle="tooltip" data-placement="right" title="" data-original-title="Por su seguridad, su correo no puede ser editado. Para hacerlo usted debe comunicarse con soporte en support@appcreex.com" type="email">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Celular</label>
        <div class="col-sm-6">
            <input id="cellphone" name="cellphone" class="form-control form-control-danger" value="{{$user->cellphone}}" data-mask="999 999 999" type="text">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label"></label>
        <div class="col-sm-6">   
            <button type="submit" class="btn btn-primary btn-lg btn-block">Editar Datos Personales</button>
        </div>
    </div>
</form>