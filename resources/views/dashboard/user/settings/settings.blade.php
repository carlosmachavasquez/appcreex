@extends('dashboard.layouts.principal')

@section('scripts')
    <script src="/assets/admin/js/users/users.js"></script>
    <script src="/assets/admin/js/sessions/updateSettings.js"></script>
    <script src="/assets/admin/js/sessions/updatePasswordSettings.js"></script>
    <script src="/assets/admin/js/sessions/updateSettingsWallet.js"></script>
    <script src="/assets/admin/js/sessions/tabsandroutes.js"></script>
@stop

@section('content')
    <div class="main-content page-profile">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">Configuración del perfil de usuario</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Usuario</a></li>
                <li class="breadcrumb-item active">{{$user->name}}</li>
            </ol>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <div class="tabs tabs-default">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab">Datos personales</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="changepassword-tab" data-toggle="tab" href="#changepassword" role="tab">Contraseña</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="wallet-tab" data-toggle="tab" href="#wallet" role="tab">Wallet</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="profile" role="tabpanel">
                                    @include('dashboard.user.settings.profile.profile-update')
                                </div>
                                <div class="tab-pane" id="changepassword" role="tabpanel">
                                    @include('dashboard.user.settings.profile.profile-update-password')
                                </div>
                                <div class="tab-pane" id="wallet" role="tabpanel">
                                    @include('dashboard.user.settings.profile.profile-wallet')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
