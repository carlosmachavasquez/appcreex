<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block card-block-price">
                <div class="row">
					<div class="col-md-2 col-xs-3">
						<div class="pt-header pt-header-bronze">
							<h3 class="pt-price"><strong>A</strong></h3>
						</div>
					</div>
					<div class="col-md-7 col-xs-9">
						<h2 class="pt-name">MAP {{ $membresia->name }}</h2>
						<div class="pt-tag">Activación mensual por herramientas: US$ 50.00</div>
                        {{-- <div class="pt-tag">Activación Mensual: {{ $membresia->precio_mes }}&nbsp;US$/año</div> --}}
					</div>
					<div class="col-md-3 col-xs-12">
						<div class="">
							<h3 class="pt-name text-yellow">{{ $membresia->precio_anual }}</h3>
							<div class="pt-tag">US$/Semestral</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 col-xl-12 mt-2">
    	<div class="card with-tabs">
    		<div class="card-header">
    			<div class="caption">
    				<h6>1. Selecciona su medio de pago:
    				</h6>
    			</div>
                {{--
    			<div class="actions tabs-simple">
    				<ul role="tablist" class="nav nav-tabs">
    					<li class="nav-item">
    						<a data-toggle="tab" href="#balanceSummry" role="tab" class="nav-link active" aria-expanded="true">BitCoins ( BTC )</a>
    					</li>
    					<li class="nav-item">
    						<a data-toggle="tab" href="#monthlyProfit" role="tab" class="nav-link" aria-expanded="false">Tarjeta </a>
    					</li>		                    					
    				</ul>
    			</div>
    			--}}
    		</div>
    		<div class="row">
                <div class="col-lg-6 col-md-6 col-xs-6 text-center">
                    <a href="/dashboard/invitado/payments/buy/{{ $membresia->id }}" class="btn btn-bitcoin"></a>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6 text-center">
                    <div class="graph-container">
                        <button onclick="config_culqi('{{ $membresia->name }}', {{ $membresia->precio_anual }});" class="btn btn-success btn-pago-normal">Pagar con tarjeta de crédito/débito</button>
                    </div>
                </div>
    		</div>
    	</div>
    </div>

    <div class="col-sm-12">
    	<p class=""> IMPORTANTE:. </p>
        <div class="card">
            <div class="card-block">
                <p class="card-text">
                     1. Es muy importante que verifique su pago antes de ser enviado a la billetera indicada. 
                    <br><br>
                    2. Asegurese de enviar el monto EXACTO a la dirección de destino, considere las comisiones de envio que son adicionales.
                    <br><br>
                    3. Algunos Wallets o Exchanges envian cantidades menores, asegurese de enviar la cantidad correcta. No podremos devolver pagos incompletos
                </p>
            </div>
        </div>
    </div>		                  
</div>
