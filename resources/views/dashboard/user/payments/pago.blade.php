@extends('dashboard.layouts.principal')

@section('styles')
	<link href="{{ asset('/assets/admin/css/payments/pago.css') }}" rel="stylesheet" type="text/css">
@stop

@section('scripts')
    <script>
        setTimeout(function(){
            var wasPay = document.getElementById('wasPay');
            var valWasPay = parseInt(wasPay.innerHTML);
            if(valWasPay === 1){
                window.location.href = '/dashboard/user';
            }

        }, 10000);

        // $('.cargo-generado-modal').on('shown.bs.modal', function (e) {
        //     console.log("ABRIOO");
        // });


        function getCharges(membresia_id){
            console.log("ENTRO AL GETCHARGES");
            console.log(membresia_id);

            $.ajax({
                type: 'GET',
                url: '/dashboard/invitado/payments/charges/' + membresia_id,
                success: function(resp) {
                    console.log(resp);
                    var lastlink = document.getElementById('lastlink');
                    var receipt = document.getElementById('receipt');

                    lastlink.href = resp.data.hosted_url;
                    lastlink.innerHTML = resp.data.hosted_url;
                    receipt.href = 'https://commerce.coinbase.com/receipts/' + resp.data.code;
                    receipt.innerHTML = 'https://commerce.coinbase.com/receipts/' + resp.data.code;
                    $('.cargo-generado-modal').modal('hide');
                    toastr.success('Se generó el cargo con éxito.');
                    window.open(resp.data.hosted_url, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
                },
                error: function(resp) {
                    console.log(resp);
                    console.log("sucedio algun error");
                }
            });
        }
    </script>
@stop

@section('content')
	<div class="main-content page-pago-membresia">
		@include('flash::message')
		<div class="page-header">
			<h3 class="page-title">Pago de membresia M.A.P. {{$name}}</h3>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{route('invitado.dashboard')}}">Invitado</a></li>
				<li class="breadcrumb-item"><a href="{{route('invitado.dashboard')}}">Payments</a></li>
				<li class="breadcrumb-item active">Pago</li>
			</ol>
		</div>
		@if ($message = Session::get('success'))
			<div class="alert alert-success">
				<p>{{ $message }}</p>
			</div>
		@endif
        <div id="wasPay" style="display: none;">
            {{$wasPay}}
        </div>
        <div class="nota bg-success">
            Nota importante: {{$infopay}}
        </div>
        <div class="table-responsive">
            <table id="tabla-pago" class="table table-hover tabla-pago-container">
                <thead class="thead-default thead-pago">
                <tr>
                    <th>Descripción</th>
                    <th>Valor</th>
                    <th>Pagar</th>
                    <th>Ultimo Link Activo</th>
                    <th>Recibo virtual</th>
                    <th>Estatus</th>
                    <th>Activo Hasta</th>
                </tr>
                </thead>
                <tbody class="tbody-pago">
                @foreach ($data as $i => $pago)
                    @php
                        $tipodepago = $pago['tipo_membresia'] == 0 ? 'Mensual' : 'Semestral';
                    @endphp
                    <tr>
                        <td>{{(isset($tipodepago)) ? $tipodepago : ''}}</td>
                        <td>{{'$'.((isset($pago['monto'])) ? $pago['monto'] : '')}}</td>
                        <td class="align-right">
                            @if(((isset($pago['estado'])) ? $pago['estado'] : '')==0)
                                <button class="btn btn-success waves-effect waves-light align-right" data-toggle="modal" data-target=".cargo-generado-modal">Pagar Ahora</button>
                            @endif
                        </td>
                        <td>
                            <a id="lastlink" href="https://commerce.coinbase.com/charges/{{((isset($pago['chargeId'])) ? $pago['chargeId'] : '')}}" target="_blank">https://commerce.coinbase.com/charges/{{((isset($pago['chargeId'])) ? $pago['chargeId'] : '')}}</a>
                        </td>
                        <td>
                            <a id="receipt" href="https://commerce.coinbase.com/receipts/{{((isset($pago['chargeId'])) ? $pago['chargeId'] : '')}}" target="_blank">https://commerce.coinbase.com/receipts/{{((isset($pago['chargeId'])) ? $pago['chargeId'] : '')}}</a>
                        </td>
                        <td>
                            <h4 class="badge {{((isset($pago['class'])) ? $pago['class'] : '')}} font-12"><b>{{((isset($pago['estado_text'])) ? $pago['estado_text'] : '')}}</b></h4>
                        </td>
                        <td class="align-right">{{((isset($pago['fecha_activo_hasta'])) ? $pago['fecha_activo_hasta'] : '')}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="backtoplans">
            <a href="{{route('invitado.dashboard')}}">Regresar a planes <i class="icon-fa icon-fa-undo" aria-hidden="true"></i></a>
        </div>
	</div>
    @include('dashboard.user.payments.modal.generarcargo')
@endsection


