@extends('dashboard.layouts.principal')
@section('content')
<div  id="dashboardPage" class="main-content page-portafolio">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">Pagos</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="">Pagos</a></li>
            </ol>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <div class="tabs tabs-default">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="mispagos-tab" data-toggle="tab" href="#primernivel" role="tab">Mis Compras</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="bonos-tab" data-toggle="tab" href="#segundonivel" role="tab">Bono</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="primernivel" role="tabpanel">
                                   {{-- @include('dashboard.user.referrals.usersnivel.users-primernivel') --}}
                                </div>
                                <div class="tab-pane" id="segundonivel" role="tabpanel">
                                    {{-- @include('dashboard.user.referrals.usersnivel.users-segundonivel') --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>




@endsection

