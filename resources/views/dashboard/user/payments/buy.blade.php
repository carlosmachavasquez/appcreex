@extends('dashboard.layouts.principal')

@section('styles')
    <link href="{{ asset('/assets/admin/css/payments/buy.css') }}" rel="stylesheet" type="text/css">
@stop

@section('content')
    <input type="hidden" value="{{ csrf_token() }}" id="token">
    <input type="hidden"  id="id_mem">
    <div id="dashboardPage" class="main-content page-portafolio">
        @include('flash::message')
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card"  id="content_pagos">
                    <div class="card-header page-header">
                        <h3 class="page-title">Elige el MAP más adecuado para ti: {{ Auth::user()->nickname }}</h3>
                        <div class="notificaciones" style="display: none;">
                            <div class="alert alert-success ">
                                <p class="notificacion_compra"></p>
                            </div>
                        </div>
                    </div>
                 
                    @if(Session::has('success_msg'))
                    <div class="alert alert-success">
                        <strong>Success!</strong> {{Session::get('success_msg')}}
                    </div>
                    @endif

                    @if(Session::has('error_msg'))
                    <div class="alert alert-danger">
                        <strong>Error!</strong> {{Session::get('error_msg')}}
                    </div>
                    @endif
                   

                    <div class="card-block">
                        <div class="row">
                            @foreach ($membresia as $data)
                                <div class="col-lg-3 col-sm-3 col-xs-6">
                                    <div class="card">
                                        <div class="card-header header-table-buy bg-dark"></div>
                                        <div class="card-block">
                                            <h3 class="BuyPlan__content__title">{{ $data->name }}</h3>
                                            {{-- <p class=""><strong>{{ $data->precio_mes }}&nbsp;US$/mes</strong><br>	          --}}
                                            
                                            @if( $data->name == "Inversionista VIP $")
                                                    <p class="">Facturado Anual</p>
                                            @else
                                                <p class="">Facturado Semestral</p>
                                            @endif
                                            
                                            <div>
                                                <hr class="BuyPlan__content__divider">
                                                <p class="BuyPlan__content__annual-price" >
                                                {{ $data->precio_anual }}&nbsp;US$
                                            </p>
                                            </div>
                                            <div class="container-adquirir-membresia">
                                              <button  class="btn btn-primary" data-toggle="modal" data-target=".mostrar_map" onclick="mostrar_map({{ $data->id }})">Adquirir membresía</button>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="detalle-membresia">
                                        @php
                                            $items = DB::table('detalle_membresia')->where('membresia_id', '=', $data->id)->get();
                                        @endphp
                                        <ul class="">
                                            @foreach ($items as $element)
                                                <li> {{ $element->item }}</li>
                                            @endforeach

                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                {{--
                <div class="panel panel-default" style="padding: 4em; text-align: center;" id="content_pagado">
                    <div class="row panel-body">
                        <div class="col-md-12">
                            <h2>Ahora puedes ingresar a tu panel</h2>
                        </div>
                        <div class="col-md-12">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus laboriosam harum omnis temporibus quisquam, iure, ullam deleniti, sunt quos labore beatae dicta sequi fugiat ea. Ipsa, quo incidunt. Rem, error.</p>
                        </div>
                        <div class="col-md-12">
                            <a href="{{ url('dashboard/user') }}" class="btn btn-success">Ingresar al panel </a>
                        </div>
                    </div>
                </div>
                 --}}
            </div>
            @include('dashboard.user.payments.modal.adquirir')
        </div>
    </div>

<style>
	.header-table-buy {
		padding: 0.25rem 0.75rem!important;
		background: #2d3a49;
	}
</style>

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js"></script>
<script>
	function mostrar_map(data)
	{
		var route = "{{url('dashboard/invitado/payments/membresia/show')}}/"+data;
        $('#mostrar_map').empty().append('Cargando ...');
        $.get(route, function(value){
            $('#mostrar_map').html(value);
            $('#id_mem').val(data);
        });
	}
</script>

<script src="https://checkout.culqi.com/v2"></script>
<script>

   function culqi() {
      if (Culqi.token) {
            $('.mostrar_map').modal('toggle');$('.modal-backdrop').remove();
                $.blockUI({ message: '<h5><img src="{{ asset('dashboard/busy.gif') }}" /> Se esta haciendo la Transacción...</h5>' });
                var token = $('#token').val();
                var id = $('#id_mem').val();
                $.ajax({
                    type: 'POST',
                    url: '{{url('dashboard/invitado/payments/membresia/pago')}}',
                    data: { token: Culqi.token.id, _token : token, id: id},
                    datatype: 'json',
                    success: function(data) {
                        var result = "";
                        if(data.constructor == String){
                            result = JSON.parse(data);
                        }
                        if(data.constructor == Object){
                            result = JSON.parse(JSON.stringify(data));
                        }
                        if(result.object === 'charge'){
                            $('.mostrar_map').modal('toggle');$('.modal-backdrop').remove();
                            $('#content_pagos').hide();
                            $('#content_pagado').show();
                            $.unblockUI();
                            toastr.success('Se ha realizado el pago con exito');
                            location.reload();
                        }
                        if (data.success == 'false_api') {
                            $.unblockUI();
                            toastr.warning(data.msj); 
                        }

                        if (data.success == 'false_save_api_user') {
                            $.unblockUI();
                            toastr.warning(data.msj); 
                        }

                        if(result.object === 'error'){  
                            $.unblockUI();             
                            console.log('no se pudo hacer el pago, contacte con el desarrollador..');
                        }
                    },
                error: function(error) {
                    $.unblockUI();
                    toastr.warning('Ocurrió un error Intente de nuevo!');
                }
            });
        } 
    };

    Culqi.publicKey = 'pk_live_muEyWqcrT2tUSxZU';//live
    //Culqi.publicKey = 'pk_test_gYPHAqlaYeO5puoz';
    function config_culqi(titulo, monto)
    {
    	Culqi.settings({
	      title: titulo,
	      currency: 'USD',
	      description: 'Membresia',
	      amount: ((monto*0.1) + parseFloat(monto) ) * 100
	     });
    	Culqi.open();
        
    }
    //Culqi.open();
</script>
@endsection

