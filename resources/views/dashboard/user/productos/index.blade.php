@extends('dashboard.layouts.principal')
@section('styles')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<link href="{{ asset('/assets/admin/css/signals/easy-autocomplete.min.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.24.4/sweetalert2.min.css">

<style>
    .app_producto_cont{
        margin: 10px 10px;
        padding: 10px;
        text-align: center;
        box-shadow: 0px 0px 8px #000;
    }

    .app_producto_cont div{

    }
</style>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js"></script>
  <script src="https://checkout.culqi.com/v2"></script>
<script>

   function culqi() {
      if (Culqi.token) {
                
                var token = $('meta[name="csrf-token"]').attr('content');
                var id = $('#id_prod').val();
                $.ajax({
                    type: 'POST',
                    url: '{{route('user.productos.pagar')}}',
                    data: { token: Culqi.token.id, _token : token, id: id},
                    datatype: 'json',
                    success: function(data) {
                        var result = "";
                        if(data.constructor == String){
                            result = JSON.parse(data);
                        }
                        if(data.constructor == Object){
                            result = JSON.parse(JSON.stringify(data));
                        }
                        if(result.object === 'charge'){
                            
                            $.unblockUI();
                            toastr.success('Se ha realizado el pago con exito');
                        }
                       

                        if(result.object === 'error'){  
                            $.unblockUI();             
                            console.log('no se pudo hacer el pago, contacte con el desarrollador..');
                        }
                    },
                error: function(error) {
                    $.unblockUI();
                    toastr.warning('Ocurrió un error Intente de nuevo!');
                }
            });
        } 
    };

    //Culqi.publicKey = 'pk_live_muEyWqcrT2tUSxZU';//live
    Culqi.publicKey = 'pk_test_gYPHAqlaYeO5puoz';
    function config_culqi(titulo, monto)
    {
        Culqi.settings({
          title: titulo,
          currency: 'USD',
          description: 'Producto',
          amount: ((monto*0.1) + parseFloat(monto) ) * 100
         });
        Culqi.open();
        
    }
    //Culqi.open();
</script>  

       
@endsection

@section('content')
    <div class="main-content page-portafolio">
        @include('flash::message')
        <div class="page-header">
            <h2 class="text-center">PRODUCTOS</h2>
        </div>
   
        <div class="container">
            <div class="row">
                
                @foreach($productos as $data)
                @php
                    $membresia = explode(',',$data->membresia);
                @endphp
                @for ($i = 0; $i < count($membresia); $i++)
                    
                @if (Auth::user()->membresia_id == $membresia[$i])
                        <div class="col-md-4">
                            <div class="app_producto_cont">
                                <div class="imagen_prod">
                                    <img src="{{ url('uploads/productos/220x220') }}/{{$data->imagen}}" alt="{{$data->nombre}}">
                                </div>
                                <div class="resumen">
                                    <p>{{$data->resumen}}</p>
                                    <input type="hidden" id="id_prod">
                                    <button class="btn btn-primary" onclick="config_culqi('{{$data->nombre}}', {{$data->precio}});">Comprar</button>
                                </div>
                            </div>
                        </div>
                    @endif
                @endfor
                @endforeach
            </div>
        </div>
    

    </div>

@endsection