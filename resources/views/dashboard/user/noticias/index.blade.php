@extends('dashboard.layouts.principal')
@section('styles')
<style>
    .app_idioma{
        cursor: pointer;
    }
</style>
@endsection
@section('content')

<div style="background: linear-gradient(90deg,#1c3643,#1e5372);">
        <div class="cabecera-interna-content row " style="padding-top: 125px; max-width: 980px; margin: 0 auto;color: white;">
            <div class="cabecera-content-left col-xl-9 col-sm-9 col-sm-12">
                <div class="cabecera-content-holder" style="padding-top: 20px; color: white;">
                    <h1 style="color: white; "><strong>Análisis funcional </strong></h1>
                    <p>En esta sección podrás encontrar las noticias de los principales portales del mundo que te permitirán estar actualizado.</p>
                </div>
            </div>
            <div class="cabecera-content-right col-xl-3 col-sm-3 col-sm-12">
                <img width="250" height="250" src="https://static.platzi.com/static/images/Forum/foro_ilustracion.0bc6d9bb569e.png" alt="">
            </div>
        </div>        
</div>

<div class="main-content page-portafolio" style="padding: 30px!important;">
    
        @include('flash::message')
        <div class="page-header">
            {{-- <h3 class="page-title">Noticias</h3> --}}
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Home</a></li>
                <li class="breadcrumb-item active">Noticias</li>
            </ol>
        </div>
        <div class="col-sm-12">
   
            {{-- <form action="{{route('user.noticias.enviar_correo')}}" method="POST">
                @csrf
                <input type="form-control" name="mensaje">
                <button class="btn btn-success">Enviar</button>
            </form> --}}
            <div class="row">
                <div class="col-sm-3">
                    <div class="card">
                        <div class="card-header">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Seleccione el tema:</label>
                                <select id="exampleFormControlSelect1" class="form-control">
                                    <option value="cryptomonedas">CryptoMonedas</option>
                                    <option value="forex">Forex</option>
                                </select>
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="list_tipo">
                                @foreach ($noticias as $data)
                                    <div class="noticia-item">
                                        <a class="nav-link active" data-toggle="tab" href="#home5" role="tab" onclick="mostrar_datos({{$data->id}})">{{$data->titulo}}</a>
                                    </div>
                                @endforeach
                            </div>
                        
                        </div>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="card with-tabs">
                        <div class="tab-content" id="get_noticias"></div>
                    </div>
                </div>
            </div>
         </div>
</div>




@endsection

@section('scripts')

<script>
    $(document).ready(function(){
        mostrar_datos(1);
    });

    function mostrar_datos(data)
    {
        var route = "{{url('dashboard/user/noticias/mostrar')}}/"+data;
        $('#get_noticias').empty().append('Cargando ...');
        $.get(route, function(value){
            $('#get_noticias').html(value);
            
        });
    }


    function noticia_lang(lang)
    {
        $('.app_idioma').removeClass('active');
        $('.lang_'+lang).addClass('active');
        if (lang == 'es') {
            $('#noticias-espanol').show();
            $('#noticias-ingles').hide();
        }else{
            $('#noticias-ingles').show();
            $('#noticias-espanol').hide();
        }

    }



    $('#exampleFormControlSelect1').change(function(e){
        var tipo = $(this).val();

        var route = "{{url('dashboard/user/noticias/tipo')}}/"+tipo;
        $('.list_tipo').empty().append('Cargando ...');
        $.get(route, function(value){
            $('.list_tipo').html(value);
            
        });
    });


</script>



@endsection