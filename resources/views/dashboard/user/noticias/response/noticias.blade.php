{{-- inicio --}}
<div>
  <article>
    <div class="row" style="padding: 20px;">
      <div class="col-xs-2 col-sm-2 center-xs">
        <span style="width: 124px;height: 124px;border-radius: 4px;">
          <img style="border-radius: 4px;" width="124" height="124" src="https://i0.wp.com/www.criptonoticias.com/wp-content/uploads/2018/08/china-comunista-funcionario-blockchain-partido.jpg?resize=1008%2C640&ssl=1" alt="">
        </span>
      </div>
      <div class="col-xs-10 col-sm-10 center-xs">
        <a href="">
          <span><time style="font-size: 12px; color:#a9aaad;">14 August 2018</time></span>
          <h5 style="margin-bottom: 5px;">CHINA BUSCA ALFABETIZAR SOBRE BLOCKCHAIN A SUS FUNCIONARIOS PÚBLICOS</h5>
          <p>Cointelegraph lo cubre todo: fintech, blockchain y Bitcoin, trayéndote las últimas noticias, precios, descubrimientos y análisis del futuro del dinero.</p>

        </a>        
      </div>
    </div>
  </article>
</div>
{{-- fin --}}

<div class="">
  <div class="card-header">
      <div class="actions tabs-simple">
          <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item">
                  <a class="nav-link app_idioma lang_es" onclick="noticia_lang('es')">Español</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link app_idioma lang_en"  onclick="noticia_lang('en')">Ingles</a>
              </li>
          </ul>
      </div>
  </div>

  <div class="card-block">
    <div class="tab-pane noticias active" id="noticias-espanol" style="display: block;">
     <table class="table table-hover">                            
          @if ($cont_es > 0)
            <tbody>
              @foreach ($data_es['items'] as $element)
                 <tr>
                   <td> <a href=" {{$element->get_permalink()}}" target="_blank"> {{$element->get_title()}}</a></td>
                   <td><span class="label label-success">{{ $element->get_date('j F Y ') }}</span></td>
                 </tr>
                @endforeach
          </tbody>
          @else
            <span>No hay Noticias...</span>
          @endif
      </table>

    
      </div>
      <div class="tab-pane noticias" id="noticias-ingles" style="display: none;">
          <table class="table table-hover">                                
              @if ($cont_en > 0)
                <tbody>
                @foreach ($data_en['items'] as $element)
                <tr>
                  <td> <a href=" {{$element->get_permalink()}}" target="_blank"> {{$element->get_title()}}</a></td>
                  <td><span class="label label-success">{{ $element->get_date('j F Y ') }}</span></td>
                </tr>
                @endforeach
                </tbody>
              @else
                <span>No news for this language</span>
              @endif
          </table>
          

      </div>
  </div>
</div>