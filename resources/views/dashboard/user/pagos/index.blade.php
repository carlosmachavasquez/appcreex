@extends('dashboard.layouts.principal')

@section('styles')
    <link href="{{ asset('/assets/admin/css/signals/signals.css') }}" rel="stylesheet" type="text/css">
@stop

@section('scripts')

@stop

@section('content')
    <div class="main-content page-signals">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">Pagos</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item active">Pagos</li>
            </ol>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <h4>Membresía: {{ $membresia }}</h4>
        <div class="table-responsive">
            <table id="pagos" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                <thead class="thead-dark">
                <tr>
                    <th>Nro</th>
                    <th>Tipo de membresia</th>
                    <th>Periodo</th>
                    <th>Descripción</th>
                    <th>Medio de Pago</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                    <th>Monto</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($pagos as $i => $pago)
                    @php
                        $estado = "PENDIENTE";
                        $bgColor = "bg-danger";
                        $tipoMembresia = "MENSUAL";

                        if( $pago->estado == "1" ){
                            $estado = "CANCELADO";
                            $bgColor = "bg-secondary";
                        }

                        if( $pago->tipo_membresia == "1" ){
                            $tipoMembresia = "SEMESTRAL";
                        }
                    @endphp
                    <tr id="pago-{{$i + 1}}" class="{{$bgColor}}">
                        <td>{{ $i + 1 }}</td>
                        <td>{{ $tipoMembresia }}</td>
                        <td>{{ $pago->fecha_pago }} - {{ $pago->fecha_activo_hasta }}</td>
                        <td>{{ $pago->description }}</td>
                        <td>{{ $pago->origen_pago }}</td>
                        <td>
                            {{ $estado }}
                        </td>
                        <td></td>
                        <td>$ {{ $pago->monto }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop