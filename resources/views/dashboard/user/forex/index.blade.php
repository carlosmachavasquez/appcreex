@extends('dashboard.layouts.principal')
@section('styles')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <style>
        .thead-dark{
             background: #212529;
            border-color: #888;
        }
        
        .notificacion{
            background: #f2dede;
            padding: 20px 20px;
            color: #fff;
        }

        .notificacion h3, .notificacion p{
            color: #a94442;
        }

        .mb5px{
            margin-bottom: 5px;
        }

        .mb0{
            margin-bottom: 0;
        }

    </style>
@stop
@section('content')
<div class="main-content page-portafolio">
    @include('flash::message')
    <div class="page-header">
        <h3 class="page-title">Forex</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Home</a></li>
            <li class="breadcrumb-item active">Forex</li>
        </ol>
    </div>
</div>


<div class="container" style="padding-bottom: 2em;">
    <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin: 0 0 20px 0;">
      <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#formulario" role="tab" aria-controls="home" aria-selected="true">Formulario</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#tabla" role="tab" aria-controls="profile" aria-selected="false">Señales Forex</a>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade" id="tabla" role="tabpanel" aria-labelledby="profile-tab">
          @include('dashboard.user.forex.result.tabla')
      </div>
      <div class="tab-pane fade show active" id="formulario" role="tabpanel" aria-labelledby="home-tab"> 
        <div class="row" style="color:red; text-align:  center; background:     ">   
            <div class=" alert-danger" style="position: relative; padding: .75rem 1.25rem; margin-bottom: 1rem; border: 1px solid transparent;     border-radius: .25rem;text-align:    center; margin:     0 auto;width:   100%; margin-bottom:    10px;">
                <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                ¡IMPORTANTE! Sigues los 3 pasos solo si ya tiene disponible lo que vas a invertir en Forex <br>  Monto minimo de inversión apartir de 300 dolares en cualquier MAP.</font></font>  <br>    
            </div>

        </div>    
        <div class="row">
            <div class="col-xs-6 col-sm-12 col-md-6" >
                <form method="POST" style="width: 100%" autocomplete="off" id="app_form_enviar">
                {{-- @csrf --}}

                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                    <div class="form-group">
                        <label for="nombre">Nombre y apellidos</label>
                        <input type="text" class="form-control" id="nombre"  placeholder="Nombre" name="nombre" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" placeholder="name@example.com" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="user">Trading login</label>
                        <input type="text" class="form-control" id="user"  placeholder="usuario de forex" name="usuario" required>
                    </div>
                    <div class="form-group">
                        <label for="pass">Trading password</label>
                        <input type="text" class="form-control" id="pass"  placeholder="Contraseña de forex" name="password" required>
                    </div>

                    <div class="form-group">
                        <label for="server">Rango aproximado de inversión</label>
                        <input type="ip" class="form-control" id="server"  placeholder="inversión" name="servidor"  required>
                    </div>
                    
                    <div class="form-group">
                        <label for="obs">Observaciones</label>
                        <textarea class="form-control" id="obs" rows="3" name="observacion"></textarea>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success app_enviar_data">Enviar datos</button>
                    </div>
                </form>
                <div class=""  >
                    <a target="_blank" href="https://www.tradeviewespanol.com/crear-cuenta-cuatro-pasos.php">Link de registro en el brocker TradeView:</a>
                    <input style="width: 100%;padding: 5px;" type="text" value="https://www.tradeviewespanol.com/crear-cuenta-cuatro-pasos.php">
                </div>
            </div>
            <div class="col-xs-6 col-sm-12 col-md-6">

                <div>
                    <h5>Paso 1 - Crea tu cuenta en el Broker(TradeView).</h5>

                    <iframe src="https://player.vimeo.com/video/284520066?color=34C4BB&title=0&byline=0&portrait=0"  width="540" height="340" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
                
                
                <div>
                    <h5>Paso 2 - Conecta tu cuenta de TradeView a tu móvil.</h5>
                    <iframe src="https://player.vimeo.com/video/284520034?color=34C4BB&title=0&byline=0&portrait=0"  width="540" height="340" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>

                <div>
                    <h5>Paso 3 - Como transferir tu inversion al Bloker.</h5>
                    <iframe src="https://player.vimeo.com/video/285024865?color=34C4BB&title=0&byline=0&portrait=0" width="540" height="340" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>

                
            </div>            
        </div>
      </div>
    </div>
    
</div>



@endsection

@section('scripts')
<script>
    $('#app_form_enviar').submit(function(e){
        e.preventDefault();

        var form = $(this);
        var data = form.serialize();
        //alert(data);
        var token = $('#token').val();
        var route = "{{route('user.forex.enviar_correo')}}";       
        $.ajax({
            url: route,
            headers: { 'X-CSRF-TOKEN': token },
            type: 'POST',
            dataType: 'json',
            data: data,
            beforeSend: function() {
                // setting a timeout
                $('.app_enviar_data').attr('disabled', true);
            },
            success:function(data){
                if (data.success = 'true') {
                    $('.form-control').val('');
                    toastr.success(data.msj)
                    $('.app_enviar_data').attr('disabled', false);
                }else{
                    toastr.error(data.msj)
                    $('.app_enviar_data').attr('disabled', false);
                }
            }
        });
    });
</script>
@endsection