<div class="app_tabla">
	<div class="table-responsive">  
        <table id="adminSignals" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
            <thead class="thead-dark">
                <tr>
                    <th>HACE</th>
                    <th>PARES</th>
                    <th>TIPO DE ORDEN</th>
                    <th>EJECUCION</th>
                    <th>LIMITE</th>
                    <th>DETENER</th>
                    <th>PAR GANANCIA 1</th>
                    <th>PAR GANANCIA 2</th>
                    <th>PAR GANANCIA 3</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($forex as $data)                     
                    <tr>
                        <td>
                            @php
                                //Our dates and times.
                                $then = $data->created_at;
                                $then = new DateTime($then);

                                $now = new DateTime();

                                $sinceThen = $then->diff($now);
                                $days = $sinceThen->d;
                                $hours = $sinceThen->h;
                                $minutes = $sinceThen->i;
                                $seconds = $sinceThen->s;

                                if($days == 0 && $hours == 0 && $seconds == 0){
                                    $seconds.'s';
                                }else if($days == 0 && $hours == 0) {
                                    echo $minutes.'m, '.$seconds.'s';
                                }else if( $days == 0 ){
                                    echo $hours.'h, '.$minutes.'m ' .$seconds.'s';
                                }else if( $days > 0){
                                    echo $days.'d, '.$hours.'h, '.$minutes.'m, '.$seconds.'s';
                                }
                            @endphp
                        </td>
                        <td>{{$data->pair}}</td>
                        <td>{{$data->order_type}}</td>
                        <td>{{$data->market_execution}}</td>
                        <td>{{$data->sell_limit}}</td>
                        <td>{{$data->stop_loss}}</td>
                        <td>{{$data->take_prof1}}</td>
                        <td>{{$data->take_prof2}}</td>
                        <td>{{$data->take_prof3}}</td>
                    </tr>
                @endforeach                    
            </tbody>
        </table>
        {{ $forex->links() }}
    </div>
    <div class="notificacion" style="background: #ffffff;">
        <h3 class="mb5px"><i class="fas fa-exclamation-circle"></i> Importante!</h3>
        <p class="mb0" style="color:#000;">Nuestras señales son entregadas por profesionales especializados en forex pero no siempre tendrán las salidas expuestas ya que los mercados financieros son variables.</p>
    </div>
</div>