@extends('dashboard.layouts.principal')

@section('nav-search')


@endsection

 


@section('content')

 <div class="row container-main-cursos">
    <div class="texto-curso col-xl-6 col-sm-6 col-sm-12">
      <h1>Formación Creex</h1>
      <h3>Tu mejor alternativa para el desarrollo de tus inversiones</h3>
    </div>
    <div class="video-content col-xl-6 col-sm-6">
      <div class="embed-responsive embed-responsive-16by9 video">      
        <iframe src="https://player.vimeo.com/video/259973814?color=44ab96&title=0&byline=0&portrait=0" min-width="320" height="320" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
      </div>
    </div>

  </div>

<div class=" page-portafolio">
  @include('flash::message')
 {{--  <div class="page-header">
      <h3 class="page-title">Cursos</h3>
      <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Home</a></li>
          <li class="breadcrumb-item active">Cursos</li>
      </ol>
  </div> --}}




  <section  class="col-xl-12 col-sm-12 ">
    <span>  </span>
    <div style="background-color:  #e7eef4; margin-right: -30px; margin-left: -30px;" class="card">
      <div class="card-block">

         {{--  <div id="filters" >
            <h6 style=" display:  inline-block; margin-right:  10px;" >  Categorias:</h6>
            <button class=" btn btn-rounded is-checked" data-filter="*">Mostrar todo</button>
            @foreach ($categorias as $data)
                <button class="btn btn-rounded" data-filter=".categoria_{{$data->id}}">{{$data->nombre}}</button>
            @endforeach
          </div> --}}
      </div>
    </div>
  </section>

  <section class="main-content-edu">
    <div class="col-xl-12 col-sm-12  grid">
      <div class="row"> 
          @foreach ($cursos as $data)
          <div class="col-xl-3 col-sm-3 element-item categoria_{{$data->id_cat}}">
              <div class="card element-item-course" data-category=".categoria_{{$data->id_cat}}">
                  <div class="" style="justify-content:space-between;">
                      {{-- <div class=""><h6>{{$data->categoria}}</h6></div>
                      <div>
                          <small>{{$data->nivel}}</small>
                      </div> --}}
                      <a href="{{url('dashboard/user/cursos')}}/{{$data->id}}">
                          <img width="100%" src="{{url('uploads')}}/cursos/{{$data->imagen}}" alt="">
                      </a>
                  </div>
                  <div class="card-block">
                      <div class="course__item_img">
                          <a href="{{url('dashboard/user/cursos')}}/{{$data->id}}"></a>
                      </div>
                      <div class="course__item_body">
                          <h5>
                              <a href="{{url('dashboard/user/cursos')}}/{{$data->id}}">{{$data->titulo}}</a>
                          </h5>
                          <a class="course__item_button" href="{{url('dashboard/user/cursos')}}/{{$data->id}}">Comenzar</a>
                      </div>
                  </div>
              </div>
          </div>
          @endforeach 

      </div>
    </div>
  </section>



</div>
@endsection

@section('scripts')
<script>
    var $grid = $('.grid').isotope({
  itemSelector: '.element-item',
  layoutMode: 'fitRows',
  getSortData: {
    name: '.name',
    symbol: '.symbol',
    number: '.number parseInt',
    category: '[data-category]',
    weight: function( itemElem ) {
      var weight = $( itemElem ).find('.weight').text();
      return parseFloat( weight.replace( /[\(\)]/g, '') );
    }
  }
});

// filter functions
var filterFns = {
  // show if number is greater than 50
  numberGreaterThan50: function() {
    var number = $(this).find('.number').text();
    return parseInt( number, 10 ) > 50;
  },
  // show if name ends with -ium
  ium: function() {
    var name = $(this).find('.name').text();
    return name.match( /ium$/ );
  }
};

// bind filter button click
$('#filters').on( 'click', 'button', function() {
  var filterValue = $( this ).attr('data-filter');
  // use filterFn if matches value
  filterValue = filterFns[ filterValue ] || filterValue;
  $grid.isotope({ filter: filterValue });
});

// bind sort button click
$('#sorts').on( 'click', 'button', function() {
  var sortByValue = $(this).attr('data-sort-by');
  $grid.isotope({ sortBy: sortByValue });
});

// change is-checked class on buttons
$('.button-group').each( function( i, buttonGroup ) {
  var $buttonGroup = $( buttonGroup );
  $buttonGroup.on( 'click', 'button', function() {
    $buttonGroup.find('.is-checked').removeClass('is-checked');
    $( this ).addClass('is-checked');
  });
});
</script>
@endsection