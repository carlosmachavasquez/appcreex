<style>
  .video-content{
    margin-bottom: 20px;
  }
  .video{
        max-width: 740px;
    margin: 0 auto;
    padding: 0 1em;
  }
  .embed-responsive-16by9::before{
    padding-top: 0!important;
  }

 

  .table th{
      color: #000;
  }
  
  .thead-dark{
      font-weight: bold;
      background: #212529;
  }

  .thead-dark th{
      color: #fff;
  }

  .content_back, #video_leccion{
    height: 420px;
    width: 100%;
  }

</style>

@extends('dashboard.layouts.principal')

@section('content')

<div class="col-sm-12"> 
  <div class="page-header text-center">
    <h3 class="page-title text-center">{{$curso->titulo}}</h3>
  </div>
  <div class="row text-center">
    <div class="col-sm-12 card-body video-content">
        <div class="embed-responsive embed-responsive-16by9 video" style="margin-top: 4em;box-shadow: 0px 0px 8px #000;">   
            
          <div class="content_back">
             <iframe id="video_leccion" src="https://player.vimeo.com/video/{{$curso->video}}" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>   
          </div> 
        </div>
       </div>
       <div class="col-sm-12">
            <div class="card card-border">
                <div class="card-block">
                  <div class=" row">
                      <div class=" text-center col-lg-12 mb-4">

                          <div class=" tabs tabs-simple">
                              <ul class=" nav nav-option-curso nav-tabs" role="tablist">
                                  <li class="nav-item">
                                      <a class="nav-link active" data-toggle="tab" href="#lecciones" role="tab" > <i class="icon-fa icon-fa-play"></i></br>Lecciones</a>
                                  </li>
                                  <li class="nav-item">
                                      <a class="nav-link " data-toggle="tab" href="#descripcion" role="tab" ><i class="icon-fa icon-fa-info"></i> </br>Descripción </a>
                                  </li>                            
                              </ul>
                              <!-- Tab panes -->
                              <div class="tab-content">
                                  <div class="tab-pane active" id="lecciones" role="tabpanel" >
                                      <input type="hidden" id="id_leccion_app" value="">                  
                                      <div class="temario">
                                          <h4>Lecciones</h4>
                                          <div class="list-lecciones">
                                              <table class="table table-default">
                                                  <thead class="thead-dark">
                                                  <tr>
                                                      <th>#</th>
                                                      <th>Nombre de Lección</th>
                                                      <th>Duración</th>
                                                      <th>Estado</th>
                                                      <th>Archivos</th>
                                                      <th>Accion</th>
                                                  </tr>
                                                  </thead>
                                                  <tbody>
                                                    @if ($lecciones->count() > 0)
                                                      @foreach ($lecciones as $data)
                                                        <tr class="table-success showData" data-href='pagina1'>
                                                          <td class="icon-box"><i class="icon-fa icon-fa-check"></i></td>
                                                          <td>{{$data->titulo}}</td>
                                                          <td><span class="label label-success">{{$data->duracion}}</span>m</td>
                                                          <td>
                                                            @php
                                                              $existe = App\Entities\Leccionuser::where('leccion_id', '=', $data->id)->where('usuario', '=', Auth::user()->id)->exists();
                                                            @endphp                                                            
                                                            @if(!$existe)
                                                            <span class="btn btn-sm btn-outline-warning">No visto</span>
                                                            @else
                                                            <span class="btn btn-sm btn-outline-success">visto</span>
                                                            @endif
                                                          </td>
                                                          <td><button class="btn btn-outline-primary" onclick="ver_archivos({{$data->id}})" data-toggle="modal" data-target=".ver_archivos">Ver Archivos</button></td>
                                                          <td>                                                            
                                                            <button class="btn btn-outline-info" onclick="ver_video({{$data->id}})">Ver video</button>
                                                          </td>
                                                        </tr>
                                                      @endforeach
                                                    @else
                                                      <tr class="table-success showData" data-href='pagina1'>No hay Lecciones</tr>
                                                    @endif
                                                  </tbody>
                                              </table>
                                           </div>
                                      </div>
                                  </div> 
                                  <div class="tab-pane" id="descripcion" role="tabpanel">
                                    <div class="descripcion-curso">
                                      <h4>{{$curso->titulo}}</h4>
                                      <p>
                                      {{$curso->descripcion}}
                                      </p>
                                    </div>
                                  </div>                               
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>

  </div>
</div>

@include('dashboard.user.educacion.curso.modal.ver_archivos')
@endsection

@section('scripts')
<script>
    function ver_video(data)
    {
        var route = "{{url('dashboard/user/cursos/video')}}/"+data;
        //alert(route);
        $.get(route, function(value){
            $('#video_leccion').attr('src', 'https://player.vimeo.com/video/'+value.video);
            $('#id_leccion_app').val(value.id);
        });
       
    }

    function ver_archivos(data)
    {
      var route = "{{url('dashboard/user/education/leccion/mostrar_archivos')}}/"+data;
        //alert(route);
        $.get(route, function(value){
            $('#list_archivos').html(value);
            
        });
    }

   

</script>
@endsection