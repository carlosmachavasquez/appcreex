@if ($archivos->count() > 0)
	<span>Lista de archivos que puedes descargar:</span><br><br>
	@foreach($archivos as $data)
		<li><i class="icon-im icon-im-folder-download"></i> <a href="{{ asset('uploads/archivos') }}/{{ $data->archivo }}">{{ $data->archivo }}</a></li>
	@endforeach
@else
	<span>No Hay archivos en esta lección...</span>
@endif