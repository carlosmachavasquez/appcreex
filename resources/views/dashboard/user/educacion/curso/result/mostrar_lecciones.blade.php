<table class="table table-default">
  <thead class="thead-dark">
  <tr>
      <th>#</th>
      <th>Nombre de Lección</th>
      <th>Duración</th>
      <th>Estado</th>
      <th>Archivos</th>
      <th>Accion</th>
  </tr>
  </thead>
  <tbody>
    @if ($lecciones->count() > 0)
      @foreach ($lecciones as $data)
        <tr class="table-success showData" data-href='pagina1'>
          <td class="icon-box"><i class="icon-fa icon-fa-check"></i></td>
          <td>{{$data->titulo}}</td>
          <td><span class="label label-success">{{$data->duracion}}</span>m</td>
          <td><span class="btn btn-outline-warning">No visto</span></td>
          <td><button class="btn btn-outline-primary" onclick="ver_archivos({{$data->id}})" data-toggle="modal" data-target=".ver_archivos">Ver Archivos</button></td>
          <td>                                                            
            <button class="btn btn-outline-info" onclick="ver_video({{$data->id}})">Ver video</button>
          </td>
        </tr>
      @endforeach
    @else
      <tr class="table-success showData" data-href='pagina1'>No hay Lecciones</tr>
    @endif
  </tbody>
</table>