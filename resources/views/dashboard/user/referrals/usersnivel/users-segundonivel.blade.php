 <div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">

            	<div class="table-responsive">
                    <table id="users-datatable" class="table table-striped table-bordered" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Nombre y Apellido</th>
                            <th>Usuario</th>
                            <th>Referidor</th>
                            <th>Membresia</th>
                            <th>Fecha de activación</th>
                        </tr>
                        </thead>
                        	@for($x=1;$x<=$referidos['niveles'];$x++)
		                    	@if ($x == 2)
		                    		@for($y=1;$y<=$referidos[$x]['total_lvl'];$y++)
		                            <tr>
		                                <td>{{ $referidos[$x][$y]['nombre'] }} {{ $referidos[$x][$y]['apellido'] }}</td>
		                                <td>{{ $referidos[$x][$y]['email'] }}</td>
		                                <td>{{ $referidos[$x][$y]['usuario'] }}</td>
		                                <td>{{ $referidos[$x][$y]['fregistro'] }}</td>
		                                <td>{{ $referidos[$x][$y]['plan'] }}</td>
		                            </tr>
                        		 	@endfor
		                    	@endif
		               		@endfor
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>