@extends('dashboard.layouts.principal')

@section('scripts')
    <script src="/assets/admin/js/users/users.js"></script>
    <script src="/assets/admin/js/sessions/updateSettings.js"></script>
    <script src="/assets/admin/js/sessions/updatePasswordSettings.js"></script>
    <script src="/assets/admin/js/sessions/updateSettingsWallet.js"></script>
    <script src="/assets/admin/js/sessions/tabsandroutes.js"></script>
@stop

@section('content')
    <div class="main-content page-profile">
        @include('flash::message')
        <div class="page-header">
            <h3 class="page-title">Equipo</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">Mis Referidos</a></li>
                <li class="breadcrumb-item active"></li>
            </ol>
        </div>
        <div class="row">
        	<div class="col-sm-3">
                <div class="card">
                	<div class="card-header">
                		<div class="row">
	                		<div class="col-sm-7">
	                			<h5>TOTAL</h5>
	                		</div>
	                		<div class="col-sm-5 center-block">
	                			<h6 class="center-block"><i></i><strong>{{ $referidos['total_ref'] }}</strong> Referidos</h6>
	                		</div>
                		</div>
                    </div>
                    <div class="card-block">
                       <div class="row">
	                		<div class="col-sm-8">
	                			<h5>Primer Nivel</h5>
	                			<h5>Segundo Nivel</h5>
                                <h5>Tercer Nivel</h5>
                                <h5>Cuarto Nivel</h5>
                                <h5>Quinto Nivel</h5>
                                <h5>Sexto Nivel</h5>
                                <h5>Septimo Nivel</h5>
                                <h5>Octavo Nivel</h5>
	                			<h5>Noveno Nivel</h5>
	                		</div>
	                		<div class="col-sm-4">
                                @for($x=1;$x<=$referidos['niveles'];$x++)
                                    <h4><i><strong>{{ $referidos[$x]['total_lvl'] }}</strong></i></h4>
                                @endfor
	                		</div>
                		</div>
                       	
                       
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="card">
                    <div class="card-block">
                        <div class="tabs tabs-default">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="primernivel-tab" data-toggle="tab" href="#primernivel" role="tab">Primer Nivel</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="segundonivel-tab" data-toggle="tab" href="#segundonivel" role="tab">Segundo Nivel</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tercernivel-tab" data-toggle="tab" href="#tercernivel" role="tab">Tercer Nivel</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="cuartonivel-tab" data-toggle="tab" href="#cuartonivel" role="tab">Cuarto Nivel</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="primernivel" role="tabpanel">
                                   @include('dashboard.user.referrals.usersnivel.users-primernivel')
                                </div>
                                <div class="tab-pane" id="segundonivel" role="tabpanel">
                                    @include('dashboard.user.referrals.usersnivel.users-segundonivel')
                                </div>
                                <div class="tab-pane" id="tercernivel" role="tabpanel">
                                   @include('dashboard.user.referrals.usersnivel.users-tercernivel')
                                </div>
                                <div class="tab-pane" id="cuartonivel" role="tabpanel">
                                   @include('dashboard.user.referrals.usersnivel.users-cuartonivel')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@stop
