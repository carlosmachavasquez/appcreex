<div class="header-bottom">

        <ul class="header-nav">
            <li class="   has-child ">
                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                        class="icon-fa icon-fa-dashboard"></i>Dashboard2</a>
                <div class="dropdown-menu">
                    <div class="">
                        <a class="dropdown-item  " href="../dashboard/basic.html">Basic</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../dashboard/ecommerce.html">Ecommerce</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../dashboard/finance.html">Finance</a>
                    </div>
                </div>
            </li>
            <li class="active   has-child ">
                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                        class="icon-fa icon-fa-th-large"></i> Portafolio</a>
                <div class="dropdown-menu">
                    <div class="">
                        <a class="dropdown-item  " href="sidebar.html">Sidebar</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="icon-sidebar.html">Icon Sidebar</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="horizontal-menu.html">Horizontal Menu</a>
                    </div>
                </div>
            </li>
            <li class=" has-child ">
                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                        class="icon-fa icon-fa-th-large"></i> Señales</a>
                <div class="dropdown-menu">
                    <div class="">
                        <a class="dropdown-item  " href="sidebar.html">Sidebar</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="icon-sidebar.html">Icon Sidebar</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="horizontal-menu.html">Horizontal Menu</a>
                    </div>
                </div>
            </li>
            <li class="   has-child ">
                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                        class="icon-fa icon-fa-star"></i> Icos</a>
                <div class="dropdown-menu">
                    <div class="">
                        <a class="dropdown-item  " href="../basic-ui/buttons.html">Buttons</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../basic-ui/cards.html">Cards</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../basic-ui/tabs.html">Tabs &amp; Accordians</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../basic-ui/typography.html">Typography</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../basic-ui/tables.html">Tables</a>
                    </div>
                </div>
            </li>
            <li class="   has-child ">
                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                        class="icon-fa icon-fa-puzzle-piece"></i> Herramientas</a>
                <div class="dropdown-menu">
                    <div class="">
                        <a class="dropdown-item  " href="../components/datatables.html">Datatables</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/notifications.html">Notifications</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/nestable-list.html"> Nestable List</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/nestable-tree.html">Nestable Tree</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/image-cropper.html">Image Cropper</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/zoom.html">Image Zoom</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/calendar.html">Calendar</a>
                    </div>
                    <div class="dropdown-submenu ">
                        <a class="dropdown-item   dropdown-subitem " href="#">Rating</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="../components/ratings/star.html">Star Ratings</a>
                            <a class="dropdown-item" href="../components/ratings/bar.html">Bar Ratings</a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="  ">
                <a href="../todos.html"><i class="icon-fa icon-fa-check"></i> CoinMarket</a>
            </li>
            <li class="   has-child ">
                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                        class="icon-fa icon-fa-puzzle-piece"></i> Noticias</a>
                <div class="dropdown-menu">
                    <div class="">
                        <a class="dropdown-item  " href="../components/datatables.html">Datatables</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/notifications.html">Notifications</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/nestable-list.html"> Nestable List</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/nestable-tree.html">Nestable Tree</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/image-cropper.html">Image Cropper</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/zoom.html">Image Zoom</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/calendar.html">Calendar</a>
                    </div>
                    <div class="dropdown-submenu ">
                        <a class="dropdown-item   dropdown-subitem " href="#">Rating</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="../components/ratings/star.html">Star Ratings</a>
                            <a class="dropdown-item" href="../components/ratings/bar.html">Bar Ratings</a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="   has-child ">
                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                        class="icon-fa icon-fa-puzzle-piece"></i> Educacion</a>
                <div class="dropdown-menu">
                    <div class="">
                        <a class="dropdown-item  " href="../components/datatables.html">Datatables</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/notifications.html">Notifications</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/nestable-list.html"> Nestable List</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/nestable-tree.html">Nestable Tree</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/image-cropper.html">Image Cropper</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/zoom.html">Image Zoom</a>
                    </div>
                    <div class="">
                        <a class="dropdown-item  " href="../components/calendar.html">Calendar</a>
                    </div>
                    <div class="dropdown-submenu ">
                        <a class="dropdown-item   dropdown-subitem " href="#">Rating</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="../components/ratings/star.html">Star Ratings</a>
                            <a class="dropdown-item" href="../components/ratings/bar.html">Bar Ratings</a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="   has-child ">
                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                        class="icon-fa icon-fa-cogs"></i> Settings2</a>
                <div class="dropdown-menu">
                    <div class="">
                        <a class="dropdown-item  " href="../settings/social.html">Social</a>
                    </div>

                </div>
            </li>
        </ul>

    </div>