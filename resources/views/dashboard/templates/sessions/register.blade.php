@extends('dashboard.templates.layouts.layout-login')

@section('scripts')
    <script src="/assets/admin/js/sessions/register.js"></script>
@stop

@section('content')
    @include('dashboard.templates.sessions.partials.register-form')
@stop
