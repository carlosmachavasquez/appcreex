<form action="{{ route('login') }}" id="loginForm" method="post">
    @csrf
    <div class="form-group">
        <input type="email" class="form-control form-control-danger" placeholder="Enter email" name="email">
        @if ($errors->has('email'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
        <input type="password" class="form-control form-control-danger" placeholder="Enter Password"
               name="password">
        @if ($errors->has('password'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
    <div class="other-actions row">
        <div class="col-6">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> 
                        {{ __('Recordar') }}
                </label>
            </div>
        </div>
        <div class="col-6 text-right">
            <a href="{{ route('password.request') }}" class="forgot-link">¡Olvido su contraseña?</a>
        </div>
    </div>
    <button type="submit" class="btn btn-theme btn-full">INGRESAR</button>

</form>