<?php
    if (isset($_GET['user'])) {
        $username = $_GET['user'];
        $user = DB::table('users')->where('nickname', '=', $username)->first();        
    }else{
        $user = 0;//no existe la variable user
    }    
?>
@if(!$user > 0)
<div class="alert alert-danger">
    <h4>Usuario No encontrado</h4>
    <p>por favor busca a un referidor válido</p>
</div>
@else
       
    <form action="{{ route('register') }}" id="registerForm" method="post">
        @csrf
        <input type="hidden" value="{{ $user->id }}" name="id_referidor">
        <input type="hidden" value="{{ $username }}" name="referido">
        <div class="form-group">
            <input id="nickname" type="text" class="form-control form-control-danger" placeholder="Enter username" name="nickname" value="{{old('nickname')}}">
            <span class="cr">{{ $errors->first('nickname') }}</span>
        </div>
        <div class="form-group">
            <input id="name" type="text" class="form-control form-control-danger" placeholder="Enter name" name="name" value="{{old('name')}}">
            <span class="cr">{{ $errors->first('name') }}</span>
        </div>
        <div class="form-group">
            <input id="lastname" type="text" class="form-control form-control-danger" placeholder="Enter lastname" name="lastname" value="{{old('lastname')}}">
            <span class="cr">{{ $errors->first('lastname') }}</span>
        </div>
        <div class="form-group">
            <input id="email" type="email" class="form-control form-control-danger" placeholder="Enter email" name="email" value="{{old('email')}}">
            <span class="cr">{{ $errors->first('email') }}</span>
        </div>
        <div class="form-group">
            <input id="cellphone" type="text" class="form-control form-control-danger" placeholder="Enter cellphone" name="cellphone" value="{{old('cellphone')}}">
            <span class="cr">{{ $errors->first('cellphone') }}</span>
        </div>
        <div class="form-group">
            <input id="password" type="password" class="form-control form-control-danger" placeholder="Enter Password" name="password">
            <span class="cr">{{ $errors->first('password') }}</span>
        </div>
        <div class="form-group">
            <input id="password-confirm" type="password" class="form-control form-control-danger" placeholder="Retype Password" name="password_confirmation">
        </div>
        <button class="btn btn-login btn-full">Register</button>
    </form>
@endif