<footer class="site-footer">
    <div class="text-center">
    	<p>
				© 2018 Grupo Creex - Todos los derechos reservados. 
				<span> | </span>
				<a href="https://www.grupocreex.com/politicas-de-privacidad" class="" style="color: #818a91;" target="_blank"> Políticas de privacidad</a>
				<a href="https://www.grupocreex.com/terminos-y-condiciones" class="" style="color: #818a91;" target="_blank">Términos y condiciones</a>

		</p>  
    </div>
    <div class="text-right">    	
        <p>
        	
        </p>
    </div>
</footer>
{{-- <footer class="site-footer">
    <div class="text-right">
      Powered by <a href="http://bytefury.com" target="_blank">Bytefury</a>
    </div>
</footer>
 --}}