<header class="site-header">
    <a href="/" class="brand-main">
        <img src="{{asset('dashboard/assets/global/img/logo-creex.png')}}" id="logo-desk" alt="Creex Logo" class="hidden-sm-down">
        <img src="{{asset('dashboard/assets/global/img/logo-mobile.png')}}" id="logo-mobile" alt="Creex Logo"
             class="hidden-md-up">
    </a>
    <a href="#" class="nav-toggle">
        <div class="hamburger hamburger--htla">
            <span>toggle menu</span>
        </div>
    </a>

    <ul class="action-list">
        {{--
        <li>
            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                    class="icon-fa icon-fa-bell"></i></a>
            <div class="dropdown-menu dropdown-menu-right notification-dropdown">
                <h6 class="dropdown-header">Notificaciones</h6>
                <a class="dropdown-item" href="#"><i class="icon-fa icon-fa-user"></i> New User was Registered</a>
                <a class="dropdown-item" href="#"><i class="icon-fa icon-fa-comment"></i> A Comment has been posted.</a>
            </div>
        </li>
        --}}
        <li>
            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="avatar">
                {{--
                <img src="{{asset('dashboard/assets/global/img/avatars/avatar.png')}}" alt="Avatar">
                --}}
                <img alt="" src="https://secure.gravatar.com/avatar/bf79fa71d3c873ca0519b4c380d56e60?s=36&amp;d=mm&amp;r=g" srcset="https://secure.gravatar.com/avatar/bf79fa71d3c873ca0519b4c380d56e60?s=72&amp;d=mm&amp;r=g 2x" class="avatar avatar-36 photo" height="36" width="36">
                <span class="header-name">{{isset( Auth::user()->name ) ? Auth::user()->name : ''}}{{--isset( Auth::user()->lastname ) ? Auth::user()->lastname : ''--}}</span>
                <span class="barrita-detalle"><i class="icon-fa icon-fa-sort-desc "></i></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="/dashboard/user/settings"><i class="icon-fa icon-fa-cogs"></i> Mi Perfil</a>
                @if(Auth::user()->role == "user")
                    <a class="dropdown-item" href="/dashboard/user/pagos"><i class="icon-fa icon-fa-tag"></i> Mis Pagos</a>
                @endif
                @if(Auth::user()->role == "admin")
                <a class="dropdown-item" href="/dashboard/admin/usuarios"><i class="icon-fa icon-fa-table"></i> Reportes Usuarios</a>
                @endif

                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="icon-fa icon-fa-sign-out"></i> Salir
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>

    </ul>
</header>