<!DOCTYPE html>
<html>
<head>
    <title>Laraspace - Laravel Admin</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
    <script src="{{asset('/assets/admin/js/core/pace.js')}}"></script>
    <link href="{{ mix('/assets/admin/css/laraspace.css') }}" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('dashboard.templates.layouts.partials.favicons')
    @yield('styles')
</head>
<body class="layout-default skin-default">
    @include('dashboard.templates.layouts.partials.laraspace-notifs')

    <div id="app" class="site-wrapper">
        @include('dashboard.templates.layouts.partials.header')
        <div class="mobile-menu-overlay"></div>
        @include('dashboard.templates.layouts.partials.sidebar',['type' => 'default'])

        @yield('content')

        @include('dashboard.templates.layouts.partials.footer')
        @if(config('laraspace.skintools'))
            @include('dashboard.templates.layouts.partials.skintools')
        @endif
    </div>

    <script src="{{mix('/assets/admin/js/core/plugins.js')}}"></script>
    <script src="{{asset('/assets/admin/js/demo/skintools.js')}}"></script>
    <script src="{{mix('/assets/admin/js/core/app.js')}}"></script>
    @yield('scripts')
    <script>
        $('#flash-overlay-modal').modal();
    </script>
    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>
      <!-- ManyChat -->
        <script src="//widget.manychat.com/1323783454370057.js" async="async">
        </script>
</body>
</html>
