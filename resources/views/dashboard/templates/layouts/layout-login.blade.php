<!DOCTYPE html>
<html>
<head>
    <title>Creex App</title>
    <link href="{{ mix('/assets/admin/css/laraspace.css') }}" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('dashboard.templates.layouts.partials.favicons')
    @yield('estilos')
</head>
<body class="login-page login-1">
<div id="app" class="login-wrapper">
    <div class="login-box">
        @include('dashboard.templates.layouts.partials.laraspace-notifs')
        <div class="logo-main">
            <a href="/"><img src="/assets/admin/img/logo-creex-new.png" alt="Creex Logo"></a>
        </div>
        @yield('content')
        <div class="page-copyright">
            <p>Diseñado y creado con pasión por el  <a href="https://www.grupocreex.com/" target="_blank">Grupo Creex ®</a></p>
        </div>
    </div>
</div>
<script src="{{mix('/assets/admin/js/core/plugins.js')}}"></script>
<script src="{{mix('/assets/admin/js/core/app.js')}}"></script>
@yield('scripts')
<script>
    $('#flash-overlay-modal').modal();
</script>
<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
</body>
</html>
