@extends('dashboard.layouts.principal')

@section('content')
    <div class="main-content" id="dashboardPage">
        @include('flash::message')

        <div class="row">
            <div class="col-lg-12 col-xl-6 mt-2">
                <div class="card">
                    <div class="card-header">
                        <h6><i class="icon-fa icon-fa-line-chart text-warning"></i> Video Bienvenida</h6>
                    </div>
                    <div class="card-block">
                        <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/273337186?autoplay=1&color=44ab96&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-xl-6 mt-2">
                <div class="card">
                    <div class="card-header">
                        <h6><i class="icon-fa icon-fa-bar-chart text-success"></i>INDICE GENERAL</h6>
                    </div>
                    <div class="card-block">
                        <ol>
                            <li> Completa tu perfil en : 
                                    <a class="btn btn-primary btn-xs" href="{{url('dashboard/user/settings')}}">
                                    Mi Perfil</a>
                            </li>
                            <li> Visualizar y evaluar nuestras señalese en :
                                <a class="btn btn-primary btn-xs" href="{{url('dashboard/user/signals')}}">Señales</a></li>
                            <li> Entérate como se encuentra el mercado minuto a minuto.
                                <a class="btn btn-primary btn-xs" href="{{url('dashboard/user/coinmarket')}}">Señales</a></li>
                            </li>
                            <li> Registra tus movimientos desde un solo lugar.
                                <a class="btn btn-primary btn-xs" href="{{url('dashboard/user/portafolio')}}">Rankig</a></li>
                            </li>
                            <li> Crea tu portafolio
                                <a class="btn btn-primary btn-xs" href="{{url('dashboard/user/portafolio')}}">Mi Portafolio</a></li>
                            </li>
                            <li> Crea tu portafolio</li>
                            <li> Acciona</li>
                        </ol>
                    </div>
                </div>
                <div class="card">
                    <div class="card-block">
                        <label for="link_referidor">Link referidor</label>
                        <input type="text" class="form-control" id="link_referidor" value="{{$link_referidor}}">
                    </div>
                </div>
            </div>

            
            @if (Auth::user()->membresia_id == 1 || Auth::user()->membresia_id == 5){{-- aprendiz --}}
                <div class="col-lg-12 col-xl-4 mt-2">
                    <div class="card">
                        <div class="card-block">
                            <div>
                                <a href="https://chat.whatsapp.com/4eW8RJmGxlYCKK8IBAwOOh">
                                    <img class="img-responsive" width="100%"  src="https://grupocreex.net/banners/aprendiz.png" alt="">
                                </a>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-xl-4 mt-2">
                    <div class="card">

                        <div class="card-block">
                            <div>
                                <img class="img-responsive" width="100%" src="https://grupocreex.net/banners/eventos-presencialesyvirtuales.png" alt="">
                            </div>
                            <br>
        
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-xl-4 mt-2">
                    <div class="card">

                        <div class="card-block">
                            <div>
                                <img class="img-responsive" width="100%" src="https://grupocreex.net/banners/Lanzamiento%20App-01.png" alt="">
                            </div>
                            <br>
        
                        </div>
                    </div>
                </div>
            @elseif (Auth::user()->membresia_id == 2 || Auth::user()->membresia_id == 6) {{--emprendedor--}}

                <div class="col-lg-12 col-xl-4 mt-2">
                    <div class="card">
                        <div class="card-block">
                            <div>
                                <a href="https://chat.whatsapp.com/5kj7qvPXnGW0sVOKa5EEii">
                                    <img class="img-responsive" width="100%"  src="https://grupocreex.net/banners/emprendedor.png" alt="">
                                </a>
                            </div>
                            <br>
                           
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-xl-4 mt-2">
                    <div class="card">

                        <div class="card-block">
                            <div>
                                <img class="img-responsive" width="100%" src="https://grupocreex.net/banners/eventos-presencialesyvirtuales.png" alt="">
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-xl-4 mt-2">
                    <div class="card">

                        <div class="card-block">
                            <div>
                                <img class="img-responsive" width="100%" src="https://grupocreex.net/banners/Lanzamiento%20App-01.png" alt="">
                            </div>
                            <br>
                        </div>
                    </div>
                </div>

            @elseif (Auth::user()->membresia_id == 3 || Auth::user()->membresia_id == 7) {{--inversionista--}}

                <div class="col-lg-12 col-xl-4 mt-2">
                    <div class="card">
                        <div class="card-block">
                            <div>
                                <a href="https://chat.whatsapp.com/2ognARrG9ep9jMI5J0RajG">
                                    <img class="img-responsive" width="100%"  src="https://grupocreex.net/banners/inversionista.png" alt="">
                                </a>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-xl-4 mt-2">
                    <div class="card">

                        <div class="card-block">
                            <div>
                                <img class="img-responsive" width="100%" src="https://grupocreex.net/banners/eventos-presencialesyvirtuales.png" alt="">
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-xl-4 mt-2">
                    <div class="card">

                        <div class="card-block">
                            <div>
                                <img class="img-responsive" width="100%" src="https://grupocreex.net/banners/Lanzamiento%20App-01.png" alt="">
                            </div>
                            <br>

                        </div>
                    </div>
                </div>

            @elseif (Auth::user()->membresia_id == 4 || Auth::user()->membresia_id == 8)
                <div class="col-lg-12 col-xl-4 mt-2">
                    <div class="card">
                        <div class="card-block">
                            <div>
                                <a href="https://chat.whatsapp.com/2ognARrG9ep9jMI5J0RajG">
                                    <img class="img-responsive" width="100%"  src="https://grupocreex.net/banners/inversionista-vip.png" alt="">
                                </a>
                            </div>
                            <br>

                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-xl-4 mt-2">
                    <div class="card">

                        <div class="card-block">
                            <div>
                                <img class="img-responsive" width="100%" src="https://grupocreex.net/banners/eventos-presencialesyvirtuales.png" alt="">
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-xl-4 mt-2">
                    <div class="card">

                        <div class="card-block">
                            <div>
                                <img class="img-responsive" width="100%" src="https://grupocreex.net/banners/Lanzamiento%20App-01.png" alt="">
                            </div>
                            <br>
                        </div>
                    </div>
                </div>{{--inversionista vip--}}
            @endif
            
        </div>

    </div>

    @yield('scripts')
      <!-- ManyChat -->
        <script src="//widget.manychat.com/1323783454370057.js" async="async">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>


@stop
