<!DOCTYPE html>
<html>
<head>
        <meta charset = "utf-8">
        <title>Creex App</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
        <script src="{{asset('dashboard/assets/global/js/core/pace.js')}}"></script>
        <link href="{{asset('dashboard/assets/global/css/laraspace.css')}}" rel="stylesheet" type="text/css">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="apple-touch-icon" sizes="57x57" href="{{asset('dashboard/assets/global/img/favicons/apple-touch-icon-57x57.png')}}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{asset('dashboard/assets/global/img/favicons/apple-touch-icon-60x60.png')}}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{asset('dashboard/assets/global/img/favicons/apple-touch-icon-72x72.png')}}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{asset('dashboard/assets/global/img/favicons/apple-touch-icon-76x76.png')}}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{asset('dashboard/assets/global/img/favicons/apple-touch-icon-114x114.png')}}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{asset('dashboard/assets/global/img/favicons/apple-touch-icon-120x120.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{asset('dashboard/assets/global/img/favicons/apple-touch-icon-144x144.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{asset('dashboard/assets/global/img/favicons/apple-touch-icon-152x152.png')}}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{asset('dashboard/assets/global/img/favicons/apple-touch-icon-180x180.png')}}">
        <link rel="icon" type="image/png" href="{{asset('dashboard/assets/global/img/favicons/favicon-32x32.png')}}" sizes="32x32">
        <link rel="icon" type="image/png" href="{{asset('dashboard/assets/global/img/favicons/android-chrome-192x192.png')}}"
              sizes="192x192">
        <link rel="icon" type="image/png" href="{{asset('dashboard/assets/global/img/favicons/favicon-96x96.png')}}" sizes="96x96">
        <link rel="icon" type="image/png" href="{{asset('dashboard/assets/global/img/favicons/favicon-16x16.png')}}" sizes="16x16">
        <link rel="manifest" href="{{asset('dashboard/assets/global/img/favicons/manifest.json')}}">
        <link rel="mask-icon" href="{{asset('dashboard/assets/global/img/favicons/safari-pinned-tab.svg')}}" color="#333333">
        <link rel="shortcut icon" href="{{asset('dashboard/assets/global/img/favicons/favicon.ico')}}">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-TileImage" content="{{asset('dashboard/assets/global/img/favicons/mstile-144x144.png')}}">
        <meta name="msapplication-config" content="/assets/global/img/favicons/browserconfig.xml">
        <meta name="theme-color" content="#333333">
</head>
<body id="app" class="page-error-404">
<div class="error-box">
    <div class="row">
        <div class="col-sm-12 text-sm-center">
            <h1>403</h1>
            <h5>No tienes permisos suficiente para acceder a este página.</h5>
            <button type="button" class="btn btn-lg bg-yellow" onclick="goBack()" style="cursor: pointer;">
                <i class="icon-fa icon-fa-arrow-left"></i> Go Back
            </button>
        </div>
    </div>
</div>
<script type="text/javascript">
    function goBack() {
        window.history.back();
    }
</script>
</body>
</html>
