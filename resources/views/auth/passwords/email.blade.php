@extends('dashboard.templates.layouts.layout-login')

@section('scripts')
{{--
    <script src="/assets/admin/js/sessions/login.js"></script>
--}}
@stop

@section('content')
	@if (session('status'))
	    <div class="alert alert-success">
	        {{ session('status') }}
	    </div>
	@endif
    <form action="{{ route('password.email') }}" id="loginForm" method="post">
       @csrf
        <div class="form-group">
            <input type="email" class="form-control form-control-danger" placeholder="Enter email" name="email">
        </div>
        <button type="submit" class="btn btn-theme btn-full">Send Resent Link</button>
    </form>
@stop
