@extends('dashboard.templates.layouts.layout-login')

@section('scripts')
{{--
    <script src="/assets/admin/js/sessions/login.js"></script>
--}}
@stop

@section('content')
    <form action="{{ route('password.request') }}" id="loginForm" method="post">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">

        <div class="form-group">
            <input type="email" class="form-control form-control-danger" placeholder="Enter your email" name="email" value="{{Session::get('email')}}">
        </div>
        <div class="form-group">
            <input type="password" class="form-control form-control-danger" placeholder="Enter password" name="password">
        </div>
        <div class="form-group">
            <input id="password-confirm" type="password" class="form-control form-control-danger" placeholder="Enter Confirm password"
                   name="password_confirmation">
        </div>
        <button type="submit" class="btn btn-theme btn-full">Reset Password</button>
    </form>
@stop
