@extends('dashboard.templates.layouts.layout-login')

@section('scripts')
    <script src="/assets/admin/js/sessions/login.js"></script>
@stop

@section('content')
	@include('flash::message')
    @include('dashboard.templates.sessions.partials.login-form')
@stop
