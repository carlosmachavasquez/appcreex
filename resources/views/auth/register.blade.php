@extends('dashboard.templates.layouts.layout-login')

@section('estilos')
<style>
	.has-error{
		background: #FDA3A3!important;
    	border-color: red!important;
	}
	.cr{
		color: red;
		text-shadow: 0px 0px #000;
	}
</style>
@endsection

@section('scripts')
    <script src="/assets/admin/js/sessions/register.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

@stop

@section('content')
    @include('dashboard.templates.sessions.partials.register-form')
@stop
