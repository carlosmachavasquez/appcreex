# README #

carlos por la ......

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Como instalar el proyecto? ###

Lo primero que hay que hacer es posicionarnos en la rama "merge-developers" con los siguientes comandos GIT.

1.- Clonar el Repo

2.- Cambiar a la rama "merge-developers" con: 
git checkout merge-developers

3.- Crear un archivo .env en la raiz del proyecto. Abajo pondré el contenido del archivo .env

* Ahora, los siguientes comandos de artisan y laravel para cargar el proyecto

4.- Instalamos Flash Messages de Laravel con esto:
composer require laracasts/flash

5.- Ahora instalamos el composer
composer install
composer update

6.- Ahora hacemos el dump autoload
composer  dump-autoload

7- Seguidamente, ejecutas todos estos comandos para limpiar caché
sudo php artisan cache:clear
sudo php artisan config:cache
sudo php artisan view:clear
sudo php artisan route:clear

8.- Al final, ejecutamos la migración para que se creen las tablas en la BD Mysql
php artisan migrate:refresh

9.- Si todo salio bien y no salio ningun error, ejecutar la aplicación con:
php artisan serve


The GD Graphics Library is for dynamically manipulating images. For Ubuntu you should install it manually:

PHP5: sudo apt-get install php5-gd
PHP7.0: sudo apt-get install php7.0-gd
PHP7.1: sudo apt-get install php7.1-gd
PHP7.2: sudo apt-get install php7.2-gd
That's all, you can verify that GD support loaded:

php -i | grep -i gd
Output should be like this:

GD Support => enabled
GD headers Version => 2.1.1-dev
gd.jpeg_ignore_warning => 0 => 0

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


Contenido del archivo .env

APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:0EUPrsKSNjsteXwgnTS9EAzJd/AMXV+I3qndg5MnZ8k=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=creex
DB_USERNAME=root
DB_PASSWORD=tamayo

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
SESSION_LIFETIME=120
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"