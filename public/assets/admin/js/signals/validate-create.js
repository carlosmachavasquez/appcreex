var validateCreateForm = function () {

    // Login form validation
    var handleValidationUpdate = function() {

        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation
        var formUpdate = $('#validateCreateForm');

        formUpdate.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                coinname: {
                    required: true,
                    minlength: 2
                },
                coinsymbol: {
                    required: true,
                    minlength: 2
                },
                coinimgurl: {
                    required: true,
                    minlength: 5
                },
                exchange: {
                    required: true,
                    minlength: 2
                },
                membresia_id: {
                    required: true,
                    minlength: 2
                },
                cointradeurl: {
                    required: true,
                    minlength: 2
                },
                coinprice: {
                    required: true,
                    minlength: 2,
                    number: true
                },
                rangeprice: {
                    required: true,
                    minlength: 2
                },
                target1: {
                    required: true,
                    minlength: 2,
                    number: true
                },
                target2: {
                    required: true,
                    minlength: 2,
                    number: true
                },
                target3: {
                    required: true,
                    minlength: 2,
                    number: true
                },
                stoploss: {
                    required: true,
                    minlength: 2,
                    number: true
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-danger'); // set danger class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-danger'); // set danger class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-danger'); // set success class to the control group
            }
        });

    };

    return {
        //main function to initiate the module
        init: function () {
            handleValidationUpdate();
        }
    };

}();

jQuery(document).ready(function() {
    //console.log("entro a validateCreateForm");
    validateCreateForm.init();
});