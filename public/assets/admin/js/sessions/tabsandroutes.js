var tabsAndRoutes = function () {

    // Login form validation
    var handleTabsAndRoutes = function() {

        var profileTab = document.getElementById('profile-tab');
        var changePasswordTab = document.getElementById('changepassword-tab');
        var walletTab = document.getElementById('wallet-tab');
        var navLink = document.getElementsByClassName('nav-link');
        var tabPane = document.getElementsByClassName('tab-pane');
        var url = window.location.href;
        var pathname = '';
        var tabname = '';


        if ( url.indexOf("#") > 0){
            pathname = url.split("#");
            tabname = pathname[1];
            var navElement = document.getElementById(tabname+'-tab');
            var tabElement = document.getElementById(tabname);

            //Clean nav and tabs
            for (var i = 0; i < navLink.length; i++) {
                navLink[i].classList.remove('active');
                tabPane[i].classList.remove('active');
            }
            navElement.classList.add('active');
            tabElement.classList.add('active');
        }else{
            //no han hecho click en ningun tab, o sea estan en el tab de datos personales
        }

        profileTab.onclick = setTabAndRoute;
        changePasswordTab.onclick = setTabAndRoute;
        walletTab.onclick = setTabAndRoute;

        function setTabAndRoute(ev) {
            console.log(ev);
            var el = ev.target;
            var elemId = el.id;
            var tabname = (elemId.split("-"))[0];
            window.history.pushState("", "", '/dashboard/user/settings#'+tabname);
        }

    };


    return {
        //main function to initiate the module
        init: function () {
            handleTabsAndRoutes();
        }
    };

}();

jQuery(document).ready(function() {
    console.log("entro a tabsAndRoutes");
    tabsAndRoutes.init();
});