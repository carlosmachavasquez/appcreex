var UpdateSettingsWalletForm = function () {

    // Login form validation
    var handleValidationUpdateWallet = function() {

        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation
        var formUpdate = $('#updateSettingsWalletForm');

        formUpdate.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                wallet: {
                    required: false,
                    minlength: 6
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-danger'); // set danger class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-danger'); // set danger class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-danger'); // set success class to the control group
            }
        });

    };


    return {
        //main function to initiate the module
        init: function () {
            handleValidationUpdateWallet();
        }
    };

}();

jQuery(document).ready(function() {
    console.log("entro a UpdateSettingsWalletForm");
    UpdateSettingsWalletForm.init();
});