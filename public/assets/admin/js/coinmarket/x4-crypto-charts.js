// X4CryptoCharts class
// data storage and system functions
function X4CryptoCharts() {
  this.history = [];
  this.historySent = {};
  this.exchangeRates = null;

  return this;
}

// set of available properties to display on the chart
X4CryptoCharts.prototype.properties = {
  price: 'Price',
  mktcap: 'Market Cap',
  volume: 'Volume',
};

// set of default colors for set of coins to use on the chart
X4CryptoCharts.prototype.colors = [
  ['rgba(33,150,243,1)', 'rgba(33,150,243,.24)'],
  ['rgba(244,67,54,1)', 'rgba(244,67,54,.24)'],
  ['rgba(76,175,80,1)', 'rgba(76,175,80,.24)'],
];

// set of available chart.js units per time periods
X4CryptoCharts.prototype.units = {
  'all': 'year',
  '365day': 'month',
  '180day': 'month',
  '90day': 'month',
  '30day': 'day',
  '7day': 'day',
  '1day': 'hour',
};

// wait for specific condition before executing some actions
X4CryptoCharts.prototype.wait = function(condition, callback) {
  var iteration = function() {
    if (condition()) {
      clearInterval(interval);
      callback();
    }
  }

  var interval = setInterval(iteration, 100);

  iteration();
}

// send http ajax request to remote server and return json result
X4CryptoCharts.prototype.request = function(url, callback) {
  var xmlHttp = new XMLHttpRequest();

  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
      callback(JSON.parse(xmlHttp.responseText));
    }
  }

  xmlHttp.open('GET', url, true);
  xmlHttp.send();
}

// get a set of exchange rates using CoinCap.io API
X4CryptoCharts.prototype.getExchangeRates = function() {
  var self = this;

  this.request('https://coincap.io/exchange_rates', function(response) {
    self.exchangeRates = response;
  });
}

// get history of specific cryptocurrency and time period using CoinCap.io API
X4CryptoCharts.prototype.getHistory = function(coin, period) {
  var self = this;

  if (this.historySent[coin + period]) {
    return;
  }

  this.historySent[coin + period] = true;

  var url = 'https://coincap.io/history/' + (period !== 'all' ? period + '/' : '') + coin

  this.request(url, function(response) {
    response.mktcap = response.market_cap;
    delete response.market_cap;

    // remove incorrect last result (CoinCap issue)
    response.price.pop();
    response.mktcap.pop();
    response.volume.pop();

    self.history[coin] = self.history[coin] || {};
    self.history[coin][period] = response;

    delete self.historySent[coin + period];
  });
}

// format price value based on fiat currency and template
X4CryptoCharts.prototype.formatPrice = function(value, fiat, format) {
  var formatted = format.template;

  if (formatted.indexOf('[symbol]') !== -1) {
    var symbol = fiat !== 'USD'
      ? x4CryptoCharts.exchangeRates.symbols[fiat]
      : '$';

    if (symbol) {
      formatted = formatted.replace('[symbol]', symbol);
      formatted = formatted.replace(/\s*\[abbreviation\]\s*/, '');
    } else {
      formatted = formatted.replace('[symbol]', '');
    }
  }

  if (formatted.indexOf('[abbreviation]') !== -1) {
    formatted = formatted.replace('[abbreviation]', fiat);
  }

  if (formatted.indexOf('[value]') !== -1) {
    var rate = fiat !== 'USD'
      ? x4CryptoCharts.exchangeRates.rates[fiat]
      : 1;

    formatted = formatted.replace('[value]', x4CryptoCharts.formatValue(value * rate, format));
  }

  return formatted;
}

// format numeric value based on factor, precision and thousands separator
X4CryptoCharts.prototype.formatValue = function(value, format) {
  var abbrs = ['K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'];
  var abbr = '';

  if (format.factor) {
    var max = format.factor !== 'auto'
      ? abbrs.indexOf(format.factor) + 1
      : 8;

    for (var i = 0; i < max; i++) {
      var val = value / 1000;

      if (format.factor === 'auto' && val < 1) {
        break;
      }

      abbr = abbrs[i];
      value = val;
    }
  }

  value = value.toFixed(format.precision).toString().split('.');
  value[0] = value[0].replace(/\B(?=(\d{3})+(?!\d))/g, format.separator);

  return value.join('.') + abbr;
}

// X4CryptoChart class
// initialize specific chart instance
function X4CryptoChart(element, options) {
  var self = this;

  this.initElement(element);
  this.initOptions(options);
  this.ensureHistory();

  x4CryptoCharts.wait(
    function() {
      return self.element && self.element.parentNode;
    },
    function() {
      self.renderContainer();

      x4CryptoCharts.wait(
        function() {
          return (self.options.fiat === 'USD' || x4CryptoCharts.exchangeRates) &&
            self.options.coins.reduce(function(res, item) {
              return res && x4CryptoCharts.history[item.coin] && x4CryptoCharts.history[item.coin][self.options.period];
            }, true);
        },
        function() {
          self.renderChart();
        }
      );
    }
  );

  return this;
}

// initialize the DOM element for injecting the chart widget
X4CryptoChart.prototype.initElement = function(element) {
  var self = this;

  if (element.parentNode) {
    return this.element = element;
  }

  var init = function() {
    self.element = document.getElementById(element) || document.querySelector(element);

    if (!self.element) {
      console.error('X4 Crypto Charts: DOM element "' + element + '" does not exist.');
    }
  }

  if (document.readyState === 'loading') {
    return document.addEventListener('DOMContentLoaded', init);
  }
  
  init();
}

// initialize the widget options based on user and default values
X4CryptoChart.prototype.initOptions = function(options) {
  var self = this;

  options = options || {};

  // predefine empty values
  options.coins = options.coins || [];
  options.controls = options.controls || {};
  options.controls.scales = options.controls.scales || {};
  options.colors = options.colors || {};
  options.font = options.font || {};
  options.format = options.format || {};

  if (options.coins.length === 0) {
    options.coins[0] = {};
  }

  options.coins.forEach(function(item) {
    item.colors = item.colors || {};
  });
  
  // mix user and default values
  this.options = {
    coins: [],
    type: options.type || 'line',
    height: options.height || 400,
    fiat: options.fiat || 'USD',
    period: options.period || '7day',
    property: options.property || 'price',
    controls: {
      crosshair: options.controls.crosshair !== undefined ? options.controls.crosshair : true,
      watermark: options.controls.watermark !== undefined ? options.controls.watermark : true,
      tooltip: options.controls.tooltip !== undefined ? options.controls.tooltip : true,
      legend: options.controls.legend !== undefined ? options.controls.legend : true,
      scales: {
        x: options.controls.scales.x !== undefined ? options.controls.scales.x : true,
        y: options.controls.scales.y !== undefined ? options.controls.scales.y : true,
      },
    },
    colors: {
      grid: options.colors.grid || 'rgba(0,0,0,.12)',
      crosshair: options.colors.crosshair || 'rgba(244,67,54,.64)',
      watermark: options.colors.watermark || 'rgba(0,0,0,.024)',
    },
    font: {
      family: options.font.family || "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
      color: options.font.color || 'rgba(0,0,0,.87)',
      size: options.font.size || 14,
    },
    format: {
      template: options.format.template || '[symbol][value] [abbreviation]',
      factor: options.format.factor !== undefined ? options.format.factor : '',
      separator: options.format.separator !== undefined ? options.format.separator : ',',
      precision: options.format.precision !== undefined ? options.format.precision : 0,
    },
  };

  if (typeof this.options.height === 'number') {
    this.options.height += 'px';
  }

  // mix user and default coin values for every coin
  options.coins.forEach(function(item, index) {
    var color = x4CryptoCharts.colors[index] || ['rgba(0,0,0,1)', 'rgba(0,0,0,0.24)'];

    self.options.coins.push({
      coin: item.coin || 'BTC',
      fill: item.fill !== undefined ? item.fill : true,
      thickness: item.thickness || 2,
      smoothness: item.smoothness || 5,
      colors: {
        border: item.colors.border || color[0],
        background: item.colors.background || color[1],
      },
    });
  });
}

// ensure that history of specific coun and time period already retrieved
X4CryptoChart.prototype.ensureHistory = function() {
  var self = this;

  this.options.coins.forEach(function(item) {
    if (!x4CryptoCharts.history[item.coin] || !x4CryptoCharts.history[item.coin][self.options.period]) {
      x4CryptoCharts.getHistory(item.coin, self.options.period);
    }
  });
}

// get responsive background font size for watermark
X4CryptoChart.prototype.getBackFontSize = function() {
  return this.element.offsetWidth > 576
    ? this.element.offsetWidth > 768
      ? this.element.offsetWidth > 992
        ? '160px'
        : '128px'
      : '96px'
    : '0';
}

// get responsive chart font size for all controls
X4CryptoChart.prototype.getChartFontSize = function() {
  return this.element.offsetWidth > 576
    ? this.element.offsetWidth > 768
      ? this.element.offsetWidth > 992
        ? this.options.font.size
        : this.options.font.size - 1
      : this.options.font.size - 2
    : this.options.font.size - 3;
}

// initialize a chart crosshair plugin
X4CryptoChart.prototype.getСrosshairPlugin = function() {
  var self = this;

  var drawing = false;
  var x, y = 0;

  return {

    // catch the chart events
    afterEvent: function(chart, event) {
      x = Math.round(event.x);
      y = Math.round(event.y);

      setTimeout(function() {
        if (!drawing) {
          chart.render({ duration: 0 });
        }
      }, 1);
    },

    // draw the crosshair
    afterDatasetsDraw: function(chart) {
      if (x < chart.chartArea.left || x > chart.chartArea.right) {
        return;
      }

      if (y < chart.chartArea.top || y > chart.chartArea.bottom) {
        return;
      }

      var canvas = self.options.canvas;
      var context = self.options.context;

      context.beginPath();
      context.lineWidth = 1;
      context.moveTo(x + 0.5, 0.5);
      context.lineTo(x + 0.5, canvas.scrollHeight + 0.5);
      context.moveTo(0.5, y + 0.5);
      context.lineTo(canvas.scrollWidth + 0.5, y + 0.5);
      context.strokeStyle = self.options.colors.crosshair;
      context.stroke();
      context.closePath();
    },

    beforeRender: function(chart) {
      drawing = true;
    },

    afterRender: function(chart) {
      drawing = false;
    },

  };
}

// render the widget container including watermark and canvas
X4CryptoChart.prototype.renderContainer = function() {
  var self = this;

  this.element.style.overflow = 'hidden';

  // container initialization
  var container = document.createElement('div');

  container.style.position = 'relative';
  container.style.marginLeft = '-20px';
  container.style.height = this.options.height;
  container.style.width = '10000px';

  if (this.element.offsetWidth > 576) {
    container.style.marginRight = '0px';
    container.style.maxWidth = (this.element.offsetWidth + 20) + 'px';
  } else {
    container.style.marginRight = '-20px';
    container.style.maxWidth = (this.element.offsetWidth + 40) + 'px';
  }

  this.options.container = container;
  this.element.appendChild(container);

  // watermark initialization
  var watermark = document.createElement('div');

  watermark.innerHTML = (this.options.coins.length === 1
    ? this.options.coins[0].coin + '/'
    : '')
    + this.options.fiat;

  watermark.style.fontSize = this.getBackFontSize();
  watermark.style.color = this.options.colors.watermark;
  watermark.style.transform = 'translateX(-50%) translateY(-50%)';
  watermark.style.position = 'absolute';
  watermark.style.fontWeight = 500;
  watermark.style.top = '50%';
  watermark.style.left = '50%';

  if (this.options.controls.watermark) {
    container.appendChild(watermark);
  }

  // loader initialization
  var loader = document.createElement('img');

  loader.src = 'data:image/gif;base64,R0lGODlhIAAgAPYAAP///wC81Pr8/dbz99r0+Pz9/brs8oDd6Yrf6sDt8/b7/Oj4+ojf6n7c6KDl7ub3+nDZ5lLR4YLd6eT3+fL6/Hzc6KLm7q7p8DbK3DrL3UDM3mrX5dz1+PT7/Hrb6Kbn7/j8/cjv9TzL3WzY5er4+p7l7njb577t89Ly9jTJ3DjK3azo8Mbv9Mzx9e75+9Tz9zDI27Tq8bzs86jn7/D6+27Y5pLh7Nj09+L2+Ybe6hLA1hDA1iTF2Q6/1i7H25rk7eD2+d71+Oz5+7Dp8Zzk7Zbi7GLV5I7g65Ti7EbO30zP4FDQ4UTN30rP4JDh687x9kjO30LN3tDy9sLu9E7Q4Jjj7RrC2CjG2jLI3BbB1xTB17jr8lrT4lTR4aTm73LZ5ljS4l7U42TV5FzT41bS4iLE2SDE2R7D2CrG2srw9cTu9GjX5bbr8mbW5CzH26ro8BjC12DU4xzD2HTa53ba57Lq8SbF2oTe6T7M3ozg6wy/1QAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJBQAAACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QkZKECzk2NJOCDxchgwU1OjsSmQoQGCIWghQiOz01npALERkYGQ4AFBqtP4ILN0ACjgISGhkpGDIANjw+KABCKNEujxMbGiowowAEHIIT0SgUkBwjGiIzhkIvKDiSJCsxwYYdmI8KFB0FjfqLAgYMEiSUEJeoAJABBAgiGnCgQQUPJlgoIgGuWyICCBhoRNBCEbRoFhEVSODAwocTIBQVwEEgiMJEChSkzNTPRQdEFF46KsABxYtphUisAxLpW7QJgkDMxAFO5yIC0V5gEjrg5kcUQB098ElCEFQURAH4CiLvEQUFg25ECwKLpiCmKBC6ui0kYILcuXjz6t3Ld1IgACH5BAkFAAAALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Ohw8Tj44XKlhbk4sKEVZZXAWZgwsxLYMdTJ1RCqEAIA1JSjOCFKhaUSCCoI8kRkpMULIKVFZaXaALN0C6jAVHS01RTFMAVVc8XgBCKNsujwsmS1AaCIJSpQAT2ygUk0AeS0oXhkIvKDihQjEyy4QdNJMgOqxqxC9RCyJFkKwYiKgAkAEE2CWi4CChDSdSFJFQx0ERiCEWQlq4oUjbto6KgCQwIOOJAEUFcBAIInGRgIKsGrrogIhCzUcFgqB40a0QiXpAMj1QJ6kVLgA41P1kxGHbi39HB/A0iaKoo6MvSAgisC0pAGRBXk4SOOjGtiCDFXCGSodCSM6GC7ze3cu3r9+/gAcFAgAh+QQJBQAAACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjoYkTj8Uj40SPGUMlYsdSzxmSiCbg0IyKIM0TTxnTAqjACAIYGNDgh1Uq1CiAB2VLl9hZGAXsGSrXAUKEjNABY4FRGJjXV0sAD8+aB8ANmItKC6PJAxiXBFIAAIhIYJVUygolI8TCNIxhkAvKDijLidTzgx1oLEJxC5GAReRkLFixZSDhwoAGUBAXiIWQy6smMFBEQl4KDoqenKi5Al+iYSAFJmIwgAUL5opKoCDQBCLM189c9HrEAWcz4LADFeIhD4gmxaAnCDIoCAcIIEuEgqToNEBvVTCI+rIxYAXJAQRgIcUwIIbQQQUPHiD7KCEOhMBTIAnJG7EBVzt6t3Lt6/fvYEAACH5BAkFAAAALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2OhiRVDhSPjQhYPkeViwpjWG5dIJuDBTdBgxRkWGhKCqOCK18QW4IdXKsRogAPHY8FNl8bG2wAIEarRgUKDW4ROI8XHl9rbS0ADhkYbwBIWj1wU48uPx4QYg4ABS1pgm09ZUc0lQtE5SeGR1hEz5sUIWkFDAkAIq9SAQGOAjIC8YLFFBQIExUAMoAAJUU41oVQs0ARCRQgOSyaABKkC0VCSopUJADHjRsTFhXAQSDIRZmvErrodYjCTV9BULw4WYjECxRANn0EGbNYRBwlfzIiKVSe0Ru9UpqsRGHAABKCCIBMCmCBqYiPBKC9MZZUTkJUEIW8PVRgAdG5ePPq3ctXbyAAIfkECQUAAAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6GQhZDHY+NSFEiRZWLCmtRGXEgm4QgCoMdYhoZYKajAA9ETmqCnRoqY6IACy6VCQgHDQkAIBAaGCMAChIpShyPTzYMDR4oADNQUUMAVXJZOj+PHRdOOR4rAAVST4Ij3joXlS7jOSyGNnA7YRSbHSgvhyAMvBHiqlEBgxNu3MCxqACQAQT2KXKBoiIKGopIWHQ20eJFRUI2NsShcMJIAkEkNixo0AWlQxRUPioQxB+vQiReoACySWNFk8MECMJhUSajCRVfYMx5g1LIijcdKSAwgIQgAhV56roBRGilAgcF3cg6KCxLAEhREDxbqACJqGwI48qdS7fuqEAAIfkECQUAAAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6GLitsCo+NJRFUM5WLICYRTSMCm4kdc59iIIIgLw+VT2woggp0EVBrogtfblFSjhNeP0hpAAINEUl0AApfZWdyTr4rFkVOBAB1YBFsAD92zlZ1jiBTbw42WwAFL7ECRmZycEYUjxRqbyW9hUfwRiSbIEGCHKLwxoKQUY1AUCjQiAQBAhMWFWjRgkCHRRRQaERBQxGJjRwwbuSoSAhIRg9u3IioqAAOAkAuMmKIsFEBFzINUZi3qUAQFC9cGCKxDsimjxpZghAFAMdGno4eaHzRkeiNiyY1Cn0EgsAAfwAIaDQKYMENIEwr0QRwY+ygtTUUAUzQeDCuoQIkttrdy7ev3799AwEAIfkECQUAAAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6GBQMDj45sI20ylIsgDG1jBwWaiQp3nl8ggiAyQxSPJCgPqZ1cdAIAJB4pbkeOCmoxF5MCR21cEgAKFTBodmO2jB0hqzM4ADIjRpkOKcw8P48cLAYrIQAFN5MFI252ZRutjiAELFschkVXZWskmgUkC4coXPjgQlQjEDj4MSJBgMCERRPA2MlgYJGCFygy0lCE5MwVH21QjcKoUREBNglY3GC04MaNh4oK4CAARIHBm4gKuOiAiAI8SgWCoHhRsBAJjEA0vcoIE8QzHBlR/Gz0IOOLjUdv8BQStWg8AjcUEsiYFEBLIM+ADrpBdlAonIIRJmQUAhcSCa918+rdy7evqEAAIfkECQUAAAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6HIAKPjkFFP0CTjB8VXx+ZigI/FRAMkgACCWwdjwVCNIICRKMHkkJ3URlIj0FPITgABQ4VNUcFIDl4KiliposCLygtUyQAIXd0LQAzuClYDo9AKFIhN4ITmAV0GSkwX6uOIBziC4ZEKT4QQpmtr4YddStcfGoEYoI+RkIIEJiwaEIYNxpkLAIBDQWKfojy6NiYRIEiihYvKjrSo2QTEIsW3LjBUNEDD1SohBgIqlmjAi7eGaJA4VOBICheCCxEAhqmSSRCtowkCEfIno8eWHzxquiNVUJCDoVH4AY1AAQsHlUJpIDPQTfEDjJLc9AEiwcP2xYqQGKr3Lt48+rdizcQACH5BAkFAAAALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Oj5CHCmkhCpGLU0gMMpeJBUOaPwWCAiwyHZAdlgACF0g5NgIALkcRTSWPEy8DQgAFdUh3uCBOVFBMELKMBTcoKC8UAC8/CC8AQ11NTBozj0DOKA+CJOIFEtp4FaiOIBzPLoZeTHge8JAFLtGGHVt1NJ2MQEzoxUgIAQITFj1og4EJm0UCBoD7l8iGHCtWlIBQFHGiIhtZQmpcZPBGQkUPxIhY8hDgoQIUlDnCt84QBX33grwzROIFCiCRSIA7CUIZDnA4Gz1w9uJfzxuohICzx47ADRKCCDgDCmDBDRyjIoUF0OznoLEuJzgj6LJQARJUCtvKnUu3rt25gQAAIfkECQUAAAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkIgkC5GMHEMzN5WKLBcOQ4MCL2oKkCAgggWdJR8FADREbWMfjyQvA0KCaRdEFwACJUZcXQ2ujRwoKC8UAEB1FhwABrJdS76OOMkoD4I0JIJOY11UOaWOIMgvNIYXZOTrkAUuzIYKJ1vwm4oCD0FCxomEECAwYRGQGhpUJPmSz5CAAdoaGrpjpyKPKzISFYCYTGIhBGZCmrFjQJELAjcKKnqwIQoTJk4E6DNUoIPNR/I6IGIxRGe8IMpcGCKR4EsbobW0qQQhE0A2KQ5QQHqQTB0AWzd0CtGW6xEIlN8AEEgGRNCCGzgA4hx0g+wgtfoTJiTrOrNQARJI6+rdy7evX76BAAAh+QQJBQAAACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QiCACkYxCTywklYoEaTIsgwUcQJEgBYM3aQYygh1vHiYtj0IvN0KCnVtTAAUrJhBrDo8cKCgvFABCLQYTAGoVwGJbjzjFKA+CCjSCDl9rRkgKjyDEL9uFWxtxNuePBS7IhiAsJ/GbigILQED2iEIEBJop4jCHShImYlAkEjDAWrtDOVKkwEIRwilEBBwquuOmY0cIilwQuCEwEQ4ISpRQmUPgnqECHWJeZPSuwyEQQ4bYhFQgiDEXhhxo0TIG6CMS1gROEpQGih4dMSA9KGYOAIlaNoUYwKOHCCQQIzUByIiCFIAFMiqUdIeqmFleLhQHTSh2K26hAiSM2t3Lt6/fv5sCAQAh+QQJBQAAACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QiAWRjRQ3BAqUihwoKByEIJOQBaIABJ0vggoJRBeZjjQ3N0KCp1IDAAUyRzkHKI9BqBQAQgMoLgBSNgwNDZ+OOJ0oC4Igr3XMJl6ljCCcL8OFagd0Dh2RBS7hhSBPIeeaiwIkODjriC4EBBOLQAdjZLpAwJXoVCcaio4wicJQgwdFBlEgTJQng0WLDxNRIHCDn6IJHsiAAVPhWTxCBTp0eNUoHbxCAmLEeOmoQLAXyAoxsCLHSE5HJKR5BCFAUJgdWqywgfQAFUISL26cQ6IDqQNIIDiSqNUJCAAFDdyI8Thq0I2ugx4UPQlgQidabA4LFSDxM67du3jz6qUUCAAh+QQJBQAAACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QkZKECkBAApOJQCgoD5mDBQWDBJwcggUDUwSQHTc3QoKkKEGCTzMODjSPOJwvHQBCAwMUAEErDkVVLo8TnCgLggIggiwWRUd1kCAcKC/EhVJVeRcKkQUu34UCNwPln4kFQg8Pv4oUBAQTixN5NW1iDVYlkoVCV6IfZLp0iRAhhyKCBhEVaUKR4h17BG7oU/TgjpiPOWi9o6TAXaNz9dRt2ZLSUYEg3ZYVysPjyoaIjUg42wgCEwAjVs7YMQDpQS9dJF7c+FXESlAv2jKSiMUJCAAFErBwMWVu0I2qgxZMe9cMBayRhAqQkIm2rdu3cATjNgoEACH5BAkFAAAALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Oj5CRkoQKQDgCk4k4KCgPmYMFBYMEnByDJBwUkB03N0KCpChBgkAsBiGQE5wvHQBCAwOqJCEydWyYjg+cKAuCAiCCHMUzuI8CHCgvqoU4dR8J0JAFLtuGOEHhn4gFNCQkyIkUBAQTiwtEBx4mSECKsSg0FH3YsKaNQST+lgVM5GDMmDAObSiSd6OeIhJHvnyZYwOHukIKFKRjNK6XIQpvLph8VCBINheGjrjBMufVIxLLLIIIKIALDzQ+6Ch4pCxbQBIvvrABgIQHjytYTjwCQeAGCVgoPJApoOBLmadeIokSdAMFka0AaHjAomTAJ10XFIiA4nD1UwESC0Z+3Mu3r9+/kAIBACH5BAkFAAAALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Oj5CRkoQCEwsFk4k4KCgLmYOYgwScHIMULpEdBDdCgqMoQYITLyg4kBOcLx0AQgMDFLycLS+QC5ydggIgsigtakCQBRwoL8CFQi1TKKGPBS7WhkKXn4unHdyIFAQEE4tCK0VONh+tia8oNIoxBw0VFR5bFN3Ll+jCl4MHYyhSd6OdIiFEJNy54wAVOUIgMnZzscuQixVsOnYLQs0iIRsZNDQw2YjEMYdPSinggkUFngMiGT3IlQ+ICjQBq/jAggGPl0cgVpEQ9ELFjjEFQHgYimGEgGiDWvjYQQaTEAg+Uvz49OKKjiKm2IT8ROFIlZwXCOPKnUu3LqRAACH5BAkFAAAALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Oj5CRkoQFJCSTijgoKAuYiASbHIMdHZEKHARCgqAoQYITLy+Xjw+bL6VCAwMUAEKbrZALv50AAiCvv6qPBRwoL7yFvig4kgUu0IYUNJ6MChTHixQEBBOLHVMrHytSi6wo24ksVUVISD/wn7/4h1MM/gw2XCgSd6PcwDdIbBBhx62QAAUClrkoZYhGDBkKIhUI4kxgoR9NIiDYx4jEr3ICWrgCIUYDFCp5KDaq5WxbDjlYDABwIEJDEiorHoEgcOMSBRU64BgpAEJCzyQmCkCSCoAEjKRhpLrwICKKBU9tkv4YRMEARk8TjvyQ2bCt27dwBONGCgQAIfkECQUAAAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkJGShAUkJJOKEygoC5iIBJscgyAgkQocBEKCoChBgg8vAzSQD5svHQBCAzcUuZsoOJALv50AAgKCmpuqjwUcKC+9hUKbwZEFLtKGFLOeiwIgBYwUBAQT3y9qCSzMiawo3Yg3dUMXFyeL7/GHUhb+FgYWUeBw45yiDgZmvIlxyVshAeKaucBliIYMNaUgFQgCzYUhL2PaVNHWiMSvcwKeAAEA4ksELnGqKHhUC9osBDxE4PtAJQKYODEegSBw4xIFPFbKbCgAIo8SnzkiOoooBEPSNuJo3KHS5Y2nEVZ4lBjUIc2UmZgm2HCA1qHbt3AF48qVFAgAIfkECQUAAAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkJGShAUkQpOKDygoC5iIBJscgyAFkQocBJcAoChBgg8vNx2Qmigvs0IDNxQAQpsoD5ALv50AAgKCE7+qjgUctryFQi8oOJIFLtGGHTSejAWljBQEBBOLBUADA0DIiqwo3YkPTy1padbuv/GIQTL+Mq4UUeBww5wiEC1OnJACwpshcJCwzdrG4knDiEFQSAlh6AIEDx8mOnKx6cgcYyFQGDvQpgadDxcbaXqDxQsAJz7wGAAwJE6bEXMSPALxQgwDARSS2IFhwliVMD9/QBJQDAcWOz7aIKPgxEibGJgWqMCqVZCCjTEjUVBix80dh4UQLuChkgZuoQck7Ordy5dQIAAh+QQJBQAAACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QkZKEBSQuk4oPKCgkmIgEmxyDAgWRChwEQoKgKEGCDwMEIJCaKC8dAEIDNxS5mygLkAu/wQCkghO/qo8FHLa9hUIvKDiSBS7Qhh00noyljRQEBBOLBUC71YusKNyJw7/Zn7/tiO+b8YcUHDfkigVBLwak60bwWhABhkCguIEQUrMiWH4YksHAxhYFkIQgMLMDgrE0L4w5qXDnCJuGjWZY6QFnBoAiGZQkAGBgDsk8LR6lyeAmj4AOS1LguWPMyxwPEthAIvFAEAkmKUR8KdXBgok7UjA9jVrjm4AbrjC5aJIigwmChTxEfYOW0IISbwgwtp1Lt66gQAAh+QQJBQAAACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QkZKEBUIuk4oPKCgkmIgEmxyDBZIKHARCgqAoQYIPAxwCkJooLx0AQgM3FLibKKmPC74LggKkABO+vI8FHLXLhEIvKDiSBS7QhR00nozHjBQEBBOLBUC6xIurKNyJwpu26r7tiEK+8YoUHDfkigU4BDgA60YQSAkZsgoJCILjm6MJSXrIKWEohIMVaRI6qrJDB5w5AAQ8uSFoho0SH1pAMqEjS5kVAIg0GcMCgBoENoh8ePCohYYUTgR0GBNliRMABergJAIEkpB0QpZEoXKAFIgtPwyAwBQ1ipIK3255okHG6x2Che54rYOWEIkPdQi2tp1Lt66gQAAh+QQJBQAAACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QkZKEBUIuk4oPKCgkmIgEmxyDBZIKHARCgqAoQYILN0ECkJooLx0AQgM3FLibKKmPC74LggKkABO+vI8FHLXLhEIvKDiSBS7QhR00nozHjBQEBBOLBUC6nYurKNyJwpsDsorr7YhCvvGLFBw35IoFOAhwqNetGw4HJ+QVInEp0gQlWXhYMHRDBosg3xodgSOnTAUABV60AnBixZYpIx15kGPGzRAAXrjUeAJAioUVbNSAePQECp4iAhSs6WKkBMgpXlac2PlICDEALsJ0iXOElIAXCaphchGnS5g8GbvREOPVRsFCR7waOBvtggGmbAbjyp0LIBAAIfkECQUAAAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkJGShAVCLpOKDygoJJiIBJscgwWSChwEQoKgKEGCCzdApI+aKC8dAEIDNxS4myi8jwu+C4ICshO+wI4FHLXKg0IvKDiSBS7PhB00noyyjBQEBBOLBUC6qYurKNuJJL433ogDagkxnYlC7/GHLWFNJrcSFcBBIAi7RR2E7ONGCAeRISAOubgUKUgXM24cGKIV6xGJMGWu+JAAoAABagBQhJCC4sEjByHdqFgB4EINCQMABDmxksAjCXbcpMgjQIGJNSZopuQpypGUCFGK3KJRYw0djSWBAFEAycU4QTQgrJlDhCEhCnPWfLFglpADtWoN2g6iIIOFALl48+YNBAAh+QQJBQAAACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QkZKEBUIuk4oPKCgkmIgEmxyDBZIKHARCgqAoQYILN0Ckj5ooLx0AQgM3FLibKLyPC74LggKyE77AjgUctcqDQi8oOJIFLs+EHTSejLKMuTcTiwVAupeKQmBKNRI3iiS+BIskKT09Ox/o8YwXTCk12AoVwEEgSMBDHVx442ZogoUYIA65OAcJyBgfKvIVgoci1iMhbXykEJEHADliAIAMe+QExkgodQBskVClFUcUohqB4JIiQxQHBUAwaODkhKAJ0h48YpBBg5OIFCQ0yBNTEAWKjSjIOKHA6p0GCIYwJAQiD9gtYwkZOOAkZ1qTHAeovZ1Ll24gACH5BAkFAAAALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Oj5CRkoQFQi6Tig8oKCSYiASbHJ4ACkEEQoKgKEGCJARABZCaKC8dAEIDNxS3myi7jwu9C4ICsQATvb+OBRy0yoNCLyg4kgUuz4QdNJFCqI3GjCsYMGudiQVAuduKQhg772+KJL0EiyQZWVlwM+y9ootDmoiYg61QARwEghQ8pMAFuFGGHswwAOIQhYWLcLQRAeWCIRLSYD0SAgEPEypVWl0CAETYoyomlXAxAEDNjyHDhPQC4ghEGyZNuswoIIBIkRlSBD148cJbIydNIhCpSMNGkQ8sBnVQAKnDFDVcAXQoUsSLGoiEBHwoYgEFWkI4DS4kWPdW0MO6ePPWDQQAIfkECQUAAAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkJGShAVCLpOKDygoJJiIBJscngAKQQRCgqAoQYIkBEAFkJooLx0AQgM3FLebKLuPC70LggKxABO9v44FHLTKg0IvKDiSBS7PhB00kS6ojcaMQyIYI52JBUADBNiGQnhWcHAXiiS9oopCUWZmZW/49oxidEnigR0lHASCGDSkgAa4UYYWXEgg4BCFhYomzFHChY0hEtKAQHJRgQqZOF4E0VAgCEgvb40cLCETZoQaAFJipNklpNcERyDm0FwTo4CAIUPUUAPw4MUAjIaIhGnzpmKHGUOm3CMFAlKHEC2MgbgwJMFWiIJYDDkxDO0gBTcKfrqdS7euXUOBAAAh+QQJBQAAACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QkZKEBUIuk4oPKCgkmIgEmxyeAApBBEKCoChBgiQEQAWQMi0oLx0AQgM3FLibKLyPORC0C4ICsQATvsCOQFBfT8yDQi8oOJI4DsWHHTSPBS4kQgKNyIokXxoZIhuoiQVAAwS3iV52djw8ZQ7nvqKJM9wIFOhFkRBfrBKRoNMEypIGl97heKVgUSUSEUchIsEmBDlDFKQ5WnAgTo0EhkhUAwKJBoI4G+jUEaQAhCAgvtw1emNkwxwJTwAEeTLg1sFN2xgJkLDhS4UTAAqwoMUSwAN5FR3NcMqGnAA1tP4BOAZJgZQXyAqkoaqxEJAnLw1EtqWQta3du3jzKgoEACH5BAkFAAAALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Oj5CRkoQFQi6Tig8oKCSYgx0FgwSbHJ4AaU0/QoKjKEGCJARAoY9zPSkGHQBCAzcUu5sov48SOz1GD4ICtBPBw444STtlT4ZCLyg4kjg/bLSFHTSPBTSWAo3fiSwbTUxJX52JBUADBLqIIEZY+zAwSIokgr3CtyGDQYMOFAkJBkRRiw1kyIxhEA9RARyyQCwCIUSIOFOJXCR4km4QhWePSDiZc6eFIRLYGj6iUIXOgTwJBIHQCABHsI+N2Jg4gODHDQAwB+hauGnBIyIHGCBxCaCVzAX1eDZSk6eImlAFbmwaCKBASUYTkonapA0kIV4EDRS4LWR2rt27ePMeCgQAIfkECQUAAAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkJGShAVCLpOKDygoJJiDFEKDBJscngAtTSlFgqMoQYIkBEAFkB5ZOlYGAEIDNxS7myi/jwxwWjsSggK0ABPBw444VHBnF4ZCLyg4khMlW8yFHTSPBTRCNOCK6Yhpc2RLER6hiQVAAwQdiSA1UVEaGniIKCIR7BUiAXSaKFQ4Q5GQYEAUSTHRps0IG/MQFcAhC8QiEC5cQDN1iEaaG+sEURjpyIWFPD9uGCKRLeIjEG+OVPmAQhAIjwBwBBvnCIWTKl5iPABAc0C+h5s6Fa1i4cIAVptsLrgHtJGCE2xkAihwY5PBsSkZCSDEYdMCkoUOKHDg0BWu3bt48+pdFAgAIfkECQUAAAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkJGShAVCLpOKDygoJJiDNEKDBJscngAtUBlVgqMoQYIkBEAFkAdmVmUyAEIDNxS7myi/j0c8Z1Y5ggK0ABPBw44TZDx2dYZCLyg4khNeMsyFHTSPBRQuNOCK6YhSB2JhcTnjiQVAAwQKiQIVXV0RS0suKCIRDIi+O2MSJhyiSEhBRQMYmDDRwME8RAVwyAKxSAAFGh1MKerwwuAhCtAeUYjhhc0DQySymXx04kOdKdsAgOAIAMezRyRW1DnxZFzMASEdbrrkyAUbGWleAmhlcsGNIAIg2esEoMCNTa8ErZsUZNMCkYUUBJkwFq3bt3AF48pFFAgAIfkECQUAAAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkJGShA8XLpOECxOEX01SJJgAU0l4JYIUKkpSHKEVblduRAAUGWQoQYIkBEAFj04wbnZoBgBObTcUAEIozMmOD2EwaDwVghO9ABPMKM6ON9E+FoZCLyg4kg8fFwKHHTSQ7hTYi/OJL0dzEBBO74kFQAMIKEgkIM+aNm3EGGGjiMQ2IP6QfJk4kViiZcwgJuJQBQECJxe6HSqAYxeIRQI6UBgYSpECHEIQURDpCESIBE8uFSJRTuOjF1OeoNgEAMRJADi20XQZQuiLdzwHdFC2TWejAgNQvAAFgEBGQQtu4KjHSMECqzeY4RJEdhIQZgsPWhoSMOGa3Lt48+rdiykQACH5BAkFAAAALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Oj5CRkoQLRTMKk4JCFyGEdDs6R5kCBxgiFoIUeDs9Jpk0XBkpKg4AFBqsRIIkBEAFjwwaGVgYMgA2PFgoAEIozhSPExsaKjASggQPghPOKNCPHCMaIjOGQi8oOJIkKzEChx00kAoUHb+M94pCFjkSEiXfEBUAMoAApkRDGlTw4MFEAkUkugFRFIOBRYss9ElU5IKNAwcfTnRQVABHLxCMFChAmWmRABcjD1EI+KgABxQvXBgigW4iJG7OJggCwRJHN5qMCDh7IY/ngJHNnkECgpMENmc+F9xQB6mAi4MAbjgLMihfS6MorLY0JOCB2rVwB+PKnUtXbiAAOwAAAAAAAAAAAA==';
  loader.style.transform = 'translateX(-50%) translateY(-50%)';
  loader.style.position = 'absolute';
  loader.style.top = '50%';
  loader.style.left = '50%';
  loader.style.width = '100px';

  container.appendChild(loader);

  // canvas initialization
  var canvas = document.createElement('canvas');
  var context = canvas.getContext('2d');

  canvas.style.position = 'relative';
  canvas.style.height = this.options.height;
  canvas.style.width = '100%';

  this.options.canvas = canvas;
  this.options.context = context;
  container.appendChild(canvas);

  // make controls responsive on window resize event
  window.addEventListener('resize', function() {
    if (self.element.offsetWidth > 576) {
      container.style.marginRight = '0px';
      container.style.maxWidth = (self.element.offsetWidth + 20) + 'px';
    } else {
      container.style.marginRight = '-20px';
      container.style.maxWidth = (self.element.offsetWidth + 40) + 'px';
    }

    watermark.style.fontSize = self.getBackFontSize();

    if (self.chartjs) {
      self.chartjs.options.scales.xAxes[0].ticks.minor.fontSize = self.getChartFontSize();
      self.chartjs.options.scales.yAxes[0].ticks.minor.fontSize = self.getChartFontSize();
      self.chartjs.resize();
    }
  });
}

// render the chart using Chart.js library
X4CryptoChart.prototype.renderChart = function() {
  var self = this;

  var min = 1000000000;
  var max = 0;

  // calculate datasets based on set of coins
  var datasets = this.options.coins.map(function(item) {
    return {
      xAxisID: 'x',
      yAxisID: 'y',
      fill: item.fill,
      borderWidth: item.thickness,
      borderColor: item.colors.border,
      backgroundColor: item.colors.background,
      label: item.coin + '/' + self.options.fiat + ' ' + x4CryptoCharts.properties[self.options.property],
      data: x4CryptoCharts.history[item.coin][self.options.period][self.options.property]
        .filter(function(item2, index2) {
          return index2 % item.smoothness === 0;
        })
        .map(function(item2) {
          if (item2[1] > max) max = item2[1];
          if (item2[1] < min) min = item2[1];

          return {
            x: new Date(item2[0]),
            y: item2[1],
          };
        }),
    };
  });

  var suggestedMax = max + (max - min) / 20;
  var suggestedMin = min - (max - min) / 20;

  if (suggestedMin < 0) {
    suggestedMin = 0;
  }

  // initialize Chart.js config
  var config = {
    type: this.options.type,
    data: {
      datasets: datasets,
    },
    plugins: [],
    options: {
      responsive: false,
      maintainAspectRatio: false,
      legend: false,
      tooltips: false,
      elements: {
        point: false,
      },
      animation: {
        duration: 0,
      },
      scales: {
        xAxes: [{
          id: 'x',
          type: 'time',
          display: false,
        }],
        yAxes: [{
          id: 'y',
          type: 'linear',
          display: false,
        }],
      },
    },
  };

  // use crosshair plugin
  if (this.options.controls.crosshair) {
    config.plugins.push(this.getСrosshairPlugin());
  }

  // initialize a chart legend
  if (this.options.controls.legend) {
    config.options.legend = {
      labels: {
        padding: 12,
        fontFamily: this.options.font.family,
        fontColor: this.options.font.color,
        fontSize: this.options.font.size,
      },
    };
  }

  // initialize a chart tooltip
  if (this.options.controls.tooltip) {
    config.options.tooltips = {
      mode: 'index',
      intersect: false,
      position: 'nearest',
      displayColors: false,
      bodyFontFamily: this.options.font.family,
      bodyFontSize: this.options.font.size,
      footerFontFamily: this.options.font.family,
      footerFontSize: this.options.font.size - 3,
      bodyFontStyle: '500',
      footerFontStyle: '400',
      yPadding: 12,
      xPadding: 24,
      callbacks: {
        title: function(items, data) {
          return '';
        },
        label: function(item, data) {
          var coin = self.options.coins[item.datasetIndex].coin;
          var price = x4CryptoCharts.formatPrice(item.yLabel, self.options.fiat, self.options.format);
          return price + ' (' + coin + ')';
        },
        footer: function(items, data) {
          var time = items[0].xLabel;
          var date = time.toDateString().split(' ');
          return date[1] + ' ' + date[2] + ', ' + ('0' + time.getHours()).slice(-2) + ':' + ('0' + time.getMinutes()).slice(-2);
        },
      },
    };
  }

  // initialize a chart horizontal scale
  if (this.options.controls.scales.x) {
    config.options.scales.xAxes[0] = {
      id: 'x',
      type: 'time',
      barPercentage: 0.95,
      categoryPercentage: 1,
      time: {
        unit: x4CryptoCharts.units[this.options.period],
      },
      gridLines: {
        color: this.options.colors.grid,
        drawBorder: false,
        tickMarkLength: 8,
        borderDash: [4, 2],
      },
      ticks: {
        padding: 4,
        maxRotation: 0,
        fontFamily: this.options.font.family,
        fontColor: this.options.font.color,
        fontSize: this.getChartFontSize(),
      },
    };
  }

  // initialize a chart vertical scale
  if (this.options.controls.scales.y) {
    config.options.scales.yAxes[0] = {
      id: 'y',
      type: 'linear',
      position: 'right',
      gridLines: {
        drawBorder: false,
        drawTicks: false,
        borderDash: [4, 2],
        color: this.options.colors.grid,
      },
      ticks: {
        padding: 8,
        suggestedMax: suggestedMax,
        suggestedMin: suggestedMin,
        fontFamily: this.options.font.family,
        fontColor: this.options.font.color,
        fontSize: this.getChartFontSize(),
        callback: function(value) {
          return self.element.offsetWidth > 576
            ? x4CryptoCharts.formatPrice(value, self.options.fiat, self.options.format)
            : '';
        },
      },
    };
  }

  // remove loader element
  var loader = this.element.querySelector('img');
  loader.parentNode.removeChild(loader);

  // initialize Chart.js instance
  this.chartjs = new Chart(this.options.canvas, config);

  // fix for IE
  this.chartjs.resize();
}

// instantiate the main store and retreive the main data
var x4CryptoCharts = new X4CryptoCharts();
x4CryptoCharts.getExchangeRates();
