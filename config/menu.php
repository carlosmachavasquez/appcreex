<?php

return [

    /*
    |--------------------------------------------------------------------------
    | user Navigation Menu
    |--------------------------------------------------------------------------
    |
    | This array is for Navigation menus of the backend.  Just add/edit or
    | remove the elements from this array which will automatically change the
    | navigation.
    |
    */

    //SIDEBAR LAYOUT - MENU

    'sidebar' => [
        [
            'title' => 'Dashboard',
            'link'  => '/user',
            'active' => 'user',
            'icon'  => 'fa fa-dashboard',
            'children' => [
                [
                    'title' => 'Basic',
                    'link' => '/user/dashboard/basic',
                    'active' => 'user/dashboard/basic',
                ]
            ]
        ],
        [
            'title' => 'Users',
            'link'  => '#',
            'active' => 'user/users*',
            'icon'  => 'fa fa-user',
            'children' => [
                [
                    'title' => 'All Users',
                    'link'  => '/user/users',
                    'active' => 'user/users',
                ],
                [
                    'title' => 'User Profile',
                    'link'  => '/user/users/1',
                    'active' => 'user/users/*',
                ]
            ]
        ],
        [
            'title' => 'Settings',
            'link' => '#',
            'active' => 'user/settings*',
            'icon' => 'icon-fa icon-fa-cogs',
            'children' => [
                [
                    'title' => 'Social',
                    'link' => '/user/settings/social',
                    'active' => 'user/settings/social',
                ],
                [
                    'title' => 'Mail Driver',
                    'link' => 'user/settings/mail',
                    'active' => 'user/settings/mail*',
                ],
                [
                    'title' => 'Environment',
                    'link' => 'user/settings/env',
                    'active' => 'user/settings/env*',
                ],
            ]
        ],
    ],

    //HORIZONTAL MENU LAYOUT -  MENU

    'horizontaladmin' => [
        [
            'title' => 'Dashboard',
            'link'  => '/dashboard/admin',
            'active' => 'dashboard/admin',
            'icon'  => 'icon-fa icon-fa-dashboard',
        ],
        [
            'title' => 'Signals',
            'link'  => '/dashboard/admin/signals',
            'active' => 'dashboard/admin/signals*',
            'icon'  => 'icon-fa icon-fa-bell',
        ],
        [
            'title' => 'Education',
            'link'  => '/dashboard/admin/education/category',
            'active' => 'dashboard/admin/education/category*',
            'icon'  => 'icon-fa icon-fa-book',
        ],
        [
            'title' => 'Courses',
            'link'  => '/dashboard/admin/education/course',
            'active' => 'dashboard/admin/education/course*',
            'icon'  => 'icon-fa icon-fa-graduation-cap',
        ],
        [
            'title' => 'Lesson',
            'link'  => '/dashboard/admin/education/leccion',
            'active' => 'dashboard/admin/education/leccion*',
            'icon'  => 'icon-fa icon-fa-graduation-cap',
        ],
        [
            'title' => 'Membership',
            'link'  => '/dashboard/admin/membresia',
            'active' => 'dashboard/admin/membresia*',
            'icon'  => 'icon-fa icon-fa-bar-chart',
        ],
        [
            'title' => 'Activation',
            'link'  => '/dashboard/admin/activation',
            'active' => 'dashboard/admin/activation*',
            'icon'  => 'icon-fa icon-fa-check',
        ],
        [
            'title' => 'Usuarios',
            'link'  => '/dashboard/admin/usuarios',
            'active' => 'dashboard/admin/usuarios*',
            'icon'  => 'icon-fa icon-fa-users',
        ],
        [
            'title' => 'Producto',
            'link'  => '/dashboard/admin/producto',
            'active' => 'dashboard/admin/producto*',
            'icon'  => 'icon-fa icon-fa-cube',
        ]

    ],

    'horizontaluser' => [
        [
            'title' => 'Inicio',
            'link'  => '/dashboard/user',
            'active' => 'dashboard/user',
            'icon'  => 'icon-fa icon-fa-dashboard',
        ],
        [
            'title' => 'Mi Cuenta',
            'link'  => '/dashboard/user/settings',
            'active' => 'dashboard/user/settings*',
            'icon'  => 'icon-fa icon-fa-cogs',
        ],
        [
            'title' => 'Señales',
            'link'  => '/dashboard/user/signals',
            'active' => 'dashboard/user/signals*',
            'icon'  => 'icon-fa icon-fa-bell',
        ],
        [
            'title' => 'CoinMarket',
            'link'  => '/dashboard/user/coinmarket',
            'active' => 'dashboard/user/coinmarket*',
            'icon'  => 'icon-fa icon-fa-bar-chart',
        ],
        [
            'title' => 'Portfolio',
            'link'  => '/dashboard/user/portafolio',
            'active' => 'dashboard/user/portafolio*',
            'icon'  => 'icon-im icon-im-folder-open',
        ],
        [
            'title' => 'Icos',
            'link'  => '/dashboard/user/icos',
            'active' => 'dashboard/user/icos*',
            'icon'  => 'icon-im icon-im-coin-dollar',
        ],
        [
            'title' => 'Noticias',
            'link'  => '/dashboard/user/noticias',
            'active' => 'dashboard/user/noticias*',
            'icon'  => 'icon-im icon-im-newspaper',
        ],
        [
            'title' => 'Formación',
            'link'  => '/dashboard/user/cursos',
            'active' => 'dashboard/user/cursos*',
            'icon'  => 'icon-im icon-im-book',
        ],
        [
            'title' => 'Mis Referidos',
            'link'  => '/dashboard/user/payments',
            'active' => 'dashboard/user/payments*',
            'icon'  => 'icon-im icon-im-users',
        ],
        [
            'title' => 'Forex',
            'link'  => '/dashboard/user/forex',
            'active' => 'dashboard/user/forex*',
            'icon'  => 'icon-fa icon-fa-dollar',
        ],
        [
            'title' => 'Producto',
            'link'  => '/dashboard/user/producto',
            'active' => 'dashboard/user/producto*',
            'icon'  => 'icon-fa icon-fa-cube',
        ]

    ],

    'horizontalinvitado' => [
        [
            'title' => 'Pagos',
            'link'  => '/dashboard/invitado',
            'active' => 'dashboard/invitado*',
            'icon'  => 'icon-fa icon-fa-dollar',
        ]
    ],

    'horizontaltrader' => [
        [
            'title' => 'Crear Señales',
            'link'  => '/dashboard/trader/signals',
            'active' => 'dashboard/trader/signals*',
            'icon'  => 'icon-fa icon-fa-bell',
        ],
        [
            'title' => 'Forex',
            'link'  => '/dashboard/trader/forex',
            'active' => 'dashboard/trader/forex*',
            'icon'  => 'icon-fa icon-fa-dollar',
        ],
        [
            'title' => 'Ver Señales',
            'link'  => '/dashboard/trader/livesignals',
            'active' => 'dashboard/user/livesignals*',
            'icon'  => 'icon-fa icon-fa-signal',
        ],
    ]
];
